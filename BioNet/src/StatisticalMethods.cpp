#include "StatisticalMethods.hpp"

Distribution::Distribution() {

	meanSet = false;
	data.clear();

	for(int i = 0; i < MAX_NUM_MOMENTS; ++i) {
		moments[i] = 0;
		cMoments[i] = 0;
	}

	maximum = 0;
	minimum = 0;
	mean = &moments[0];
	variance = &cMoments[1];
}

Distribution::~Distribution() {

	meanSet = false;
	data.clear();

	for(int i = 0; i < MAX_NUM_MOMENTS; ++i) {
		moments[i] = 0;
		cMoments[i] = 0;
	}

	maximum = 0;
	minimum = 0;
	mean = NULL;
	variance = NULL;
}

void Distribution::setData(vector<scalar> d) {

	// New data, so everything is wrong
	Distribution();

	// Set data
	data = d;
}

vector<scalar> Distribution::getData() {
	return data;
}

scalar Distribution::getMean() {
	return *mean;
}

scalar Distribution::getVariance() {
	return *variance;
}

scalar Distribution::getStandardDeviation() {
	return sqrt(*variance);
}

scalar Distribution::getMaximum() {
	return maximum;
}

scalar Distribution::getMinimum() {
	return minimum;
}

void Distribution::calculateMoments(int n) {
	for(int i = 0; i < n; ++i) {
		calculateMoment(i);
	}
}

void Distribution::calculateMoment(int n) {

	// Undefined behaviour
	if(data.size() == 0) {
		fileLog->printWarning("Distribution::data", "No data present (array empty)");
		return;
	}

	vector<scalar>::iterator it;
	
	// Initialise
	moments[n] = 0;
	for(it = data.begin(); it != data.end(); ++it) {
		moments[n] += pow(*it, n + 1);
	}

	moments[n] /= data.size();

	// Check for mean
	if(n == 0) {
		meanSet = true;
	}
}

void Distribution::calculateCentralMoments(int n) {
	for(int i = 0; i < n; ++i) {
		calculateCentralMoment(i);
	}
}

void Distribution::calculateCentralMoment(int n) {

	// Make sure we have the correct mean first
	if(!meanSet) {
		calculateMoment(0);
	}

	// Check for simplicity (already initialised to zero)
	if(n == 0) {
		return;
	}

	// Undefined behaviour
	if(data.size() == 0) {
		fileLog->printWarning("Distribution::data", "No data present (array empty)");
		return;
	}

	vector<scalar>::iterator it;
	
	// Initialise
	cMoments[n] = 0;
	for(it = data.begin(); it != data.end(); ++it) {
		cMoments[n] += pow(*it - getMean(), n + 1);
	}

	cMoments[n] /= data.size();
}

void Distribution::calculateMean() {

	// Calculate 1st moment
	calculateMoment(0);
}

void Distribution::calculateVariance() {

	// Calculate second central moment
	calculateCentralMoment(1);
}

void Distribution::calculateLimits() {

	// Sort vector, and get data limits
	sort(data.begin(), data.end());

	minimum = data.front();
	maximum = data.back();
}
