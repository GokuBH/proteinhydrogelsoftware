#include "NetworkFileWriter.hpp"

// Define static variables
const vector<string> NetworkFileWriter::proteinAttributes {"name", "type", "density", "viscosity", "maxNumConnections"};
const vector<string> NetworkFileWriter::siteAttributes {"name", "type", "position"};
const vector<string> NetworkFileWriter::bondAttributes {"name", "type", "k", "l"};
const vector<string> NetworkFileWriter::nodeAttributes {"protein", "position", "velocity", "radius"};
const vector<string> NetworkFileWriter::connectionAttributes {"bond", "nodes", "sites"};

const int NetworkFileWriter::maxXMLLevel = 2;

NetworkFileWriter::NetworkFileWriter() {

	cNumThreads = 0;
	XMLLevel = 0;
	net = NULL;
	box = NULL;
}

NetworkFileWriter::~NetworkFileWriter() {

	cNumThreads = 0;
	XMLLevel = 0;
	net = NULL;
	box = NULL;
}


//
// Input Files
//
int NetworkFileWriter::writeInputFile(PhysicalSystem *pSystem, bfs::path oFname) {

	// User Info
	fileLog->printMethodTask("Writing initialised system to file...\n");

	// Get doc and XML objects
	XMLDocument doc;
	XMLElement *rootEl, *compEl, *sysEl;

	// And some objects we'll need
	Network *net;
	NetworkParameters *params;
	SimulationBox *box;

	net = pSystem->getNetwork();
	if(net == NULL) {
		fileLog->printError("PhysicalSystem::net", "Does not exist.");
		return BioNetError;
	}

	params = pSystem->getParameters();
	if(net == NULL) {
		fileLog->printError("PhysicalSystem::params", "Does not exist.");
		return BioNetError;
	}

	box = pSystem->getSimulationBox();
	if(box == NULL) {
		fileLog->printError("PhysicalSystem::box", "Does not exist.");
		return BioNetError;
	}

	// Root wrapper
	rootEl = doc.NewElement("BioNet");
	doc.InsertFirstChild(rootEl);

	// Components
	compEl = doc.NewElement("components");  
	rootEl->InsertEndChild(compEl);

	// Proteins
	if(writeProteinsToInputFile(&doc, compEl, net) == BioNetError) {
		fileLog->printError("Unable to write proteins to file.");
		return BioNetError;
	}

	// Bonds
	if(writeBondsToInputFile(&doc, compEl, net) == BioNetError) {
		fileLog->printError("Unable to write bonds to file.");
		return BioNetError;
	}

	// System
	sysEl = doc.NewElement("system");  
	rootEl->InsertEndChild(sysEl);

	// Params
	if(writeParametersToInputFile(&doc, sysEl, params) == BioNetError) {
		fileLog->printError("Unable to write parameters to file.");
		return BioNetError;
	}

	// Nodes
	if(writeNodesToInputFile(&doc, sysEl, net) == BioNetError) {
		fileLog->printError("Unable to write nodes to file.");
		return BioNetError;
	}

	// Connections
	if(writeConnectionsToInputFile(&doc, sysEl, net, params) == BioNetError) {
		fileLog->printError("Unable to write connections to file.");
		return BioNetError;
	}

	// Simulation Box
	if(writeSimulationBoxToInputFile(&doc, sysEl, box) == BioNetError) {
		fileLog->printError("Unable to write simulation box to file.");
		return BioNetError;
	}

	doc.SaveFile(oFname.string().c_str());

	fileLog->printMethodSuccess();
	return BioNetSuccess;
}

int NetworkFileWriter::writeProteinsToInputFile(XMLDocument *doc, XMLElement *parent, Network *net) {

	// User info
	fileLog->printMethodTask("Writing proteins...");

	int i;
	XMLElement *el; 
	vTObject **obj, *anObj;

	// Add the element wrapper
	el = doc->NewElement("proteins");
	parent->InsertEndChild(el);

	// Check proteins are available
	obj = net->getProteins();
	if(obj == NULL) {
		fileLog->printError("Network::protein", "Does not exist.");
		return BioNetError;
	}

	for(i = 0; i < net->getNumProteins(); ++i) {
		anObj = obj[i];
		if(anObj == NULL) {
			fileLog->printError("Network::protein[" + to_string(i) + "]", "Does not exist.");
			return BioNetError;
		}

		if(writeVTObjectToInputFile(doc, el, "protein", proteinAttributes, anObj) == BioNetError) {
			fileLog->printError("Network::protein[" + to_string(i) + "]", "Unable to write to file.");
		}
	}

	fileLog->printMethodSuccess();
	return BioNetSuccess;
}

int NetworkFileWriter::writeBondsToInputFile(XMLDocument *doc, XMLElement *parent, Network *net) {

	// User info
	fileLog->printMethodTask("Writing bonds...");

	int i;
	XMLElement *el; 
	cTObject **obj, *anObj;

	// Add the element wrapper
	el = doc->NewElement("bonds");
	parent->InsertEndChild(el);

	// Check bonds are available
	obj = net->getBonds();
	if(obj == NULL) {
		fileLog->printError("Network::bond", "Does not exist.");
		return BioNetError;
	}

	for(i = 0; i < net->getNumBonds(); ++i) {
		anObj = obj[i];
		if(anObj == NULL) {
			fileLog->printError("Network::bond[" + to_string(i) + "]", "Does not exist.");
			return BioNetError;
		}


		if(writeCTObjectToInputFile(doc, el, "bond", bondAttributes, anObj) == BioNetError) {
			fileLog->printError("Network::bond[" + to_string(i) + "]", "Unable to write to file.");
		}
	}

	fileLog->printMethodSuccess();
	return BioNetSuccess;
}

int NetworkFileWriter::writeParametersToInputFile(XMLDocument *doc, XMLElement *parent, NetworkParameters *params) {

	XMLElement *paramsEl, *el;
	const vector<string> att;
	vector<string>::const_iterator it;
	string val;

	// User info
	fileLog->printMethodTask("Writing parameters...");

	// Add the element wrapper
	paramsEl = doc->NewElement("params");
	parent->InsertEndChild(paramsEl);

	// Start writing stuff
	for(it = NetworkParameters::paramList.begin(); it != NetworkParameters::paramList.end(); ++it) {
		try {
			val = params->getParameter(*it);

			// What should we skip?
			if((val=="tFname" || val=="mFname") && (!requiresInitialisation(params->initType) && !isDynamicSimulation(params->simType))) {
				continue;
			} else if (val == "sFname" && isDynamicSimulation(params->simType)) {
				continue;
			}

			if(val != "") {
				el = doc->NewElement("param");
				paramsEl->InsertEndChild(el);
				el->SetAttribute("name", (*it).c_str());
				el->SetAttribute("value", val.c_str());
			}
		} catch (exception &e) {
			fileLog->printError("NetworkParameters::" + *it, "Not found.");
			return BioNetError;
		}	
	}

	fileLog->printMethodSuccess();
	return BioNetSuccess;
}

int NetworkFileWriter::writeNodesToInputFile(XMLDocument *doc, XMLElement *parent, Network *net) {

	// User info
	fileLog->printMethodTask("Writing nodes...");

	int i;
	XMLElement *el;
	vector<vObject*> obj; 
	vObject *anObj;

	// Add the element wrapper
	el = doc->NewElement("nodes");
	parent->InsertEndChild(el);

	// Check nodes are available
	obj = net->getNodes();
	if(obj.size() == 0) {
		return BioNetSuccess;
	}

	for(i = 0; i < net->getNumNodes(); ++i) {
		anObj = obj[i];
		if(anObj == NULL) {
			fileLog->printError("Network::node[" + to_string(i) + "]", "Does not exist.");
			return BioNetError;
		}

		if(writeVObjectToInputFile(doc, el, "node", nodeAttributes, anObj) == BioNetError) {
			fileLog->printError("Network::node[" + to_string(i) + "]", "Unable to write to file.");
		}
	}

	fileLog->printMethodSuccess();
	return BioNetSuccess;
}

int NetworkFileWriter::writeConnectionsToInputFile(XMLDocument *doc, XMLElement *parent, Network *net, NetworkParameters *params) {

	// User info
	fileLog->printMethodTask("Writing connections...");

	int i;
	XMLElement *el;
	vector<cObject*> obj;
	//cObject **obj;
	cObject *anObj;

	// Add the element wrapper
	el = doc->NewElement("springs");
	parent->InsertEndChild(el);

	// Check connections are available
	obj = net->getConnections();
//	if(obj == NULL) {
	if(obj.size() == 0) {
		return BioNetSuccess;
	}

	for(i = 0; i < net->getNumConnections(); ++i) {
		anObj = obj[i];
		if(anObj == NULL) {
			fileLog->printError("Network::connection[" + to_string(i) + "]", "Does not exist.");
			return BioNetError;
		}

		if(writeCObjectToInputFile(doc, el, "spring", connectionAttributes, anObj, params) == BioNetError) {
			fileLog->printError("Network::connection[" + to_string(i) + "]", "Unable to write to file.");
		}
	}

	fileLog->printMethodSuccess();
	return BioNetSuccess;
}

int NetworkFileWriter::writeVTObjectToInputFile(XMLDocument *doc, XMLElement *parent, string title, const vector<string> att, vTObject *obj) {
		
	XMLElement *el;
	vector<string>::const_iterator it;

	// Add wrapper
	el = doc->NewElement(title.c_str());  
	parent->InsertEndChild(el);

	// Start writing stuff
	for(it = att.begin(); it != att.end(); ++it) {
		try {
			el->SetAttribute((*it).c_str(), obj->getParameter(*it).c_str());
		} catch (exception &e) {
			fileLog->printError(title, "Parameter '" + *it + "' not found.");
			return BioNetError;
		}	
	}

	// Sites, if necessary
	if(title == "protein") {

		int i, numSites;
		vTObject *tSite;

		numSites = stoi(obj->getParameter("numSites"));
		if(numSites != 0) {
			for(i = 0; i < numSites; ++i) {
				tSite = obj->getTSite(i);
				if(tSite == NULL) {
					fileLog->printError("Protein::tSite[" + to_string(i) + "]", "Does not exist.");
					return BioNetError;
				}

				if(writeVTObjectToInputFile(doc, el, "site", siteAttributes, tSite) == BioNetError) {
					fileLog->printError("Protein::tSite[" + to_string(i) + "]", "Unable to write to file.");
				}
			}
		}
	}

	return BioNetSuccess;
}

int NetworkFileWriter::writeCTObjectToInputFile(XMLDocument *doc, XMLElement *parent, string title, const vector<string> att, cTObject *obj) {
		
	XMLElement *el;
	vector<string>::const_iterator it;

	// Add wrapper
	el = doc->NewElement(title.c_str());  
	parent->InsertEndChild(el);

	// Start writing stuff
	for(it = att.begin(); it != att.end(); ++it) {
		try {
			el->SetAttribute((*it).c_str(), obj->getParameter(*it).c_str());
		} catch (exception &e) {
			fileLog->printError(title, "Parameter '" + *it + "' not found.");
			return BioNetError;
		}	
	}

	return BioNetSuccess;
}

int NetworkFileWriter::writeVObjectToInputFile(XMLDocument *doc, XMLElement *parent, string title, const vector<string> att, vObject *obj) {
		
	XMLElement *el;
	vector<string>::const_iterator it;
	string t;
	int rOffset = 0;

	// Add wrapper
	el = doc->NewElement(title.c_str());  
	parent->InsertEndChild(el);

	// Try to get a template type
	t = obj->getParameter("type");
	if(t == "Point") {
		rOffset += 1;
	}
	
	// Start writing stuff
	for(it = att.begin(); it != att.end() - rOffset; ++it) {
		try {
			el->SetAttribute((*it).c_str(), obj->getParameter(*it).c_str());
		} catch (exception &e) {
			fileLog->printError(title, "Parameter '" + *it + "' not found.");
			return BioNetError;
		}	
	}

	return BioNetSuccess;
}

int NetworkFileWriter::writeCObjectToInputFile(XMLDocument *doc, XMLElement *parent, string title, const vector<string> att, cObject *obj, NetworkParameters *params) {
		
	XMLElement *el;
	vector<string>::const_iterator it;
	string t;
	int rOffset = 0;

	// Add wrapper
	el = doc->NewElement(title.c_str());  
	parent->InsertEndChild(el);

	// Do we need sites?
	if(!params->localConnectivitySet) {
		rOffset += 1;
	}

	// Try to get a template type
	t = obj->getParameter("type");	

	// Start writing stuff
	for(it = att.begin(); it != att.end() - rOffset; ++it) {
		try {
			el->SetAttribute((*it).c_str(), obj->getParameter(*it).c_str());
		} catch (exception &e) {
			fileLog->printError(title, "Parameter '" + *it + "' not found.");
			return BioNetError;
		}	
	}

	return BioNetSuccess;
}

int NetworkFileWriter::writeSimulationBoxToInputFile(XMLDocument *doc, XMLElement *parent, SimulationBox *box) {

	// Check first
	if(!box->exists) {
		return BioNetSuccess;
	}

	XMLElement *boxEl, *el;
	const vector<string> att;
	vector<string>::const_iterator it;
	string val;

	// User info
	fileLog->printMethodTask("Writing simulation box...");

	// Add the element wrapper
	boxEl = doc->NewElement("box");
	parent->InsertEndChild(boxEl);

	// Start writing stuff
	for(it = SimulationBox::paramList.begin(); it != SimulationBox::paramList.end(); ++it) {
		try {
			val = box->getParameter(*it);
			if(val != "") {
				el = doc->NewElement("param");
				boxEl->InsertEndChild(el);
				el->SetAttribute("name", (*it).c_str());
				el->SetAttribute("value", val.c_str());
			}
		} catch (exception &e) {
			fileLog->printError("SimulationBox::" + *it, "Not found.");
			return BioNetError;
		}	
	}

	fileLog->printMethodSuccess();
	return BioNetSuccess;
}


//
// Output Files
//

void NetworkFileWriter::writeTrajectoryHeader(ofstream& fout) {

	// Global data
	fout << "BioNet Trajectory" << endl;

	// End of header
	fout << "#####" << endl;
}

void NetworkFileWriter::writeMeasurementHeader(ofstream& fout) {

	// Global data
	fout << "BioNet Measurement" << endl;

	// End of header
	fout << "#####" << endl;
}

void NetworkFileWriter::writeTopologyHeader(ofstream& fout) {

	// Global data
	fout << "BioNet Topology" << endl;

	// End of header
	fout << "#####" << endl;
}

void NetworkFileWriter::writeCheckpointHeader(ofstream& fout) {

	// Global data
	fout << "BioNet Checkpoint" << endl;

	// End of header
	fout << "#####" << endl;
}

void NetworkFileWriter::writeForceHeader(ofstream& fout) {

	// Global data
	fout << "BioNet Unique Forces" << endl;

	// End of header
	fout << "#####" << endl;

}

void NetworkFileWriter::writeNormalModeHeader(ofstream& fout) {

	// Global data
	fout << "BioNet Eigensystem" << endl;

	// End of header
	fout << "#####" << endl;
}

void NetworkFileWriter::writeTrajectoryInitialisationHeader(NetworkParameters *params, PhysicalSystem *pSys, ofstream& fout) {

	// Title
	fout << "Initialisation (Type - " << getInitTypeString(params->initType) << ")" << endl;

	fout << "Nodes: " << pSys->getNetwork()->getNumNodes() << ", Connections: " << pSys->getNetwork()->getNumConnections() << ", Sites:";
	for(int i = 0; i < pSys->getNetwork()->getNumNodes(); ++i) {
		fout << " " << pSys->getNetwork()->getNodes()[i]->getNumChildren();
	}
	fout << endl;

	// End of header
	fout << "#####" << endl;
}

void NetworkFileWriter::writeMeasurementInitialisationHeader(NetworkParameters *params, PhysicalSystem *pSys, ofstream& fout) {

	// Title
	fout << "Initialisation (Type - " << getInitTypeString(params->initType) << ")" << endl;

	fout << "Nodes: " << pSys->getNetwork()->getNumNodes() << ", Connections: " << pSys->getNetwork()->getNumConnections() << ", Sites:";
	for(int i = 0; i < pSys->getNetwork()->getNumNodes(); ++i) {
		fout << " " << pSys->getNetwork()->getNodes()[i]->getNumChildren();
	}
	fout << endl;

	// Data
	fout << "Time (ns), Centroid [xyz] (nm)";

	fout << ", Elastic Energy (pN.Nm)";
	if (isInertialSimulation(params->simType)) {
		fout << ", Kinetic Energy (pN.Nm)";
	}

	if(params->stericForcesActive) {
		fout << ", Steric Energy (pN.Nm)";
	}

	if(params->vdwForcesActive) {
		fout << ", VdW Energy (pN.Nm)";
	}

	if(params->wallForcesActive) {
		fout << ", Wall Energy (pN.Nm)";
	}
	
	fout << ", Coordination (Mean)";
	fout << endl;

	// End of header
	fout << "#####" << endl;
}

void NetworkFileWriter::writeTopologyInitialisationHeader(NetworkParameters *params, PhysicalSystem *pSys, ofstream& fout) {

	// Title
	fout << "Initialisation (Type - " << getInitTypeString(params->initType) << ")" << endl;

	fout << "Nodes: " << pSys->getNetwork()->getNumNodes() << ", Connections: " << pSys->getNetwork()->getNumConnections() << ", Sites:";
	for(int i = 0; i < pSys->getNetwork()->getNumNodes(); ++i) {
		fout << " " << pSys->getNetwork()->getNodes()[i]->getNumChildren();
	}
	fout << endl;

	// End of header
	fout << "#####" << endl;
}

void NetworkFileWriter::writeCheckpointInitialisationHeader(NetworkParameters *params, PhysicalSystem *pSys, ofstream& fout) {

	// Title
	fout << "Initialisation (Type - " << getInitTypeString(params->initType) << ")" << endl;

	// Filenames
	fout << "Trajectory " << pSys->getParameters()->tFname << endl;
	fout << "Measurement " << pSys->getParameters()->mFname << endl;
	fout << "Topology " << pSys->getParameters()->tpFname << endl;

	// Initial seeds (threads then seeds)
	fout << "RNG " << rngCont->getNumThreads();
	for(int i = 0; i < rngCont->getNumThreads(); ++i) {
		fout << " " << rngCont->getThermalRNGSeed(i);
	}

	fout << " " << rngCont->getMinorRNGSeed() << endl;

	// End of header
	fout << "#####" << endl;
}

void NetworkFileWriter::writeTrajectoryInitialisationFooter(ofstream& fout) {
	
	// End of section
	fout << "#####" << endl;
}

void NetworkFileWriter::writeMeasurementInitialisationFooter(ofstream& fout) {
	
	// End of section
	fout << "#####" << endl;
}

void NetworkFileWriter::writeTopologyInitialisationFooter(ofstream& fout) {
	
	// End of section
	fout << "#####" << endl;
}

void NetworkFileWriter::writeCheckpointInitialisationFooter(ofstream& fout) {
	
	// End of section
	fout << "#####" << endl;
}

void NetworkFileWriter::writeTrajectorySimulationHeader(NetworkParameters *params, PhysicalSystem *pSys, ofstream& fout) {

	// Title
	fout << "Simulation (Type - " << getSimTypeString(params->simType) << ")" << endl;

	fout << "Nodes: " << pSys->getNetwork()->getNumNodes() << ", Connections: " << pSys->getNetwork()->getNumConnections() << ", Sites:";
	for(int i = 0; i < pSys->getNetwork()->getNumNodes(); ++i) {
		fout << " " << pSys->getNetwork()->getNodes()[i]->getNumChildren();
	}
	fout << endl;

	// End of header
	fout << "#####" << endl;
}

void NetworkFileWriter::writeMeasurementSimulationHeader(NetworkParameters *params, PhysicalSystem *pSys, ofstream& fout) {

	// Title
	fout << "Simulation (Type - " << getSimTypeString(params->simType) << ")" << endl;

	fout << "Nodes: " << pSys->getNetwork()->getNumNodes() << ", Connections: " << pSys->getNetwork()->getNumConnections() << ", Sites:";
	for(int i = 0; i < pSys->getNetwork()->getNumNodes(); ++i) {
		fout << " " << pSys->getNetwork()->getNodes()[i]->getNumChildren();
	}
	fout << endl;

	// Data
	fout << "Time (ns), Centroid [xyz] (nm)";

	fout << ", Elastic Energy (pN.Nm)";
	if (isInertialSimulation(params->simType)) {
		fout << ", Kinetic Energy (pN.Nm)";
	}

	if(params->stericForcesActive) {
		fout << ", Steric Energy (pN.Nm)";
	}

	if(params->vdwForcesActive) {
		fout << ", VdW Energy (pN.Nm)";
	}

	if(params->wallForcesActive) {
		fout << ", Wall Energy (pN.Nm)";
	}
	
	fout << ", Coordination (Mean)";
	fout << endl;

	// End of header
	fout << "#####" << endl;
}

void NetworkFileWriter::writeTopologySimulationHeader(NetworkParameters *params, PhysicalSystem *pSys, ofstream& fout) {

	// Title
	fout << "Simulation (Type - " << getSimTypeString(params->simType) << ")" << endl;

	fout << "Nodes: " << pSys->getNetwork()->getNumNodes() << ", Connections: " << pSys->getNetwork()->getNumConnections() << ", Sites:";
	for(int i = 0; i < pSys->getNetwork()->getNumNodes(); ++i) {
		fout << " " << pSys->getNetwork()->getNodes()[i]->getNumChildren();
	}
	fout << endl;

	// End of header
	fout << "#####" << endl;
}

void NetworkFileWriter::writeCheckpointSimulationHeader(NetworkParameters *params, PhysicalSystem *pSys, ofstream& fout) {

	// Title
	fout << "Simulation (Type - " << getSimTypeString(params->simType) << ")" << endl;

	// Filenames
	fout << "Trajectory " << pSys->getParameters()->tFname << endl;
	fout << "Measurement " << pSys->getParameters()->mFname << endl;
	fout << "Topology " << pSys->getParameters()->tpFname << endl;

	// Initial seeds (threads then seeds)
	fout << "RNG " << rngCont->getNumThreads();
	for(int i = 0; i < rngCont->getNumThreads(); ++i) {
		fout << " " << rngCont->getThermalRNGSeed(i);
	}

	fout << " " << rngCont->getMinorRNGSeed() << endl;

	// End of header
	fout << "#####" << endl;
}

void NetworkFileWriter::writeForceSimulationHeader(NetworkParameters *params, PhysicalSystem *pSys, ofstream& fout) {

	// Title
	fout << "Simulation (Type - " << getSimTypeString(params->simType) << ")" << endl;

	fout << "Nodes: " << pSys->getNetwork()->getNumNodes() << endl;

	// End of header
	fout << "#####" << endl;
}

void NetworkFileWriter::writeNormalModeSimulationHeader(NetworkParameters *params, PhysicalSystem *pSys, ofstream& fout) {

	// Title
	fout << "Simulation (Type - " << getSimTypeString(params->simType) << ")" << endl;

	fout << "Nodes: " << pSys->getNetwork()->getNumNodes() << endl;
	fout << "Modes: " << params->numNMModes << endl;

	// End of header
	fout << "#####" << endl;
}

void NetworkFileWriter::writeTrajectorySimulationFooter(ofstream& fout) {
	
	// End of section
	fout << "#####" << endl;
}

void NetworkFileWriter::writeMeasurementSimulationFooter(ofstream& fout) {
	
	// End of section
	fout << "#####" << endl;
}

void NetworkFileWriter::writeTopologySimulationFooter(ofstream& fout) {
	
	// End of section
	fout << "#####" << endl;
}

void NetworkFileWriter::writeCheckpointSimulationFooter(ofstream& fout) {
	
	// End of section
	fout << "#####" << endl;
}

void NetworkFileWriter::writeForceSimulationFooter(ofstream& fout) {
	
	// End of section
	fout << "#####" << endl;
}

void NetworkFileWriter::writeNormalModeSimulationFooter(ofstream& fout) {
	
	// End of section
	fout << "#####" << endl;
}

void NetworkFileWriter::writeTrajectoryFrame(scalar time, NetworkParameters *params, PhysicalSystem *pSys, ofstream& fout) {

	// Assign network for quick access
	net = pSys->getNetwork();
	vector<vObject*> n = net->getNodes();

	// Time (1fs)
	fout << boost::format("%.6f") % time << endl;

	//
	// Calculations
	//

	// All positions and velocities of the network
	for(int i = 0; i < net->getNumNodes(); ++i) {
		writeVObjectToTrajectoryFile(n[i], fout, params->localConnectivitySet);
		fout << endl;
	}
	
	// End of frame
	fout << "###" << endl;
}

void NetworkFileWriter::writeMeasurementFrame(scalar time, NetworkParameters *params, PhysicalSystem *pSys, ofstream& fout) {

	// Assign network for quick access
	net = pSys->getNetwork();
	box = pSys->getSimulationBox();

	// Time (1fs)
	fout << boost::format("%.6f") % time;

	//
	// Calculations
	//

	// Centroid (1/10th angstrom)
	fout << boost::format(",%.2f,%.2f,%.2f") % (net->getCentroid()(0) * InternalUnits::Length) % (net->getCentroid()(1) * InternalUnits::Length) % (net->getCentroid()(2) * InternalUnits::Length);
//	fout << ", " << net->getCentroid()(0) * InternalUnits::Length;
//	fout << ", " << net->getCentroid()(1) * InternalUnits::Length;
//	fout << ", " << net->getCentroid()(2) * InternalUnits::Length;

	// Elastic energy (1/100th kBT (ish))
	fout << boost::format(",%.2f") % (net->getElasticEnergy() * InternalUnits::Energy);
//	fout << ", " << net->getElasticEnergy() * InternalUnits::Energy;

	// Kinetic energy (1/100th kBT (ish))
	if (isInertialSimulation(params->simType)) {
		fout << boost::format(",%.2f") % (net->getKineticEnergy() * InternalUnits::Energy);
//		fout << ", " << net->getKineticEnergy() * InternalUnits::Energy;
	}

	// Steric energy (1/100th kBT (ish))
	if(params->stericForcesActive) {
		fout << boost::format(",%.2f") % (box->getStericEnergy() * InternalUnits::Energy);
//		fout << ", " << box->getStericEnergy() * InternalUnits::Energy;
	}

	// VdW energy (already calculated)
	if(params->vdwForcesActive) {
		fout << boost::format(",%.2f") % (box->getVdWEnergy() * InternalUnits::Energy);
//		fout << ", " << box->getVdWEnergy() * InternalUnits::Energy;
	}

	if(params->wallForcesActive) {
		fout << boost::format(",%.2f") % (box->getWallEnergy() * InternalUnits::Energy);
//		fout << ", " << box->getVdWEnergy() * InternalUnits::Energy;
	}
	
	// Average Coordination (needs calculating)
	fout << boost::format(",%d") % (net->getCoordination());
//	fout << ", " << net->getCoordination();
	
	// End
	fout << endl;
}

void NetworkFileWriter::writeTopologyFrame(scalar time, NetworkParameters *params, PhysicalSystem *pSys, ofstream& fout) {
	
	// Assign network for quick access
	net = pSys->getNetwork();

	// Time (1fs)
	fout << boost::format("%.6f") % time << endl;

	// All positions and velocities of the network
	vector<cObject*> c = net->getConnections();
	for(vector<cObject*>::iterator it = c.begin(); it != c.end(); ++it) {
		writeCObjectToTopologyFile(*it, fout);
	}
	
	// End of frame
	fout << "###" << endl;
}

void NetworkFileWriter::writeCheckpointFrame(scalar time, NetworkParameters *params, PhysicalSystem *pSys, ofstream& fout) {
	
	int i;

	// Time (1fs)
	fout << boost::format("%.6f") % time << endl;

	// Output the states of the RNGs
	for(i = 0; i < rngCont->getNumThreads(); ++i) {
		fout << rngCont->getThermalRNG(i) << endl << "#" << endl << rngCont->getNormalDistribution(i) << endl << "#" << endl;
	}
	
	fout << rngCont->getMinorRNG() << endl << "#" << endl << rngCont->getUniformDistribution() << endl;

	// End of frame
	fout << "###" << endl;
}

void NetworkFileWriter::writeForceFrame(scalar time, NetworkParameters *params, PhysicalSystem *pSys, ofstream& fout) {

	// Assign network for quick access
	net = pSys->getNetwork();
	vector<fObject*> f = net->getUniqueForces();

	// Time (1fs)
	fout << boost::format("%.6f") % time << endl;

	//
	// Calculations
	//

	// All positions and velocities of the network
	for(int i = 0; i < net->getNumForces(); ++i) {
		writeFObjectToTrajectoryFile(f[i], fout, params->localConnectivitySet);
		fout << endl;
	}
	
	// End of frame
	fout << "###" << endl;
}

void NetworkFileWriter::writeEigensystem(NetworkParameters *params, PhysicalSystem *pSys, ofstream& fout) {

	int i, j;

	// Assign network for quick access
	net = pSys->getNetwork();

	// Get things to write
	VectorXd eigVal = net->getEigenvalues();
	MatrixXd eigVec = net->getEigenvectors();

	// First line is the eigenvalues
	fout << boost::format("%.6f") % (eigVal[0]);
	for(i = 1; i < eigVal.rows(); ++i) {
		fout << ", " << boost::format("%.6f") % (eigVal[i]);
	}
	fout << endl;

	// Each of the next lines are the eigenvectors
	for(j = 0; j < eigVec.cols(); ++j) {	
		fout << boost::format("%.6f") % (eigVec(j,0));
		for(i = 1; i < eigVec.rows(); ++i) {
			fout << ", " << boost::format("%.6f") % (eigVec(i,j));
		}
		fout << endl;
	}

	// End of frame
	fout << "###" << endl;
}

int NetworkFileWriter::truncateCheckpoint(NetworkParameters *params, PhysicalSystem *pSys, bfs::path cFname) {

	fileLog->printMethodTask("Truncating checkpoint...");

	// Find the location to read final frame from
	ifstream fin;
	streampos begin, mid, end;
	int fileSize, frameSize, count;
	scalar startTime;
	string line;

	// First, read the header

	// Open
	fin.open(cFname.string(), ios::binary);
	
	// Get start
	begin = fin.tellg();

	// Seek to  where we need to be
	count = 0;
	while(true) {
		getline(fin, line);
		if(line == "") {
			end = fin.tellg();
		} else if (line == "#####") {
			count += 1;
			if(count == 2) {
				end = fin.tellg();
				break;
			}
		} else {
			mid = fin.tellg();
		}
	}

	// Close
	fin.close();

	// Truncate to end point
	fileSize = mid - begin;
	bfs::resize_file(cFname, fileSize);

	fileLog->printMethodSuccess();
	return BioNetSuccess;
}

int NetworkFileWriter::truncateTrajectory(NetworkParameters *params, PhysicalSystem *pSys, bfs::path tFname) {

	fileLog->printMethodTask("Truncating trajectory...");

	// Find the location to read final frame from
	ifstream fin;
	streampos begin, mid, end;
	int fileSize, frameSize, count;
	scalar startTime;
	string line;

	// Open
	fin.open(tFname.string(), ios::binary);
	begin = fin.tellg();

	// Get to header
	count = 0;
	while(true) {
		getline(fin, line);
		if(line == "#####") {
			count += 1;
			if(count == 2) {
				mid = fin.tellg();
				break;
			}
		}
	}

	// Seek to  where we need to be
	while(true) {
		getline(fin, line);
		if(line == "#####" || line == "") {
			end = fin.tellg();
			break;

		} else if (line == "###") {
			mid = fin.tellg();
		}
	}

	// Read frame
	fin.close();

	// Truncate to end point
	fileSize = mid - begin;
	bfs::resize_file(tFname, fileSize);

	fileLog->printMethodSuccess();
	return BioNetSuccess;
}

int NetworkFileWriter::truncateMeasurement(NetworkParameters *params, PhysicalSystem *pSys, bfs::path mFname) {

	fileLog->printMethodTask("Truncating measurement...");

	// Currently this is relatively easy, we just cut off the last line
	ifstream fin;
	streampos begin, mid, end;
	int fileSize, count;
	string line;
	vector<string> lineVec;

	// Open
	fin.open(mFname.string(), ios::binary);

	// Get filesize
	begin = fin.tellg();

	// Header
	count = 0;
	while(true) {
		getline(fin, line);
		if(line == "#####") {
			count += 1;
			if(count == 2) {
				mid = fin.tellg();
				break;
			}
		}
	}

	// Seek to where we need to be
	while(true) {
		getline(fin, line);
		if(line == "#####" || line == "") {
			end = fin.tellg();
			break;
		} else {
			mid = fin.tellg();
		}
	}
	
	// Close
	fin.close();

	// Truncate to end point
	fileSize = mid - begin;
	bfs::resize_file(mFname, fileSize);

	fileLog->printMethodSuccess();
	return BioNetSuccess;
}

int NetworkFileWriter::truncateTopology(NetworkParameters *params, PhysicalSystem *pSys, bfs::path tpFname) {

	fileLog->printMethodTask("Truncating topology...");

	// Find the location to read final frame from
	ifstream fin;
	streampos begin, mid, end;
	int fileSize, frameSize, count;
	scalar startTime;
	string line;

	// Open
	fin.open(tpFname.string(), ios::binary);
	begin = fin.tellg();

	// Get to header
	count = 0;
	while(true) {
		getline(fin, line);
		if(line == "#####") {
			count += 1;
			if(count == 2) {
				mid = fin.tellg();
				break;
			}
		}
	}

	// Seek to  where we need to be
	while(true) {
		getline(fin, line);
		if(line == "#####" || line == "") {
			end = fin.tellg();
			break;

		} else if (line == "###") {
			mid = fin.tellg();
		}
	}

	// Read frame
	fin.close();

	// Truncate to end point
	fileSize = mid - begin;
	bfs::resize_file(tpFname, fileSize);

	fileLog->printMethodSuccess();
	return BioNetSuccess;
}

int NetworkFileWriter::truncateForce(NetworkParameters *params, PhysicalSystem *pSys, bfs::path cfFname) {

	fileLog->printMethodTask("Truncating forces...");

	// Find the location to read final frame from
	ifstream fin;
	streampos begin, mid, end;
	int fileSize, frameSize, count;
	scalar startTime;
	string line;

	// Open
	fin.open(cfFname.string(), ios::binary);
	begin = fin.tellg();

	// Get to header
	count = 0;
	while(true) {
		getline(fin, line);
		if(line == "#####") {
			count += 1;
			if(count == 2) {
				mid = fin.tellg();
				break;
			}
		}
	}

	// Seek to  where we need to be
	while(true) {
		getline(fin, line);
		if(line == "#####" || line == "") {
			end = fin.tellg();
			break;

		} else if (line == "###") {
			mid = fin.tellg();
		}
	}

	// Read frame
	fin.close();

	// Truncate to end point
	fileSize = mid - begin;
	bfs::resize_file(cfFname, fileSize);

	fileLog->printMethodSuccess();
	return BioNetSuccess;
}

void NetworkFileWriter::writeVObjectToTrajectoryFile(vObject *n, ofstream& fout, bool local) {
	
	int i;

	// Position (1/100th angstrom)
	fout << boost::format("%.3f,%.3f,%.3f") % (n->getPosition()(0) * InternalUnits::Length) % (n->getPosition()(1) * InternalUnits::Length) % (n->getPosition()(2) * InternalUnits::Length);

	// Velocity (1/100th nm/ns)
	fout << boost::format(",%.2f,%.2f,%.2f") % (n->getVelocity()(0) * InternalUnits::Velocity) % (n->getVelocity()(1) * InternalUnits::Velocity) % (n->getVelocity()(2) * InternalUnits::Velocity);

	// Force (1/100th pN)
	fout << boost::format(",%.2f,%.2f,%.2f") % ((n->getTotalForce()(0) - n->getThermalForce()(0)) * InternalUnits::Force) % ((n->getTotalForce()(1) - n->getThermalForce()(1)) * InternalUnits::Force) % ((n->getTotalForce()(2) - n->getThermalForce()(2)) * InternalUnits::Force);
//	fout << boost::format(",%.2f,%.2f,%.2f") % (n->getTotalForce()(0) * InternalUnits::Force) % (n->getTotalForce()(1) * InternalUnits::Force) % (n->getTotalForce()(2) * InternalUnits::Force);

	// Orientation (1/100th degree (ish))
	if(local) {
		for(int i = 0; i < 9; ++i) {
			fout << boost::format(",%.3f") % n->getOrientationMatrix()(i);
	//		fout << ", " << n->getOrientationMatrix()(i);
		}
	}

	// Boundary conditions
	fout << boost::format(",%d,%d,%d") % (n->getWrap()(0)) % (n->getWrap()(1)) % (n->getWrap()(2));
}

void NetworkFileWriter::writeFObjectToTrajectoryFile(fObject *f, ofstream& fout, bool local) {

	// Position
	fout << boost::format("%.3f,%.3f,%.3f") % (f->getPosition()(0) * InternalUnits::Length) % (f->getPosition()(1) * InternalUnits::Length) % (f->getPosition()(2) * InternalUnits::Length);

	// Force (1/100th pN)
	fout << boost::format(",%.4f,%.4f,%.4f") % (f->getForce()(0) * InternalUnits::Force) % (f->getForce()(1) * InternalUnits::Force) % (f->getForce()(2) * InternalUnits::Force);
}
	
void NetworkFileWriter::writeCObjectToTopologyFile(cObject *c, ofstream& fout) {

	// For all terminals, get every vertex and write the indices up to the top parent
	int i;
	vObject *v;
	if(c != NULL) {
/*
		for(i = 0; i < c->getNumTerminals(); ++i) {

			// Get terminal
			v = c->getTerminal(i);

			// Write the index
			fout << v->getIndex();

			// Now, same for all parents
			v = v->getParent();
			while(v != NULL) {
				fout << "," << v->getIndex();
				v = v->getParent();
			}

			fout << " ";
		}
*/
		for(i = 0; i < c->getNumTerminals(); ++i) {

			// Get terminal
			v = c->getTerminal(i);
//			if(i == 0) {
//				v = c->getStartTerminal();
//			} else {
//				v = c->getEndTerminal();
//			}

			// Write the index
			fout << v->getIndex();

			// Now, same for all parents
			v = v->getParent();
			while(v != NULL) {
				fout << "," << v->getIndex();
				v = v->getParent();
			}

			fout << " ";
		}

		// Offset
		fout << c->getOffset()[0] << "," << c->getOffset()[1] << "," << c->getOffset()[2];
		fout << " ";

		// Protein template index
		fout << c->getTemplateIndex() << endl;
	}
}

int NetworkFileWriter::readFrameFromCheckpointFile(NetworkParameters *params, PhysicalSystem *pSys, ifstream& fin) {
	
	int i;
	string line;
	vector<string> lineVec;
	stringstream ss;

	// Get time
	getline(fin, line);
	pSys->setSimTime(stos(line) - params->timePerFrame);

	// Now get the RNG states
	i = 0;
	for(i = 0; i < cNumThreads; ++i) {

		// Thermal RNG State
		getline(fin, line);
		if(i < rngCont->getNumThreads()) {
			ss << line;
			rngCont->setThermalRNGState(i, ss);
		}

		// Thermal distribution state
		getline(fin, line);
		getline(fin, line);
		if(i < rngCont->getNumThreads()) {
			ss << line;
			rngCont->setThermalRNGState(i, ss);
		}
		getline(fin, line);

	}

	// Minor RNG State
	getline(fin, line);
	ss << line;
	rngCont->setMinorRNGState(ss);

	// Minor distribution state
	getline(fin, line);
	getline(fin, line);
	ss << line;
	rngCont->setMinorRNGState(ss);

	return BioNetSuccess;

}

int NetworkFileWriter::readFrameFromTrajectoryFile(NetworkParameters *params, PhysicalSystem *pSys, ifstream& fin) {

	int i, j, lineSize;
	string line;
	vector<vObject*> nodes;
	vector<string> lineVec;
	vector<string>::iterator it;
	Matrix3 oM;

	// Get node pointers
	nodes = pSys->getNetwork()->getNodes();

	// Set time to here (minus 1 frame for consistency with output schedule)
	getline(fin, line);
//	pSys->setSimTime(stos(line) - params->timePerFrame);
	for(i = 0; i < pSys->getNetwork()->getNumNodes(); ++i) {
		getline(fin, line);
		boost::split(lineVec, line, boost::is_any_of(","));

		// Test line size (maybe put this outside loop later)
//		53.866,14.511,66.871,0.00,0.00,0.00,0.00,0.00,0.00,0.54,0.84,0.00,-0.84,0.54,0.00,0.00,0.00,1.00
		if(params->localConnectivitySet) {
			lineSize = 18;
		} else {
			lineSize = 9;
		}

		if(lineVec.size() != lineSize) {
			fileLog->printError("NetworkFileWriter::readFrameFromTrajectoryFile", "Different number of coordinates specified in trajectory than objects in the system. Unable to read trajectory. :( Please check whether local connectivity has been set correctly");
			return BioNetError;
		}

		// Assign node positions
		nodes[i]->setPosition(stos(lineVec[0]) / InternalUnits::Length, stos(lineVec[1]) / InternalUnits::Length, stos(lineVec[2]) / InternalUnits::Length);
		nodes[i]->setVelocity(stos(lineVec[3]) / InternalUnits::Velocity, stos(lineVec[4]) / InternalUnits::Velocity, stos(lineVec[5]) / InternalUnits::Velocity);
//		nodes[i]->setOrientation(stos(lineVec[6]) / InternalUnits::Angle, stos(lineVec[7]) / InternalUnits::Angle, stos(lineVec[8]) / InternalUnits::Angle);

		if(params->localConnectivitySet) {
			for(j = 0; j < 9; ++j) {
				oM(j) = stos(lineVec[9 + j]);
			}
			nodes[i]->setOrientationMatrix(oM);

		}
/*
		for(j = 0; j < nodes[i]->getNumChildren(); ++j) {
			nodes[i]->getChild(j)->setPosition(stos(lineVec[6 * (j + 1) + 3]) / InternalUnits::Length, stos(lineVec[6 * (j + 1) + 4]) / InternalUnits::Length, stos(lineVec[6 * (j + 1) + 5]) / InternalUnits::Length);
			nodes[i]->getChild(j)->setVelocity(stos(lineVec[6 * (j + 1) + 6]) / InternalUnits::Velocity, stos(lineVec[6 * (j + 1) + 7]) / InternalUnits::Velocity, stos(lineVec[6 * (j + 1) + 8]) / InternalUnits::Velocity);
			orien = nodes[i]->getChild(j)->getPosition() - nodes[i]->getPosition();
			orien /= orien.norm();
			nodes[i]->getChild(j)->setRelativePosition(orien[0], orien[1], orien[2]);
		}
*/
	}
	
	// Read final '###' line
	getline(fin, line);
	return BioNetSuccess;
}

int NetworkFileWriter::readFrameFromTopologyFile(NetworkParameters *params, PhysicalSystem *pSys, ifstream& fin) {

	int i, j, k;
	string line;

	// Get time (and ignore)
	getline(fin, line);
	
	// Build the connectivity setup
	vector<string> termSplit, nodeSplit;
	vector<int> indices;
	vector<int>::iterator it;
	
	while(true) {
		getline(fin, line);

		// When to break?
		if(line == "###") {
			break;
		}

		// Split the line
		boost::split(termSplit, line, boost::is_any_of(" "));

		// Test line size (maybe put this outside loop later)
		if(params->localConnectivitySet) {
			for(j = 0; j < termSplit.size(); ++j) {
				boost::split(nodeSplit, termSplit[j], boost::is_any_of(","));
				for(k = 0; k < nodeSplit.size(); ++k) {
					try {
						indices.push_back(stoi(nodeSplit[k]));
					} catch (exception &e) {
						break;
					}
				}
			}
		} else {
			for(j = 0; j < termSplit.size(); ++j) {
				indices.push_back(stoi(termSplit[j]));
				indices.push_back(-1);
			}

		}

		// Was the protein index specified (backwards compatibility)
		if(indices.size() % 5 != 0) {
			indices.push_back(0);
		}
	}

	// Build the connections
//	if(pSys->getNetwork()->buildConnections(indices) == BioNetError) {
//		fileLog->printError("NetworkFileWriter::readFrameFromTopologyFile", "Unable to rebuild connectivity from topology file :( Please check that you have the same number of connection templates and nodes as you did in the initial simulation.");
//		return BioNetError;
//	}
	return BioNetSuccess;
}

int NetworkFileWriter::readFrameFromForceFile(NetworkParameters *params, PhysicalSystem *pSys, ifstream& fin) {

/*	
	int i, j, lineSize;
	string line;
	vector<vObject*> nodes;
	vector<string> lineVec;
	vector<string>::iterator it;
	Matrix3 oM;

	// Get node pointers
	nodes = pSys->getNetwork()->getNodes();

	// Set time to here (minus 1 frame for consistency with output schedule)
	getline(fin, line);
	for(i = 0; i < pSys->getNetwork()->getNumNodes(); ++i) {
		getline(fin, line);
		boost::split(lineVec, line, boost::is_any_of(","));

		// Test line size (maybe put this outside loop later)
		if(params->localConnectivitySet) {
			lineSize = 18;
		} else {
			lineSize = 9;
		}

		if(lineVec.size() != lineSize) {
			fileLog->printError("NetworkFileWriter::readFrameFromForceFile", "Different number of coordinates specified in trajectory than objects in the system. Unable to read trajectory. :( Please check whether local connectivity has been set correctly");
			return BioNetError;
		}

		// Assign node positions
		nodes[i]->setPosition(stos(lineVec[0]) / InternalUnits::Length, stos(lineVec[1]) / InternalUnits::Length, stos(lineVec[2]) / InternalUnits::Length);
		nodes[i]->setVelocity(stos(lineVec[3]) / InternalUnits::Velocity, stos(lineVec[4]) / InternalUnits::Velocity, stos(lineVec[5]) / InternalUnits::Velocity);

		if(params->localConnectivitySet) {
			for(j = 0; j < 9; ++j) {
				oM(j) = stos(lineVec[9 + j]);
			}
			nodes[i]->setOrientationMatrix(oM);

		}
	}
	
	// Read final '###' line
	getline(fin, line);
	
*/
	return BioNetSuccess;
}
