#include "NetworkPrimitives.hpp"

// Global Methods
string getProteinTypeString(ProteinType proteinType) {
	return proteinTypeString[proteinType];
}

ProteinType getProteinType(string s) {

	boost::algorithm::to_lower(s);
	if(s == "point") {
		return PPoint;
	} else if (s == "sphere") {
		return PSphere;
	} else if (s == "ellipsoid") {
		return PEllipsoid;
	} else if (s == "blob") {
		return PBlob;
	} else {
		return NoProt;
	}
}

string getBondTypeString(BondType bondType) {
	return bondTypeString[bondType];
}

BondType getBondType(string s) {

	boost::algorithm::to_lower(s);
	if(s == "spring" or s == "linear") {
		return BSpring;
	} else if(s == "rigid") {
		return BRigid;
	} else if(s == "fene") {
		return BFENE;
	} else if(s == "angle") {
		return BAngle;
	} else {
		return NoBond;
	}
}

string getISiteTypeString(ISiteType iSiteType) {
	return iSiteTypeString[iSiteType];
}

ISiteType getISiteType(string s) {

	boost::algorithm::to_lower(s);
	if(s == "point") {
		return ISPoint;
	} else if (s == "circle") {
		return ISCircle;
	} else {
		return NoISite;
	}
}

tObject::tObject() {
	tIndex = 0;
	name = "";
}

tObject::~tObject() {
	tIndex = 0;
	name = "";
}

int tObject::setParameter(string name, string value) {

	boost::algorithm::to_lower(name);
	if(name == "tindex" || name == "index") {
		setIndex(stoi(value));
	} else if (name == "name") {
		setName(value);
	} else {
		return BioNetWarning;
	}
	return BioNetSuccess;
}

string tObject::getParameter(string name) {
	if(name == "tIndex" || name == "index") {
		return to_string(getIndex());
	} else if (name == "name") {
		return getName();
	} else {
		return "";
	}
}

void tObject::setIndex(int i) {
	tIndex = i;
}

int tObject::getIndex() {
	return tIndex;
}

void tObject::setName(string s) {
	name = s;
}

string tObject::getName() {
	return name;
}

vTObject::vTObject() : tObject() {

	radius = 0;
	density = 0;
	mass = 0;
	viscosity = 0;
	drag = 0;

	aDrag = 0.0;
	momentOfInertia = 0.0;

	// This limit is so objects do not form a 'black hole', so to speak, infinite connections at a single point in space. 
	// This limit will be auto-changed if sites exist
	// This limit is the combination of a simple cubic and FCC combination
	maxNumConnections = 14;

	chargeDensity = 0.0;
	vdwPotential = 0.0;
	vdwEquilibrium = 0.0;
	reactionRate = sInf;

	relPos.setZero();

	tSite.clear();
	numTSites = 0;
}

vTObject::~vTObject() {

	radius = 0;
	density = 0;
	mass = 0;
	viscosity = 0;
	drag = 0;

	aDrag = 0.0;
	momentOfInertia = 0.0;

	maxNumConnections = 0;

	chargeDensity = 0.0;
	vdwPotential = 0.0;
	vdwEquilibrium = 0.0;
	reactionRate = 0.0;

	relPos.setZero();

	for(int i = 0; i < numTSites; ++i) {
		delete tSite.at(i);
		tSite[i] = NULL;
	}

	tSite.clear();
	numTSites = 0;
}

int vTObject::setParameter(string name, string value) {

	boost::algorithm::to_lower(name);
	if (name == "radius") {
		setRadius(stos(value) / InternalUnits::Length);
	} else if (name == "density") {
		setDensity(stos(value) / InternalUnits::Density);
	} else if (name == "mass") {
		setMass(stos(value) / InternalUnits::Mass);
	} else if (name == "viscosity") {
		setViscosity(stos(value) / InternalUnits::Viscosity);
	} else if (name == "drag") {
		setDrag(stos(value) / InternalUnits::Drag);
	} else if (name == "rdrag" || name == "rotationaldrag") {
		setRotationalDrag(stos(value) / InternalUnits::AngularDrag);
	} else if(name == "momentofinertia") {
		setMomentOfInertia(stos(value) / InternalUnits::MomentOfInertia);
	} else if (name == "chargedensity") {
		setChargeDensity(stos(value) / InternalUnits::ChargeDensity);
	} else if (name == "vdwpotential") {
		setVdWPotential(stos(value) / InternalUnits::Energy);
	} else if (name == "vdwequilibrium") {
		setVdWEquilibrium(stos(value) / InternalUnits::Length);
	} else if (name == "rate" || name == "reactionrate") {
		setReactionRate(stos(value) / InternalUnits::Frequency);
	} else if (name == "position") {
		if(setRelativePosition(value, true) != BioNetSuccess) {
			fileLog->printError("vTObject::relPos", "Unable to assign from string '" + value + "'.");
			return BioNetError;
		}
	} else if (name == "maxkineticbonds" || name == "maxbonds" || name == "maxNumKineticConnections" || name == "maxnumconnections" || name == "maxconnections") {
		setMaxNumConnections(stoi(value));
	} else {
		return tObject::setParameter(name, value);
	}	
	return BioNetSuccess;
}

string vTObject::getParameter(string name) {

	ostringstream sStream;

	if (name == "radius") {
		sStream << getRadius();
		return sStream.str();
	} else if (name == "density") {
		sStream << getDensity();
		return sStream.str();
	} else if (name == "mass") {
		sStream << getMass();
		return sStream.str();
	} else if (name == "viscosity") {
		sStream << viscosity;
		return sStream.str();
	} else if (name == "drag") {
		sStream << getDrag();
		return sStream.str();
	} else if (name == "chargeDensity") {
		sStream << getChargeDensity();
		return sStream.str();
	} else if (name == "vdwPotential") {
		sStream << getVdWPotential();
		return sStream.str();
	} else if (name == "vdwEquilibrium") {
		sStream << getVdWEquilibrium();
		return sStream.str();
	} else if (name == "maxNumConnections") {
		return to_string(getMaxNumConnections());
	} else if (name == "numsites") {
		return to_string(getNumTSites());
	} else {
		return tObject::getParameter(name);
	}
}

void vTObject::setRadius(scalar r) {
	radius = r;
}

scalar vTObject::getRadius() {
	return radius;
}

int vTObject::getNumTSites() {
	return numTSites;
}

void vTObject::setMass(scalar m) {
	mass = m;
}

scalar vTObject::getMass() {
	return mass;
}

void vTObject::setDensity(scalar d) {
	density = d;
}

scalar vTObject::getDensity() {
	return density;
}

void vTObject::setViscosity(scalar v) {
	viscosity = v;
}

scalar vTObject::getViscosity() {
	return viscosity;
}

void vTObject::setDrag(scalar d) {
	drag = d;
}

scalar vTObject::getDrag() {
	return drag;
}

void vTObject::setRotationalDrag(scalar d) {
	aDrag = d;
}

scalar vTObject::getRotationalDrag() {
	return aDrag;
}

void vTObject::setMomentOfInertia(scalar I) {
	momentOfInertia = I;
}

scalar vTObject::getMomentOfInertia() {
	return momentOfInertia;
}

void vTObject::setChargeDensity(scalar d) {
	chargeDensity = d;
}

scalar vTObject::getChargeDensity() {
	return chargeDensity;
}

void vTObject::setVdWPotential(scalar p) {
	vdwPotential = p;
}

scalar vTObject::getVdWPotential() {
	return vdwPotential;
}

void vTObject::setVdWEquilibrium(scalar l) {
	vdwEquilibrium = l;
}

scalar vTObject::getVdWEquilibrium() {
	return vdwEquilibrium;
}

void vTObject::setReactionRate(scalar r) {
	reactionRate = r;
}

scalar vTObject::getReactionRate() {
	return reactionRate;
}

int vTObject::setRelativePosition(string val, bool normalise) {

	// Split the vector
	vector<string> sVec;
	boost::split(sVec, val, boost::is_any_of(","));

	// Check the vector
	if(sVec.size() != 2 && sVec.size() != 3) {
		fileLog->printError("vTObject::relativePosition", "String must contain 2 or 3 components.");
		return BioNetError;		
	}

	// Convert
	try {
		if(sVec.size() == 2) {
			setRelativePosition(ProcessMaths(sVec.at(0)) / InternalUnits::Angle, ProcessMaths(sVec.at(1)));
		} else {
			setRelativePosition(stos(sVec.at(0)) / InternalUnits::Length, stos(sVec.at(1)) / InternalUnits::Length, stos(sVec.at(2)) / InternalUnits::Length);
		}
	} catch (exception &e) {
		fileLog->printError("vTObject::relativePosition", "String must contain numerical values.");
		return BioNetError;
	}

	// Normalisation
	if(normalise) {
		relPos.normalize();
	}
	return BioNetSuccess;	
}


void vTObject::setRelativePosition(scalar theta, scalar phi) {
	
	relPos = MathFunctions::cartesian(theta, phi);
}

void vTObject::setRelativePosition(scalar x, scalar y, scalar z) {

	relPos[0] = x;
	relPos[1] = y;
	relPos[2] = z;
}

Vector3 vTObject::getRelativePosition() {

	return relPos;
}

void vTObject::setMaxNumConnections(int n) {
	maxNumConnections = n;
}

int vTObject::getMaxNumConnections() {
	return maxNumConnections;
}

vTObject * vTObject::getTSite(int index) {
	if(index > numTSites) {
		return NULL;
	}

	return tSite[index];
}

int vTObject::addTSite(vTObject *n) {

	// Add the terminal
	tSite.push_back(n);
	numTSites += 1;

	return BioNetSuccess;
}

int vTObject::removeTSite(int index) {

	// Check for the index
	if(index >= numTSites) {
		return BioNetError;
	}

	// Remove the connection
	tSite.erase(tSite.begin() + index);
	numTSites -= 1;

	return BioNetSuccess;
}

int vTObject::removeTSite(vTObject *n) {

	// Iterate and remove
	vector<vTObject*>::iterator it = tSite.begin();

	while(it != tSite.end()) {
		if((*it) == n) {
			tSite.erase(it);
			numTSites -= 1;

			return BioNetSuccess;
		}
	}

	return BioNetError;
}

void vTObject::removeTSites() {

	// Remove all connections
	tSite.clear();
	numTSites = 0;

}

void vTObject::printDetails() {

}

cTObject::cTObject() : tObject() {

	k = 0.0;
	l = 0.0;
	kLim = 411.0;
	lm = 0.0;
	cutoff = -1;
	bondEnergy = -1.0 * sInf;

	kineticBond = false;
}

cTObject::~cTObject() {

	k = 0.0;
	l = 0.0;
	kLim = 0.0;
	lm = 0.0;
	cutoff = 0.0;
	bondEnergy = 0.0;

	kineticBond = false;
}

int cTObject::setParameter(string name, string value) {

	boost::algorithm::to_lower(name);
	if (name == "k") {
		setElasticConstant(stos(value) / InternalUnits::ElasticConstant);
	} else if (name == "l") {
		setEquilibriumLength(stos(value) / InternalUnits::Length);
	} else if(name == "lm" || name == "llim" || name == "maxlength") {
		setMaximumLength(stos(value) / InternalUnits::Length);
	} else if(name == "klim") {
		setElasticConstantLimit(stos(value) / InternalUnits::ElasticConstant);
	} else if (name == "kappa" || name == "ak") {
		setElasticConstant(stos(value) / InternalUnits::AngularElasticConstant);
	} else if (name == "theta" || name == "theta0") {
		setEquilibriumLength(ProcessMaths(value) / InternalUnits::Angle);
	} else if (name == "cutoff") {
		setCutoff(stos(value) / InternalUnits::Length);
	} else if (name == "e" || name == "bondenergy" || name == "bonddissociationenergy") {

		// Must be negative
		if(stos(value) > 0.0) {
			setBondEnergy(-1 * stos(value) / InternalUnits::Energy);
		} else {
			setBondEnergy(stos(value) / InternalUnits::Energy);
		}
	} else if (name == "kinetics" || name == "kineticbond"|| name == "kinetic") {
		setKinetic(toBool(value));
	} else {
		return tObject::setParameter(name, value);
	}

	return BioNetSuccess;
}

string cTObject::getParameter(string name) {

	ostringstream sStream;
	if (name == "type") {
		return getTypeString();
	} else if (name == "k") {
		sStream << getElasticConstant();
		return sStream.str();
	} else if (name == "l") {
		sStream << getEquilibriumLength();
		return sStream.str();
	} else if (name == "klim") {
		sStream << getElasticConstantLimit();
		return sStream.str();
	} else if (name == "lm") {
		sStream << getMaximumLength();
		return sStream.str();

	} else if (name == "E") {
		sStream << getBondEnergy();
		return sStream.str();
	} else {
		return tObject::getParameter(name);
	}
}

void cTObject::setElasticConstant(scalar k) {
	this->k = k;
}

scalar cTObject::getElasticConstant() {
	return k;
}

void cTObject::setEquilibriumLength(scalar l) {
	this->l = l;
}

scalar cTObject::getEquilibriumLength() {
	return l;
}

void cTObject::setElasticConstantLimit(scalar k) {
	kLim = k;
}

scalar cTObject::getElasticConstantLimit() {
	return kLim;
}

void cTObject::setMaximumLength(scalar l) {
	lm = l;
}

scalar cTObject::getMaximumLength() {
	return lm;
}


void cTObject::setCutoff(scalar c) {
	cutoff = c;
}

scalar cTObject::getCutoff() {
	return cutoff;
}

void cTObject::setBondEnergy(scalar E) {
	bondEnergy = E;
}

scalar cTObject::getBondEnergy() {
	return bondEnergy;
}

void cTObject::setKinetic(bool b) {
	kineticBond = b;
}

bool cTObject::isKinetic() {
	return kineticBond;
}

void cTObject::printDetails() {

}

Protein::Protein() : vTObject() {

	type = NoProt;
}

Protein::~Protein() {
	type = NoProt;
}

int Protein::setParameter(string name, string value) {

	boost::algorithm::to_lower(name);
	if (name == "type") {
		return setType(value);
	} else {
		return vTObject::setParameter(name, value);
	}
	
	return BioNetSuccess;
}

string Protein::getParameter(string name) {

	boost::algorithm::to_lower(name);
	ostringstream sStream;
	if (name == "type") {
		return getTypeString();
	} else {
		return vTObject::getParameter(name);
	}
}

int Protein::setType(string s) {

	setType(getProteinType(s));
	return BioNetSuccess;
}

void Protein::setType(ProteinType t) {
	type = t;
}

ProteinType Protein::getType() {
	return type;
}

string Protein::getTypeString() {
	return getProteinTypeString(type);
}


int Protein::buildSites(map<int, map<string, string>> siteMap, NetworkParameters* params) {
	
	// Check for zero sized map (no sites)
	if(siteMap.size() == 0) {
		return BioNetSuccess;
	}

	int cIndex, numWarnings = 0;
	map<string, string> aSite;
	map<int, map<string,string>>::iterator it;
	map<string,string>::iterator itIn;
	string lValue, rValue;

	// Assign the memory
	numTSites = 0;

	vTObject *aTSite;

	for(it = siteMap.begin(); it != siteMap.end(); ++it) {
		
		// Get an iSite structure
		cIndex = it->first;
		aSite = it->second;

		//
		// Build polymorphic object
		//

		aTSite = new InteractionSite();

		// Set the index
		if(aTSite->setParameter("tIndex", to_string(cIndex)) == BioNetWarning) {
			numWarnings++;
		}

		// Loop over the attributes
		for(itIn = aSite.begin(); itIn != aSite.end(); ++itIn) {

			lValue = itIn->first;
			rValue = itIn->second;
			try {
				if(aTSite->setParameter(lValue, rValue) == BioNetWarning) {
					numWarnings++;
				}
			} catch (invalid_argument& e) {
				fileLog->printError(lValue, "Unable to parse parameter '" + rValue + "'");
				return BioNetError;
			}
		}

		// Add to list
		addTSite(aTSite);
	}

	// Are we ok with parameters?
	if(numWarnings != 0) {
		fileLog->printWarning("For your sites, you attempted to specify " + to_string(numWarnings) + " unrecognised parameters.\n\t\tWe will attempt to continue.");
		return BioNetWarning;
	}
	return BioNetSuccess;
}


bool Protein::validate(NetworkParameters *params) {

	if(type == NoProt) {
		fileLog->printError("Protein::type", "Protein must have a valid type specified.");
		return BioNetInvalid;
	}

	if(isInertialSimulation(params->simType)) {
		if(density <= 0) {
			fileLog->printError("Protein::density", "Must be > 0");
			return BioNetInvalid;
		}
	}

	if(isDynamicSimulation(params->simType) || requiresInitialisation(params->initType)) {
		if(viscosity <= 0) {
			fileLog->printError("Protein::viscosity", "Must be > 0");
			return BioNetInvalid;
		}
	}

	// Success!
	return BioNetValid;
	
}

void Protein::printDetails() {
	cout << endl << endl;
	cout << "Class: Protein" << endl;
	cout << "\tName = " << getName() << ", Type = " << getTypeString() << endl;
	cout << "\tIndex = " << getIndex() << endl;
	cout << "\tMax Connections = " << getMaxNumConnections() << endl;
	cout << "\tDensity = " << getDensity() << ", Viscosity = " << getViscosity() << endl;
	cout << "\tMass = " << getMass() << ", Drag = " << getDrag() << endl;
	cout << "\tRadius = " << getRadius() << endl;
	if(numTSites != 0) {
		cout << "Site Objects:" << endl;
		for(int i = 0; i < numTSites; ++i) {
			tSite[i]->printDetails();
		}
	}
}

Bond::Bond() : cTObject() {

	type = NoBond;
}

Bond::~Bond() {

	type = NoBond;
}

int Bond::setParameter(string name, string value) {

	// Bond energy has a negative conversion. User submits a positive value

	boost::algorithm::to_lower(name);
	if (name == "type") {
		return setType(value);
	} else {
		return cTObject::setParameter(name, value);
	}

	return BioNetSuccess;
}

string Bond::getParameter(string name) {

	ostringstream sStream;
	if (name == "type") {
		return getTypeString();
	} else {
		return cTObject::getParameter(name);
	}
}

int Bond::setType(string s) {

	setType(getBondType(s));
	return BioNetSuccess;
}

void Bond::setType(BondType t) {
	type = t;
}

BondType Bond::getType() {
	return type;
}

string Bond::getTypeString() {
	return ::getBondTypeString(type);
}

bool Bond::validate(NetworkParameters *params) {

	if(type == NoBond) {
		fileLog->printError("Bond::type", "Bond must have a valid type specified.");
		return BioNetInvalid;
	} else if (type == BRigid) {
		fileLog->printError("Bond::type", "Rigid not yet supported :(");
		return BioNetInvalid;	
	} 

	if(k < 0) {
		fileLog->printError("Bond::k (Elastic Constant)", "Must be >= 0");
		return BioNetInvalid;
	}

	if(l < 0) {
		fileLog->printError("Bond::l (Equilibrium Length)", "Must be >= 0");
		return BioNetInvalid;
	}

	if(bondEnergy >= 0) {
		fileLog->printError("Bond::E (Bond Dissociation Energy)", "Set > 0 (we will make negative internally)");
		return BioNetInvalid;		
	}

	return BioNetValid;
}

void Bond::printDetails() {
	cout << endl << endl;
	cout << "Class: Bond" << endl;
	cout << "\tName = " << getName() << ", Type = " << getTypeString() << endl;
	cout << "\tIndex = " << getIndex() << endl;
	cout << "\tk = " << getElasticConstant() << ", l = " << getEquilibriumLength() << endl;
}

InteractionSite::InteractionSite() : vTObject() {

	type = NoISite;
}

InteractionSite::~InteractionSite() {

	type = NoISite;
}

int InteractionSite::setParameter(string name, string value) {

	boost::algorithm::to_lower(name);

	if (name == "type") {
		return setType(value);
	} else {
		return vTObject::setParameter(name, value);
	}

	return BioNetSuccess;
}

string InteractionSite::getParameter(string name) {

	ostringstream sStream;
	if (name == "type") {
		return getTypeString();
	} else {
		return vTObject::getParameter(name);
	}
}

int InteractionSite::setType(string s) {

	setType(getISiteType(s));
	return BioNetSuccess;
}

void InteractionSite::setType(ISiteType t) {
	type = t;
}

ISiteType InteractionSite::getType() {
	return type;
}

string InteractionSite::getTypeString() {
	return ::getISiteTypeString(type);
}

int InteractionSite::buildSites(map<int, map<string, string>> siteMap, NetworkParameters* params) {
	return BioNetSuccess;
}

bool InteractionSite::validate(NetworkParameters *params) {

	if(type == NoISite) {
		fileLog->printError("InteractionSite::type", "Protein must have a valid type specified.");
		return BioNetInvalid;
	}

	if(isDynamicSimulation(params->simType) && params->electrostaticForcesActive) {
		if(chargeDensity <= 0) {
			fileLog->printError("InteractionSite::chargeDensity", "Must be > 0");
			return BioNetInvalid;
		}
	}

	if(isDynamicSimulation(params->simType) && params->vdwForcesActive) {
		if(vdwPotential < 0) {
			fileLog->printError("InteractionSite::vdwPotential", "Must be >= 0");
			return BioNetInvalid;
		}

		if(vdwEquilibrium <= 0) {
			fileLog->printError("InteractionSite::vdwEquilibrium", "Must be > 0");
			return BioNetInvalid;
		}
	}
	return BioNetValid;
}

void InteractionSite::printDetails() {
	cout << endl << endl;
	cout << "Class: InteractionSite" << endl;
	cout << "Parameters: " << endl;
	cout << "\tIndices: Template Index = " << tIndex << endl;
	cout << "\tName = " << getName() << ", Type = " << getTypeString() << endl;
	cout << "\tRelative Position = (" << getRelativePosition()[0] << ", " << getRelativePosition()[1] << ", " << getRelativePosition()[2] << ")" << endl;
	cout << "\tCharge Density = " << getChargeDensity() << endl;
	cout << "\tVdW Potential = " << getVdWPotential() << ", VdW Equilibrium = " << getVdWEquilibrium() << endl;
}

nObject::nObject() {
	index = -1;
	structureDimension = 0;
}

nObject::~nObject() {
	index = 0;
	structureDimension = 0;
}

int nObject::setParameter(string name, string value) {

	boost::algorithm::to_lower(name);
	if(name == "index") {
		setIndex(stoi(value));
	} else {
		return BioNetWarning;
	}
	return BioNetSuccess;
}

string nObject::getParameter(string name) {
	if(name == "index") {
		return to_string(index);
	} else {
		return "";
	}
}

void nObject::setIndex(int i) {
	index = i;
}

int nObject::getIndex() {
	return index;
}

int nObject::getStructureDimension() {
	return structureDimension;
}

vObject::vObject() : nObject() {

	numConnections = 0;
	maxNumConnections = 14;

	connection.clear();
	saturated = false;

	pos.setZero();
	relPos.setZero();
	vel.setZero();
	acc.setZero();

	pos0.setZero();
	relPos0.setZero();
	vel0.setZero();
	acc0.setZero();

	// Set initial orientations to zero
	orientation.setZero();
	orientation0.setZero();
	orientationMatrix.setIdentity();

	aVel.w() = 0.0;
	aVel.vec().setZero();
	aAcc.w() = 0.0;
	aAcc.vec().setZero();

	aVel0.w() = 0.0;
	aVel0.vec().setZero();
	aAcc0.w() = 0.0;
	aAcc0.vec().setZero();

	mass = 0.0;
	drag = 0.0;
	noiseMagnitude = 0.0;

	aDrag = 0.0;
	aNoiseMagnitude = 0.0;

	momentOfInertia = 0.0;

	reactionRate = sInf;
	reactionProbability = sInf;
	reactive = true;

	frozen = false;
	frozenXYZ[0] = false;
	frozenXYZ[1] = false;
	frozenXYZ[2] = false;

	viscousForce.setZero();
	thermalForce.setZero();
	externalForce.setZero();
	totalForce.setZero();

	viscousTorque.setZero();
	thermalTorque.setZero();
	externalTorque.setZero();
	totalTorque.setZero();

	kineticEnergy = 0.0;
	totalEnergy = 0.0;

	wrapped.setZero();

	vTO = NULL;

	parent = NULL;
	child = NULL;
	numChildren = 0;
}

vObject::~vObject() {

	numConnections = 0;
	maxNumConnections = 0;

	connection.clear();
	saturated = false;

	pos.setZero();
	relPos.setZero();
	vel.setZero();
	acc.setZero();

	pos0.setZero();
	relPos0.setZero();
	vel0.setZero();
	acc0.setZero();

	orientation.setZero();
	orientation0.setZero();
	orientationMatrix.setZero();

	aVel.w() = 0.0;
	aVel.vec().setZero();
	aAcc.w() = 0.0;
	aAcc.vec().setZero();

	aVel0.w() = 0.0;
	aVel0.vec().setZero();
	aAcc0.w() = 0.0;
	aAcc0.vec().setZero();

	mass = 0.0;
	drag = 0.0;
	noiseMagnitude = 0.0;

	aDrag = 0.0;
	aNoiseMagnitude = 0.0;

	momentOfInertia = 0.0;

	reactionRate = 0.0;
	reactionProbability = 0.0;
	reactive = false;

	frozen = false;
	frozenXYZ[0] = false;
	frozenXYZ[1] = false;
	frozenXYZ[2] = false;

	viscousForce.setZero();
	thermalForce.setZero();
	externalForce.setZero();
	totalForce.setZero();

	viscousTorque.setZero();
	thermalTorque.setZero();
	externalTorque.setZero();
	totalTorque.setZero();

	kineticEnergy = 0.0;
	totalEnergy = 0.0;

	wrapped.setZero();

	vTO = NULL;

	parent = NULL;
	for(int i = 0; i < numChildren; ++i) {

		delete child[i];
		child[i] = NULL;
	}
	delete[] child;
	child = NULL;
	numChildren = 0;
}

void vObject::printDetails() {

}

int vObject::setParameter(string name, string value) {

	boost::algorithm::to_lower(name);
	if(name == "position" || name == "pos") {
		setPosition(stringToVector3(value) / InternalUnits::Length);
		setInitialPosition(getPosition());
	} else if (name == "velocity" || name == "vel") {
		setVelocity(stringToVector3(value) / InternalUnits::Velocity);
		setInitialVelocity(getVelocity());
	} else if (name == "acceleration" || name == "acc") {
		setAcceleration(stringToVector3(value) / InternalUnits::Acceleration);
		setInitialAcceleration(getAcceleration());
	} else if (name == "force") {
		setTotalForce(stringToVector3(value) / InternalUnits::Force);
	} else if(name == "orientation") {

		// If we have 9 bits here, set it as an orientation matrix. Else, set as euler angles
		try {
			setOrientationMatrix(stringToMatrix3(value));
//		cout << getOrientationMatrix() << endl;
//		exit(0);
		} catch (exception &e) {
//			setOrientation(stringToVector3(value) / InternalUnits::Angle);
//			setInitialOrientation(getOrientation());
			setOrientationMatrix(MathFunctions::eulerToZXZ(stringToVector3(value) / InternalUnits::Angle));

		}

	} else if(name == "orientationmatrix") {
		setOrientationMatrix(stringToMatrix3(value));
//		Vector3 ori = MathFunctions::ZXZToEuler(stringToMatrix3(value));
//		setOrientation(ori);
//		setInitialOrientation(getOrientation());
	} else if (name == "maxkineticbonds" || name == "maxbonds" || name == "maxnumkineticconnections" || name == "maxnumconnections" || name == "maxconnections") {
		setMaxNumConnections(stoi(value));
	} else if (name == "static" || name == "frozen") {
		setFrozen(toBool(value));

	} else if(name == "mass") {
		setMass(stos(value));
	} else if (name == "drag") {
		setDrag(stos(value));
	} else if (name == "rdrag" || name == "rotationaldrag") {
		setRotationalDrag(stos(value));
	} else if(name == "momentofinertia") {
		setMomentOfInertia(stos(value));
	} else if(name == "wrap" || name == "wrapped") {
		setWrap(stringToVector3(value));
	} else if (name == "template" || name == "templateindex" || name == "protein") {

		// Must be set explicitly
		return BioNetSuccess;
	} else {
		return nObject::setParameter(name, value);
	}
	return BioNetSuccess;
}

string vObject::getParameter(string name) {

	boost::algorithm::to_lower(name);
	if(name == "protein" && vTO != NULL) {
		return vTO->getParameter("name");
	} else if(name == "position") {
		return Vector3ToString(pos);
	} else if (name == "velocity") {
		return Vector3ToString(vel);
	} else if (name == "acceleration") {
		return Vector3ToString(acc);
	} else if (name == "maxNumConnections") {
		return to_string(getMaxNumConnections());
	} else if(name == "mass") {
		return to_string(mass);
	} else if (name == "drag") {
		return to_string(drag);
	} else {
		return nObject::getParameter(name);
	}
}

int vObject::getTemplateIndex() {
	if(vTO != NULL) {
		return vTO->getIndex();
	} else {
		return -1;
	}
}

vTObject * vObject::getTemplate() {
	return vTO;
}

int vObject::getParentIndex(int i) {

	if(i == 0 || parent == this) {
		return getIndex();
	} else {
		if(parent != NULL) {
			return parent->getParentIndex(i - 1);
		} else {
			return -1;
		}
	}
}

int vObject::getParentIndex() {
	return getParentIndex(1);
}

int vObject::getTopParentIndex() {
	if(parent != NULL && parent != this) {
		return parent->getTopParentIndex();
	} else {
		return getIndex();
	}
}

void vObject::setParent(vObject *p) {
	parent = p;
}

vObject * vObject::getParent() {
	return parent;
}

vObject * vObject::getParent(int i) {
	if(i == 0 || parent == this) {
		return this;
	} else {
		if(parent != NULL) {
			return parent->getParent(i - 1);
		} else {
			return NULL;
		}
	}
}

vObject * vObject::getTopParent() {
	if(parent != NULL && parent != this) {
		return parent->getTopParent();
	} else {
		return this;
	}
}

void vObject::setPosition(scalar x, scalar y, scalar z) {
	
	pos(0) = x;
	pos(1) = y;
	pos(2) = z;

}

void vObject::setPosition(Vector3 v) {

	pos = v;
}

void vObject::setInitialPosition(Vector3 v) {

	pos0 = v;
}

Vector3 vObject::getPosition() {
	
	return pos;
}

void vObject::setRelativePosition(scalar x, scalar y, scalar z) {
	
	relPos(0) = x;
	relPos(1) = y;
	relPos(2) = z;

}

void vObject::setRelativePosition(Vector3 v) {

	relPos = v;
}

void vObject::setInitialRelativePosition(Vector3 v) {

	relPos0 = v;
}

Vector3 vObject::getRelativePosition() {
	
	return relPos;
}

Vector3 vObject::getInitialRelativePosition() {

	return relPos0;
}

void vObject::setVelocity(scalar x, scalar y, scalar z) {
	
	vel(0) = x;
	vel(1) = y;
	vel(2) = z;

}

void vObject::setVelocity(Vector3 v) {
	
	vel = v;
}

void vObject::setInitialVelocity(Vector3 v) {
	
	vel0 = v;
}

Vector3 vObject::getVelocity() {
	
	return vel;
}

void vObject::setAcceleration(scalar x, scalar y, scalar z) {
	
	acc(0) = x;
	acc(1) = y;
	acc(2) = z;

}

void vObject::setAcceleration(Vector3 v) {
	
	acc = v;
}

void vObject::setInitialAcceleration(Vector3 v) {
	
	acc0 = v;
}

Vector3 vObject::getAcceleration() {
	
	return acc;
}

void vObject::setOrientation(scalar alpha, scalar beta, scalar gamma) {

	orientation[0] = alpha;
	orientation[1] = beta;
	orientation[2] = gamma;

	orientationMatrix = MathFunctions::eulerToZXZ(orientation);
}

void vObject::setOrientation(Vector3 v) {
	orientation = v;		
}

void vObject::setInitialOrientation(Vector3 o) {
	
	orientation0 = o;
}

Vector3 vObject::getOrientation() {
	
	return orientation;
}

Matrix3 vObject::getOrientationMatrix() {
	return orientationMatrix;
}

void vObject::setOrientationMatrix(Matrix3 m) {
	orientationMatrix = m;
}

void vObject::applyOrientationMatrix(Matrix3 m) {
}

Quat vObject::getOrientationQuaternion() {
	return orientationQuaternion;
}

int vObject::setAngularVelocity(string val) {

	// Split the vector
	vector<string> sVec;
	boost::split(sVec, val, boost::is_any_of(","));

	// Check the vector
	if(sVec.size() != 2 && sVec.size() != 3) {
		fileLog->printError("vObject::aVel", "String must contain 2 or 3 components.");
		return BioNetError;		
	}

	// Convert
	try {
		if(sVec.size() == 2) {
//			setAngularVelocity(stos(sVec.at(0)) / InternalUnits::AngularVelocity, stos(sVec.at(1)) / InternalUnits::AngularVelocity);
		} else {
			setAngularVelocity(stos(sVec.at(0)) / InternalUnits::Velocity, stos(sVec.at(1)) / InternalUnits::Velocity, stos(sVec.at(2)) / InternalUnits::Velocity);
		}
	} catch (exception &e) {
		fileLog->printError("vObject::aVel", "String must contain numerical values.");
		return BioNetError;
	}

	// Initial orientation
	aVel0 = getAngularVelocity();
	
	return BioNetSuccess;	
}

/*
void vObject::setAngularVelocity(scalar tDot, scalar pDot) {
	
	aVel.vec() = MathFunctions::cartesian(tDot, pDot);
}

void vObject::setAngularVelocity(Vector2 o) {
	
	setAngularVelocity(o(0), o(1));
}
*/

void vObject::setAngularVelocity(scalar x, scalar y, scalar z) {

	aVel.x() = x;
	aVel.y() = y;
	aVel.z() = z;
}

void vObject::setAngularVelocity(Vector3 o) {
	
	setAngularVelocity(o(0), o(1), o(2));
}

void vObject::setAngularVelocity(Quat q) {
	aVel = q;
}

Quat vObject::getAngularVelocity() {
	return aVel;
}

int vObject::setAngularAcceleration(string val) {

	// Split the vector
	vector<string> sVec;
	boost::split(sVec, val, boost::is_any_of(","));

	// Check the vector
	if(sVec.size() != 2 && sVec.size() != 3) {
		fileLog->printError("vObject::aAcc", "String must contain 2 or 3 components.");
		return BioNetError;		
	}

	// Convert
	try {
		if(sVec.size() == 2) {
//			setAngularAcceleration(stos(sVec.at(0)) / InternalUnits::AngularAcceleration, stos(sVec.at(1)) / InternalUnits::AngularAcceleration);
		} else {
			setAngularAcceleration(stos(sVec.at(0)) / InternalUnits::Acceleration, stos(sVec.at(1)) / InternalUnits::Acceleration, stos(sVec.at(2)) / InternalUnits::Acceleration);
		}
	} catch (exception &e) {
		fileLog->printError("vObject::aAcc", "String must contain numerical values.");
		return BioNetError;
	}

	// Initial orientation
	aAcc0 = getAngularAcceleration();
	
	return BioNetSuccess;	
}
/*
void vObject::setAngularAcceleration(scalar tDDot, scalar pDDot) {
	
	aVel.vec() = MathFunctions::cartesian(tDDot, pDDot);
}

void vObject::setAngularAcceleration(Vector2 o) {
	
	setAngularAcceleration(o(0), o(1));
}
*/
void vObject::setAngularAcceleration(scalar x, scalar y, scalar z) {

	aAcc.x() = x;
	aAcc.y() = y;
	aAcc.z() = z;
}

void vObject::setAngularAcceleration(Vector3 o) {
	
	setAngularAcceleration(o(0), o(1), o(2));
}

void vObject::setAngularAcceleration(Quat q) {
	aAcc = q;
}

Quat vObject::getAngularAcceleration() {
	return aAcc;
}
	
void vObject::setViscousForce(scalar x, scalar y, scalar z) {
	
	// Update total
	totalForce -= viscousForce;

	// Set viscous
	viscousForce(0) = x;
	viscousForce(1) = y;
	viscousForce(2) = z;

	// Update total
	totalForce += viscousForce;

}

void vObject::setViscousForce(Vector3 v) {

	// Update total
	totalForce -= viscousForce;

	// Set viscous
	viscousForce = v;

	// Update total
	totalForce += viscousForce;
}

Vector3 vObject::getViscousForce() {
	return viscousForce;
}

void vObject::zeroViscousForce() {

	// Sort total force too
	totalForce -= viscousForce;
	viscousForce.setZero();
}

void vObject::addViscousForce(Vector3 v) {
	viscousForce += v;
	totalForce += v;
}

void vObject::setThermalForce(scalar x, scalar y, scalar z) {
	
	// Update total
	totalForce -= thermalForce;

	// Set thermal
	thermalForce(0) = x;
	thermalForce(1) = y;
	thermalForce(2) = z;

	// Update total
	totalForce += thermalForce;

}

void vObject::setThermalForce(Vector3 v) {
	
	// Update total
	totalForce -= thermalForce;

	// Set thermal
	thermalForce = v;

	// Update total
	totalForce += thermalForce;
}

Vector3 vObject::getThermalForce() {
	return thermalForce;
}

void vObject::zeroThermalForce() {
	
	// Sort total force too
	totalForce -= thermalForce;
	thermalForce.setZero();
}

void vObject::addThermalForce(Vector3 v) {
	thermalForce += v;
	totalForce += v;
}

void vObject::setExternalForce(scalar x, scalar y, scalar z) {
	
	// Update total
	totalForce -= externalForce;

	// Set external
	externalForce(0) = x;
	externalForce(1) = y;
	externalForce(2) = z;

	// Update total
	totalForce += externalForce;

}

void vObject::setExternalForce(Vector3 v) {
	
	// Update total
	totalForce -= externalForce;

	// Set external
	externalForce = v;

	// Update total
	totalForce += externalForce;
}

Vector3 vObject::getExternalForce() {
	return externalForce;
}

void vObject::zeroExternalForce() {
	
	// Sort total force too
	totalForce -= externalForce;
	externalForce.setZero();
}

void vObject::addExternalForce(scalar s, int direction) {
	externalForce[direction] += s;
	totalForce[direction] += s;
}

void vObject::addExternalForce(Vector3 v) {
	externalForce += v;
	totalForce += v;
}

void vObject::setTotalForce(scalar x, scalar y, scalar z) {
	
	// So that forces always sum to total, make forces except external equal zero (completely arbitrary choice)
	zeroViscousForce();
	zeroThermalForce();

	externalForce(0) = x;
	externalForce(1) = y;
	externalForce(2) = z;

	totalForce = externalForce;

}

void vObject::setTotalForce(Vector3 v) {

	// So that forces always sum to total, make forces except thermal equal zero (completely arbitrary choice)
	zeroViscousForce();
	zeroThermalForce();

	externalForce = v;

	totalForce = externalForce;
}

Vector3 vObject::getTotalForce() {
	return totalForce;
}

void vObject::zeroTotalForce() {

	// Sort all forces as well as total
	viscousForce.setZero();
	thermalForce.setZero();
	externalForce.setZero();

	totalForce.setZero();
}

void vObject::zeroTotalForce(int direction) {

	// Sort all forces as well as total
	viscousForce[direction] = 0.0;
	thermalForce[direction] = 0.0;
	externalForce[direction] = 0.0;

	totalForce[direction] = 0.0;
}

void vObject::addTotalForce(Vector3 v) {

	// To make all forces add up, assume this is an external contribution (completely arbitrary choice)
	externalForce += v;
	totalForce += v;
}

void vObject::setViscousTorque(scalar x, scalar y, scalar z) {
	
	// Update total
	totalTorque -= viscousTorque;

	// Set viscous
	viscousTorque(0) = x;
	viscousTorque(1) = y;
	viscousTorque(2) = z;

	// Update total
	totalTorque += viscousTorque;

}

void vObject::setViscousTorque(Vector3 v) {

	// Update total
	totalTorque -= viscousTorque;

	// Set viscous
	viscousTorque = v;

	// Update total
	totalTorque += viscousTorque;
}

Vector3 vObject::getViscousTorque() {
	return viscousTorque;
}

void vObject::zeroViscousTorque() {

	// Sort total Torque too
	totalTorque -= viscousTorque;
	viscousTorque.setZero();
}

void vObject::addViscousTorque(Vector3 v) {
	viscousTorque += v;
	totalTorque += v;
}

void vObject::setThermalTorque(scalar x, scalar y, scalar z) {
	
	// Update total
	totalTorque -= thermalTorque;

	// Set thermal
	thermalTorque(0) = x;
	thermalTorque(1) = y;
	thermalTorque(2) = z;

	// Update total
	totalTorque += thermalTorque;

}

void vObject::setThermalTorque(Vector3 v) {
	
	// Update total
	totalTorque -= thermalTorque;

	// Set thermal
	thermalTorque = v;

	// Update total
	totalTorque += thermalTorque;
}

Vector3 vObject::getThermalTorque() {
	return thermalTorque;
}

void vObject::zeroThermalTorque() {
	
	// Sort total Torque too
	totalTorque -= thermalTorque;
	thermalTorque.setZero();
}

void vObject::addThermalTorque(Vector3 v) {
	thermalTorque += v;
	totalTorque += v;
}

void vObject::setExternalTorque(scalar x, scalar y, scalar z) {
	
	// Update total
	totalTorque -= externalTorque;

	// Set external
	externalTorque(0) = x;
	externalTorque(1) = y;
	externalTorque(2) = z;

	// Update total
	totalTorque += externalTorque;

}

void vObject::setExternalTorque(Vector3 v) {
	
	// Update total
	totalTorque -= externalTorque;

	// Set external
	externalTorque = v;

	// Update total
	totalTorque += externalTorque;
}

Vector3 vObject::getExternalTorque() {
	return externalTorque;
}

void vObject::zeroExternalTorque() {
	
	// Sort total Torque too
	totalTorque -= externalTorque;
	externalTorque.setZero();
}

void vObject::addExternalTorque(Vector3 v) {
	externalTorque += v;
	totalTorque += v;
}

void vObject::setTotalTorque(scalar x, scalar y, scalar z) {
	
	// So that Torques always sum to total, make Torques except external equal zero (completely arbitrary choice)
	zeroViscousTorque();
	zeroThermalTorque();

	externalTorque(0) = x;
	externalTorque(1) = y;
	externalTorque(2) = z;

	totalTorque = externalTorque;

}

void vObject::setTotalTorque(Vector3 v) {

	// So that Torques always sum to total, make Torques except thermal equal zero (completely arbitrary choice)
	zeroViscousTorque();
	zeroThermalTorque();

	externalTorque = v;

	totalTorque = externalTorque;
}

Vector3 vObject::getTotalTorque() {
	return totalTorque;
}

void vObject::zeroTotalTorque() {

	// Sort all Torques as well as total
	viscousTorque.setZero();
	thermalTorque.setZero();

//	cout << "External: " << getExternalTorque()[0] << ", " << getExternalTorque()[1] << ", " << getExternalTorque()[2] << endl;
//	cout << "External Mag: " << getExternalTorque().norm() << endl; 
	externalTorque.setZero();
	totalTorque.setZero();
}

void vObject::addTotalTorque(Vector3 v) {

	// To make all Torques add up, assume this is an external contribution (completely arbitrary choice)
	externalTorque += v;
	totalTorque += v;
}

void vObject::setKineticEnergy(scalar E) {

	// Update total
	totalEnergy -= kineticEnergy;

	// Set kinetic
	kineticEnergy = E;

	// Update total
	totalEnergy += kineticEnergy;	
}

scalar vObject::getKineticEnergy() {
	return kineticEnergy;
}

void vObject::zeroKineticEnergy() {

	// Sort total energy too
	totalEnergy -= kineticEnergy;
	kineticEnergy = 0;
}

void vObject::addKineticEnergy(scalar E) {

	kineticEnergy += E;
	totalEnergy += E;
}

void vObject::setTotalEnergy(scalar E) {

	// To make everything add up properly, assume this is all kinetic energy
	kineticEnergy = E;
	totalEnergy = kineticEnergy;
}

scalar vObject::getTotalEnergy() {
	return totalEnergy;
}

void vObject::zeroTotalEnergy() {

	// Sort total energy too
	kineticEnergy = 0;
	totalEnergy = 0;
}

void vObject::addTotalEnergy(scalar E) {

	// To make everything add up properly, assume this is all kinetic energy
	kineticEnergy += E;
	totalEnergy += E;
}

void vObject::setMass(scalar m) {
	mass = m;
}

scalar vObject::getMass() {
	return mass;
}

void vObject::setDrag(scalar d) {
	drag = d;
}

scalar vObject::getDrag() {
	return drag;
}

void vObject::setRotationalDrag(scalar d) {
	aDrag = d;
}

scalar vObject::getRotationalDrag() {
	return aDrag;
}

void vObject::setMomentOfInertia(scalar I) {
	momentOfInertia = I;
}

scalar vObject::getMomentOfInertia() {
	return momentOfInertia;
}

void vObject::setReactionRate(scalar r) {
	reactionRate = r;
}

scalar vObject::getReactionRate() {
	return reactionRate;
}

void vObject::calculateReactionProbability(scalar dt) {
	reactionProbability = dt * reactionRate;
	if(reactionProbability > 1.0) {
		reactionProbability = 1.0;
		reactionRate = reactionProbability / dt;
	}
}

scalar vObject::getReactionProbability() {
	return reactionProbability;
}

void vObject::setReactive(bool r) {
	reactive = r;
}

bool vObject::isReactive() {
	return reactive;
}

void vObject::setMaxNumConnections(int n) {
	maxNumConnections = n;
}

int vObject::getMaxNumConnections() {
	return maxNumConnections;
}

void vObject::setWrap(Vector3 v) {
	wrapped = v;
}

void vObject::setWrap(int direction, int w) {
	wrapped[direction] = w;
}

void vObject::addWrap(int direction, int w) {
	wrapped[direction] += w;
}

Vector3 vObject::getWrap() {
	return wrapped;
}

scalar vObject::getWrap(int direction) {
	return wrapped[direction];
}

void vObject::setFrozen(bool f) {
	frozen = f;
	frozenXYZ[0] = f;
	frozenXYZ[0] = f;
	frozenXYZ[0] = f;
}

void vObject::setFrozen(bool f, int direction) {
	
	frozenXYZ[direction] = f;

	if(frozenXYZ[direction] and frozenXYZ[direction + 1 % 3] and frozenXYZ[direction + 2 % 3]) {
		frozen = true;
	} else {
		frozen = false;
	}
}

bool vObject::isFrozen() {
	return frozen;
}

bool vObject::isFrozen(int direction) {
	return frozenXYZ[direction];
}

int vObject::getNumConnections() {
	return numConnections;
}

cObject * vObject::getConnection(int index) {

	// Check index
	if(index >= numConnections) {
		return NULL;
	}

	return connection.at(index);
}

int vObject::addConnection(cObject *c) {

	// Saturation is not just kinetic. It's all bonds. So if more bonds are tried to
	// be added here than should be, we have a problem
	
//	bool kin = c->getTemplate()->isKinetic();

	// Check for kinetic saturation
	if(isSaturated()) {
		return BioNetWarning;
	}


	// Add the connection
	connection.push_back(c);
	numConnections += 1;

	// Saturation status
	if(numConnections == getMaxNumConnections()) {
		saturated = true;
	}
	
	return BioNetSuccess;
}

int vObject::removeConnection(int index) {

	// Saturation is for everything, including non-kinetic stuff

	// Check for the index
	if(index >= numConnections) {
		return BioNetError;
	}

	// Remove the connection
//	bool kin = connection[index]->getTemplate()->isKinetic();

	connection.erase(connection.begin() + index);
	numConnections -= 1;

	// Saturation status
	setSaturated(false);

	return BioNetSuccess;
}

int vObject::removeConnection(cObject *c) {

	// Saturation is only for pair pair bonds i.e. non-empirical bonds

	// Iterate and remove
	vector<cObject*>::iterator it = connection.begin();

	while(it != connection.end()) {
		if((*it) == c) {
//			bool kin = c->getTemplate()->isKinetic();

			connection.erase(it);
			numConnections -= 1;

			// Saturation status
			setSaturated(false);

			return BioNetSuccess;
		}
		it++;
	}

	return BioNetError;
}

void vObject::removeConnections() {

	// Remove all connections
	connection.clear();
	numConnections = 0;

	// Saturation status
	saturated = false;
}

void vObject::setSaturated(bool b) {
	saturated = b;
}

bool vObject::isSaturated() {
	return saturated;
}

ProteinType vObject::getProteinType() {

	if(vTO != NULL) {
		return ::getProteinType(vTO->getParameter("type"));
	} else {
		return NoProt;
	}
}

void vObject::translate(Vector3 t) {
	pos = pos + t;
}

void vObject::rotate(Matrix3 m) {
	orientationMatrix = m * orientationMatrix;
}

void vObject::rotate(Quat q) {
	setOrientation(q * getOrientation());
}

/*
void vObject::rotateAboutPoint(Vector3 p, Vector2 r, scalar rad) {
	
	// To do this, move Point p to origin, rotate object, and move back

	// Translate
	translate(-1 * p);

	// Rotate
	Vector3 sPos = MathFunctions::spherical3(getPosition());
	sPos(1) += r(0);
	sPos(2) += r(1);
	if(sPos(1) > M_PI) {
		sPos(1) = 2 * M_PI - sPos(1);
		sPos(2) = fmod(sPos(2) + M_PI, 2 * M_PI);
	}
	setPosition(MathFunctions::cartesian(sPos));

	// Translate back
	translate(p);
	return;
}
*/

void vObject::calculateNoiseMagnitude(scalar kBT, scalar dt) {

	// Here, it is a simple function of the viscosity (and simulation timestep)
	if(dt == 0.0) {
		noiseMagnitude = 0.0;
	} else {
		noiseMagnitude = sqrt(2 * kBT * getDrag() / dt);
	}
}

void vObject::calculateRotationalNoiseMagnitude(scalar kBT, scalar dt) {

	// Here, it is a simple function of the viscosity (and simulation timestep)
	if(dt == 0.0) {
		aNoiseMagnitude = 0.0;
	} else {
		aNoiseMagnitude = sqrt(6 * kBT * getRotationalDrag() / dt);
	}
}

void vObject::calculateViscousForce() {

	setViscousForce(-1 * getDrag() * getVelocity());	
}

void vObject::calculateThermalForce(scalar rn1, scalar rn2, scalar rn3) {

	Vector3 v;
	
	// Gaussian random vector
	v(0) = rn1;
	v(1) = rn2;
	v(2) = rn3;

	setThermalForce(v *  noiseMagnitude);
}

void vObject::calculateTotalForce() {

	// All forces individually
//	calculateViscousForce();
//	calculateThermalForce();
}

void vObject::calculateViscousTorque() {

	setViscousTorque(-1 * getRotationalDrag() * getAngularVelocity().vec());	
}

/*
void vObject::calculateThermalTorque() {

	int i;
	Vector2 v2;
	Vector3 v;
	

	// Gaussian random vector (should this be 3?)
	for(i = 0; i < 3; ++i) {
		v(i) = GlobalVariables::normalDistribution(GlobalVariables::gaussRng);
	}
//	v(2) = 0.0;

//	for(i = 0; i < 2; ++i) {
//		v2(i) = GlobalVariables::normalDistribution(GlobalVariables::gaussRng);
//	}
//	v2(0) = GlobalVariables::uniformDistribution(GlobalVariables::rng) * M_PI;
//	v2(1) = GlobalVariables::uniformDistribution(GlobalVariables::rng) * 2 * M_PI;
//	v = MathFunctions::cartesian(v2);
	v.normalize();

	setThermalTorque(v *  aNoiseMagnitude);
}
*/

void vObject::calculateThermalTorque(scalar rn1, scalar rn2, scalar rn3) {

	Vector3 v;

	// Gaussian random vector
	v(0) = rn1;
	v(1) = rn2;
	v(2) = rn3;
	v.normalize();

	setThermalTorque(v *  aNoiseMagnitude);
}

void vObject::calculateTotalTorque() {

	// All torques individually
//	calculateViscousTorque();
//	calculateThermalTorque();
}

void vObject::calculateKineticEnergy() {
	setKineticEnergy(0.5 * getMass() * pow(getVelocity().norm(), 2));
}

void vObject::calculateTotalEnergy() {
	
	// All energies individually
	calculateKineticEnergy();
}

void vObject::calculateEulerAngles() {
	
	// ZXZ formulation, calculate from the orientation matrix
	orientation[1] = acos(orientationMatrix(8));

	// If zero, this reduces to a single rotation about Z
	if(fabs(orientation[1]) < 0.001) {
		orientation[0] = atan2(orientationMatrix(1), orientationMatrix(0));
		orientation[2] = 0.0;
	} else {
		orientation[0] = atan2(orientationMatrix(2) / sin(orientation[1]) , orientationMatrix(5) / sin(orientation[1]));
		orientation[2] = atan2(-orientationMatrix(6) / sin(orientation[1]), orientationMatrix(7)/ sin(orientation[1]));
	}
}

void vObject::calculateOrientationQuaternion() {
	
	// Formalism irrelevent, the quaternion should be interchangeable with the matrix in general
	// Internet says matrix is faster to apply in bulk, so this is to save space on output
	orientationQuaternion.w() = 0.5 * sqrt(1 + orientationMatrix(0) + orientationMatrix(4) + orientationMatrix(8));

	// Stability
	if(orientationQuaternion.w() < 0.01) {
		orientationQuaternion.x() = 0.5 * sqrt(1 + orientationMatrix(0) - orientationMatrix(4) - orientationMatrix(8));
		orientationQuaternion.y() = 0.25 * (orientationMatrix(1) + orientationMatrix(3)) / orientationQuaternion.x();
		orientationQuaternion.z() = 0.25 * (orientationMatrix(6) + orientationMatrix(2)) / orientationQuaternion.x();
		orientationQuaternion.w() = 0.25 * (orientationMatrix(5) - orientationMatrix(7)) / orientationQuaternion.x();
	} else {
		orientationQuaternion.x() = 0.25 * (orientationMatrix(5) - orientationMatrix(7)) / orientationQuaternion.w();
		orientationQuaternion.y() = 0.25 * (orientationMatrix(6) - orientationMatrix(2)) / orientationQuaternion.w();
		orientationQuaternion.z() = 0.25 * (orientationMatrix(1) - orientationMatrix(3)) / orientationQuaternion.w();
	}
}

void vObject::applyTemplate(vTObject *vTO, NetworkParameters *params) {

	// Set pointer to template
	this->vTO = vTO;

	// Set parameters that have not been set
	if(vTO->getReactionRate() != sInf) {
		setReactionRate(vTO->getReactionRate());
	}
}

cObject::cObject() : nObject() {

	numAttachedTerminals = 0;
	numTerminals = 0;
	terminal.reserve(numTerminals);
	saturated = false;
	offset.setZero();

	k = 0;
	l = 0;
	
	elasticEnergy = 0.0;
	totalEnergy = 0.0;
	bondEnergy = -1 * sInf;

	elasticForce.setZero();
	totalForce.setZero();

	cTO = NULL;
}

cObject::~cObject() {

	numAttachedTerminals = 0;
	numTerminals = 0;
	terminal.clear();
	saturated = false;
	offset.setZero();

	k = 0;
	l = 0;

	elasticEnergy = 0.0;
	totalEnergy = 0.0;
	bondEnergy = 0.0;

	elasticForce.setZero();
	totalForce.setZero();

	cTO = NULL;
}

void cObject::printDetails() {

}

int cObject::setParameter(string name, string value) {

	boost::algorithm::to_lower(name);

	if (name == "k" || name == "elasticconstant") {
		setElasticConstant(stos(value) / InternalUnits::ElasticConstant);
	} else if (name == "l" || name == "length" || name == "equilibriumlength") {
		setEquilibriumLength(stos(value) / InternalUnits::Length);
	} else if (name == "offset") {
		setOffset(stringToVector3(value));
	} else if (name == "nodes" || name == "sites" || name == "template" || name == "templateindex" || name == "bond") {

		// Must be set explicitly
		return BioNetSuccess;

//	} else if (name == "nodes") {
//		setParentNodeIndices(stringToVectorI(value));
//	} else if (name == "sites") {
//		setChildNodeIndices(stringToVectorI(value));
	} else {
		return nObject::setParameter(name, value);
	}

	return BioNetSuccess;
}

string cObject::getParameter(string name) {

	boost::algorithm::to_lower(name);
	if(name == "bond" && cTO != NULL) {
		return cTO->getParameter("name");
	} else if (name == "nodes") {
		if(getStartTerminal() != NULL && getEndTerminal() != NULL) {
			return to_string(getStartTerminal()->getTopParentIndex()) + "," + to_string(getEndTerminal()->getTopParentIndex());
		} else {
			return "";
		}

	} else if (name == "sites" || name == "isites") {
		if(getStartTerminal() != NULL && getEndTerminal() != NULL) {
			return to_string(getStartTerminal()->getIndex()) + "," + to_string(getEndTerminal()->getIndex());
		} else {
			return "";
		}

	} else if (name == "k" || name == "elasticconstant") {
		return to_string(getElasticConstant());
	} else if (name == "l" || name == "length" || name == "equilibriumlength") {
		return to_string(getEquilibriumLength());
	} else {
		return nObject::getParameter(name);
	}
}

int cObject::getTemplateIndex() {
	if(cTO != NULL) {
		return cTO->getIndex();
	} else {
		return -1;
	}
}

cTObject * cObject::getTemplate() {
	return cTO;
}

int cObject::getNumAttachedTerminals() {
	return numAttachedTerminals;
}

int cObject::getNumTerminals() {
	return numTerminals;
}

void cObject::setElasticConstant(scalar k) {
	this->k = k;
}

scalar cObject::getElasticConstant() {
	return k;
}

void cObject::setEquilibriumLength(scalar l) {
	this->l = l;
}

scalar cObject::getEquilibriumLength() {
	return l;
}

void cObject::setElasticForce(scalar x, scalar y, scalar z) {
	
	// Update total
	totalForce -= elasticForce;

	// Set elastic
	elasticForce(0) = x;
	elasticForce(1) = y;
	elasticForce(2) = z;

	// Update total
	totalForce += elasticForce;

}

void cObject::setElasticForce(Vector3 v) {

	// Update total
	totalForce -= elasticForce;

	// Set elastic
	elasticForce = v;

	// Update total
	totalForce += elasticForce;
}

Vector3 cObject::getElasticForce() {
	return elasticForce;
}

void cObject::zeroElasticForce() {

	// Sort total force too
	totalForce -= elasticForce;
	elasticForce.setZero();
}

void cObject::setTotalForce(scalar x, scalar y, scalar z) {
	
	// So that forces always sum to total, make forces except elastic equal zero (completely arbitrary choice)
	elasticForce(0) = x;
	elasticForce(1) = y;
	elasticForce(2) = z;

	totalForce = elasticForce;

}

void cObject::setTotalForce(Vector3 v) {

	// So that forces always sum to total, make forces except elastic equal zero (completely arbitrary choice)
	elasticForce = v;

	totalForce = elasticForce;
}

Vector3 cObject::getTotalForce() {
	return totalForce;
}

void cObject::zeroTotalForce() {

	// Sort all forces as well as total
	elasticForce.setZero();

	totalForce.setZero();
}

void cObject::setElasticEnergy(scalar E) {

	// Update total
	totalEnergy -= elasticEnergy;

	// Set elastic
	elasticEnergy = E;

	// Update total
	totalEnergy += elasticEnergy;	
}

void cObject::addElasticEnergy(scalar E) {

	// Update elastic
	elasticEnergy += E;

	// Update total
	totalEnergy += E;	
}

scalar cObject::getElasticEnergy() {
	return elasticEnergy;
}

void cObject::zeroElasticEnergy() {

	// Sort total energy too
	totalEnergy -= elasticEnergy;
	elasticEnergy = 0;
}

void cObject::setTotalEnergy(scalar E) {

	// To make everything add up properly, assume this is all elastic energy
	elasticEnergy = E;
	totalEnergy = elasticEnergy;
}

scalar cObject::getTotalEnergy() {
	return totalEnergy;
}

void cObject::zeroTotalEnergy() {

	// Sort total energy too
	elasticEnergy = 0;
	totalEnergy = 0;
}

void cObject::setBondEnergy(scalar E) {
	bondEnergy = E;
}

scalar cObject::getBondEnergy() {
	return bondEnergy;
}

void cObject::zeroBondEnergy() {
	bondEnergy = 0;
}

vObject * cObject::getTerminal(int index) {
	if(index > numAttachedTerminals) {
		return NULL;
	}

	return terminal[index];
}

vObject * cObject::getStartTerminal() {
	return terminal.front();
}

vObject * cObject::getEndTerminal() {
	return terminal.back();
}

int cObject::addTerminal(vObject *n) {

	// Check for saturation
	if (isSaturated()) {
		return BioNetError;
	}

	// Add the terminal
	terminal.push_back(n);
	numAttachedTerminals += 1;

	// Saturation status
	if(numAttachedTerminals == numTerminals) {
		saturated = true;
	}

	return BioNetSuccess;
}

int cObject::removeTerminal(int index) {

	// Check for the index
	if(index >= numAttachedTerminals) {
		return BioNetError;
	}

	// Remove the connection
	terminal.erase(terminal.begin() + index);
	numAttachedTerminals -= 1;

	// Saturation status
	saturated = false;

	return BioNetSuccess;
}

int cObject::removeTerminal(vObject *n) {

	// Iterate and remove
	vector<vObject*>::iterator it = terminal.begin();

	while(it != terminal.end()) {
		if((*it) == n) {
			terminal.erase(it);
			numAttachedTerminals -= 1;

			// Saturation status
			saturated = false;

			return BioNetSuccess;
		}
	}

	return BioNetError;
}

void cObject::removeTerminals() {

	// Remove all connections
	terminal.clear();
	numAttachedTerminals = 0;

	// Saturation status
	saturated = false;
}

void cObject::setSaturated(bool b) {
	saturated = b;
}

bool cObject::isSaturated() {
	return saturated;
}

void cObject::setOffset(Vector3 v) {
	offset = v;
}

Vector3 cObject::getOffset() {
	return offset;
}

BondType cObject::getBondType() {

	if(cTO != NULL) {
		return ::getBondType(cTO->getParameter("type"));
	} else {
		return NoBond;
	}
}

/*
void cObject::calculateElasticForce() {

	int i;
	scalar ext, n;
	Vector3 vSep;

	// This is a linear spring only! So elasticForce is only dependent on exelasticForce
	vSep = getEndTerminal()->getPosition() - getStartTerminal()->getPosition();
	for(i = 0; i < 3; ++i) {
		vSep(i) -= getEquilibriumLength();
	}


	// Use these methods to update total values too
	setElasticForce(getElasticConstant() * vSep);
	setElasticEnergy(0.5 * getElasticConstant() * vSep.dot(vSep));
}
*/


/*
void cObject::calculateElasticEnergy() {
	Vector3 a;
	a = getEndTerminal()->getPosition() - getStartTerminal()->getPosition();
	for(int i = 0; i < 3; ++i) {
		a(i) -= getEquilibriumLength();
	}
	setElasticEnergy(0.5 * getElasticConstant() * a.dot(a));

}
*/

void cObject::applyTemplate(cTObject *cTO) {

	// Set pointer to template
	this->cTO = cTO;
}

Point::Point() : vObject()  {

}

Point::~Point() {

}

int Point::setParameter(string name, string value) {
	return vObject::setParameter(name, value);
}

string Point::getParameter(string name) {
	return vObject::getParameter(name);
}

scalar Point::getLengthscale() {
	return 0;
}

int Point::getNumChildren() {
	return 0;
}

vObject * Point::getChild(int index) {
	return NULL;
}

//virtual void Point::addChild(vObject *c) {
	// Do nothing
//}

//void Point::zeroTorque() {
//
//}

//void Point::addTorque(Vector3 t) {
//
//}

void Point::calculateExtrinsicProperties(NetworkParameters *params) {

}

bool Point::validate(NetworkParameters *params) {

	if(index < 0) {
		fileLog->printError("Point::index", "Must be >= 0. This shouldn't have happend, please contact tech support :(");
		return BioNetError;
	}

	if(structureDimension < 0) {
		fileLog->printError("Point::structureDimension", "Must be >= 0. This shouldn't have happend, please contact tech support :(");
		return BioNetError;
	}

	return BioNetValid;
}

Sphere::Sphere() : Point() {

	structureDimension = 3;

	radius = 0.0;
	density = 0.0;
	viscosity = 0.0;
	mfp = 0.0;
	volume = 0.0;
	crossSection = 0.0;
}

Sphere::~Sphere() {

	radius = 0.0;
	density = 0.0;
	viscosity = 0.0;
	mfp = 0.0;
	volume = 0.0;
	crossSection = 0.0;
}

int Sphere::setParameter(string name, string value) {
	if(name == "radius") {
		setRadius(stos(value) / InternalUnits::Length);
	} else if(name == "density") {
		setDensity(stos(value) / InternalUnits::Density);
	} else if(name == "viscosity") {
		setViscosity(stos(value) / InternalUnits::Viscosity);
	} else {
		return Point::setParameter(name, value);
	}

	return BioNetSuccess;
}

string Sphere::getParameter(string name) {

	ostringstream sStream;
	if(name == "radius") {
		sStream << radius;
		return sStream.str();
	} else if(name == "density") {
		sStream << density;
		return sStream.str();
	} else if(name == "viscosity") {
		sStream << viscosity;
		return sStream.str();
	} else {
		return Point::getParameter(name);
	}
}

scalar Sphere::getLengthscale() {
	return radius;
}


void Sphere::translate(Vector3 t) {

	// Translate parent object
	vObject::translate(t);


	// Translate all iSites
	for(int i = 0; i < numChildren; ++i) {
		child[i]->translate(t);
	}
}

void Sphere::rotate(Matrix3 m) {

	// Rotate self
	vObject::rotate(m);

	// Rotate all children, and recalculate position
	for(int i = 0; i < numChildren; ++i) {
		child[i]->rotate(m);
		child[i]->setPosition(getPosition() + child[i]->getRelativePosition() * getRadius());
	}
}

void Sphere::applyOrientationMatrix(Matrix3 m) {

	// Reset the sphere
//	setOrientation(0,0,0);

	// Rotate entire sphere by this amount
	rotate(m);	
}

/*
void Sphere::rotate(Vector2 r) {

	// Rotate parent object
	Sphere::rotate(r);

	// Rotate all iSites
	for(int i = 0; i < numChildren; ++i) {
		iSite[i]->rotateAboutPoint(getPosition(), r, getRadius());
	}
}


void Sphere::zeroTotalForce() {
	vObject::zeroTotalForce();
	for(int i = 0; i < numChildren; ++i) {
		iSite[i]->zeroTotalForce();
	}
}
*/

void Sphere::setRadius(scalar r) {
	radius = r;
}

scalar Sphere::getRadius() {
	return radius;
}

void Sphere::setDensity(scalar d) {
	density = d;
}

scalar Sphere::getDensity() {
	return density;
}

void Sphere::setViscosity(scalar v) {
	viscosity = v;
}

scalar Sphere::getViscosity() {
	return viscosity;
}

int Sphere::getNumChildren() {
	return numChildren;
}

vObject * Sphere::getChild(int index) {
	if(index < numChildren) {
		return child[index];
	}
	
	return NULL;
}

scalar Sphere::getMeanFreePath() {
	return mfp;
}

scalar Sphere::getCrossSection() {
	return crossSection;
}

scalar Sphere::getVolume() {
	return volume;
}

void Sphere::calculateExtrinsicProperties(NetworkParameters *params) {

	calculateCrossSection();
	calculateVolume();

	calculateMass();
	calculateMomentOfInertia();

	calculateDrag(params->viscosity);
	calculateNoiseMagnitude(params->kBT, params->dt);

	calculateRotationalDrag(params->viscosity);
	calculateRotationalNoiseMagnitude(params->kBT, params->dt);
}

void Sphere::calculateKineticEnergy() {

	// Linear
	setKineticEnergy(0.5 * getMass() * pow(getVelocity().norm(), 2));

	// Rotational
	addKineticEnergy(0.5 * getMomentOfInertia() * pow(getAngularVelocity().vec().norm(), 2));
	
}

void Sphere::calculateTotalEnergy() {
	
	// All energies individually
	calculateKineticEnergy();
}

void Sphere::calculateVolume() {
	volume = (4.0/3.0) * M_PI * pow(getRadius(), 3);
}

void Sphere::calculateCrossSection() {
	crossSection = M_PI * pow(getRadius(), 2);
}

void Sphere::calculateMomentOfInertia() {
	momentOfInertia = (2.0 / 5.0) * getMass() * pow(getRadius(), 2);
}

void Sphere::calculateDrag(scalar extVisc) {
	drag = 6 * M_PI * extVisc * getRadius();
}

void Sphere::calculateRotationalDrag(scalar extVisc) {
	aDrag = 8 * M_PI * extVisc * pow(getRadius(), 3);
}

void Sphere::calculateMass() {
	mass = (4.0/3.0) * M_PI * pow(getRadius(), 3) * getDensity();
}

void Sphere::calculateReactionProbability(scalar dt) {
	vObject::calculateReactionProbability(dt);
	for(int i = 0; i < numChildren; ++i) {
		getChild(i)->calculateReactionProbability(dt);
	}
}

bool Sphere::validate(NetworkParameters *params) {

	if(radius <= 0) {
		fileLog->printError("Sphere::radius", "Must be > 0");
		return BioNetInvalid; 
	}

	return Point::validate(params);
}


// Ellipsoid
Ellipsoid::Ellipsoid() : Point() {

	structureDimension = 3;

	radius = 0.0;
	density = 0.0;
	viscosity = 0.0;
	mfp = 0.0;
	volume = 0.0;
	crossSection = 0.0;
}

Ellipsoid::~Ellipsoid() {

	radius = 0.0;
	density = 0.0;
	viscosity = 0.0;
	mfp = 0.0;
	volume = 0.0;
	crossSection = 0.0;
}

int Ellipsoid::setParameter(string name, string value) {
	if(name == "radius") {
		setRadius(stos(value) / InternalUnits::Length);
	} else if(name == "density") {
		setDensity(stos(value) / InternalUnits::Density);
	} else if(name == "viscosity") {
		setViscosity(stos(value) / InternalUnits::Viscosity);
	} else {
		return Point::setParameter(name, value);
	}

	return BioNetSuccess;
}

string Ellipsoid::getParameter(string name) {

	ostringstream sStream;
	if(name == "radius") {
		sStream << radius;
		return sStream.str();
	} else if(name == "density") {
		sStream << density;
		return sStream.str();
	} else if(name == "viscosity") {
		sStream << viscosity;
		return sStream.str();
	} else {
		return Point::getParameter(name);
	}
}

scalar Ellipsoid::getLengthscale() {
	return radius;
}


void Ellipsoid::translate(Vector3 t) {

	// Translate parent object
	vObject::translate(t);


	// Translate all iSites
	for(int i = 0; i < numChildren; ++i) {
		child[i]->translate(t);
	}
}

void Ellipsoid::rotate(Matrix3 m) {

	// Rotate self
	vObject::rotate(m);

	// Rotate all children, and recalculate position
	for(int i = 0; i < numChildren; ++i) {
		child[i]->rotate(m);
		child[i]->setPosition(getPosition() + child[i]->getRelativePosition() * getRadius());
	}
}

void Ellipsoid::applyOrientationMatrix(Matrix3 m) {

	// Reset the Ellipsoid
//	setOrientation(0,0,0);

	// Rotate entire Ellipsoid by this amount
	rotate(m);	
}

/*
void Ellipsoid::rotate(Vector2 r) {

	// Rotate parent object
	Ellipsoid::rotate(r);

	// Rotate all iSites
	for(int i = 0; i < numChildren; ++i) {
		iSite[i]->rotateAboutPoint(getPosition(), r, getRadius());
	}
}


void Ellipsoid::zeroTotalForce() {
	vObject::zeroTotalForce();
	for(int i = 0; i < numChildren; ++i) {
		iSite[i]->zeroTotalForce();
	}
}
*/

void Ellipsoid::setRadius(scalar r) {
	radius = r;
}

scalar Ellipsoid::getRadius() {
	return radius;
}

void Ellipsoid::setDensity(scalar d) {
	density = d;
}

scalar Ellipsoid::getDensity() {
	return density;
}

void Ellipsoid::setViscosity(scalar v) {
	viscosity = v;
}

scalar Ellipsoid::getViscosity() {
	return viscosity;
}

int Ellipsoid::getNumChildren() {
	return numChildren;
}

vObject * Ellipsoid::getChild(int index) {
	if(index < numChildren) {
		return child[index];
	}
	
	return NULL;
}

scalar Ellipsoid::getMeanFreePath() {
	return mfp;
}

scalar Ellipsoid::getCrossSection() {
	return crossSection;
}

scalar Ellipsoid::getVolume() {
	return volume;
}

void Ellipsoid::calculateExtrinsicProperties(NetworkParameters *params) {

	calculateCrossSection();
	calculateVolume();

	calculateMass();
	calculateMomentOfInertia();

	calculateDrag(params->viscosity);
	calculateNoiseMagnitude(params->kBT, params->dt);

	calculateRotationalDrag(params->viscosity);
	calculateRotationalNoiseMagnitude(params->kBT, params->dt);
}

void Ellipsoid::calculateKineticEnergy() {

	// Linear
	setKineticEnergy(0.5 * getMass() * pow(getVelocity().norm(), 2));

	// Rotational
	addKineticEnergy(0.5 * getMomentOfInertia() * pow(getAngularVelocity().vec().norm(), 2));
	
}

void Ellipsoid::calculateTotalEnergy() {
	
	// All energies individually
	calculateKineticEnergy();
}

void Ellipsoid::calculateVolume() {
	volume = (4.0/3.0) * M_PI * pow(getRadius(), 3);
}

void Ellipsoid::calculateCrossSection() {
	crossSection = M_PI * pow(getRadius(), 2);
}

void Ellipsoid::calculateMomentOfInertia() {
	momentOfInertia = (2.0 / 5.0) * getMass() * pow(getRadius(), 2);
}

void Ellipsoid::calculateDrag(scalar extVisc) {
	drag = 6 * M_PI * extVisc * getRadius();
}

void Ellipsoid::calculateRotationalDrag(scalar extVisc) {
	aDrag = 8 * M_PI * extVisc * pow(getRadius(), 3);
}

void Ellipsoid::calculateMass() {
	mass = (4.0/3.0) * M_PI * pow(getRadius(), 3) * getDensity();
}

void Ellipsoid::calculateReactionProbability(scalar dt) {
	vObject::calculateReactionProbability(dt);
	for(int i = 0; i < numChildren; ++i) {
		getChild(i)->calculateReactionProbability(dt);
	}
}

bool Ellipsoid::validate(NetworkParameters *params) {

	if(radius <= 0) {
		fileLog->printError("Ellipsoid::radius", "Must be > 0");
		return BioNetInvalid; 
	}

	return Point::validate(params);
}

TwoNodeCObject::TwoNodeCObject() : cObject() {
	structureDimension = 1;
	numTerminals = 2;
	terminal.reserve(numTerminals);
}

TwoNodeCObject::~TwoNodeCObject() {
	numTerminals = 0;
}

int TwoNodeCObject::setParameter(string name, string value) {
	return cObject::setParameter(name, value);
}

string TwoNodeCObject::getParameter(string name) {
	return cObject::getParameter(name);
}

bool TwoNodeCObject::validate(NetworkParameters *params) {

	if(index < 0) {
		fileLog->printError("TwoNodeCObject::index", "Must be >= 0. This shouldn't have happend, please contact tech support :(");
		return BioNetError;
	}

	if(structureDimension < 0) {
		fileLog->printError("TwoNodeCObject::structureDimension", "Must be >= 0. This shouldn't have happend, please contact tech support :(");
		return BioNetError;
	}

	// Must have two connections
	if(getStartTerminal() == NULL || getEndTerminal() == NULL) {
		fileLog->printError("TwoNodeCObject::terminal", "A connection must be connected to two vObjects to function.");
		return BioNetInvalid;
	}
	
	return BioNetValid;
}

void TwoNodeCObject::calculateExtrinsicProperties(NetworkParameters *params) {

}

ThreeNodeCObject::ThreeNodeCObject() : cObject() {
	structureDimension = 1;
	numTerminals = 3;
	terminal.reserve(numTerminals);

	elasticTorque.setZero();
	ef1.setZero();
	ef2.setZero();
}

ThreeNodeCObject::~ThreeNodeCObject() {
	numTerminals = 0;

	elasticTorque.setZero();
	ef1.setZero();
	ef2.setZero();
}

int ThreeNodeCObject::setParameter(string name, string value) {
	return cObject::setParameter(name, value);
}

string ThreeNodeCObject::getParameter(string name) {
	return cObject::getParameter(name);
}

bool ThreeNodeCObject::validate(NetworkParameters *params) {

	if(index < 0) {
		fileLog->printError("ThreeNodeCObject::index", "Must be >= 0. This shouldn't have happend, please contact tech support :(");
		return BioNetError;
	}

	if(structureDimension < 0) {
		fileLog->printError("ThreeNodeCObject::structureDimension", "Must be >= 0. This shouldn't have happend, please contact tech support :(");
		return BioNetError;
	}

	// Must have three connections
	for(int i = 0; i < numTerminals; ++i) {
		if(getTerminal(i) == NULL) {
			fileLog->printError("ThreeNodeCObject::terminal", "This connection must be connected to three vObjects to function.");
			return BioNetInvalid;
		}
	}
	
	return BioNetValid;
}

void ThreeNodeCObject::calculateExtrinsicProperties(NetworkParameters *params) {

}

vObject * ThreeNodeCObject::getMidTerminal() {
	return getTerminal(1);
}

void ThreeNodeCObject::setElasticTorque(Vector3 v) {
	
	// Set elastic
	elasticTorque = v;

}

void ThreeNodeCObject::setElasticForces(Vector3 v1, Vector3 v2) {
	
	ef1 = v1;
	ef2 = v2;

}

LinearSpring::LinearSpring() : TwoNodeCObject() {
	
}

LinearSpring::~LinearSpring() {

}

bool LinearSpring::validate(NetworkParameters *params) {


	if(getElasticConstant() < 0) {
		fileLog->printError("Bond::k (Elastic Constant)", "Must be >= 0");
		return BioNetInvalid;
	}

	if(getEquilibriumLength() < 0) {
		fileLog->printError("Bond::l (Equilibrium Length)", "Must be >= 0");
		return BioNetInvalid;
	}

	if(getBondEnergy() >= 0) {
		fileLog->printError("Bond::E (Bond Dissociation Energy)", "Set > 0 (we will make negative internally)");
		return BioNetInvalid;	
	}

	return TwoNodeCObject::validate(params);
}

void LinearSpring::applyElasticForce() {

	// Add as external force to the vObjects
	getStartTerminal()->addExternalForce(elasticForce);
	getEndTerminal()->addExternalForce(-1 * elasticForce);
}

void LinearSpring::calculateElasticForce() {

	scalar ext, n;
	Vector3 vSep;
	Vector3 pSep;

	// This is a linear spring only! So elasticForce is only dependent on exelasticForce
	vSep = getEndTerminal()->getPosition() - getStartTerminal()->getPosition();
	if(vSep(0) == 0 && vSep(1) == 0 && vSep(2) == 0) {

		// No distance
		n = 0;

		// But point force in a random direction
		for(int i = 0; i < 3; ++i) {
			vSep(i) = rngCont->getMinorUniformRN();
//			vSep(i) = GlobalVariables::uniformDistribution(GlobalVariables::rng);
		}
		vSep /= vSep.norm();
	} else {
		n = vSep.norm();
		vSep /= n;
	}

//	cout << n << ", ";
	// Use these methods to update total values too
	setElasticForce(getElasticConstant() * (n - getEquilibriumLength()) * vSep);
//	cout << getElasticConstant() << ", " << n << ", " << getEquilibriumLength() << endl;
	setElasticEnergy(0.5 * getElasticConstant() * pow(n - getEquilibriumLength(), 2));
}

void LinearSpring::applyTotalForce() {

	// Add as external force to the vObjects
	getStartTerminal()->addExternalForce(totalForce);
	getEndTerminal()->addExternalForce(-1 * totalForce);
}

void LinearSpring::calculateTotalForce() {

	// All forces individually
	calculateElasticForce();
}

void LinearSpring::calculateElasticEnergy() {

	setElasticEnergy(0.5 * getElasticConstant() * pow( (getEndTerminal()->getPosition() - getStartTerminal()->getPosition()).norm() - getEquilibriumLength(), 2));

}

void LinearSpring::calculateTotalEnergy() {
	
	// All energies individually
	calculateElasticEnergy();
}

void LinearSpring::applyTemplate(cTObject *cTO) {

	// Do the parent method
	cObject::applyTemplate(cTO);

	// Everything on this spring needs to be copied from the bond
	setElasticConstant(stos(cTO->getParameter("k")));
	setEquilibriumLength(stos(cTO->getParameter("l")));
	setBondEnergy(stos(cTO->getParameter("E")));
}

void LinearSpring::printDetails() {
	cout << endl << endl;
	cout << "Class: LinearSpring (" << getStructureDimension() << "D Object)" << endl;
	cout << "\tSub-classes: TwoNodeCObject" << endl << endl;
	cout << "\tIndex = " << getIndex() << endl;

	cout << "\tk = " << getElasticConstant() << ", l = " << getEquilibriumLength() << endl << endl;

	cout << "\tTotal Force = (" << getTotalForce()[0] << ", " << getTotalForce()[1] << ", " << getTotalForce()[2] << ")" << endl;
	cout << "\tTotal Energy = " << getTotalEnergy() << endl << endl;

	cout << "\tBond Template:" << endl;
	cout << "\t\tTemplate Index = " << cTO->getIndex() << endl;
	cout << "\t\tName = " << cTO->getName() << endl;
	cout << "\t\tType = " << cTO->getTypeString() << endl << endl;

	cout << "\tNum Terminals = " << getNumTerminals() << ", Num Attached Terminals = " << getNumAttachedTerminals() << ", Saturated: " << toString(isSaturated()) << endl;
	cout << "\tConnected to " << getNumAttachedTerminals() << " vObjects:" << endl;
	cout << "\t\tIndices (P,C): ";

	for(int i = 0; i < getNumAttachedTerminals(); ++i) {
		cout << "(" << getTerminal(i)->getTopParentIndex();
		if(getTerminal(i)->getParent() == NULL) {
			cout << ", -) ";
		} else {
			cout << ", " << getTerminal(i)->getIndex() << ") ";
		}
	}

	cout << endl;

}

FENESpring::FENESpring() : TwoNodeCObject() {

	lm = 0.0;
	rLim = 0.0;
	kLim = 411;
}

FENESpring::~FENESpring() {

	lm = 0.0;
	rLim = 0.0;
	kLim = 0.0;
}

void FENESpring::setMaximumLength(scalar l) {
	lm = l;
}

scalar FENESpring::getMaximumLength() {
	return lm;
}

void FENESpring::setElasticConstantLimit(scalar k) {
	kLim = k;
}

scalar FENESpring::getElasticConstantLimit() {
	return kLim;
}

void FENESpring::setLengthLimit(scalar r) {
	rLim = r;
}

scalar FENESpring::getLengthLimit() {
	return rLim;
}

void FENESpring::setEquilibriumLengthLimit(scalar l) {
	lLim = l;
}

scalar FENESpring::getEquilibriumLengthLimit() {
	return lLim;
}
		
bool FENESpring::validate(NetworkParameters *params) {


	if(getElasticConstant() < 0) {
		fileLog->printError("Bond::k (Elastic Constant)", "Must be >= 0");
		return BioNetInvalid;
	}
	
	if(getElasticConstantLimit() < 0 || getElasticConstantLimit() <= getElasticConstant()) {
		fileLog->printError("Bond::klim (Elastic Constant Limit)", "Must be >= 0 and also > k");
		return BioNetInvalid;
	}

	if(getEquilibriumLength() < 0) {
		fileLog->printError("Bond::l (Equilibrium Length)", "Must be >= 0");
		return BioNetInvalid;
	}

	if(getMaximumLength() < 0 || getMaximumLength() <= getEquilibriumLength()) {
		fileLog->printError("Bond::lm (Max Length)", "Must be >= 0 and also > k");
		return BioNetInvalid;
	}
	
	if(getBondEnergy() >= 0) {
		fileLog->printError("Bond::E (Bond Dissociation Energy)", "Set > 0 (we will make negative internally)");
		return BioNetInvalid;	
	}

	return TwoNodeCObject::validate(params);
}

void FENESpring::applyElasticForce() {

	// Add as external force to the vObjects
	getStartTerminal()->addExternalForce(elasticForce);
	getEndTerminal()->addExternalForce(-1 * elasticForce);
}

void FENESpring::calculateElasticForce() {

	scalar ext, r;
	Vector3 vSep;
	Vector3 pSep;

	// This is a FENE spring! Elastic force is piecewise for numerical stability
	vSep = getEndTerminal()->getPosition() - getStartTerminal()->getPosition();
	if(vSep(0) == 0 && vSep(1) == 0 && vSep(2) == 0) {

		// No distance
		r = 0;

		// But point force in a random direction
		for(int i = 0; i < 3; ++i) {
			vSep(i) = rngCont->getMinorUniformRN();
		}
		vSep /= vSep.norm();
	} else {
		r = vSep.norm();
		vSep /= r;
	}

	// Use these methods to update total values too
	if (r < getLengthLimit()) {
		setElasticForce(getElasticConstant() * ((r - getEquilibriumLength()) / (1 - pow((r - getEquilibriumLength()) / getMaximumLength(), 2))) * vSep);
		setElasticEnergy(-0.5 * getElasticConstant() * pow(getMaximumLength(), 2) * log(1 - pow((r - getEquilibriumLength()) / getMaximumLength(), 2)));
//		cout << -0.5 * getElasticConstant() * pow(getMaximumLength(), 2) * log(1 - pow((r - getEquilibriumLength()) / getMaximumLength(), 2)) << endl;
	} else {
		setElasticForce(getElasticConstantLimit() * (r - getEquilibriumLengthLimit()) * vSep);
		setElasticEnergy(0.5 * getElasticConstantLimit() * pow(r - getEquilibriumLengthLimit(), 2));
//		cout << 0.5 * getElasticConstantLimit() * pow(r - getEquilibriumLengthLimit(), 2) << endl;
	}
}

void FENESpring::applyTotalForce() {

	// Add as external force to the vObjects
	getStartTerminal()->addExternalForce(totalForce);
	getEndTerminal()->addExternalForce(-1 * totalForce);
}

void FENESpring::calculateTotalForce() {

	// All forces individually
	calculateElasticForce();
}

void FENESpring::calculateElasticEnergy() {

	scalar ext, r;
	Vector3 vSep;

	// This is a FENE spring! Elastic energy is piecewise for numerical stability
	vSep = getEndTerminal()->getPosition() - getStartTerminal()->getPosition();
	r = vSep.norm();
	
	// Use these methods to update total values too
	if (r < getLengthLimit()) {
		setElasticEnergy(-0.5 * getElasticConstantLimit() * pow(getMaximumLength(), 2) * log(1 - pow((r - getEquilibriumLength()) / getMaximumLength(), 2)));
	} else {
		setElasticEnergy(0.5 * getElasticConstantLimit() * pow(r - getEquilibriumLengthLimit(), 2));	
	}
}

void FENESpring::calculateTotalEnergy() {
	
	// All energies individually
	calculateElasticEnergy();
}

scalar FENESpring::calculateEffectiveElasticConstant(scalar r) {

	// FENE df/dx
	scalar extension, keff, powerBit;
	
	extension = r - getEquilibriumLength();
	powerBit = pow(extension / getMaximumLength(), 2);

	return (getElasticConstant() / (1 - powerBit)) * (1 + 2 * powerBit / (1 - powerBit));
}

scalar FENESpring::calculateEffectiveElasticConstant() {

	// FENE df/dx
	scalar r;
	Vector3 vSep;
	vSep = getEndTerminal()->getPosition() - getStartTerminal()->getPosition();
	r = vSep.norm();

	return calculateEffectiveElasticConstant(r);
}

void FENESpring::calculateLengthLimit() {

	// Do a quick in half way interpolation to find the length limit
	scalar testR, maxR, minR, eps, keff, feneForce;
	
	// Initialise
	eps = 0.001;
	minR = 0.0;
	maxR = getMaximumLength();
	testR = maxR * (1 - eps);
	keff = calculateEffectiveElasticConstant(testR);
	
	// Algorithm to get the length limit
	while(fabs(keff - getElasticConstantLimit()) / getElasticConstantLimit() > eps) {
		testR = 0.5 * (minR + maxR);
		keff = calculateEffectiveElasticConstant(testR);
		
		if (keff > getElasticConstantLimit()) {
			maxR = testR;
		} else {
			minR = testR;
		}
	}
	
	// Set the new k value
	setElasticConstantLimit(keff);
	setLengthLimit(testR);
	
	feneForce =  getElasticConstant() * ((testR - getEquilibriumLength()) / (1 - pow((testR - getEquilibriumLength()) / getMaximumLength(), 2)));

	// Set the new equilibrium length
	setEquilibriumLengthLimit(testR - (feneForce / keff));
}

void FENESpring::applyTemplate(cTObject *cTO) {

	// Do the parent method
	cObject::applyTemplate(cTO);

	// Everything on this spring needs to be copied from the bond
	setElasticConstant(stos(cTO->getParameter("k")));
	setElasticConstantLimit(stos(cTO->getParameter("klim")));
	setEquilibriumLength(stos(cTO->getParameter("l")));
	setMaximumLength(stos(cTO->getParameter("lm")));
	setBondEnergy(stos(cTO->getParameter("E")));
	
	// Non-primitive stuff
	calculateLengthLimit();
}

void FENESpring::printDetails() {
	cout << endl << endl;
	cout << "Class: FENESpring (" << getStructureDimension() << "D Object)" << endl;
	cout << "\tSub-classes: TwoNodeCObject" << endl << endl;
	cout << "\tIndex = " << getIndex() << endl;

	cout << "\tk = " << getElasticConstant() << ", l = " << getEquilibriumLength() << endl;
	cout << "\tlm = " << getMaximumLength() << endl;
	cout << "\tklim = " << getElasticConstantLimit() << ", llim = " << getEquilibriumLengthLimit() << endl << endl;
	
	cout << "\tTotal Force = (" << getTotalForce()[0] << ", " << getTotalForce()[1] << ", " << getTotalForce()[2] << ")" << endl;
	cout << "\tTotal Energy = " << getTotalEnergy() << endl << endl;

	cout << "\tBond Template:" << endl;
	cout << "\t\tTemplate Index = " << cTO->getIndex() << endl;
	cout << "\t\tName = " << cTO->getName() << endl;
	cout << "\t\tType = " << cTO->getTypeString() << endl << endl;

	cout << "\tNum Terminals = " << getNumTerminals() << ", Num Attached Terminals = " << getNumAttachedTerminals() << ", Saturated: " << toString(isSaturated()) << endl;
	cout << "\tConnected to " << getNumAttachedTerminals() << " vObjects:" << endl;
	cout << "\t\tIndices (P,C): ";

	for(int i = 0; i < getNumAttachedTerminals(); ++i) {
		cout << "(" << getTerminal(i)->getTopParentIndex();
		if(getTerminal(i)->getParent() == NULL) {
			cout << ", -) ";
		} else {
			cout << ", " << getTerminal(i)->getIndex() << ") ";
		}
	}

	cout << endl;

}

AngularSpring::AngularSpring() : ThreeNodeCObject() {
	
}

AngularSpring::~AngularSpring() {

}

bool AngularSpring::validate(NetworkParameters *params) {


	if(getElasticConstant() < 0) {
		fileLog->printError("Bond::k (Elastic Constant)", "Must be >= 0");
		return BioNetInvalid;
	}

	if(getEquilibriumLength() < 0) {
		fileLog->printError("Bond::l (Equilibrium Length)", "Must be >= 0");
		return BioNetInvalid;
	}

	if(getBondEnergy() >= 0) {
		fileLog->printError("Bond::E (Bond Dissociation Energy)", "Set > 0 (we will make negative internally)");
		return BioNetInvalid;	
	}

	return ThreeNodeCObject::validate(params);
}

void AngularSpring::applyElasticForce() {


	// Add as external force to the vObjects
	getStartTerminal()->addExternalForce(ef1);
	getEndTerminal()->addExternalForce(ef2);
}

void AngularSpring::calculateElasticForce() {

	scalar ang, d1, d2;
	Vector3 v1, v2, tau;
	
	// Get all required data
	v1 = getEndTerminal()->getPosition() - getMidTerminal()->getPosition();
	v2 = getMidTerminal()->getPosition() - getStartTerminal()->getPosition();

	if(v1(0) == 0 && v1(1) == 0 && v1(2) == 0) {

		// No energy (it's a torque property)
		setElasticForce(v1);
		setElasticEnergy(0);
		return;

	} else {
		d1 = v1.norm();
		v1 /= d1;
	}

	if(v2(0) == 0 && v2(1) == 0 && v2(2) == 0) {

		// No energy (it's a torque property)
		setElasticForce(v2);
		setElasticEnergy(0);
		return;
	} else {
		d2 = v2.norm();
		v2 /= d2;
	}


	// Get the angle 
	ang = acos(v1.dot(v2));
	
	// And the torque
	tau = v1.cross(v2) * getElasticConstant() * (ang - getEquilibriumLength());

	// Update everything
	setElasticForces(tau.cross(v1) / d1, -tau.cross(v2) / d2);
	setElasticEnergy(0.5 * getElasticConstant() * pow(ang - getEquilibriumLength(), 2));
/*


	// Use these methods to update total values too
	setElasticForce(getElasticConstant() * (n - getEquilibriumLength()) * vSep);
	setElasticEnergy(0.5 * getElasticConstant() * pow(n - getEquilibriumLength(), 2));
*/
}

void AngularSpring::applyTotalForce() {

	// Add as external force to the vObjects
	getStartTerminal()->addExternalForce(ef1);
	getEndTerminal()->addExternalForce(ef2);
}

void AngularSpring::calculateTotalForce() {

	// All forces individually
	calculateElasticForce();
}

void AngularSpring::calculateElasticEnergy() {

	scalar ang;
	Vector3 v1, v2;
	
	// Get all required data
	v1 = getEndTerminal()->getPosition() - getMidTerminal()->getPosition();
	v2 = getMidTerminal()->getPosition() - getStartTerminal()->getPosition();

	if(v1(0) == 0 && v1(1) == 0 && v1(2) == 0) {

		// No energy (it's a torque property)
		setElasticForce(v1);
		setElasticEnergy(0);
		return;

	} else {
		v1 /= v1.norm();
	}

	if(v2(0) == 0 && v2(1) == 0 && v2(2) == 0) {

		// No energy (it's a torque property)
		setElasticEnergy(0);
		return;
	} else {
		v2 /= v2.norm();
	}


	// Get the angle 
	ang = acos(v1.dot(v2));

	// Set the energy
	setElasticEnergy(0.5 * getElasticConstant() * pow(ang - getEquilibriumLength(), 2));

}

void AngularSpring::calculateTotalEnergy() {
	
	// All energies individually
	calculateElasticEnergy();
}

void AngularSpring::applyTemplate(cTObject *cTO) {

	// Do the parent method
	cObject::applyTemplate(cTO);

	// Everything on this spring needs to be copied from the bond
	setElasticConstant(stos(cTO->getParameter("k")));
	setEquilibriumLength(stos(cTO->getParameter("l")));
	setBondEnergy(stos(cTO->getParameter("E")));
}

void AngularSpring::printDetails() {
	cout << endl << endl;
	cout << "Class: AngularSpring (" << getStructureDimension() << "D Object)" << endl;
	cout << "\tSub-classes: ThreeNodeCObject" << endl << endl;
	cout << "\tIndex = " << getIndex() << endl;
	cout << "\tConnected to " << getNumAttachedTerminals() << " vObjects:" << endl;

	cout << "\t\tIndices (P,C): ";

	for(int i = 0; i < getNumAttachedTerminals(); ++i) {
		cout << "(" << getTerminal(i)->getTopParentIndex() << ", " << getTerminal(i)->getIndex() << ") ";
	}

	cout << endl;

	cout << "\tTotal Force = (" << getTotalForce()[0] << ", " << getTotalForce()[1] << ", " << getTotalForce()[2] << ")" << endl;
	cout << "\tTotal Energy = " << getTotalEnergy() << endl;
	cout << "\tNum Terminals = " << getNumTerminals() << ", Saturated: " << toString(isSaturated()) << endl;
	cout << "\tK = " << getElasticConstant() << ", theta0 = " << getEquilibriumLength() << endl;
	cout << "\tBond Template:" << endl;
	cout << "\t\tTemplate Index = " << cTO->getIndex() << endl;
	cout << "\t\tName = " << cTO->getName() << endl;
	cout << "\t\tType = " << cTO->getTypeString() << endl;
}


RigidRod1D::RigidRod1D() : TwoNodeCObject() {
	
}

RigidRod1D::~RigidRod1D() {

}


bool RigidRod1D::validate(NetworkParameters *params) {


	fileLog->printError("Bond::type", "Rigid not yet supported :(");
	return BioNetInvalid;	
	
//	return TwoNodeCObject::validate(params);
}

void RigidRod1D::applyElasticForce() {

	// Add as external force to the vObjects
	getStartTerminal()->addExternalForce(elasticForce);
	getEndTerminal()->addExternalForce(-1 * elasticForce);
}

void RigidRod1D::calculateElasticForce() {

	// The elastic force is a restraint. It simply negates all force in the axial direction of the rod on both nodes
	scalar ext, n;
	Vector3 vSep;
	Vector3 pSep;
	Vector3 ef, sf;
	
	// Get current forces
	ef = getEndTerminal()->getTopParent()->getTotalForce();
	sf = getStartTerminal()->getTopParent()->getTotalForce();

	// Normal vector along rod
	vSep = getEndTerminal()->getPosition() - getStartTerminal()->getPosition();
	if(vSep(0) != 0 || vSep(1) != 0 || vSep(2) != 0) {
		n = vSep.norm();
		vSep /= n;
	}

	// Take off the bits of force we do not want
	ef -= ef.dot(vSep) * vSep;
	sf -= sf.dot(vSep) * vSep;

//	getStartTerminal()->addExternalForce(-);
//	getEndTerminal()->addExternalForce(-1 * totalForce);	

	// Use these methods to update total values too
	setElasticForce(vSep);
	setElasticEnergy(0);
}

void RigidRod1D::applyTotalForce() {

	// This ought to be where we apply restraints

//	// Add as external force to the vObjects
//	getStartTerminal()->addExternalForce(totalForce);
//	getEndTerminal()->addExternalForce(-1 * totalForce);
}

void RigidRod1D::calculateTotalForce() {

	// Do nothing
}

void RigidRod1D::calculateElasticEnergy() {

	setElasticEnergy(0);

}

void RigidRod1D::calculateTotalEnergy() {
	
	// Do nothing
}


void RigidRod1D::applyTemplate(cTObject *cTO) {

	// Do the parent method
	cObject::applyTemplate(cTO);

}

void RigidRod1D::printDetails() {
	cout << endl << endl;
	cout << "Class: RigidRod1D (" << getStructureDimension() << "D Object)" << endl;
	cout << "\tSub-classes: TwoNodeCObject" << endl << endl;
	cout << "\tIndex = " << getIndex() << endl;
	cout << "\tConnected to " << getNumAttachedTerminals() << " vObjects:" << endl;

	cout << "\t\tIndices (P,C): ";

	for(int i = 0; i < getNumAttachedTerminals(); ++i) {
		cout << "(" << getTerminal(i)->getTopParentIndex() << ", " << getTerminal(i)->getIndex() << ") ";
	}

	cout << endl;

	cout << "\tTotal Force = (" << getTotalForce()[0] << ", " << getTotalForce()[1] << ", " << getTotalForce()[2] << ")" << endl;
	cout << "\tTotal Energy = " << getTotalEnergy() << endl;
	cout << "\tNum Terminals = " << getNumTerminals() << ", Num Attached Terminals = " << getNumAttachedTerminals() << ", Saturated: " << toString(isSaturated()) << endl;
	cout << "\tk = " << getElasticConstant() << ", l = " << getEquilibriumLength() << endl;
	cout << "\tBond Template:" << endl;
	cout << "\t\tTemplate Index = " << cTO->getIndex() << endl;
	cout << "\t\tName = " << cTO->getName() << endl;
	cout << "\t\tType = " << cTO->getTypeString() << endl;
}

PointNode::PointNode() : Point() {
	
}

PointNode::~PointNode() {

}

scalar PointNode::calculateVolumeOverlap(vObject *v) {
	return v->calculateVolumeOverlap(this);
}


scalar PointNode::calculateStericInteraction(scalar kst, vObject *v, Vector3 trans) {
	return v->calculateStericInteraction(kst, this, trans);
}

scalar PointNode::calculateVdWInteraction(scalar req, scalar eps, scalar alpha, scalar cutoff, vObject *v, Vector3 trans) {
	return v->calculateVdWInteraction(req, eps, alpha, cutoff, this, trans);
}

void PointNode::applyTemplate(vTObject *vTO, NetworkParameters *params) {

	// Do the parent method
	vObject::applyTemplate(vTO, params);

	// We need to get mass and drag from the template
	setMass(stos(vTO->getParameter("mass")));
	setDrag(stos(vTO->getParameter("drag")));
	setMaxNumConnections(vTO->getMaxNumConnections());

	// And calculate the noise
	calculateNoiseMagnitude(params->kBT, params->dt);

}

bool PointNode::validate(NetworkParameters *params) {
	return Point::validate(params);
}

void PointNode::printDetails() {
	cout << endl << endl;
	cout << "Class: PointNode (" << getStructureDimension() << "D Object)" << endl;
	cout << "\tSub-classes: Point" << endl << endl;
	cout << "\tIndex = " << getIndex() << endl;

	cout << "\tPos = (" << getPosition()[0] << ", " << getPosition()[1] << ", " << getPosition()[2] << ")" << endl;
	cout << "\tVel = (" << getVelocity()[0] << ", " << getVelocity()[1] << ", " << getVelocity()[2] << ")" << endl;
	cout << "\tMass = " << getMass() << ", Drag = " << getDrag() << endl;

	cout << "\tTotal Force = (" << getTotalForce()[0] << ", " << getTotalForce()[1] << ", " << getTotalForce()[2] << ")" << endl;
	cout << "\tTotal Energy = " << getTotalEnergy() << endl;

	cout << "\tMax Connections = " << getMaxNumConnections() << ", Saturated: " << toString(isSaturated()) << endl;

	cout << "\tProtein Template:" << endl;
	cout << "\t\tTemplate Index = " << vTO->getIndex() << endl;
	cout << "\t\tName = " << vTO->getName() << endl;
	cout << "\t\tType = " << vTO->getTypeString() << endl;

	cout << "\tConnected to " << getNumConnections() << " cObjects:" << endl;
	cout << "\t\tIndices: ";
	for(int i = 0; i < getNumConnections(); ++i) {
		cout << getConnection(i)->getIndex() << " ";
	}
	cout << endl;

}

scalar PointNode::calculateVolumeOverlap(PointNode *p) {
	return ::calculateVolumeOverlap(this, p);
}

scalar PointNode::calculateVolumeOverlap(SphereNode *s) {
	return ::calculateVolumeOverlap(this, s);
}

scalar PointNode::calculateVolumeOverlap(EllipsoidNode *e) {
	return ::calculateVolumeOverlap(this, e);
}

scalar PointNode::calculateStericInteraction(scalar kst, PointNode *p, Vector3 trans) {
	return ::calculateStericInteraction(kst, p, this, trans);
}

scalar PointNode::calculateStericInteraction(scalar kst, SphereNode *s, Vector3 trans) {
	return ::calculateStericInteraction(kst, this, s, trans);
}

scalar PointNode::calculateStericInteraction(scalar kst, EllipsoidNode *e, Vector3 trans) {
	return ::calculateStericInteraction(kst, this, e, trans);
}

scalar PointNode::calculateVdWInteraction(scalar req, scalar eps, scalar alpha, scalar cutoff, PointNode *p, Vector3 trans) {
	return ::calculateVdWInteraction(req, eps, alpha, cutoff, this, p, trans);
}

scalar PointNode::calculateVdWInteraction(scalar req, scalar eps, scalar alpha, scalar cutoff, SphereNode *s, Vector3 trans) {
	return ::calculateVdWInteraction(req, eps, alpha, cutoff, this, s, trans);
}

scalar PointNode::calculateVdWInteraction(scalar req, scalar eps, scalar alpha, scalar cutoff, EllipsoidNode *e, Vector3 trans) {
	return ::calculateVdWInteraction(req, eps, alpha, cutoff, this, e, trans);
}

PointInteractionSite::PointInteractionSite() : Point() {

	maxNumConnections = 1;
}

PointInteractionSite::~PointInteractionSite() {

}

int PointInteractionSite::setParameter(string name, string value) {

	return Point::setParameter(name, value);
}

void PointInteractionSite::addExternalForce(Vector3 v) {

	// Add force to parents
	parent->addExternalForce(v);

	// What about torques?
	parent->addExternalTorque((getPosition() - parent->getPosition()).cross(v));
}

void PointInteractionSite::applyTemplate(vTObject *vTO, NetworkParameters *params) {

	// Do the parent method
	vObject::applyTemplate(vTO, params);

	// Input file parameters

	// All interaction sites can only connect once, whatever the input file says
	setMaxNumConnections(1);

	// Non-primitve stuff
	setRelativePosition(vTO->getRelativePosition());
	setInitialRelativePosition(getRelativePosition());
}

scalar PointInteractionSite::calculateVolumeOverlap(vObject *v) {
	return 0.0;
}

scalar PointInteractionSite::calculateStericInteraction(scalar kst, vObject *v, Vector3 trans) {
	return 0.0;
}

scalar PointInteractionSite::calculateVdWInteraction(scalar req, scalar eps, scalar alpha, scalar cutoff, vObject *v, Vector3 trans) {
	return 0.0;
}

bool PointInteractionSite::validate(NetworkParameters *params) {
	
	// This shouldn't have more than 1 connection
	string errString;

/*
	if(connection.size() > 1) {
		errString = "An interaction site can only contain a maximum of 1 connection.";
		errString += " (connected to";
		for(int i = 0 ; i < connection.size(); ++i) {
			errString += " " + to_string(connection.at(i)->getIndex());
		}
		errString += ")";

		fileLog->printError("PointInteractionSite::connection", errString);
		return BioNetInvalid;
	}
*/
	return Point::validate(params);
}

void PointInteractionSite::printDetails() {
	cout << endl << endl;
	cout << "Class: PointInteractionSite (" << getStructureDimension() << "D Object)" << endl;
	cout << "\tSub-classes: Point" << endl << endl;

	cout << "\tPosition = (" << getPosition()[0] << ", " << getPosition()[1] << ", " << getPosition()[2] << ")" << endl;
	cout << "\tRelative Position = (" << getRelativePosition()[0] << ", " << getRelativePosition()[1] << ", " << getRelativePosition()[2] << ")" << endl;

//	cout << "\tCharge Density = " << getChargeDensity() << endl;
//	cout << "\tVdW Potential = " << getVdWPotential() << ", VdW Equilibrium = " << getVdWEquilibrium() << endl;

	cout << "\tMax Connections = " << getMaxNumConnections() << ", Saturated: " << toString(isSaturated()) << endl << endl;

	cout << "\tTemplate:" << endl;
	cout << "\t\tTemplate Index = " << vTO->getIndex() << endl;
	cout << "\t\tName = " << vTO->getName() << endl;
	cout << "\t\tType = " << vTO->getTypeString() << endl << endl;

	cout << "\tConnected to " << getNumConnections() << " cObjects:" << endl;
	cout << "\t\tIndices: ";
	for(int i = 0; i < getNumConnections(); ++i) {
		cout << getConnection(i)->getIndex() << " ";
	}
	cout << endl;

}


void PointInteractionSite::rotate(Matrix3 m) {

	// Rotate self
//	vObject::rotate(m);

	// Rotate the local orientations
	setRelativePosition(m * getRelativePosition());
}

scalar PointInteractionSite::calculateVolumeOverlap(PointNode *p) {
	return 0.0;
}

scalar PointInteractionSite::calculateVolumeOverlap(SphereNode *s) {
	return 0.0;
}

scalar PointInteractionSite::calculateVolumeOverlap(EllipsoidNode *e) {
	return 0.0;
}

scalar PointInteractionSite::calculateStericInteraction(scalar kst, PointNode *p, Vector3 trans) {
	return 0.0;
}

scalar PointInteractionSite::calculateStericInteraction(scalar kst, SphereNode *s, Vector3 trans) {
	return 0.0;
}

scalar PointInteractionSite::calculateStericInteraction(scalar kst, EllipsoidNode *e, Vector3 trans) {
	return 0.0;
}

scalar PointInteractionSite::calculateVdWInteraction(scalar req, scalar eps, scalar alpha, scalar cutoff, PointNode *p, Vector3 trans) {
	return 0.0;
}

scalar PointInteractionSite::calculateVdWInteraction(scalar req, scalar eps, scalar alpha, scalar cutoff, SphereNode *s, Vector3 trans) {
	return 0.0;
}

scalar PointInteractionSite::calculateVdWInteraction(scalar req, scalar eps, scalar alpha, scalar cutoff, EllipsoidNode *s, Vector3 trans) {
	return 0.0;
}

SphereNode::SphereNode() : Sphere() {

}

SphereNode::~SphereNode() {

}

int SphereNode::setParameter(string name, string value) {

	boost::algorithm::to_lower(name);
	return Sphere::setParameter(name, value);
}

string SphereNode::getParameter(string name) {
	
	ostringstream sStream;
	if(name == "mass") {
		sStream << mass;
		return sStream.str();
	} else if(name == "drag") {
		sStream << drag;
		return sStream.str();
	} else {
		return Sphere::getParameter(name);
	}
}

void SphereNode::setPosition(Vector3 v) {

	int i;
	Vector3 trans;

	// Convert to translation for children
	trans = v - getPosition();

	for(i = 0; i < numChildren; ++i) {
		getChild(i)->translate(trans);
	}

	// Set adult position
	vObject::setPosition(v);
}

bool SphereNode::validate(NetworkParameters *params) {

	int i;

	// Node stuff
	if(radius <= 0) {
		fileLog->printError("SphereNode::radius", "Must be > 0");
		return BioNetInvalid; 
	}

	if(isInertialSimulation(params->simType)) {
		if(Sphere::density <= 0) {
			fileLog->printError("SphereNode::density", "Must be > 0");
			return BioNetInvalid;
		}

		if(mass <= 0) {
			fileLog->printError("SphereNode::mass", "Must be > 0");
			return BioNetInvalid;
		}
	}

	if(params->simType == Brownian) {
//		if(Sphere::viscosity <= 0) {
//			fileLog->printError("SphereNode::viscosity", "Must be > 0");
//			return BioNetInvalid;
//		}

		if(drag <= 0) {
			fileLog->printError("SphereNode::drag", "Must be > 0");
			return BioNetInvalid;
		}
	}

	// ISite stuff
	for(i = 0; i < numChildren; ++i) {
		if(child[i]->validate(params) == BioNetInvalid) {
			fileLog->printError("Child vObject " + to_string(i) + " invalid :(");
			return BioNetInvalid;
		}
	}

	return BioNetValid;
}

scalar SphereNode::calculateStericInteraction(scalar kst, vObject *v, Vector3 trans) {
	return v->calculateStericInteraction(kst, this, trans);
}

scalar SphereNode::calculateStericInteraction(scalar kst, PointNode *p, Vector3 trans) {
	return ::calculateStericInteraction(kst, p, this, trans);
}

scalar SphereNode::calculateStericInteraction(scalar kst, SphereNode *s, Vector3 trans) {
	return ::calculateStericInteraction(kst, s, this, trans);
}

scalar SphereNode::calculateStericInteraction(scalar kst, EllipsoidNode *e, Vector3 trans) {
	return ::calculateStericInteraction(kst, this, e, trans);
}

scalar SphereNode::calculateVdWInteraction(scalar req, scalar eps, scalar alpha, scalar cutoff, vObject *v, Vector3 trans) {
	return v->calculateVdWInteraction(req, eps, alpha, cutoff, this, trans);
}

scalar SphereNode::calculateVdWInteraction(scalar req, scalar eps, scalar alpha, scalar cutoff, PointNode *p, Vector3 trans) {
	return ::calculateVdWInteraction(req, eps, alpha, cutoff, p, this, trans);
}

scalar SphereNode::calculateVdWInteraction(scalar req, scalar eps, scalar alpha, scalar cutoff, SphereNode *s, Vector3 trans) {
	return ::calculateVdWInteraction(req, eps, alpha, cutoff, s, this, trans);
}

scalar SphereNode::calculateVdWInteraction(scalar req, scalar eps, scalar alpha, scalar cutoff, EllipsoidNode *e, Vector3 trans) {
	return ::calculateVdWInteraction(req, eps,alpha, cutoff, this, e, trans);
}

scalar SphereNode::calculateVolumeOverlap(vObject *v) {
	return v->calculateVolumeOverlap(this);
}

scalar SphereNode::calculateVolumeOverlap(PointNode *p) {
	return ::calculateVolumeOverlap(p, this);
}

scalar SphereNode::calculateVolumeOverlap(SphereNode *s) {
	return ::calculateVolumeOverlap(this, s);
}

scalar SphereNode::calculateVolumeOverlap(EllipsoidNode *e) {
	return ::calculateVolumeOverlap(this, e);
}

void SphereNode::applyTemplate(vTObject *vTO, NetworkParameters *params) {

	// Do the parent method
	vObject::applyTemplate(vTO, params);
	
	setRadius(stos(vTO->getParameter("radius")));
	setDensity(stos(vTO->getParameter("density")));
	setViscosity(stos(vTO->getParameter("viscosity")));
	setMaxNumConnections(vTO->getMaxNumConnections());

	// Extrinsic properties
	calculateExtrinsicProperties(params);

	// Get the children (sites)
	if(params->localConnectivitySet) {

		// Get the number of sites
		numChildren = vTO->getNumTSites();

		// Set new saturation point
		setMaxNumConnections(numChildren);

		if(numChildren != 0) {

			// Pointer memory
			child = new vObject*[numChildren];

			// Template for copying and object
			vTObject *childTemplate;

			for(int i = 0; i < numChildren; ++i) {

				// An object
				child[i] = new PointInteractionSite();

				// Stuff to set from file
				child[i]->setParameter("index", to_string(i));

				// Pass parent properties down
				child[i]->setReactionRate(getReactionRate() / numChildren);

				// Template and new object
				childTemplate = vTO->getTSite(i);
				child[i]->applyTemplate(childTemplate, params);

				// Link it to this object
				child[i]->setParent(this);
			}
		}
	}

	// Rotation matrix
//	applyOrientationMatrix(MathFunctions::eulerToZXZ(getOrientation()));
//	applyOrientationMatrix(getOrientationMatrix());
	Matrix3 m = getOrientationMatrix();
	orientationMatrix.setIdentity();
	applyOrientationMatrix(m);
}

void SphereNode::printDetails() {
	
	calculateEulerAngles();

	cout << endl << endl;
	cout << "Class: SphereNode (" << getStructureDimension() << "D Object)" << endl;
	cout << "\tSub-classes: Sphere" << endl << endl;
	cout << "\tIndex = " << getIndex() << endl;
	cout << "\tDensity = " << getDensity() << ", Viscosity = " << getViscosity() << endl;
	cout << "\tMass = " << getMass() << ", Drag = " << getDrag() << endl;
	cout << "\tRadius = " << getLengthscale() << endl << endl;
	cout << "\tPos = (" << pos[0] << ", " << pos[1] << ", " << pos[2] << ")" << endl;
	cout << "\tVel = (" << vel[0] << ", " << vel[1] << ", " << vel[2] << ")" << endl;
	cout << "\tAcc = (" << acc[0] << ", " << acc[1] << ", " << acc[2] << ")" << endl;
//	cout << "\tOrientation = (" << orientation[0] << ", " << orientation[1] << ", " << orientation[2] << ")" << endl << endl;
	cout << "\tTotal Force = (" << getTotalForce()[0] << ", " << getTotalForce()[1] << ", " << getTotalForce()[2] << ")" << endl;
	cout << "\tTotal Energy = " << getTotalEnergy() << endl;

	cout << "\tMax Connections = " << getMaxNumConnections() << ", Saturated: " << toString(isSaturated()) << endl << endl;

	if(numChildren != 0) {
		cout << "\tChild vObjects:" << endl;
		for(int i = 0; i < numChildren; ++i) {
			child[i]->printDetails();
		}
	}

	cout << "\tTemplate:" << endl;
	cout << "\t\tTemplate Index = " << vTO->getIndex() << endl;
	cout << "\t\tName = " << vTO->getName() << endl;
	cout << "\t\tType = " << vTO->getTypeString() << endl << endl;

	cout << endl << "\tConnected to " << getNumConnections() << " cObjects:" << endl;
	cout << "\t\tIndices: ";
	for(int i = 0; i < getNumConnections(); ++i) {
		cout << getConnection(i)->getIndex() << " ";
	}
	cout << endl;
}

EllipsoidNode::EllipsoidNode() : Ellipsoid() {

}

EllipsoidNode::~EllipsoidNode() {

}

int EllipsoidNode::setParameter(string name, string value) {

	boost::algorithm::to_lower(name);
	return Ellipsoid::setParameter(name, value);
}

string EllipsoidNode::getParameter(string name) {
	
	ostringstream sStream;
	if(name == "mass") {
		sStream << mass;
		return sStream.str();
	} else if(name == "drag") {
		sStream << drag;
		return sStream.str();
	} else {
		return Ellipsoid::getParameter(name);
	}
}

void EllipsoidNode::setPosition(Vector3 v) {

	int i;
	Vector3 trans;

	// Convert to translation for children
	trans = v - getPosition();

	for(i = 0; i < numChildren; ++i) {
		getChild(i)->translate(trans);
	}

	// Set adult position
	vObject::setPosition(v);
}

bool EllipsoidNode::validate(NetworkParameters *params) {

	int i;

	// Node stuff
	if(radius <= 0) {
		fileLog->printError("EllipsoidNode::radius", "Must be > 0");
		return BioNetInvalid; 
	}

	if(isInertialSimulation(params->simType)) {
		if(Ellipsoid::density <= 0) {
			fileLog->printError("EllipsoidNode::density", "Must be > 0");
			return BioNetInvalid;
		}

		if(mass <= 0) {
			fileLog->printError("EllipsoidNode::mass", "Must be > 0");
			return BioNetInvalid;
		}
	}

	if(params->simType == Brownian) {
		if(Ellipsoid::viscosity <= 0) {
			fileLog->printError("EllipsoidNode::viscosity", "Must be > 0");
			return BioNetInvalid;
		}

		if(drag <= 0) {
			fileLog->printError("EllipsoidNode::drag", "Must be > 0");
			return BioNetInvalid;
		}
	}

	// ISite stuff
	for(i = 0; i < numChildren; ++i) {
		if(child[i]->validate(params) == BioNetInvalid) {
			fileLog->printError("Child vObject " + to_string(i) + " invalid :(");
			return BioNetInvalid;
		}
	}

	return BioNetValid;
}

scalar EllipsoidNode::calculateStericInteraction(scalar kst, vObject *v, Vector3 trans) {
	return v->calculateStericInteraction(kst, this, trans);
}

scalar EllipsoidNode::calculateStericInteraction(scalar kst, PointNode *p, Vector3 trans) {
	return ::calculateStericInteraction(kst, p, this, trans);
}

scalar EllipsoidNode::calculateStericInteraction(scalar kst, SphereNode *s, Vector3 trans) {
	return ::calculateStericInteraction(kst, s, this, trans);
}

scalar EllipsoidNode::calculateStericInteraction(scalar kst, EllipsoidNode *e, Vector3 trans) {
	return ::calculateStericInteraction(kst, e, this, trans);
}

scalar EllipsoidNode::calculateVdWInteraction(scalar req, scalar eps, scalar alpha, scalar cutoff, vObject *v, Vector3 trans) {
	return v->calculateVdWInteraction(req, eps, alpha, cutoff, this, trans);
}

scalar EllipsoidNode::calculateVdWInteraction(scalar req, scalar eps, scalar alpha, scalar cutoff, PointNode *p, Vector3 trans) {
	return ::calculateVdWInteraction(req, eps, alpha, cutoff, p, this, trans);
}

scalar EllipsoidNode::calculateVdWInteraction(scalar req, scalar eps, scalar alpha, scalar cutoff, SphereNode *s, Vector3 trans) {
	return ::calculateVdWInteraction(req, eps, alpha, cutoff, s, this, trans);
}

scalar EllipsoidNode::calculateVdWInteraction(scalar req, scalar eps, scalar alpha, scalar cutoff, EllipsoidNode *e, Vector3 trans) {
	return ::calculateVdWInteraction(req, eps, alpha, cutoff, e, this, trans);
}

scalar EllipsoidNode::calculateVolumeOverlap(vObject *v) {
	return v->calculateVolumeOverlap(this);
}

scalar EllipsoidNode::calculateVolumeOverlap(PointNode *p) {
	return ::calculateVolumeOverlap(p, this);
}

scalar EllipsoidNode::calculateVolumeOverlap(SphereNode *s) {
	return ::calculateVolumeOverlap(s, this);
}

scalar EllipsoidNode::calculateVolumeOverlap(EllipsoidNode *e) {
	return ::calculateVolumeOverlap(this, e);
}

void EllipsoidNode::applyTemplate(vTObject *vTO, NetworkParameters *params) {

	// Do the parent method
	vObject::applyTemplate(vTO, params);

	setRadius(stos(vTO->getParameter("radius")));
	setDensity(stos(vTO->getParameter("density")));
	setViscosity(stos(vTO->getParameter("viscosity")));
	setMaxNumConnections(vTO->getMaxNumConnections());

	// Extrinsic properties
	calculateExtrinsicProperties(params);

	if(params->localConnectivitySet) {

		// Get the number of sites
		numChildren = vTO->getNumTSites();
 
		// Set new saturation point
		setMaxNumConnections(numChildren);

		if(numChildren != 0) {

			// Pointer memory
			child = new vObject*[numChildren];

			// Template for copying and object
			vTObject *childTemplate;

			for(int i = 0; i < numChildren; ++i) {

				// An object
				child[i] = new PointInteractionSite();

				// Stuff to set from file
				child[i]->setParameter("index", to_string(i));

				// Pass parent properties down
				child[i]->setReactionRate(getReactionRate());

				// Template and new object
				childTemplate = vTO->getTSite(i);
				child[i]->applyTemplate(childTemplate, params);

				// Link it to this object
				child[i]->setParent(this);
			}

			// Rotation matrix
		//	applyOrientationMatrix(MathFunctions::eulerToZXZ(getOrientation()));
		//	applyOrientationMatrix(getOrientationMatrix());
			Matrix3 m = getOrientationMatrix();
			orientationMatrix.setIdentity();
			applyOrientationMatrix(m);

		}
	}
}

void EllipsoidNode::printDetails() {
	
	calculateEulerAngles();

	cout << endl << endl;
	cout << "Class: SphereNode (" << getStructureDimension() << "D Object)" << endl;
	cout << "\tSub-classes: Sphere" << endl << endl;
	cout << "\tIndex = " << getIndex() << endl;
	cout << "\tDensity = " << getDensity() << ", Viscosity = " << getViscosity() << endl;
	cout << "\tMass = " << getMass() << ", Drag = " << getDrag() << endl;
	cout << "\tRadius = " << getLengthscale() << endl << endl;
	cout << "\tPos = (" << pos[0] << ", " << pos[1] << ", " << pos[2] << ")" << endl;
	cout << "\tVel = (" << vel[0] << ", " << vel[1] << ", " << vel[2] << ")" << endl;
	cout << "\tAcc = (" << acc[0] << ", " << acc[1] << ", " << acc[2] << ")" << endl;
//	cout << "\tOrientation = (" << orientation[0] << ", " << orientation[1] << ", " << orientation[2] << ")" << endl << endl;
	cout << "\tTotal Force = (" << getTotalForce()[0] << ", " << getTotalForce()[1] << ", " << getTotalForce()[2] << ")" << endl;
	cout << "\tTotal Energy = " << getTotalEnergy() << endl;

	cout << "\tMax Connections = " << getMaxNumConnections() << ", Saturated: " << toString(isSaturated()) << endl << endl;

	if(numChildren != 0) {
		cout << "\tChild vObjects:" << endl;
		for(int i = 0; i < numChildren; ++i) {
			child[i]->printDetails();
		}
	}

	cout << "\tTemplate:" << endl;
	cout << "\t\tTemplate Index = " << vTO->getIndex() << endl;
	cout << "\t\tName = " << vTO->getName() << endl;
	cout << "\t\tType = " << vTO->getTypeString() << endl << endl;

	cout << "\tConnected to " << getNumConnections() << " cObjects:" << endl;
	cout << "\t\tIndices: ";
	for(int i = 0; i < getNumConnections(); ++i) {
		cout << getConnection(i)->getIndex() << " ";
	}
	cout << endl;
}

scalar calculateStericInteraction(scalar kst, PointNode *a, PointNode *b, Vector3 trans) {

	// Does nothing
	return 0.0;
}

/*
scalar calculateStericInteraction(scalar kst, PointNode *a, SphereNode *b, Vector3 trans) {

	// Force is infinite potential (zero size is technically zero force for vol overlap, but that doesn't make physical sense. Nothing really has zero physical size)
	scalar d;
	Vector3 sep;

	// Get separation
	sep = a->getPosition() - (b->getPosition() + trans);
	
	// Check size, and move if necessary
	d = sep.norm();
	if(d < b->getRadius()) {
		a->setPosition((b->getPosition() + trans) + sep * (b->getRadius() / d));
	}

	return 0.0;
}
*/

scalar calculateStericInteraction(scalar kst, PointNode *a, SphereNode *b, Vector3 trans) {

	// Abrupt discrete movement will lead to instabilities. Do a k(r-l) thing using the steric k
	scalar r, R, rOverlap, fMag, minR;
	Vector3 rSep, rHat;

	// Force is based on volume overlap and distance overlap (U=0.5k(V^2/A1*A2 + d^2)
	
	//
	// Volume overlap and gradient
	//	

	R = b->getRadius();
		
	// Now, get separation
	rSep = a->getPosition() - (b->getPosition() + trans);

	if(rSep(0) != 0 || rSep(1) != 0 || rSep(2) != 0) {
		r = rSep.norm();
	} else {
		r = 0;
	}

	// Conditional to save on calculation
	if(r > R) {
		return 0.0;
	} else {
		rOverlap = R - r;

		//
		// Force
		//

		// Calculate unit vector
		if(r != 0) {
			rHat = rSep / r;
		} else {

			// But point force in a random direction
			for(int i = 0; i < 3; ++i) {
				rHat(i) = rngCont->getMinorUniformRN();
			}
			rHat /= rHat.norm();
		}

		// Force magnitude and vector
		fMag = kst * rOverlap;
		rHat *= fMag;

		// Add (directional) forces to nodes
		b->addExternalForce(-1 * rHat);
		a->addExternalForce(rHat);

		//
		// Energy (return value)
		//

		return 0.5 * kst * rOverlap*rOverlap;
	}
}

scalar calculateStericInteraction(scalar kst, PointNode *a, EllipsoidNode *b, Vector3 trans) {

	// Force is based on radial overlap, but also volume. Tends to infinity as one of the particles becomes size zero
	scalar d;
	Vector3 sep;

	// Get separation
	sep = a->getPosition() - (b->getPosition() + trans);
	
	// Check size, and move if necessary
	d = sep.norm();
	if(d < b->getRadius()) {
		a->setPosition((b->getPosition() + trans) + sep * (b->getRadius() / d));
	}

	return 0.0;
}

scalar calculateStericInteraction(scalar kst, SphereNode *a, SphereNode *b, Vector3 trans) {

	scalar r, R1, R2, R1pR2, R1mR2, V, dVdr, fMag, minR;
	Vector3 rSep, rHat;

	// Force is based on volume overlap and distance overlap (U=0.5k(V^2/A1*A2 + d^2)
	
	//
	// Volume overlap and gradient
	//	

	R1 = a->getRadius();
	R2 = b->getRadius();
		
	// Now, get separation
	rSep = a->getPosition() - (b->getPosition() + trans);

	/*
	cout << a->getIndex() << ", " << b->getIndex() << ", " << a->getPosition()[0] << ", " << b->getPosition()[0] << ", " << trans[0] << ", " << rSep[0] << endl;
	if (a->getIndex() == 6 && b->getIndex() == 7) {
		cout << a->getPosition()[0] << ", " << b->getPosition()[0] << ", " << trans[0] << ", " << rSep[0] << endl;
		exit(0);
	}
	if (b->getIndex() == 6 && a->getIndex() == 7) {
		cout << a->getPosition()[0] << ", " << b->getPosition()[0] << ", " << trans[0] << ", " << rSep[0] << endl;
		exit(0);
	}
	*/	
	if(rSep(0) != 0 || rSep(1) != 0 || rSep(2) != 0) {
		r = rSep.norm();
	} else {
		r = 0;
	}

	// Get some of the combinations we'll need
	R1pR2 = R1 + R2;
	R1mR2 = R1 - R2;

	// Conditional to save on calculation (the maximum overlap check was performed at the SimulationBox operator level i.e. there will always be a non-zero force calculated here)
	if (r > fabs(R1mR2)) {
		if(R1 == R2) {
			V = (M_PI / 6.0) * (2 * R1pR2 + r) * (R1pR2 - r) * (R1pR2 - r);
			dVdr = (M_PI / 2.0) * (R1pR2 + r) * (R1pR2 - r);
		} else {
			V = (M_PI / (6.0 * r)) * (r * r + 2 * r * R1pR2 - 3 * R1mR2 * R1mR2) * (R1pR2 - r) * (R1pR2 - r);
//			dVdr = (M_PI / (2.0 * r * r)) * (R1mR2 + r) * (R1mR2 - r) * (R1pR2 + r) * (R1pR2 - r);
//			dVdr = (M_PI / (2.0 * r * r)) * (R1mR2 + r) * -1 * (R1mR2 - r) * (R1pR2 + r) * (R1pR2 - r);	// The -1 is correct!
			dVdr = fabs((M_PI / (2.0 * r * r)) * (R1mR2 + r) * (R1mR2 - r) * (R1pR2 + r) * (R1pR2 - r));

//			cout << "V: " << V << ", dVdr: " << dVdr << ", E, " << 0.5 * kst * V * V / pow(min(a->getCrossSection(), b->getCrossSection()), 2) << endl; 
		}

	} else {
		minR = min(R1,R2);
		V = (8.0 / 3.0) * M_PI * minR * minR * minR;		// Overlap is double to account for deformation in both spheres
		dVdr = 0;
//		cout << a->getIndex() << ", " << b->getIndex() << endl;
//		cout << a->getPosition() << endl;
//		cout << b->getPosition() << endl; 
//		cout << "V: " << V << ", dVdr: " << dVdr << ", E, " << 0.5 * kst * V * V / pow(min(a->getCrossSection(), b->getCrossSection()), 2) << endl; 
	}

	//
	// Force
	//

	// Calculate unit vector
	if(r != 0) {
		rHat = rSep / r;
	} else {

		// But point force in a random direction
		for(int i = 0; i < 3; ++i) {
			rHat(i) = rngCont->getMinorUniformRN();
		}
		rHat /= rHat.norm();
	}

	// Force magnitude and vector
	fMag = kst * V * dVdr / pow(min(a->getCrossSection(), b->getCrossSection()), 2);
//	fMag = kst * V * dVdr / (a->getCrossSection() * b->getCrossSection());
	rHat *= fMag;

	// Add (directional) forces to nodes
	b->addExternalForce(-1 * rHat);
	a->addExternalForce(rHat);

	//
	// Energy (return value)
	//
//	return 0.5 * kst * V * V / (a->getCrossSection() * b->getCrossSection());
	return 0.5 * kst * V * V / pow(min(a->getCrossSection(), b->getCrossSection()), 2);
}

scalar calculateStericInteraction(scalar kst, SphereNode *a, EllipsoidNode *b, Vector3 trans) {

	scalar eM, fMag, VMOA, A, rpR;
	Vector3 rHat;
	Vector3 VOP;

	// Force is based on volume overlap and distance overlap (U=0.5k(V^2/A1*A2 + d^2)
	
	//
	// Volume overlap and gradient
	//	
	VOP = calculateVolumeOverlapParameters(a, b, trans);
//	cout << "Center to Center distance: " << VOP(0) << endl;
//	cout << "Volume Overlap: " << VOP(1) << endl;
//	cout << "Volume Overlap Gradient: " << VOP(2) << endl;

	// Do we even need to do the rest of this calculation?
	if(VOP(1) <= 0) {
		return 0.0;
	}

	//
	// Spring constant (make this a lookup table at some point)
	//


	// Get maximum overlap and appropriate cross-section
	if(a->getRadius() < b->getRadius()) {
		A = a->getCrossSection();
		VMOA = 2 * a->getVolume() / A;
	} else {
		A = b->getCrossSection();
		VMOA = 2 * b->getVolume() / A;
	}


	// Calculate overlap distance
	rpR = a->getRadius() + b->getRadius();

	//
	// Force
	//

	// Calculate unit vector
	if(VOP(0) == 0) {

		// But point force in a random direction
		for(int i = 0; i < 3; ++i) {
			rHat(i) = rngCont->getMinorUniformRN();
//			rHat(i) = GlobalVariables::uniformDistribution(GlobalVariables::rng);
		}
		rHat /= rHat.norm();
	} else {
		rHat = a->getPosition() - (b->getPosition() + trans);
		rHat /= VOP(0);
	}

	// Force magnitude
	fMag = kst * ( VOP(1) * VOP(2) / (A * A));// + (rpR - VOP(0)));

	// Add (directional) forces to nodes
	rHat *= fMag;
	b->addExternalForce(-1 * rHat);
	a->addExternalForce(rHat);

	//
	// Energy (return value)
	//
//	cout << "E: " << 0.5 * kst * (pow(VOP(1) / A, 2)) << endl;
//	cout << "F: " << fMag << endl;
//	exit(0);
	return 0.5 * kst * (pow(VOP(1) / A, 2));// + pow(rpR - VOP(0), 2));		
}

scalar calculateStericInteraction(scalar kst, EllipsoidNode *a, EllipsoidNode *b, Vector3 trans) {

	scalar eM, fMag, VMOA, A, rpR;
	Vector3 rHat;
	Vector3 VOP;

	// Force is based on volume overlap and distance overlap (U=0.5k(V^2/A1*A2 + d^2)
	
	//
	// Volume overlap and gradient
	//	
	VOP = calculateVolumeOverlapParameters(a, b, trans);
//	cout << "Center to Center distance: " << VOP(0) << endl;
//	cout << "Volume Overlap: " << VOP(1) << endl;
//	cout << "Volume Overlap Gradient: " << VOP(2) << endl;

	// Do we even need to do the rest of this calculation?
	if(VOP(1) <= 0) {
		return 0.0;
	}

	//
	// Spring constant (make this a lookup table at some point)
	//


	// Get maximum overlap and appropriate cross-section
	if(a->getRadius() < b->getRadius()) {
		A = a->getCrossSection();
		VMOA = 2 * a->getVolume() / A;
	} else {
		A = b->getCrossSection();
		VMOA = 2 * b->getVolume() / A;
	}


	// Calculate overlap distance
	rpR = a->getRadius() + b->getRadius();

	//
	// Force
	//

	// Calculate unit vector
	if(VOP(0) == 0) {

		// But point force in a random direction
		for(int i = 0; i < 3; ++i) {
			rHat(i) = rngCont->getMinorUniformRN();
//			rHat(i) = GlobalVariables::uniformDistribution(GlobalVariables::rng);
		}
		rHat /= rHat.norm();
	} else {
		rHat = a->getPosition() - (b->getPosition() + trans);
		rHat /= VOP(0);
	}

	// Force magnitude
	fMag = kst * ( VOP(1) * VOP(2) / (A * A));// + (rpR - VOP(0)));

	// Add (directional) forces to nodes
	rHat *= fMag;
	b->addExternalForce(-1 * rHat);
	a->addExternalForce(rHat);

	//
	// Energy (return value)
	//
//	cout << "E: " << 0.5 * kst * (pow(VOP(1) / A, 2)) << endl;
//	cout << "F: " << fMag << endl;
//	exit(0);
	return 0.5 * kst * (pow(VOP(1) / A, 2));// + pow(rpR - VOP(0), 2));		
}

//
// These currently differ only by the distance, r, that is used. Leaving separate, though, in case it changes in the future to
// take the full surface into account
//
scalar calculateVdWInteraction(scalar req, scalar eps, scalar alpha, scalar cutoff, PointNode *a, PointNode *b, Vector3 trans) {
	
	// Force is directly between the two points
	Vector3 rHat;
	scalar r;
	scalar fMag, energy;

	// We need the scalar distance between the two surfaces and a direction to apply the force
	rHat = a->getPosition() - (b->getPosition() + trans);
	r = rHat.norm();
	
	// Cutoff check
	if (r > cutoff) {
		return 0.0;
	}
	
	rHat /= r;	
	if(r <= req) {

		//
		// Interpolation (spring potential through origin sets all soft parameters)
		//

		scalar k;

		// Spring constant
		k = 2 * eps / (req * req);

		// Force
		fMag = k * (req - r);

		// Energy
		energy = 0.5 * k * pow(r - req, 2) - eps;

	} else {

		//
		// Lennard-Jones
		//
		scalar reqonralpha, reqonr2alpha;

		// Force
		reqonralpha = pow(req / r, alpha);
		reqonr2alpha = reqonralpha * reqonralpha;

		fMag = (2*alpha) * eps * (reqonr2alpha - reqonralpha) / r;

		// Energy
		energy = eps * (reqonr2alpha - 2 * reqonralpha);

	}

	// Add (directional) forces to nodes
	rHat *= fMag;
	b->addExternalForce(-1 * rHat);
	a->addExternalForce(rHat);

	// Energy (return value)
	return energy;
}

scalar calculateVdWInteraction(scalar req, scalar eps, scalar alpha, scalar cutoff, PointNode *a, SphereNode *b, Vector3 trans) {

	// Force is directly between the point and the nearest surface point
	Vector3 rHat;
	scalar r;
	scalar fMag, energy;

	// We need the scalar distance between the two surfaces and a direction to apply the force
	rHat = a->getPosition() - (b->getPosition() + trans);
	r = rHat.norm();

	rHat /= r;	
	r -= b->getRadius();

	// Cutoff check
	if (r > cutoff) {
		return 0.0;
	}
		
	if(r <= req) {

		//
		// Interpolation (spring potential through origin sets all soft parameters)
		//

		scalar k;

		// Spring constant
		k = 2 * eps / (req * req);

		// Force
		fMag = k * (req - r);

		// Energy
		energy = 0.5 * k * pow(r - req, 2) - eps;

	} else {

		//
		// Lennard-Jones
		//
		scalar reqonralpha, reqonr2alpha;

		// Force
		reqonralpha = pow(req / r, alpha);
		reqonr2alpha = reqonralpha * reqonralpha;

		fMag = (2 * alpha) * eps * (reqonr2alpha - reqonralpha) / r;

		// Energy
		energy = eps * (reqonr2alpha - 2 * reqonralpha);

	}

	// Add (directional) forces to nodes
	rHat *= fMag;
	b->addExternalForce(-1 * rHat);
	a->addExternalForce(rHat);

	// Energy (return value)
	return energy;
}

scalar calculateVdWInteraction(scalar req, scalar eps, scalar alpha, scalar cutoff, PointNode *a, EllipsoidNode *b, Vector3 trans) {

	// Force is directly between the point and the nearest surface point
	Vector3 rHat;
	scalar r;
	scalar fMag, energy;

	// We need the scalar distance between the two surfaces and a direction to apply the force
	rHat = a->getPosition() - (b->getPosition() + trans);
	r = rHat.norm();
	rHat /= r;	
	r -= b->getRadius();

	// Cutoff check
	if (r > cutoff) {
		return 0.0;
	}
	
	if(r <= req) {

		//
		// Interpolation (spring potential through origin sets all soft parameters)
		//

		scalar k;

		// Spring constant
		k = 2 * eps / (req * req);

		// Force
		fMag = k * (req - r);

		// Energy
		energy = 0.5 * k * pow(r - req, 2) - eps;

	} else {

		//
		// Lennard-Jones
		//
		scalar reqonralpha, reqonr2alpha;

		// Force
		reqonralpha = pow(req / r, alpha);
		reqonr2alpha = reqonralpha * reqonralpha;

		fMag = (2 * alpha) * eps * (reqonr2alpha - reqonralpha) / r;

		// Energy
		energy = eps * (reqonr2alpha - 2 * reqonralpha);

	}

	// Add (directional) forces to nodes
	rHat *= fMag;
	b->addExternalForce(-1 * rHat);
	a->addExternalForce(rHat);

	// Energy (return value)
	return energy;
}

scalar calculateVdWInteraction(scalar req, scalar eps, scalar alpha, scalar cutoff, SphereNode *a, SphereNode *b, Vector3 trans) {

	// Force is directly between the two nearest surface points
	Vector3 rHat;
	scalar r;
	scalar fMag, energy;

	// We need the scalar distance between the two surfaces and a direction to apply the force
	rHat = a->getPosition() - (b->getPosition() + trans);
	r = rHat.norm();
	rHat /= r;	
	r -= a->getRadius() + b->getRadius();

	// Cutoff check
	if (r > cutoff) {
		return 0.0;
	}
	

	if(r <= req) {

		//
		// Interpolation (spring potential through origin sets all soft parameters)
		//

		scalar k;

		// Spring constant
		k = 2 * eps / (req * req);

		// Force
		fMag = k * (req - r);

		energy = 0.5 * k * pow(req - r, 2) - eps;

	} else {

		//
		// Lennard-Jones
		//
		scalar reqonralpha, reqonr2alpha;

		// Force
		reqonralpha = pow(req / r, alpha);
		reqonr2alpha = reqonralpha * reqonralpha;

		fMag = (2 * alpha) * eps * (reqonr2alpha - reqonralpha) / r;

		// Energy
		energy = eps * (reqonr2alpha - 2 * reqonralpha);
	}

	// Add (directional) forces to nodes
	rHat *= fMag;
	b->addExternalForce(-1 * rHat);
	a->addExternalForce(rHat);

	// Energy (return value)
	return energy;
}

scalar calculateVdWInteraction(scalar req, scalar eps, scalar alpha, scalar cutoff, SphereNode *a, EllipsoidNode *b, Vector3 trans) {

	// Force is directly between the two nearest surface points
	Vector3 rHat;
	scalar r;
	scalar fMag, energy;

	// We need the scalar distance between the two surfaces and a direction to apply the force
	rHat = a->getPosition() - (b->getPosition() + trans);
	r = rHat.norm();
	rHat /= r;	
	r -= a->getRadius() + b->getRadius();

	// Cutoff check
	if (r > cutoff) {
		return 0.0;
	}

	if(r <= req) {

		//
		// Interpolation (spring potential through origin sets all soft parameters)
		//

		scalar k;

		// Spring constant
		k = 2 * eps / (req * req);

		// Force
		fMag = k * (req - r);

		energy = 0.5 * k * pow(req - r, 2) - eps;

	} else {

		//
		// Lennard-Jones
		//
		scalar reqonralpha, reqonr2alpha;

		// Force
		reqonralpha = pow(req / r, alpha);
		reqonr2alpha = reqonralpha * reqonralpha;

		fMag = (2 * alpha) * eps * (reqonr2alpha - reqonralpha) / r;

		// Energy
		energy = eps * (reqonr2alpha - 2 * reqonralpha);
	}

	// Add (directional) forces to nodes
	rHat *= fMag;
	b->addExternalForce(-1 * rHat);
	a->addExternalForce(rHat);

	// Energy (return value)
	return energy;
}

scalar calculateVdWInteraction(scalar req, scalar eps, scalar alpha, scalar cutoff, EllipsoidNode *a, EllipsoidNode *b, Vector3 trans) {

	// Force is directly between the two nearest surface points
	Vector3 rHat;
	scalar r;
	scalar fMag, energy;

	// We need the scalar distance between the two surfaces and a direction to apply the force
	rHat = a->getPosition() - (b->getPosition() + trans);
	r = rHat.norm();
	rHat /= r;	
	r -= a->getRadius() + b->getRadius();

	// Cutoff check
	if (r > cutoff) {
		return 0.0;
	}
	
	if(r <= req) {

		//
		// Interpolation (spring potential through origin sets all soft parameters)
		//

		scalar k;

		// Spring constant
		k = 2 * eps / (req * req);

		// Force
		fMag = k * (req - r);

		energy = 0.5 * k * pow(req - r, 2) - eps;

	} else {

		//
		// Lennard-Jones
		//
		scalar reqonralpha, reqonr2alpha;

		// Force
		reqonralpha = pow(req / r, alpha);
		reqonr2alpha = reqonralpha * reqonralpha;

		fMag = (2 * alpha) * eps * (reqonr2alpha - reqonralpha) / r;

		// Energy
		energy = eps * (reqonr2alpha - 2 * reqonralpha);
	}

	// Add (directional) forces to nodes
	rHat *= fMag;
	b->addExternalForce(-1 * rHat);
	a->addExternalForce(rHat);

	// Energy (return value)
	return energy;
}

scalar calculateVolumeOverlap(PointNode *a, PointNode *b) {
	return 0.0;
}

scalar calculateVolumeOverlap(PointNode *a, SphereNode *b) {
	return 0.0;
}

scalar calculateVolumeOverlap(PointNode *a, EllipsoidNode *b) {
	return 0.0;
}

scalar calculateVolumeOverlap(SphereNode *a, SphereNode *b) {

	scalar r, R, d, d2, rpR;
	scalar minD;
	Vector3 sep;

	// Firstly, find out which radius is smaller
	if(a->getRadius() < b->getRadius()) {
		r = a->getRadius();
		R = b->getRadius();
	} else {
		r = b->getRadius();
		R = a->getRadius();
	}
	
	// Lower limiit (to avoid infinities)
	minD = R - r;

	// Now, get separation
	sep = a->getPosition() - b->getPosition();
	if(sep(0) == 0 && sep(1) == 0 && sep(2) == 0) {
		d = 0;
	} else {
		d = sep.norm();
	}

	rpR = a->getRadius() + b->getRadius();

	// Conditional to save on calculation (also overlap is double to take into account compression of both spheres)
	if(d >= rpR) {
		return 0.0;
	} else if (d <= minD) {
		return (8.0 / 3.0) * M_PI * pow(r, 3);
	} else if (r == R) {
		return (M_PI / 6.0) * pow(2 * R - d, 2) * (d + 4 * R);
	} else {
		d2 = d * d;
		//return (M_PI / (12.0 * d)) * ((d + 3 * r) * (d - r) + R * (2 * d + 6 * r - 3 * R));
		return (M_PI / (6.0 * d)) * (rpR - d) * (rpR - d) * (d2 + 2 * d * rpR - 3 * minD * minD);	
	}
}

scalar calculateVolumeOverlap(SphereNode *a, EllipsoidNode *b) {

	scalar r, R, d, d2, rpR;
	scalar minD;
	Vector3 sep;

	// Firstly, find out which radius is smaller
	if(a->getRadius() < b->getRadius()) {
		r = a->getRadius();
		R = b->getRadius();
	} else {
		r = b->getRadius();
		R = a->getRadius();
	}
	
	// Lower limiit (to avoid infinities)
	minD = R - r;

	// Now, get separation
	sep = a->getPosition() - b->getPosition();
	if(sep(0) == 0 && sep(1) == 0 && sep(2) == 0) {
		d = 0;
	} else {
		d = sep.norm();
	}

	rpR = a->getRadius() + b->getRadius();

	// Conditional to save on calculation (also overlap is double to take into account compression of both spheres)
	if(d >= rpR) {
		return 0.0;
	} else if (d <= minD) {
		return (8.0 / 3.0) * M_PI * pow(r, 3);
	} else if (r == R) {
		return (M_PI / 6.0) * pow(2 * R - d, 2) * (d + 4 * R);
	} else {
		d2 = d * d;
		//return (M_PI / (12.0 * d)) * ((d + 3 * r) * (d - r) + R * (2 * d + 6 * r - 3 * R));
		return (M_PI / (6.0 * d)) * (rpR - d) * (rpR - d) * (d2 + 2 * d * rpR - 3 * minD * minD);	
	}
}

scalar calculateVolumeOverlap(EllipsoidNode *a, EllipsoidNode *b) {

	scalar r, R, d, d2, rpR;
	scalar minD;
	Vector3 sep;

	// Firstly, find out which radius is smaller
	if(a->getRadius() < b->getRadius()) {
		r = a->getRadius();
		R = b->getRadius();
	} else {
		r = b->getRadius();
		R = a->getRadius();
	}
	
	// Lower limiit (to avoid infinities)
	minD = R - r;

	// Now, get separation
	sep = a->getPosition() - b->getPosition();
	if(sep(0) == 0 && sep(1) == 0 && sep(2) == 0) {
		d = 0;
	} else {
		d = sep.norm();
	}

	rpR = a->getRadius() + b->getRadius();

	// Conditional to save on calculation (also overlap is double to take into account compression of both spheres)
	if(d >= rpR) {
		return 0.0;
	} else if (d <= minD) {
		return (8.0 / 3.0) * M_PI * pow(r, 3);
	} else if (r == R) {
		return (M_PI / 6.0) * pow(2 * R - d, 2) * (d + 4 * R);
	} else {
		d2 = d * d;
		//return (M_PI / (12.0 * d)) * ((d + 3 * r) * (d - r) + R * (2 * d + 6 * r - 3 * R));
		return (M_PI / (6.0 * d)) * (rpR - d) * (rpR - d) * (d2 + 2 * d * rpR - 3 * minD * minD);	
	}
}

scalar calculateVolumeOverlapGradient(PointNode *a, PointNode *b) {
	return 0.0;
}

scalar calculateVolumeOverlapGradient(PointNode *a, SphereNode *b) {
	return 0.0;
}

scalar calculateVolumeOverlapGradient(PointNode *a, EllipsoidNode *b) {
	return 0.0;
}

scalar calculateVolumeOverlapGradient(SphereNode *a, SphereNode *b) {

	scalar r, R, r2, R2, d, d2, rpR;
	scalar minD;
	Vector3 sep;

	// Firstly, find out which radius is smaller
	if(a->getRadius() < b->getRadius()) {
		r = a->getRadius();
		R = b->getRadius();
	} else {
		r = b->getRadius();
		R = a->getRadius();
	}
	
	// Lower limiit (to avoid infinities)
	minD = R - r;

	// Now, get separation
	sep = a->getPosition() - b->getPosition();
	if(sep(0) == 0 && sep(1) == 0 && sep(2) == 0) {
		d = 0;
	} else {
		d = sep.norm();
	}

	rpR = a->getRadius() + b->getRadius();

	// Conditional to save on calculation
	if(d >= rpR) {
		return 0.0;
	} else if (d <= minD) {
		return 0.0;
	} else {
		d2 = d * d;
		R2 = R * R;
		if (r == R) {
			//return M_PI * (d/2.0 - R) * (d/2.0 + R);
			return 0.5 * M_PI * (d2 - 4 * R2);
		} else {
			r2 = r * r;
			return (M_PI / 2.0) * (d2 - 2 * (r2 + R2) + pow((r2 - R2) / d, 2));
		}
	}
}

scalar calculateVolumeOverlapGradient(SphereNode *a, EllipsoidNode *b) {

	scalar r, R, r2, R2, d, d2, rpR;
	scalar minD;
	Vector3 sep;

	// Firstly, find out which radius is smaller
	if(a->getRadius() < b->getRadius()) {
		r = a->getRadius();
		R = b->getRadius();
	} else {
		r = b->getRadius();
		R = a->getRadius();
	}
	
	// Lower limiit (to avoid infinities)
	minD = R - r;

	// Now, get separation
	sep = a->getPosition() - b->getPosition();
	if(sep(0) == 0 && sep(1) == 0 && sep(2) == 0) {
		d = 0;
	} else {
		d = sep.norm();
	}

	rpR = a->getRadius() + b->getRadius();

	// Conditional to save on calculation
	if(d >= rpR) {
		return 0.0;
	} else if (d <= minD) {
		return 0.0;
	} else {
		d2 = d * d;
		R2 = R * R;
		if (r == R) {
			//return M_PI * (d/2.0 - R) * (d/2.0 + R);
			return 0.5 * M_PI * (d2 - 4 * R2);
		} else {
			r2 = r * r;
			return (M_PI / 2.0) * (d2 - 2 * (r2 + R2) + pow((r2 - R2) / d, 2));
		}
	}
}

scalar calculateVolumeOverlapGradient(EllipsoidNode *a, EllipsoidNode *b) {

	scalar r, R, r2, R2, d, d2, rpR;
	scalar minD;
	Vector3 sep;

	// Firstly, find out which radius is smaller
	if(a->getRadius() < b->getRadius()) {
		r = a->getRadius();
		R = b->getRadius();
	} else {
		r = b->getRadius();
		R = a->getRadius();
	}
	
	// Lower limiit (to avoid infinities)
	minD = R - r;

	// Now, get separation
	sep = a->getPosition() - b->getPosition();
	if(sep(0) == 0 && sep(1) == 0 && sep(2) == 0) {
		d = 0;
	} else {
		d = sep.norm();
	}

	rpR = a->getRadius() + b->getRadius();

	// Conditional to save on calculation
	if(d >= rpR) {
		return 0.0;
	} else if (d <= minD) {
		return 0.0;
	} else {
		d2 = d * d;
		R2 = R * R;
		if (r == R) {
			//return M_PI * (d/2.0 - R) * (d/2.0 + R);
			return 0.5 * M_PI * (d2 - 4 * R2);
		} else {
			r2 = r * r;
			return (M_PI / 2.0) * (d2 - 2 * (r2 + R2) + pow((r2 - R2) / d, 2));
		}
	}
}

Vector3 calculateVolumeOverlapParameters(PointNode *a, PointNode *b) {
	Vector3 v;
	v.setZero();
	return v;
}

Vector3 calculateVolumeOverlapParameters(PointNode *a, SphereNode *b) {
	Vector3 v;
	v.setZero();
	return v;
}

Vector3 calculateVolumeOverlapParameters(PointNode *a, EllipsoidNode *b) {
	Vector3 v;
	v.setZero();
	return v;
}

Vector3 calculateVolumeOverlapParameters(SphereNode *a, SphereNode *b, Vector3 trans) {

	scalar r, R, r2, R2, d2, rpR;
	scalar minD;
	Vector3 sep;

	Vector3 VOP;

	// Firstly, find out which radius is smaller
	if(a->getRadius() < b->getRadius()) {
		r = a->getRadius();
		R = b->getRadius();
	} else {
		r = b->getRadius();
		R = a->getRadius();
	}
	
	// Lower limiit (to avoid infinities)
	minD = R - r;

	// Now, get separation
	VOP.setZero();
	sep = a->getPosition() - (b->getPosition() + trans);
//	cout << endl << "a:" << a->getPosition()(0) << ", " << a->getPosition()(1) << ", " << a->getPosition()(2) << endl;
//	cout << "b:" << b->getPosition()(0) << ", " << b->getPosition()(1) << ", " << b->getPosition()(2) << endl;
//	cout << "sep:" << sep(0) << ", " << sep(1) << ", " << sep(2) << endl;
	if(sep(0) == 0 && sep(1) == 0 && sep(2) == 0) {
		VOP(0) = 0;
	} else {
		VOP(0) = sep.norm();
	}

	
	rpR = a->getRadius() + b->getRadius();

	// Conditional to save on calculation
	if(VOP(0) >= rpR) {
		// Already zeroed

	} else if (VOP(0) <= minD) {
		VOP(1) = (8.0 / 3.0) * M_PI * pow(r, 3);
	} else {
		d2 = VOP(0) * VOP(0);
		R2 = R * R;
		if (r == R) {
			VOP(1) = (M_PI / 6.0) * pow(2 * R - VOP(0), 2) * (4 * R + VOP(0));
			VOP(2) = 0.5 * M_PI * (4 * R2 - d2);
		} else {
			r2 = r * r;
			VOP(1) = (M_PI / (6.0 * VOP(0))) * pow(rpR - VOP(0), 2) * (d2 + 2 * VOP(0) * rpR - 3 * minD * minD);
			VOP(2) = (M_PI / 2.0) * (d2 - 2 * (r2 + R2) + pow((r2 - R2) / VOP(0), 2));	
		}
	}

	return VOP;
}

Vector3 calculateVolumeOverlapParameters(SphereNode *a, EllipsoidNode *b, Vector3 trans) {

	scalar r, R, r2, R2, d2, rpR;
	scalar minD;
	Vector3 sep;

	Vector3 VOP;

	// Firstly, find out which radius is smaller
	if(a->getRadius() < b->getRadius()) {
		r = a->getRadius();
		R = b->getRadius();
	} else {
		r = b->getRadius();
		R = a->getRadius();
	}
	
	// Lower limiit (to avoid infinities)
	minD = R - r;

	// Now, get separation
	VOP.setZero();
	sep = a->getPosition() - (b->getPosition() + trans);
//	cout << endl << "a:" << a->getPosition()(0) << ", " << a->getPosition()(1) << ", " << a->getPosition()(2) << endl;
//	cout << "b:" << b->getPosition()(0) << ", " << b->getPosition()(1) << ", " << b->getPosition()(2) << endl;
//	cout << "sep:" << sep(0) << ", " << sep(1) << ", " << sep(2) << endl;
	if(sep(0) == 0 && sep(1) == 0 && sep(2) == 0) {
		VOP(0) = 0;
	} else {
		VOP(0) = sep.norm();
	}

	
	rpR = a->getRadius() + b->getRadius();

	// Conditional to save on calculation
	if(VOP(0) >= rpR) {
		// Already zeroed

	} else if (VOP(0) <= minD) {
		VOP(1) = (8.0 / 3.0) * M_PI * pow(r, 3);
	} else {
		d2 = VOP(0) * VOP(0);
		R2 = R * R;
		if (r == R) {
			VOP(1) = (M_PI / 6.0) * pow(2 * R - VOP(0), 2) * (4 * R + VOP(0));
			VOP(2) = 0.5 * M_PI * (4 * R2 - d2);
		} else {
			r2 = r * r;
			VOP(1) = (M_PI / (6.0 * VOP(0))) * pow(rpR - VOP(0), 2) * (d2 + 2 * VOP(0) * rpR - 3 * minD * minD);
			VOP(2) = (M_PI / 2.0) * (d2 - 2 * (r2 + R2) + pow((r2 - R2) / VOP(0), 2));	
		}
	}

	return VOP;
}

Vector3 calculateVolumeOverlapParameters(EllipsoidNode *a, EllipsoidNode *b, Vector3 trans) {

	scalar r, R, r2, R2, d2, rpR;
	scalar minD;
	Vector3 sep;

	Vector3 VOP;

	// Firstly, find out which radius is smaller
	if(a->getRadius() < b->getRadius()) {
		r = a->getRadius();
		R = b->getRadius();
	} else {
		r = b->getRadius();
		R = a->getRadius();
	}
	
	// Lower limiit (to avoid infinities)
	minD = R - r;

	// Now, get separation
	VOP.setZero();
	sep = a->getPosition() - (b->getPosition() + trans);
//	cout << endl << "a:" << a->getPosition()(0) << ", " << a->getPosition()(1) << ", " << a->getPosition()(2) << endl;
//	cout << "b:" << b->getPosition()(0) << ", " << b->getPosition()(1) << ", " << b->getPosition()(2) << endl;
//	cout << "sep:" << sep(0) << ", " << sep(1) << ", " << sep(2) << endl;
	if(sep(0) == 0 && sep(1) == 0 && sep(2) == 0) {
		VOP(0) = 0;
	} else {
		VOP(0) = sep.norm();
	}

	
	rpR = a->getRadius() + b->getRadius();

	// Conditional to save on calculation
	if(VOP(0) >= rpR) {
		// Already zeroed

	} else if (VOP(0) <= minD) {
		VOP(1) = (8.0 / 3.0) * M_PI * pow(r, 3);
	} else {
		d2 = VOP(0) * VOP(0);
		R2 = R * R;
		if (r == R) {
			VOP(1) = (M_PI / 6.0) * pow(2 * R - VOP(0), 2) * (4 * R + VOP(0));
			VOP(2) = 0.5 * M_PI * (4 * R2 - d2);
		} else {
			r2 = r * r;
			VOP(1) = (M_PI / (6.0 * VOP(0))) * pow(rpR - VOP(0), 2) * (d2 + 2 * VOP(0) * rpR - 3 * minD * minD);
			VOP(2) = (M_PI / 2.0) * (d2 - 2 * (r2 + R2) + pow((r2 - R2) / VOP(0), 2));	
		}
	}

	return VOP;
}
