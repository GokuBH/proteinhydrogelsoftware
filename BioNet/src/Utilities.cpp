#include "Utilities.hpp"

string getCurrentTime() {
	
	time_t now = time(0);
	string str(ctime(&now));	
	return str;
}

string getCurrentDateTime() {

	time_t now = time(0);
	struct tm tstruct;
	char buf[80];
	tstruct = *localtime(&now);
	strftime(buf, sizeof(buf), "%Y-%m-%d.%X", &tstruct);
	
	return buf;
}

string incrementFileName(bfs::path p) {

	// Use boost
//	bfs::path p{fName};
	string base, stem, ext;
	vector<string> stemBits;
	int i, index;

	// Get bits
	base = getAbsolutePath(p.parent_path().string());
	stem = p.stem().filename().string();
	ext = p.extension().filename().string();

	// Check which index we're on
	stemBits.clear();
	boost::split(stemBits, stem, boost::is_any_of("-"));
	if(stemBits.size() == 1) {

		// Just add suffix
		stem += "-1";
	} else {

		// Rebuild stem
		stem = "";
		for(i = 0; i < stemBits.size() - 1; ++i) {
			stem += stemBits.at(i) + "-";
		}
		stem = stem.substr(0, stem.size() - 1);
		
		// Try to get index
		try {
			index = stoi(stemBits.at(i)) + 1;
		} catch(exception& e) {
			stem += "-" + stemBits.at(i);
			index = 1;
		}

		stem += "-" + to_string(index);
	}

	return base + "/" + stem + ext;
}


bfs::path getRelativePath(bfs::path a, bfs::path b) {

	// Use boost
	return bfs::relative(a, b.parent_path());	
}

string getAbsolutePath(bfs::path p) {

	// Use boost to make absolute
//	bfs::path p{s};
	bfs::path abs;
	string absFname = "";

	// Check path exists
	if(!bfs::exists(p)) {

		// Doesn't exist. Does the directory exist?
		bfs::path dir = p.parent_path();
		if(!bfs::is_directory(dir)) {
			dir = bfs::path(".");
		}
		// Assume we're writing to this file and concatenate everything (taking OS into account)
		abs = bfs::canonical(dir, ".");
		#ifdef BOOST_OS_MACOS
			absFname = abs.string() + "/" +  p.filename().string();
		#elif BOOST_OS_LINUX
			absFname = abs.string() + "/" +  p.filename().string();
		#elif BOOST_OS_WINDOWS
			absFname = abs.string() + "\\" +  p.filename().string();
		#else
			absFname = "";
		#endif

	} else {

		// Return absolute
		abs = bfs::canonical(p, ".");
		absFname = abs.string(); 
	}

	return absFname;
}

bool fileExists(string fname) {
	ifstream fin(fname.c_str());
	return fin.good();
}

int findStringInFile(ifstream &fin, string toFind) {
	
	string line;
	int start, pos = 0;

	// Search line by line
	start = fin.tellg();
	while(fin.good() && pos != string::npos) {
		getline(fin, line);
		pos = line.find(toFind);
	}

	// Reset file and return
	pos = fin.tellg();
	return pos;
}

int findSubtringInString(string fullString, string subString) {
	
	return fullString.find(subString);
}

string readFileAsString(ifstream &fin) {

	string fileString;

	// Reserve memory up front for speed
	fin.seekg(0, ios::end);
	fileString.reserve(fin.tellg());
	fin.seekg(0, ios::beg);

	// Read it all immediately
	fileString.assign(istreambuf_iterator<char>(fin), istreambuf_iterator<char>());

	return fileString;
}

vector<string> convertStringToLines(string fullString) {

	vector<string> stringLines;

	// Split by '\n'
	boost::split(stringLines, fullString, boost::is_any_of("\n"));
	return stringLines;
}

// String stuff
Vector2 stringToVector2(string s) {
	int i;
	vector<string> sVec;
	Vector2 v;

	boost::split(sVec, s, boost::is_any_of(","));

	// Check for failure
	if(v.size() != 2) {
		throw invalid_argument("String contained " + to_string(v.size()) + " elements instead of 2.");
	}

	
	for(i = 0; i < 2; ++i) {
		v(i) = ProcessMaths(sVec.at(i));
	}

	return v;
}

Vector2i stringToVector2i(string s) {
	int i;
	vector<string> sVec;
	Vector2i v;

	boost::split(sVec, s, boost::is_any_of(","));

	// Check for failure
	if(v.size() != 2) {
		throw invalid_argument("String contained " + to_string(v.size()) + " elements instead of 2.");
	}

	
	for(i = 0; i < 2; ++i) {
		v(i) = (int)ProcessMaths(sVec.at(i));
	}

	return v;
}

Vector3 stringToVector3(string s) {
	int i;
	vector<string> sVec;
	Vector3 v;

	boost::split(sVec, s, boost::is_any_of(","));

	// Check for failure
	if(v.size() != 3) {
		throw invalid_argument("String contained " + to_string(v.size()) + " elements instead of 3.");
	}

	for(i = 0; i < 3; ++i) {
		v(i) = ProcessMaths(sVec.at(i));
	}

	return v;
}

Vector3i stringToVector3i(string s) {
	int i;
	vector<string> sVec;
	Vector3i v;

	boost::split(sVec, s, boost::is_any_of(","));

	// Check for failure
	if(v.size() != 3) {
		throw invalid_argument("String contained " + to_string(v.size()) + " elements instead of 3.");
	}

	for(i = 0; i < 3; ++i) {
		v(i) = (int)ProcessMaths(sVec.at(i));
	}

	return v;
}

Matrix3 stringToMatrix3(string s) {
	int i, j;
	vector<string> sVec;
	Matrix3 m;
	boost::split(sVec, s, boost::is_any_of(","));

	// Check for failure
	if(m.size() != 9) {
		throw invalid_argument("String contained " + to_string(m.size()) + " elements instead of 9.");
	}

	for(i = 0; i < 9; ++i) {
		m(i) = stos(sVec.at(i));
	}

	return m;
}

vector<int> stringToVectorI(string s) {
	int i;
	vector<string> sVec;
	vector<string>::iterator it;
	vector<int> v;

	// Method start
	boost::split(sVec, s, boost::is_any_of(","));
	
	for(it = sVec.begin(); it != sVec.end(); ++it) {
		v.push_back(stoi(*it));
	}

	return v;
}

vector<int> stringToVectorIHard(string s) {
	int i, bit;
	vector<string> sVec;
	vector<string>::iterator it;
	vector<int> v;

	// Method start
	boost::split(sVec, s, boost::is_any_of(","));
	
	for(it = sVec.begin(); it != sVec.end(); ++it) {
		try {
			bit = stoi(*it);
			v.push_back(bit);	
		} catch (invalid_argument& e) {
			v.push_back(0);
		}
	}

	return v;
}

string Vector2ToString(Vector2 v) {
	ostringstream sStream;
	sStream << v(0) << "," << v(1);
	return sStream.str();
//	return to_string(v(0)) + "," + to_string(v(1));	
}

string Vector2iToString(Vector2i v) {
	return to_string(v(0)) + "," + to_string(v(1));	
}

string Vector3ToString(Vector3 v) {
	ostringstream sStream;
	sStream << v(0) << "," << v(1) << "," << v(2);
	return sStream.str();
//	return to_string(v(0)) + "," + to_string(v(1)) + "," + to_string(v(2));	
}

string Vector3iToString(Vector3i v) {
	return to_string(v(0)) + "," + to_string(v(1)) + "," + to_string(v(2));	
}

string VectorIToString(vector<int> v) {
	string s;
	s = to_string(v.at(0));
	for(int i = 1; i < v.size(); ++i) {
		s += "," + to_string(v.at(i));
	}
	return s;
}

string toLower(string s) {
	return boost::algorithm::to_lower_copy(s);
}

bool toBool(string s) {

	// Lower case, then compare
	s = toLower(s);
	if(s == "yes" or s == "y" or s == "true" or s == "1") {
		return true;
	}

	return false;
}

string toString(bool b) {

	if(b) {
		return "true";
	}

	return "false";
}

//bool compareInteractions(pair<pair<pair<int, int>, pair<int, int>>, scalar> a, pair<pair<pair<int, int>, pair<int, int>>, scalar> b) {
//	return a.second < b.second;
//}

bool compareInteractions(Interaction a, Interaction b) {
	return a.energy < b.energy;
}
