#include "InteractionTypes.hpp"

Interaction::Interaction() {
	energy = 0;
	nIndex[0] = 0;
	nIndex[1] = 0;
	bIndex = -1;
}

Interaction::~Interaction() {
	energy = 0;
	nIndex[0] = 0;
	nIndex[1] = 0;
	bIndex = 0;
}

void Interaction::printDetails() {
	cout << endl;
	cout << "Class: Interaction" << endl;
	cout << "Parameters: " << endl;
	cout << "\tEnergy: " << energy << endl;
	cout << "\tNode Indices: " << nIndex[0] << ", " << nIndex[1] << endl;
	cout << "\tBond Index: " << bIndex << endl;
}
