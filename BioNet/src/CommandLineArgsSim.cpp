#include "CommandLineArgsSim.hpp"

CommandLineArgs::CommandLineArgs() {

	iFname = bfs::path{""};
	oFname = bfs::path{""};
	tFname = bfs::path{""};
	mFname = bfs::path{""};
	tpFname = bfs::path{""};
	cFname = bfs::path{""};
	cfFname = bfs::path{""};
	nmFname = bfs::path{""};

	iFnameSet = false;
	oFnameSet = false;
	tFnameSet = false;
	mFnameSet = false;
	tpFnameSet = false;
	cFnameSet = false;
	cfFnameSet = false;
	nmFnameSet = false;
	
	localConnectivity = false;
	restart = false;

	simType = NoSim;

	initType = NoInit;
	initTypeSequence.clear();

	numNMModes = 1;

	numThreads = 0;

	verbosity = false;

	thermalForces = 1;
	vdwForces = 0;
	stericForces = 1;
	


	constantForces = 1;

	electrostaticForces = 0;

	kinetics = 0;
	bondKinetics = 0;
	proteinKinetics = 0;
	
	boxFlag = 1;
	centerFlag = 1;
}

CommandLineArgs::~CommandLineArgs() {

	iFname = bfs::path{""};
	oFname = bfs::path{""};
	tFname = bfs::path{""};
	mFname = bfs::path{""};
	tpFname = bfs::path{""};
	cFname = bfs::path{""};
	cfFname = bfs::path{""};
	nmFname = bfs::path{""};

	iFnameSet = false;
	oFnameSet = false;
	tFnameSet = false;
	mFnameSet = false;
	tpFnameSet = false;
	cFnameSet = false;
	cfFnameSet = false;
	
	localConnectivity = false;
	restart = false;

	simType = NoSim;

	initType = NoInit;
	initTypeSequence.clear();

	numNMModes = 1;
	
	numThreads = 0;

	verbosity = false;

	thermalForces = 0;
	vdwForces = 0;
	stericForces = 0;

	wallForces = 0;
	constantForces = 0;

	electrostaticForces = 0;

	kinetics = 0;
	bondKinetics = 0;
	proteinKinetics = 0;
	
	boxFlag = 0;
	centerFlag = 0;
}

int CommandLineArgs::read(int argc, char** argv) {

	// Message
	fileLog->printMethodTask("Reading command-line arguments...");

	// First, check for no args
	if(argc == 1) {
		printHelp();
		exit(0);
	}

	// Variable for stores the option index here
	int c, optionIndex;

	while (true) {

		// Get a container to pass to getopts
		static struct option long_options[] = {

			// None flag options
			{"input", required_argument, 0, 'i'},
			{"output", optional_argument, 0, 'o'},
			{"struct", optional_argument, 0, 'w'},
			{"traj", optional_argument, 0, 'x'},
			{"meas", optional_argument, 0, 'y'},
			{"top", optional_argument, 0, 'z'},
			{"net", optional_argument, 0, 'n'},

			{"type", required_argument, 0, 't'},
			{"num-modes", required_argument, 0, 'm'},
			{"num-threads", required_argument, 0, 'j'},

			{"help", no_argument, 0, 'h'},
			{"verbose", no_argument, 0, 'v'},
			{"local", no_argument, 0, 'l'},
			{"restart", no_argument, 0, 'r'},

			{"thermal", no_argument, &thermalForces, 1},
			{"no-thermal", no_argument, &thermalForces, 0},

			{"steric", no_argument, &stericForces, 1},
			{"no-steric", no_argument, &stericForces, 0},

			{"vdw", no_argument, &vdwForces, 1},
			{"no-vdw", no_argument, &vdwForces, 0},

			{"wall", no_argument, &wallForces, 1},
			{"no-wall", no_argument, &wallForces, 0},
			
			{"box", no_argument, &boxFlag, 1},
			{"no-box", no_argument, &boxFlag, 0},

			{"center", no_argument, &centerFlag, 1},
			{"no-center", no_argument, &centerFlag, 0},

			{"electro", no_argument, &electrostaticForces, 1},
			{"no-electro", no_argument, &electrostaticForces, 0},

			{"kinetics", no_argument, &kinetics, 1},
			{"no-kinetics", no_argument, &kinetics, 0},
			{"bond-kinetics", no_argument, &kinetics, 2},
			{"protein-kinetics", no_argument, &kinetics, 3},


			{"constant", no_argument, &constantForces, 1},
			{"no-constant", no_argument, &constantForces, 0},

			{0, 0, 0, 0}
		};

		// Parse an option
		c = getopt_long(argc, argv, "i:o:c:x:y:z:n:m:t:j:hlrv", long_options, &optionIndex);

		// Check for end
		if(c == -1) {
			break;
		}

		switch (c) {
			case 0:
				/* If this option set a flag, do nothing else now. */
				if(long_options[optionIndex].flag != 0) {
					break;
				}
				
				printf ("option %s", long_options[optionIndex].name);
				if(optarg) {
					printf (" with arg %s", optarg);
				}
				printf ("\n");
				break;

			case 'h':
				printHelp();
				exit(BioNetSuccess);
			case 'i':
				setInputFname(optarg);
				break;
			case 'o':
				setOutputFname(optarg);
				break;
			case 'c':
				setCheckpointFname(optarg);
				break;
			case 'f':
				setConstantForceFname(optarg);
				break;
			case 'x':
				setTrajectoryFname(optarg);
				break;
			case 'y':
				setMeasurementFname(optarg);
				break;
			case 'z':
				setTopologyFname(optarg);
				break;
			case 'n':
				setNetworkModelFname(optarg);
				break;
			case 't':
				setSimType(optarg);
				break;
			case 'l':
				setLocalConnectivity(true);
				break;
			case 'r':
				setRestart(true);
				break;
			case 'm':
				try {
					setNumModes(stoi(optarg));
				} catch (exception &e) {
					fileLog->printError("CommandLineArgs::numNMModes", "Unable to parse argument of option '-m'");
					return BioNetError;
				}	
				break;
			case 'j':
				try {
					setNumThreads(stoi(optarg));
				} catch (exception &e) {
					fileLog->printError("CommandLineArgs::numThreads", "Unable to parse argument of option '-j'");
					return BioNetError;
				}	
				break;
			case 'v':
				setVerbosityLevel(true);
				break;
			case '?':
				/* getopt_long already printed an error message. */
				break;

			default:
				return BioNetError;
		}
	}

	fileLog->printMethodSuccess();
	return BioNetSuccess;
}

bool CommandLineArgs::validate() {

	// Message
	fileLog->printMethodTask("Validating command-line arguments...");

	// Test whether input file exists (it must always exist)
	bfs::path p;

	p = bfs::path(iFname);
	if(!iFnameSet) {
		fileLog->printError("Input filename (-i)", "Not supplied");
		return BioNetInvalid;
	} else if(!bfs::exists(p)) {
		fileLog->printError("Input filename (-i)", "'" + p.filename().string() + "' not found");
		return BioNetInvalid;
	}

	// Now check the simulation type (required now to validate other filename choices)
	if(simType == NoSim) {
		fileLog->printError("Simulation Type (-t)", "Not supplied");
		return BioNetInvalid;

	} else if(simType == WrongSim) {
		fileLog->printError("Simulation Type (-t)", "Not recognised");
		return BioNetInvalid;

	} else if (!simTypeSupported(simType)) {
		fileLog->printError("Simulation Type (-t)", "'" + getSimTypeString(simType) + "' currently in development");
		return BioNetInvalid;

	}

	// Can we restart?
	if(!isDynamicSimulation(getSimType()) && getRestart()) {
		fileLog->printError("simType", "Cannot restart a network model simulation :(");
		return BioNetInvalid;
	}
		
	// Sort filenames
	if(validateAndBuildFilenames() == BioNetInvalid) {
		fileLog->printError("One or more filenames are invalid :(");
		return BioNetInvalid;
	}

	// What about conflicting / dependent force fields?
	if(!boxFlag && (vdwForces || stericForces || electrostaticForces || wallForces)) {
		fileLog->printError("Box flag (--no-box)", "Must be set (--box) if steric flag (--steric) or VdW flag (--vdw) are set");
		return BioNetInvalid;
	}

	if(!boxFlag && centerFlag) {
		fileLog->printError("Box flag (--no-box)", "Must be set (--box) if centering flag is set");
		return BioNetInvalid;
	}

	fileLog->printMethodSuccess();
	return BioNetValid;
}

bool CommandLineArgs::validateAndBuildFilenames() {

	int i, j;
	vector<string> check, titles;
	bfs::path p;
	string baseFname;

	// What file specifications conflict?
	if(oFnameSet && (tFnameSet || mFnameSet || (bondKinetics && tpFnameSet) || (constantForces && cfFnameSet) || cFnameSet)) {
		fileLog->printError("Output filename (-o)", "Must be the only output filename set (-o cannot be used with -c/-x/-y/-z)");
		return BioNetInvalid;
	}


	// If oFname was set, then build new filenames from there
	p = bfs::path(oFname);
	if(oFnameSet) {

		string baseFname;

		// Check the directory for writing exists
		if(!bfs::is_directory(p.parent_path())) {
			fileLog->printError("Output filename (-o)", "'" + p.parent_path().string() + "' not a read/writeable directory");
			return BioNetInvalid;
		}

		// Get the basename
		baseFname = p.parent_path().string() + "/" + p.stem().string();

		// Get all filenames		
		tFname = baseFname + ".bntrj";
		mFname = baseFname + ".bnms";
		tpFname = baseFname + ".bntop";
		cFname = baseFname + ".bnchk";
		cfFname = baseFname + ".bncf";
		nmFname = baseFname + ".bnnm";
		
		// Now they're set
		tFnameSet = true;
		mFnameSet = true;
		tpFnameSet = true;
		cFnameSet = true;
		cfFnameSet = true;
		nmFnameSet = true;
	}

	// Now check we have all the required files set and whether we can read / write
	if(isDynamicSimulation(getSimType())) {
		if(!tFnameSet) {
			fileLog->printError("Trajectory filename (-x)", "Must always be set (-o can also be used to set all filenames)");
			return BioNetInvalid;
		} else {

			p = bfs::path(tFname);
			if(!bfs::is_directory(p.parent_path())) {
				fileLog->printError("Trajectory filename (-x)", "'" + p.parent_path().string() + "' not a read/writeable directory");
				return BioNetInvalid;
			}
		}

		if(!mFnameSet) {
			fileLog->printError("Measurement filename (-y)", "Must always be set (-o can also be used to set all filenames)");
			return BioNetInvalid;
		} else {
			p = bfs::path(mFname);
			if(!bfs::is_directory(p.parent_path())) {
				fileLog->printError("Measurement filename (-y)", "'" + p.parent_path().string() + "' not a read/writeable directory");
				return BioNetInvalid;
			}
		}

		if(!tpFnameSet) {
			if(bondKinetics) {
				fileLog->printError("Topology filename (-z)", "Must be set if bond kinetics are active (-o can also be used to set all filenames)");
				return BioNetInvalid;
			}
		} else {

			p = bfs::path(tpFname);
			if(!bfs::is_directory(p.parent_path())) {
				fileLog->printError("Topology filename (-z)", "'" + p.parent_path().string() + "' not a read/writeable directory");
				return BioNetInvalid;
			}
		}

		if(!cFnameSet) {

			// Set if restart, otherwise make it for them from the trajectory filename (which has already been checked)
			if(getRestart()) {
				fileLog->printError("Checkpoint filename (-c)", "Must be set if restarting (-o can also be used to set all filenames)");
				return BioNetInvalid;
			} else {
				p = bfs::path(tFname);
				baseFname = p.parent_path().string() + "/" + p.stem().string();
				cFname = baseFname + ".bnchk";
			}
		} else {

			p = bfs::path(cFname);
			if(!bfs::is_directory(p.parent_path())) {
				fileLog->printError("Checkpoint filename (-c)", "'" + p.parent_path().string() + "' not a read/writeable directory");
				return BioNetInvalid;
			}
		}
		
		if(!cfFnameSet) {
			if(constantForces) {
				fileLog->printError("Constant Force filename (-f)", "Must be set if forces are active (-o can also be used to set all filenames)");
				return BioNetInvalid;
			}
		} else {

			p = bfs::path(cfFname);
			if(!bfs::is_directory(p.parent_path())) {
				fileLog->printError("Constant Force filename (-f)", "'" + p.parent_path().string() + "' not a read/writeable directory");
				return BioNetInvalid;
			}
		}
	} else if (isNetworkModelSimulation(getSimType())) {
	
		// We only need one filename
		if(!nmFnameSet) {
			fileLog->printError("Network model filename (-n)", "Must always be set (-o can also be used to set all filenames)");
			return BioNetInvalid;
		} else {

			p = bfs::path(nmFname);
			if(!bfs::is_directory(p.parent_path())) {
				fileLog->printError("Network model filename (-n)", "'" + p.parent_path().string() + "' not a read/writeable directory");
				return BioNetInvalid;
			}
		}
	}
	
	// Now check none of the files are the same
	check.push_back(iFname.string());
	titles.push_back("Input filename (-i)");
	check.push_back(tFname.string());
	titles.push_back("Trajectory filename (-x)");
	check.push_back(mFname.string());
	titles.push_back("Measurement filename (-y)");
	if(bondKinetics) {
		check.push_back(tpFname.string());
		titles.push_back("Topology filename (-z)");
	}
	if(constantForces) {
		check.push_back(cfFname.string());
		titles.push_back("Constant force filename (-f)");	
	}

	check.push_back(cFname.string());
	titles.push_back("Checkpoint filename (-c)");

	for(i = 0; i < check.size(); ++i) {
		for(j = i + 1; j < check.size(); ++j) {
			if(check.at(i) == check.at(j)) {
				fileLog->printError(titles.at(i), "Identical to " + titles.at(j) + ". All filenames should be unique.");
				return BioNetInvalid;
			}
		}
	}
		
	// Now check for existance and backups
	if(isDynamicSimulation(getSimType())) {
		if(getRestart()) {


			// Trajectory
			p = bfs::path(tFname);
			if(!bfs::exists(p)) {
				fileLog->printError("Trajectory filename (-x)", "Must exist if restarting (-o can also be used to set all filenames)");
				return BioNetInvalid;
			}

			// Measurement
			p = bfs::path(mFname);
			if(!bfs::exists(p)) {
				fileLog->printError("Measurement filename (-y)", "Must exist if restarting (-o can also be used to set all filenames)");
				return BioNetInvalid;
			}

			// Topology
			p = bfs::path(tpFname);
			if(bondKinetics && !bfs::exists(p)) {
				fileLog->printError("Topology filename (-x)", "Must exist if restarting a bond kinetic simulation (-o can also be used to set all filenames)");
				return BioNetInvalid;
			}

			// Checkpoint
			p = bfs::path(cFname);
			if(!bfs::exists(p)) {
				fileLog->printError("Checkpoint filename (-c)", "Must exist if restarting (-o can also be used to set all filenames)");
				return BioNetInvalid;
			}

			// Constant forces
			p = bfs::path(cfFname);
			if(constantForces && !bfs::exists(p)) {
				fileLog->printError("Constant force filename (-f)", "Must exist if simulation with constant forces (-o can also be used to set all filenames)");
				return BioNetInvalid;
			}
			
		} else {

			// Trajectory
			p = bfs::path(tFname);	
			if(bfs::exists(p)) {
				while(bfs::exists(p)) {
					p = bfs::path(incrementFileName(p.string()));
				}
				boost::filesystem::rename(tFname, p);
				fileLog->printMessage("Trajectory filename (-x)", "Backed up to '" + p.string() + "'");
			}

			// Measurement
			p = bfs::path(mFname);
			if(bfs::exists(p)) {
				while(bfs::exists(p)) {
					p = bfs::path(incrementFileName(p.string()));
				}
				boost::filesystem::rename(mFname, p);
				fileLog->printMessage("Measurement filename (-y)", "Backed up to '" + p.string() + "'");
			}

			// Topology
			if(bondKinetics) {
				p = bfs::path(tpFname);
				if(bfs::exists(p)) {
					while(bfs::exists(p)) {
						p = bfs::path(incrementFileName(p.string()));
					}
					boost::filesystem::rename(tpFname, p);
					fileLog->printMessage("Topology filename (-z)", "Backed up to '" + p.string() + "'");
				}
			}

			// Constant forces
			if(constantForces) {
				p = bfs::path(cfFname);
				if(bfs::exists(p)) {
					while(bfs::exists(p)) {
						p = bfs::path(incrementFileName(p.string()));
					}
					boost::filesystem::rename(cfFname, p);
					fileLog->printMessage("Constant forces filename (-f)", "Backed up to '" + p.string() + "'");
				}
			}


			// Checkpoint
			p = bfs::path(cFname);
			if(bfs::exists(p)) {
				while(bfs::exists(p)) {
					p = bfs::path(incrementFileName(p.string()));
				}
				boost::filesystem::rename(cFname, p);
				fileLog->printMessage("Checkpoint filename (-c)", "Backed up to '" + p.string() + "'");
			}
		}
	} else if (isNetworkModelSimulation(getSimType())) {

		// Could not get here if restart was active
			
		// Network model
		p = bfs::path(nmFname);	
		if(bfs::exists(p)) {
			while(bfs::exists(p)) {
				p = bfs::path(incrementFileName(p.string()));
			}
			boost::filesystem::rename(nmFname, p);
			fileLog->printMessage("Network model filename (-n)", "Backed up to '" + p.string() + "'");
		}		
	}
	
	return BioNetValid;
}

void CommandLineArgs::setInputFname(string fname) {

	iFname = getAbsolutePath(bfs::path{fname});
	iFnameSet = true;
}

bfs::path CommandLineArgs::getInputFname() {
 	return iFname;
}

void CommandLineArgs::setOutputFname(string fname) {
	oFname = getAbsolutePath(bfs::path{fname});
	oFnameSet = true;
}

bfs::path CommandLineArgs::getOutputFname() {
	return oFname;
}

void CommandLineArgs::setTrajectoryFname(string fname) {
	tFname = getAbsolutePath(bfs::path{fname});
	tFnameSet = true;
}

bfs::path CommandLineArgs::getTrajectoryFname() {
	return tFname;
}

void CommandLineArgs::setMeasurementFname(string fname) {
	mFname = getAbsolutePath(bfs::path{fname});
	mFnameSet = true;
}

bfs::path CommandLineArgs::getMeasurementFname() {
	return mFname;
}

void CommandLineArgs::setTopologyFname(string fname) {
	tpFname = getAbsolutePath(bfs::path{fname});
	tpFnameSet = true;
}


bfs::path CommandLineArgs::getTopologyFname() {
	return tpFname;
}

void CommandLineArgs::setNetworkModelFname(string fname) {
	nmFname = getAbsolutePath(bfs::path{fname});
	nmFnameSet = true;
}

bfs::path CommandLineArgs::getNetworkModelFname() {
	return nmFname;
}

void CommandLineArgs::setCheckpointFname(string fname) {
	cFname = getAbsolutePath(bfs::path{fname});
	cFnameSet = true;
}
		
bfs::path CommandLineArgs::getCheckpointFname() {
	return cFname;
}

void CommandLineArgs::setConstantForceFname(string fname) {
	cfFname = getAbsolutePath(bfs::path{fname});
	cfFnameSet = true;
}

bfs::path CommandLineArgs::getConstantForceFname() {
	return cfFname;
}

void CommandLineArgs::setSimType(string type) {

	// Standard
	boost::algorithm::to_lower(type);

	if(type == "gnm") {
		simType = GNM;
	} else if (type == "anm") {
		simType = ANM;
	} else if (type == "enm") {
		simType = ENM;
	} else if (type == "vgnm") {
		simType = VGNM;
	} else if (type == "vanm") {
		simType = VANM;
	} else if (type == "venm") {
		simType = VENM;
	} else if (type == "langevin" || type == "l") {
		simType = Langevin;
	} else if (type == "brownian" || type == "b") {
		simType = Brownian;
	} else if (type == "montecarlo" || type == "mc") {
		simType = MonteCarloS;
	} else {
		simType = WrongSim;
	}
}

SimType CommandLineArgs::getSimType() {
	return simType;
}

InitType CommandLineArgs::getInitType() {
	return initType;
}

vector<InitType> CommandLineArgs::getInitTypeSequence() {
	return initTypeSequence;
}

void CommandLineArgs::setNumModes(int n) {
	numNMModes = n;
}

int CommandLineArgs::getNumModes() {
	return numNMModes;
}

void CommandLineArgs::setNumThreads(int n) {
	numThreads = n;
}

int CommandLineArgs::getNumThreads() {
	return numThreads;
}

void CommandLineArgs::setLocalConnectivity(bool lCon) {
	localConnectivity = lCon;
}

bool CommandLineArgs::getLocalConnectivity() {
	return localConnectivity;
}

void CommandLineArgs::setRestart(bool res) {
	restart = res;
}

bool CommandLineArgs::getRestart() {
	return restart;
}

void CommandLineArgs::setVerbosityLevel(bool vLevel) {
	verbosity = vLevel;
}

bool CommandLineArgs::getVerbosityLevel() {
	return verbosity;
}

void CommandLineArgs::printHelp() {
	cout << endl << "*****************************" << endl;
	cout << "This program is designed to build a visco-elastic network model" << endl << endl;
	cout << "Options:" << endl << endl;

	cout << "\t -h / --help\t\t\tPrint this help stuff" << endl;
	cout << "\t -i / --input\t\t\tInput file containing network parameters" << endl;
	cout << "\t -o / --output\t\t\tOutput file for visualisation / analysis" << endl;
	cout << "\t -t / --type\t\t\tSimulation type" << endl;
	cout << "\t -l / --local\t\t\tActivate local protein dynamics" << endl;

	cout << "\t -r / --restart\t\t\tRestart from old simulation" << endl;
	cout << "\t -j / --num-threads\t\tRequest threads" << endl;

	cout << endl;
	cout << "\t --traj \t\t\tTrajectory output file for visualisation / analysis" << endl;
	cout << "\t --meas \t\t\tMeasurement output file for visualisation / analysis" << endl;
	cout << "\t --top \t\t\t\tTopology output file for visualisation / analysis" << endl;
	cout << "\t --struct \t\t\tFinal structure XML file for visualisation of resubmission" << endl;

	cout << endl;
	cout << "\t--verbose\t\t\tVerbose user information output" << endl;
	cout << "\t--thermal/--no-thermal\t\tToggle thermal forces" << endl;
	cout << "\t--steric/--no-steric\t\tToggle steric forces" << endl;
	cout << "\t--vdw/--no-vdw\t\t\tToggle Van der Waals forces" << endl;
	cout << "\t--electro/--no-electro\t\tToggle electrostatic forces" << endl;
	cout << "\t--wall/--no-wall\t\t\tToggle Box wall forces" << endl;
	cout << "\t--constant/--no-constant\tToggle constant forces" << endl;
	cout << "\t--kinetics/--no-kinetics/--bond-kinetics/--prot-kinetics\tToggle kinetic changes" << endl;
	cout << "\t--box/--no-box\t\t\tToggle Simulation Box" << endl;
	cout << "\t--center/--no-center\t\tCenter in Simulation Box" << endl;
	cout << endl;
}

void CommandLineArgs::printDetails() {
	cout << endl;
	cout << "Class: CommandLineArgs" << endl;
	cout << "Command Line Arguments: " << endl;
	cout << "\tInput Fname = " << iFname << endl;
	cout << "\tOutput Fname = " << oFname << endl;
	cout << "\tTrajectory Fname = " << tFname << endl;
	cout << "\tMeasurement Fname = " << mFname << endl;
	cout << "\tTopology Fname = " << tpFname << endl;
	cout << "\tCheckpoint Fname = " << cFname << endl;
	cout << "\tConstant forces Fname = " << cfFname << endl;
	cout << "\tSim Type = " << getSimTypeString(simType) << endl;
	cout << "\tLocal Dynamics = " << localConnectivity << endl;
	cout << "\tVerbose Output = " << verbosity << endl;

	cout << "\tNum Threads Requested = " << getNumThreads() << endl;

	cout << "\tForce Types: " << endl;
	cout << "\t\tThermal Forces Active = " << thermalForces << endl;
	cout << "\t\tSteric Forces Active = " << stericForces << endl;
	cout << "\t\tVan der Waals Forces Active = " << vdwForces << endl;
	cout << "\t\tElectrostatic Forces Active = " << electrostaticForces << endl;
	cout << "\t\tBox wall Forces Active = " << wallForces << endl;
	cout << "\t\tConstant Forces Active = " << constantForces << endl;
	cout << "\t\tKinetics Active = " << kinetics << endl;
	cout << "\t\tSimulation Box Active = " << boxFlag << endl;
	cout << "\t\tCentered = " << centerFlag << endl;
}
