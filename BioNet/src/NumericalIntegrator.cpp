#include "NumericalIntegrator.hpp"

//
// Constructors & Destructors
//
NumericalIntegrator::~NumericalIntegrator() {

}

InertialEulerIntegrator::InertialEulerIntegrator() {

}

InertialEulerIntegrator::~InertialEulerIntegrator() {

}

ViscousEulerIntegrator::ViscousEulerIntegrator() {

}

ViscousEulerIntegrator::~ViscousEulerIntegrator() {

}

//
// Integration Methods
//
void InertialEulerIntegrator::integrate(vObject *v, scalar dt) {

	v->setVelocity(v->getVelocity() + (dt * v->getTotalForce() / v->getMass()));
	v->translate(dt * v->getVelocity());

	// And zero the force
//	v->zeroTotalForce();

	// Torque
	Quat q;
	scalar ang, angSpeed;

	// Angular velocity
	v->setAngularVelocity(v->getAngularVelocity().vec() + (dt * v->getTotalTorque() / v->getMomentOfInertia()));

	// Rotation angle
	q.vec() = v->getAngularVelocity().vec();
	angSpeed = q.vec().norm();
	ang = angSpeed * dt;

	// Check it
	if(!isnan(angSpeed) && angSpeed > 0.0) {
		
		// Half it for quaternion algebra
		ang /= 2;

		// Quaternion
		q.w() = cos(ang);
		q.vec() *= sin(ang) / angSpeed;

		v->rotate(q.toRotationMatrix());
	}

	// And zero the torque
//	v->zeroTotalTorque();
}

void ViscousEulerIntegrator::integrate(vObject *v, scalar dt) {

//	cout << v->getTotalForce().norm() << endl; 
//	if(v->getIndex() == 12 || v->getIndex() == 7) {
//		cout << v->getTotalForce().norm() << endl; 
//	}
	v->setVelocity(v->getTotalForce() / v->getDrag());
	v->translate(dt * v->getVelocity());

	// And zero the force
//	v->zeroTotalForce();

	// Torque
	scalar ang, angSpeed;
	Quat q;

	// Angular velocity
	v->setAngularVelocity(v->getTotalTorque() / v->getRotationalDrag());

	// Rotation angle
	q.vec() = v->getAngularVelocity().vec();
	angSpeed = q.vec().norm();
	ang = angSpeed * dt;

	// Check it
	if(!isnan(angSpeed) && angSpeed > 0.0) {
		
		// Half it for quaternion algebra
		ang /= 2;

		// Quaternion
		q.w() = cos(ang);
		q.vec() *= sin(ang) / angSpeed;

		v->rotate(q.toRotationMatrix());
	}

	// And zero the torque
//	v->zeroTotalTorque();
}
