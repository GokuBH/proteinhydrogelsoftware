#include "Forces.hpp"

// Global Methods
string getForceTypeString(ForceType forceType) {
	return forceTypeString[forceType];
}

ForceType getForceType(string s) {

	boost::algorithm::to_lower(s);
	if(s == "steric") {
		return FSteric;
	} else if (s == "vdw") {
		return FVdW;
	} else if(s == "constant") {
		return FConstant;
	} else if(s == "constant") {
		return FConstant;
	} else if(s == "constantvelocity") {
		return FConstantVelocity;
	} else if(s == "wall") {
		return FWall;
	} else {
		return NoForce;
	}
}

fObject::fObject() {
	index = 0;
	numTargets = 0;
	magnitude = 0.0;
	force.setZero();
	target.clear();
}

fObject::~fObject() {
	index = 0;
	numTargets = 0;
	magnitude = 0.0;
	force.setZero();
	target.clear();
}

int fObject::setParameter(string name, string value) {

	boost::algorithm::to_lower(name);
	if(name == "index") {
		setIndex(stoi(value));
	} else if (name == "type" || name == "name") {
		// Nothing
	} else {
		return BioNetWarning;
	}
	return BioNetSuccess;
}

string fObject::getParameter(string name) {
	if(name == "index") {
		return to_string(getIndex());
	} else {
		return "";
	}
}


void fObject::setPosition(scalar a, scalar b, scalar c) {
	pos(0) = a;
	pos(1) = b;
	pos(2) = c;
}

void fObject::setPosition(Vector3 p) {
	pos = p;
}

Vector3 fObject::getPosition() {
	return pos;
}

void fObject::setVelocity(scalar a, scalar b, scalar c) {
	vel(0) = a;
	vel(1) = b;
	vel(2) = c;
}

void fObject::setVelocity(Vector3 v) {
	vel = v;
}

Vector3 fObject::getVelocity() {
	return vel;
}

void fObject::setIndex(int i) {
	index = i;
}

int fObject::getIndex() {
	return index;
}

int fObject::getNumTargets() {
	return numTargets;
}

void fObject::addTarget(vObject* v) {
	target.push_back(v);
	numTargets += 1;
}

vObject * fObject::getTarget(int index) {
	if(index > numTargets) {
		return NULL;
	}

	return target[index];
}

void fObject::setForce(Vector3 v) {
	force = v;
}

void fObject::addForce(Vector3 v) {
	force += v;
}

void fObject::zeroForce(Vector3 v) {
	force.setZero();
}

Vector3 fObject::getForce() {
	return force;
}

void fObject::setMagnitude(scalar m) {
	magnitude = m;
	
	// Rescale force
	force.normalize();
	force *= m;
}

scalar fObject::getMagnitude() {
	return magnitude;
}

int fObject::setOrientation(string val) {

	// Split the vector
	vector<string> sVec;
	boost::split(sVec, val, boost::is_any_of(","));

	// Check the vector
	if(sVec.size() != 2 && sVec.size() != 3) {
		fileLog->printError("fObject::orientation", "String must contain 2 or 3 components.");
		return BioNetError;		
	}

	// Convert
	try {
		if(sVec.size() == 2) {
			setOrientation(ProcessMaths(sVec.at(0)) / InternalUnits::Angle, ProcessMaths(sVec.at(1)) / InternalUnits::Angle);
		} else {
			setOrientation(stos(sVec.at(0)) / InternalUnits::Length, stos(sVec.at(1)) / InternalUnits::Length, stos(sVec.at(2)) / InternalUnits::Length);
		}
	} catch (exception &e) {
		fileLog->printError("fObject::orientation", "String must contain numerical values.");
		return BioNetError;
	}
	
	return BioNetSuccess;	
}

void fObject::setOrientation(scalar theta, scalar phi) {
	
	force = MathFunctions::cartesian(theta, phi);
	force *= getMagnitude();
}

void fObject::setOrientation(scalar x, scalar y, scalar z) {
	force(0) = x;
	force(1) = y;
	force(2) = z;
	force.normalize();
	force *= getMagnitude();
}

Vector3 fObject::getOrientation() {
	return force / force.norm();
}

void fObject::apply(scalar dt) {

	vector<vObject*>::iterator it;
	for(it = target.begin(); it != target.end(); ++it) {
		apply(*it, dt);
	}
}

ConstantForce::ConstantForce() : fObject() {

}

ConstantForce::~ConstantForce() {

}

int ConstantForce::setParameter(string name, string value) {

	boost::algorithm::to_lower(name);

	if (name == "mag" || name == "magnitude") {
		setMagnitude(stos(value) / InternalUnits::Force);
	} else if (name == "orientation") {
		if(setOrientation(value) != BioNetSuccess) {
			fileLog->printError("ConstantForce::orientation", "Unable to assign from string '" + value + "'.");
		}
	} else if (name == "nodes" || name == "sites") {
		// Nothing
	} else {
		return fObject::setParameter(name, value);
	}

	return BioNetSuccess;
}

string ConstantForce::getParameter(string name) {

	ostringstream sStream;
	if (name == "mag" || name == "magnitude") {
		sStream << getMagnitude();
		return sStream.str();
	} else if (name == "orientation") {
		sStream << getOrientation()(0) << ", " << getOrientation()(1) << ", " << getOrientation()(2);
		return sStream.str();	
	} else {
		return fObject::getParameter(name);
	}
}

void ConstantForce::calculateNonPrimitives() {
	return;
}
		
void ConstantForce::apply(vObject *v, scalar dt) {
	v->addExternalForce(force);
}

void ConstantForce::printDetails() {
	cout << endl;
	cout << "Class: ConstantForce" << endl;
	cout << "Parameters: " << endl;
	cout << "\tIndex = " << getIndex() << endl;
	cout << "\tApplying to " << getNumTargets() << " vObjects:" << endl;
	cout << "\t\tIndices: ";
	for(int i = 0; i < getNumTargets(); ++i) {
		cout << getTarget(i)->getIndex() << " ";
	}
	cout << endl;

	cout << "\t\tTopParentIndices: ";
	for(int i = 0; i < getNumTargets(); ++i) {
		cout << getTarget(i)->getTopParentIndex() << " ";
	}
	cout << endl;
	cout << "\tForce = (" << getForce()(0) << ", " << getForce()(1) << ", " << getForce()(2) << ")" << endl;
}

ConstantVelocityForce::ConstantVelocityForce() : fObject() {
	pos.setZero();
	vel.setZero();
	orientation.setZero();
	posSet = false;
}

ConstantVelocityForce::~ConstantVelocityForce() {

	pos.setZero();
	vel.setZero();
	orientation.setZero();
	posSet = false;
}

int ConstantVelocityForce::setParameter(string name, string value) {

	boost::algorithm::to_lower(name);

	if (name == "mag" || name == "magnitude") {
		setSpeed(stos(value) / InternalUnits::Velocity);
	} else if (name == "orientation") {
		if(fObject::setOrientation(value) != BioNetSuccess) {
			fileLog->printError("ConstantVelocityForce::orientation", "Unable to assign from string '" + value + "'.");
		}
	} else if (name == "k") {
		setElasticConstant(stos(value) / InternalUnits::ElasticConstant);
	} else if (name == "nodes" || name == "sites") {
		// Nothing
	} else if (name == "position") {
		setPosition(stringToVector3(value) / InternalUnits::Length);
		posSet = true;
	} else {
		return fObject::setParameter(name, value);
	}

	return BioNetSuccess;
}

string ConstantVelocityForce::getParameter(string name) {

	ostringstream sStream;
	if (name == "mag" || name == "magnitude") {
		sStream << getMagnitude();
		return sStream.str();
	} else if (name == "orientation") {
		sStream << getOrientation()(0) << ", " << getOrientation()(1) << ", " << getOrientation()(2);
		return sStream.str();	
	} else {
		return fObject::getParameter(name);
	}
}

void ConstantVelocityForce::setOrientation(scalar theta, scalar phi) {
	
	orientation = MathFunctions::cartesian(theta, phi);
}

void ConstantVelocityForce::setOrientation(scalar x, scalar y, scalar z) {
	orientation(0) = x;
	orientation(1) = y;
	orientation(2) = z;
	orientation.normalize();
}

Vector3 ConstantVelocityForce::getOrientation() {
	return orientation;
}

void ConstantVelocityForce::setElasticConstant(scalar k) {
	
	this->k = k;
}

scalar ConstantVelocityForce::getElasticConstant() {
	
	return k;
}

void ConstantVelocityForce::setSpeed(scalar v) {
	
	this->v = v;
}
scalar ConstantVelocityForce::getSpeed() {
	
	return v;
}

void ConstantVelocityForce::calculateNonPrimitives() {

	// Initial position
	if(!posSet) {
		setPosition(getTarget(0)->getPosition());
	}
	
	// Velocity vector
	vel = getSpeed() * getOrientation();
}

void ConstantVelocityForce::apply(vObject *v, scalar dt) {

	// Calculate force
	force = getElasticConstant() * (getPosition() - v->getPosition()).norm() * getOrientation();
	
	// Add force
	v->addExternalForce(force);
	
	// Update probe tip
	pos += vel * dt;
}

void ConstantVelocityForce::printDetails() {
	cout << endl;
	cout << "Class: ConstantVelocityForce" << endl;
	cout << "Parameters: " << endl;
	cout << "\tIndex = " << getIndex() << endl;
	cout << "\tApplying to " << getNumTargets() << " vObjects:" << endl;
	cout << "\t\tIndices: ";
	for(int i = 0; i < getNumTargets(); ++i) {
		cout << getTarget(i)->getIndex() << " ";
	}
	cout << endl;

	cout << "\t\tTopParentIndices: ";
	for(int i = 0; i < getNumTargets(); ++i) {
		cout << getTarget(i)->getTopParentIndex() << " ";
	}
	cout << endl;

	cout << "\tk = " << getElasticConstant() << ", v = " << getSpeed() << endl;

	cout << "\tPosition = (" << getPosition()(0) << ", " << getPosition()(1) << ", " << getPosition()(2) << ")" << endl;
	cout << "\tVelocity = (" << getVelocity()(0) << ", " << getVelocity()(1) << ", " << getVelocity()(2) << ")" << endl;
	cout << "\tForce = (" << getForce()(0) << ", " << getForce()(1) << ", " << getForce()(2) << ")" << endl;
}
