#include "ElasticNetwork.hpp"

// Cyclic dependencies
#include "PhysicalSystem.hpp"
#include "SimulationBox.hpp"
#include "NetworkFileWriter.hpp"

ElasticNetwork::ElasticNetwork() {

	pos.setZero();

	centroid.setZero();

	dimension.setZero();
	dimensionLL.setZero();
	dimensionUL.setZero();

	orientation.w() = 0.0;
	orientation.vec().setZero();

	protein = NULL;
	bond = NULL;

	node.clear();
	activeNIndex.clear();
	deletedNIndex.clear();
	freeNode.clear();

	connection.clear();
	activeCIndex.clear();
	deletedCIndex.clear();

	kineticBondTemplate = NULL;
	kineticProteinTemplate = NULL;

	force.clear();

	numThreads = 1;

	numProteins = 0;
	numBonds = 0;
	numNodes = 0;
	numConnections = 0;
	numUniqueForces = 0;

	integrator = NULL;

	stiffnessMatrix.setZero();
	
	tauE(0) = sInf;
	tauE(1) = sInf;

	tauI(0) = sInf;
	tauI(1) = sInf;

	tauR(0) = sInf;
	tauR(1) = sInf;

	coordination = 0.0;

	elasticEnergy = 0.0;
	kineticEnergy = 0.0;
	bondEnergy = 0.0;
	totalEnergy = 0.0;

	box = NULL;
	fileOut = NULL;

	simType = NoSim;
	initType = NoInit;
	initTypeSequence.clear();

	localConnectivitySet = false;
}

ElasticNetwork::~ElasticNetwork() {
	
	pos.setZero();

	centroid.setZero();

	dimension.setZero();
	dimensionLL.setZero();
	dimensionUL.setZero();

	orientation.w() = 0.0;
	orientation.vec().setZero();

	numThreads = 0;

	// This order is important!
	deleteConnections();
	deleteNodes();

	deleteProteins();
	deleteBonds();

	kineticBondTemplate = NULL;
	kineticProteinTemplate = NULL;

	deleteForces();

	delete integrator;
	integrator = NULL;

	stiffnessMatrix.setZero();
	
	tauE(0) = 0.0;
	tauE(1) = 0.0;

	tauI(0) = 0.0;
	tauI(1) = 0.0;

	tauR(0) = 0.0;
	tauR(1) = 0.0;

	elasticEnergy = 0.0;
	kineticEnergy = 0.0;
	bondEnergy = 0.0;
	totalEnergy = 0.0;

	coordination = 0.0;

	box = NULL;
	fileOut = NULL;

	simType = NoSim;
	initType = NoInit;
	initTypeSequence.clear();

	localConnectivitySet = false;
}

void ElasticNetwork::deleteProteins() {
	if(protein != NULL) {
		for(int i = 0; i < numProteins; ++i) {
			delete protein[i];
			protein[i] = NULL;
		}
		delete[] protein;
	}

	numProteins = 0;
}

void ElasticNetwork::deleteBonds() {
	if(bond != NULL) {
		for(int i = 0; i < numBonds; ++i) {
			delete bond[i];
			bond[i] = NULL;
		}
		delete[] bond;
		bond = NULL;
	}
	numBonds = 0;
}

void ElasticNetwork::deleteNodes() {
	if(node.size() != 0) {

		int i;
		for(i = 0; i < node.size(); ++i) {
			if(node[i] != NULL) {
				delete node[i];
				node[i] = NULL;
			}
		}
		node.clear();
		node.shrink_to_fit();

		activeNIndex.clear();
		deletedNIndex.clear();

		freeNode.clear();
		freeNode.shrink_to_fit();

		numNodes = 0;
	}
}

void ElasticNetwork::deleteConnections() {

	if(connection.size() != 0) {

		int i, j;

		// Delete connections, and all pointers to them
		for(i = 0; i < connection.size(); ++i) {
			if(connection[i] != NULL) {
				for(j = 0; j < connection[i]->getNumTerminals(); ++j) {
					connection[i]->getTerminal(j)->removeConnection(connection[i]);
				}

				delete connection[i];
				connection[i] = NULL;
			}
		}

		// Get rid of all list memory
		connection.clear();
		connection.shrink_to_fit();

		activeCIndex.clear();
		deletedCIndex.clear();

		numConnections = 0;
	}

	// System bond energy
	zeroBondEnergy();
}

void ElasticNetwork::deleteForces() {

	if(force.size() != 0) {

		int i;

		// Delete connections, and all pointers to them
		for(i = 0; i < force.size(); ++i) {
			delete force[i];
			force[i] = NULL;
		}

		// Get rid of all list memory
		force.clear();
		force.shrink_to_fit();
		numUniqueForces = 0;
	}
}

int ElasticNetwork::setParameter(string name, string value) {

	boost::algorithm::to_lower(name);

	if(name == "numproteins") {
		setNumProteins(stoi(value));
	} else if(name == "numbonds") {
		setNumBonds(stoi(value));
	} else if(name == "numnodes") {
		setNumNodes(stoi(value));
	} else if(name == "numconnections") {
		setNumConnections(stoi(value));
	} else if(name == "pos") {
		setPosition(stringToVector3(value));
	} else if(name == "globalrotation") {
		setOrientation(stringToVector2(value));
	} else {
		return BioNetError;
	}
	return BioNetSuccess;
}

string ElasticNetwork::getParameter(string name) {

	if(name == "numProteins") {
		return to_string(getNumProteins());
	} else if(name == "numBonds") {
		return to_string(getNumBonds());
	} else if(name == "numNodes") {
		return to_string(getNumNodes());
	} else if(name == "numConnections") {
		return to_string(getNumConnections());
	} else if(name == "pos") {
		return Vector3ToString(getPosition());
	} else if(name == "globalRotation") {
//		return Vector2ToString(getOrientation());
		return "";
	} else {
		return "";
	}
}

void ElasticNetwork::setNumThreads(int n) {
	numThreads = n;
}

int ElasticNetwork::getNumThreads() {
	return numThreads;
}

void ElasticNetwork::setNumProteins(int n) {
	numProteins = n;
}

int ElasticNetwork::getNumProteins() {
	return numProteins;
}


void ElasticNetwork::setNumBonds(int n) {
	numBonds = n;
}

int ElasticNetwork::getNumBonds() {
	return numBonds;
}

void ElasticNetwork::setNumNodes(int n) {
	numNodes = n;
}

int ElasticNetwork::getNumNodes() {
	return numNodes;
}

void ElasticNetwork::setNumConnections(int n) {
	numConnections = n;
}

int ElasticNetwork::getNumConnections() {
	return numConnections;
}

int ElasticNetwork::getNumForces() {
	return numUniqueForces;
}

void ElasticNetwork::setPosition(scalar x, scalar y, scalar z) {
	pos(0) = x;
	pos(1) = y;
	pos(2) = z;
}

void ElasticNetwork::setPosition(Vector3 p) {
	pos = p;
}

Vector3 ElasticNetwork::getPosition() {
	return pos;
}

void ElasticNetwork::setCentroid(scalar x, scalar y, scalar z) {
	centroid(0) = x;
	centroid(1) = y;
	centroid(2) = z;
}

void ElasticNetwork::setCentroid(Vector3 p) {
	
	int i;
	Vector3 trans;	

	// Work out translation vector
	calculateCentroid();
	trans = p - getCentroid();
	
	// Translate all nodes by this amount
	for(int i = 0; i < node.size(); ++i) {
		if(node[i] != NULL) {
			node[i]->translate(trans);

		}
	}
	calculateCentroid();
}

Vector3 ElasticNetwork::getCentroid() {
	return centroid;
}

Vector3 ElasticNetwork::getDimensions() {
	return dimension;
}

Distribution * ElasticNetwork::getDispersion() {
	return &dispersion;
}

void ElasticNetwork::setOrientation(scalar theta, scalar phi) {

}

void ElasticNetwork::setOrientation(Vector2 o) {

}

void ElasticNetwork::setOrientation(Quat q) {
	orientation= q;
}

Quat ElasticNetwork::getOrientation() {
	return orientation;
}

vTObject * ElasticNetwork::getProtein(string pName) {

	for(int i = 0; i < numProteins; ++i) {
		if(protein[i]->getName() == pName) {
			return protein[i];
		}
	}
	return NULL;
}

vTObject * ElasticNetwork::getProtein(int index) {

	if(index >= numProteins) {
		return NULL;
	}

	return protein[index];
}

vTObject ** ElasticNetwork::getProteins() {
	return protein;
}

cTObject * ElasticNetwork::getBond(string bName) {
	for(int i = 0; i < numBonds; ++i) {
		if(bond[i]->getName() == bName) {
			return bond[i];
		}
	}
	return NULL;
}

cTObject * ElasticNetwork::getBond(int index) {

	if(index >= numBonds) {
		return NULL;
	}

	return bond[index];
}

cTObject ** ElasticNetwork::getBonds() {
	return bond;
}

cTObject * ElasticNetwork::getKineticBond() {
	return kineticBondTemplate;
}

vObject * ElasticNetwork::getNode(int index) {

	if(index >= numNodes) {
		return NULL;
	}
	return node[index];
}

vector<vObject*> ElasticNetwork::getNodes() {
	return node;
}

cObject * ElasticNetwork::getConnection(int index) {

	if(index >= numConnections) {
		return NULL;
	}

	return connection[index];
}

vector<cObject*> ElasticNetwork::getConnections() {
	return connection;
}

vector<fObject*> ElasticNetwork::getUniqueForces() {
	return force;
}
		
VectorXd ElasticNetwork::getEigenvalues() {
	return nmEigenvalues;
}

MatrixXd ElasticNetwork::getEigenvectors() {
	return nmEigenvectors;
}

scalar ElasticNetwork::getElasticEnergy() {
	return elasticEnergy;
}

scalar ElasticNetwork::getKineticEnergy() {
	return kineticEnergy;
}

scalar ElasticNetwork::getBondEnergy() {
	return bondEnergy;
}

void ElasticNetwork::zeroBondEnergy() {
	bondEnergy = 0.0;
}

scalar ElasticNetwork::getTotalEnergy() {
	return totalEnergy;
}

void ElasticNetwork::zeroTotalEnergy() {

	elasticEnergy = 0.0;
	kineticEnergy = 0.0;
	totalEnergy = 0.0;
}

void ElasticNetwork::setBox(SimulationBox *b) {
	box = b;
}

void ElasticNetwork::setPhysicalSystem(PhysicalSystem *s) {
	pSys = s;
}

void ElasticNetwork::resetSaturation() {

	int i, j;
	vObject *iS;
	for(i = 0; i < node.size(); ++i) {
		if(node[i] != NULL) {
			node[i]->setSaturated(false);
			for(j = 0; j < node[i]->getNumChildren(); ++j) {
				node[i]->getChild(j)->setSaturated(false);
			} 
		}
	}
}

void ElasticNetwork::recalculateSaturation() {

	int i, j;
	for(i = 0; i < node.size(); ++i) {
		if(node[i] != NULL) {
	//		node[i]->calculateSaturation();
			for(j = 0; j < node[i]->getNumChildren(); ++j) {
//				node[i]->getChild(j)->calculateSaturation();
			} 
		}
	}
}

int ElasticNetwork::build(NetworkParameters *params, NetworkFileReader *fileIn, NetworkFileWriter *fileOut) {

	// Message
	fileLog->printMethodTask("Building Elastic Network Components...");

	// Set num threads locally immediately
	#ifdef _OPENMP
		setNumThreads(omp_get_max_threads());
	#endif

	// Set general variables
	this->params = params;
	initType = params->initType;
	initTypeSequence = params->initTypeSequence; 
	simType = params->simType;
	localConnectivitySet = params->localConnectivitySet;

	// First, we need to build the lists of available structure (proteins and bonds)
	if(buildProteins(fileIn->proteins, fileIn->iSites, params) == BioNetError) {
		fileLog->printError("Unable to build protein templates.");
		return BioNetError;
	}

	if(buildBonds(fileIn->bonds, params) == BioNetError) {
		fileLog->printError("Unable to build bond templates.");
		return BioNetError;
	}

	// We need to build a set of nodes. The nodes are spatial locations with templates applied
	if(buildNodes(fileIn->nodes, params) == BioNetError) {
		fileLog->printError("Unable to build network nodes.");
		return BioNetError;
	}

	// We need to build a set of connections for the nodes. The connections point to nodes, and have templates applied
	if(buildConnections(fileIn->connections, params) == BioNetError) {
		fileLog->printError("Unable to build network connections.");
		return BioNetError;
	}

	if(buildForces(fileIn->forces, params) == BioNetError) {
		fileLog->printError("Unable to build network force objects.");
		return BioNetError;
	}

	// Get some stuff that we'll need
	if(isInertialSimulation(simType)) {
		integrator = new InertialEulerIntegrator();
	} else {
		integrator = new ViscousEulerIntegrator();		
	}

	// Final non-trivial things
	calculateDispersion();

	// If all of this was successful, connect to the global NetworkFileWriter object
	this->fileOut = fileOut;

//	printNodeDetails();
//	printConnectionDetails();

	fileLog->printMethodSuccess();
//	return BioNetError;
	return BioNetSuccess;
}

int ElasticNetwork::rebuildFromTrajectory(NetworkParameters *params, NetworkFileReader *fileIn, NetworkFileWriter *fileOut) {

	// Message
	fileLog->printMethodTask("Rebuilding Elastic Network Components...");

	// Delete existing (order important)
	deleteConnections();
	deleteNodes();

	// For rebuilding, we currently need to build a set of nodes and connections. The nodes are spatial locations with templates applied
	if(buildNodes(fileIn->nodes, params) == BioNetError) {
		fileLog->printError("Unable to build network nodes.");
		return BioNetError;
	}

	// We need to build a set of connections for the nodes. The connections point to nodes, and have templates applied
	if(buildConnections(fileIn->connections, params) == BioNetError) {
		fileLog->printError("Unable to build network connections.");
		return BioNetError;
	}

	// We need to build a set of unique forces
	if(buildForces(fileIn->forces, params) == BioNetError) {
		fileLog->printError("Unable to build network force objects.");
		return BioNetError;
	}

	// Final non-trivial things
	calculateDispersion();

	fileLog->printMethodSuccess();
//	return BioNetError;
	return BioNetSuccess;
}

int ElasticNetwork::buildProteins(map<int, map<string,string>> proteinMap, map<int, map<int, map<string, string>>> childMap, NetworkParameters* params) {

	// Delete first, to make this method reusable
	deleteProteins();

	// Check for zero sized map (no proteins)
	if(proteinMap.size() == 0) {
		return BioNetSuccess;
	}

	// Get all of the nodes (currently all nodes. In future, expand to build from 'type')
	protein = new vTObject*[proteinMap.size()];

	// Get some variables
	map<string, string> aProtein;
	map<int, map<string,string>>::iterator itOut;
	map<string,string>::iterator itIn;
	string lValue, rValue;

	// Loop over map
	numProteins = 0;
	int numWarnings = 0, pIndex;
	for(itOut = proteinMap.begin(); itOut != proteinMap.end(); ++itOut) {

		// Get a protein structure
		pIndex = itOut->first;
		aProtein = itOut->second;

		//
		// Build template object
		//
		protein[numProteins] = new Protein();

		// Set the index (so it knows where it is in the list)
		if(protein[numProteins]->setParameter("tIndex", to_string(pIndex)) == BioNetWarning) {
			numWarnings++;
		}

		// Loop over the attributes
		for(itIn = aProtein.begin(); itIn != aProtein.end(); ++itIn) {

			lValue = itIn->first;
			rValue = itIn->second;
			try {
				if(protein[numProteins]->setParameter(lValue, rValue) == BioNetWarning) {
					numWarnings++;
				}
			} catch (invalid_argument& e) {
				fileLog->printError(lValue, "Unable to parse parameter '" + rValue + "'");
				return BioNetError;
			}
		}

		// We also need to deal with interaction sites here
		if(protein[numProteins]->buildSites(childMap[pIndex], params) == BioNetError) {
			fileLog->printError("Unable to build network protein " + to_string(numProteins) + " sites.");
			return BioNetError;
		}

		numProteins += 1;
	}

	if(numWarnings != 0) {
		fileLog->printWarning("For your proteins, you attempted to specify " + to_string(numWarnings) + " unrecognised parameters.\n\t\tWe will attempt to continue.");
	}

	return BioNetSuccess;
}

int ElasticNetwork::buildBonds(map<int, map<string,string>> bondMap, NetworkParameters* params) {

	// Delete first, to make this method reusable
	deleteBonds();

	// Check for zero sized map (no bonds)
	if(bondMap.size() == 0) {
		return BioNetSuccess;
	}

	// Get all of the connections
	bond = new cTObject*[bondMap.size()];

	// Get some variables
	map<string, string> aBond;
	map<int, map<string,string>>::iterator itOut;
	map<string,string>::iterator itIn;

	string lValue, rValue;

	// Loop over map
	numBonds = 0;
	int numWarnings = 0;
	int bIndex;
	for(itOut = bondMap.begin(); itOut != bondMap.end(); ++itOut) {

		// Get a bond
		bIndex = itOut->first;
		aBond = itOut->second;

		//
		// Build template object
		//
		bond[numBonds] = new Bond();

		// Set the index (so it knows where it is in the list)
		if(bond[numBonds]->setParameter("tIndex", to_string(bIndex)) == BioNetWarning) {
			numWarnings++;
		}

		// Loop over the attributes
		for(itIn = aBond.begin(); itIn != aBond.end(); ++itIn) {

			lValue = itIn->first;
			rValue = itIn->second;
			try {
				if(bond[numBonds]->setParameter(lValue, rValue) == BioNetWarning) {
					numWarnings++;
				}
			} catch (invalid_argument& e) {
				fileLog->printError(lValue, "Unable to parse parameter '" + rValue + "'");
				return BioNetError;
			}
		}

		// Register
		numBonds += 1;
	}

	if(numWarnings != 0) {
		fileLog->printWarning("For your bonds, you attempted to specify " + to_string(numWarnings) + " unrecognised parameters.\n\t\tWe will attempt to continue.");
	}

	// Set the kinetic bond template, if kinetics are on
	bool kineticBondSet = false;
	if(params->bondKineticsActive) {
		for(bIndex = 0; bIndex < numBonds; ++bIndex) {
			if(bond[bIndex]->isKinetic()) {
				kineticBondTemplate = bond[bIndex];
				kineticBondSet = true;
				break;
			}
		}

		// Check
		if(!kineticBondSet) {

			// Set the top bond (there must be at least one bond if we're here)
			kineticBondTemplate = bond[0];
			bond[0]->setKinetic(true);
		}
	}

	return BioNetSuccess;
}

int ElasticNetwork::buildNodes(map<int, map<string,string>> nodeMap, NetworkParameters *params) {

	// Delete first, to make this method reusable
	deleteNodes();

	// Check for zero sized map (no nodes)
	if(nodeMap.size() == 0) {
		return BioNetSuccess;
	}

	// Check for templates
	if(numProteins == 0) {
		fileLog->printError("ElasticNetwork::numProteins", "We need at least one protein (template) to build a node object.");
		return BioNetError;
	}


	// Reserve some memory space
	node.reserve(nodeMap.size());

	// Get some variables
	map<string, string> aNode;
	map<int, map<string,string>>::iterator itOut;
	map<string,string>::iterator itIn;
	
	string lValue, rValue;

	// Loop over map
	numNodes = 0;

	int numWarnings = 0;
	int nIndex;
	string pName = "", typestring;
	vTObject *prot;
	vObject *nod;
	for(itOut = nodeMap.begin(); itOut != nodeMap.end(); ++itOut) {

		// Get a node
		nIndex = itOut->first;
		aNode = itOut->second;

		//
		// Build simulated object
		//

		// Get template
		if(aNode["templateindex"] != "") {
			prot = getProtein(stoi(aNode["templateindex"]));
		} else {
			pName = aNode["template"];
			if(pName == "") {
				pName = aNode["protein"];
				if(pName == "") {
					fileLog->printError("vObject::template", "Required, but not provided");
					return BioNetError;
				}
			}

			prot = getProtein(pName);

		}

		// This protein should be templated. If not, return an error
		if(prot == NULL) {
			fileLog->printError("vObject::template", "'" + pName + "' does not correspond to a specified template");
			return BioNetError;
		}

		// Define node type depending on template data
		typestring = prot->getParameter("type");
		if(typestring == proteinTypeString[PPoint]) {
			nod = new PointNode();
		} else if (typestring == proteinTypeString[PSphere]) {
			nod = new SphereNode();
		} else if (typestring == proteinTypeString[PEllipsoid]) {
			nod = new EllipsoidNode();
		} else {
			fileLog->printError("Protein::type", "'" + prot->getParameter("type") + "' does not correspond to a recognised protein type. Cannot build node.");
			return BioNetError;
		}

		// Set the index (so it knows where it is in the list)
		if(nod->setParameter("index", to_string(nIndex)) == BioNetWarning) {
			numWarnings++;
		}

		// Loop over the attributes
		for(itIn = aNode.begin(); itIn != aNode.end(); ++itIn) {

			lValue = itIn->first;
			rValue = itIn->second;
			try {
				if(lValue == "protein") {
					continue;
				} else if(nod->setParameter(lValue, rValue) == BioNetWarning) {
					numWarnings++;
				}
			} catch (invalid_argument& e) {
				fileLog->printError(lValue, "Unable to parse parameter '" + rValue + "'");
				delete nod;
				nod = NULL;
				return BioNetError;
			}
		}


		// Now, apply protein template
		nod->applyTemplate(prot, params);

		// Quite consistency check
//		if(params->localConnectivitySet && nod->getProteinType() != PPoint && nod->getNumChildren() == 0) {
//			fileLog->printError("vObject::numChildren", "With local connectivity (-l) set, node must have children. Please provide an appropriate template.");
//			return BioNetError;
//		}

		// Add to the network
		if(addNode(nod) == BioNetError) {
			fileLog->printError("ElasticNetwork::nodes", "Unable to add node " + to_string(numNodes) + " to the network :(");
			delete nod;
			nod = NULL;
			return BioNetError;			
		}
	}

	// Resize the memory properly
	node.shrink_to_fit();

	// The number of freeNodes we may have is the same size 
//	freeNode.resize(node.size(), -1);

	if(numWarnings != 0) {
		fileLog->printWarning("For your node, you attempted to specify approximately " + to_string(float(numWarnings) / numNodes) + " unrecognised parameters per node.\n\t\tWe will attempt to continue.");
	}
	return BioNetSuccess;
}

int ElasticNetwork::buildConnections(map<int, map<string,string>> connectionMap, NetworkParameters *params) {

	
	// Delete first, to make this method reusable
	deleteConnections();

	// Check for templates (if necessary)
	if(numBonds == 0) {
	
		// No templates needed if there are no bonds and never will be
		if(connectionMap.size() == 0 && !params->bondKineticsActive) {
			fileLog->printError("ElasticNetwork::numBonds", "We need at least one bond (template) to build a connection object.");
			return BioNetError;
		}
	}

	// Check for zero sized map (no nodes)
	if(connectionMap.size() == 0) {
		return BioNetSuccess;
	}

	// Get all of the connections
	connection.reserve(connectionMap.size());

	// Get some variables
	map<string, string> aConnection;
	map<int, map<string,string>>::iterator itOut;
	map<string,string>::iterator itIn;

	string lValue, rValue;

	// Loop over map
	numConnections = 0;
	int i, numWarnings = 0, cIndex;
	string bName = "", typestring;
	cTObject *bon;
	cObject *con;
	vObject *nod;
	vector<int> nIndices, sIndices;
	vector<vObject*> terminals;

	for(itOut = connectionMap.begin(); itOut != connectionMap.end(); ++itOut) {

		// Get a connection
		cIndex = itOut->first;
		aConnection = itOut->second;

		//
		// Build simulated object
		//

		// Get template
		if(aConnection["templateindex"] != "") {
			bon = getBond(stoi(aConnection["templateindex"]));
		} else {

			bName = aConnection["template"];
			if(bName == "") {
				bName = aConnection["bond"];
				if(bName == "") {
					fileLog->printError("cObject::template", "Required, but not provided");
					return BioNetError;
				}
			}

			bon = getBond(bName);
		}

		// This bond should be templated. If not, return an error
		if(bon == NULL) {
			fileLog->printError("cObject::template", "'" + bName + "' does not correspond to a specified template");
			return BioNetError;
		}
		
		// Define bond type depending on template data
		typestring = bon->getParameter("type");
		if(typestring == bondTypeString[BRigid]) {
			con = new RigidRod1D();
		} else if(typestring == bondTypeString[BSpring]) {
			con = new LinearSpring();
		} else if(typestring == bondTypeString[BFENE]) {
			con = new FENESpring();
		} else if(typestring == bondTypeString[BAngle]) {
			con = new AngularSpring();
		} else if(typestring == bondTypeString[BWLC]) {
//			con = new WormLikeChain();
			fileLog->printError("Bond::type", "'" + bon->getParameter("type") + "' does not correspond to a recognised bond type. Cannot build connection.");
			return BioNetError;
			
		} else {
			fileLog->printError("Bond::type", "'" + bon->getParameter("type") + "' does not correspond to a recognised bond type. Cannot build connection.");
			return BioNetError;
		}

		// Set index (so it knows where it is in the list)
		con->setIndex(itOut->first);

		// Add to active connection list
		activeCIndex.insert(itOut->first);
		
		// Loop over the attributes
		nIndices.clear();
		sIndices.clear();
		terminals.clear();
		for(itIn = aConnection.begin(); itIn != aConnection.end(); ++itIn) {

			lValue = itIn->first;
			rValue = itIn->second;
			try {
				if(lValue == "bond") {
					continue;

				// Explicit
				} else if(lValue == "nodes" || lValue == "node") {
					nIndices = stringToVectorI(rValue);
				} else if(lValue == "sites" || lValue == "site"|| lValue == "isite") {
					sIndices = stringToVectorIHard(rValue);

				// General
				} else if(con->setParameter(lValue, rValue) == BioNetWarning) {
					fileLog->printWarning(lValue, "Unrecognised Parameter.");
					numWarnings++;
				}
			} catch (invalid_argument& e) {
				fileLog->printError(lValue, "Unable to parse parameter '" + rValue + "'");
				delete con;
				con = NULL;
				return BioNetError;
			}
		}

		// Now, determine the connectivity (sites or nodes)

		// Check lengths for consistency
		if(nIndices.size() == 0) {
			fileLog->printError("cObject::nodes", "Node indices must be set in order to connect this cObject to a node.");
			delete con;
			con = NULL;
			return BioNetError;
		}

		if(localConnectivitySet) {

			if(sIndices.size() != nIndices.size()) {
				fileLog->printError("cObject::nodes", "The number of nodes and sites must be equal. If a node cannot support sites, use any integer (it will be ignored).");
				delete con;
				con = NULL;
				return BioNetError;
			}
		}

		// Now, build the terminal array
		for(i = 0; i < nIndices.size(); ++i) {
			
			// More consistency checks
			if(nIndices.at(i) < 0 || nIndices.at(i) >= numNodes) {
				fileLog->printError("cObject::nodes", "Node indices cannot exceed numNodes - 1 (connection "+ to_string(numConnections) + " trying to connect to node "+ to_string(nIndices.at(i)) + ")");
				delete con;
				con = NULL;
				return BioNetError;
			}

			// Get the node
			nod = getNode(nIndices.at(i));

			// Here is where we override for points)
			if(localConnectivitySet && nod->getProteinType() != PPoint) {

				// More consistency checks
				if(sIndices.at(i) < 0 || sIndices.at(i) >= nod->getNumChildren()) {
					fileLog->printError("cObject::sites", "Site indices cannot exceed numChildren - 1 on the associated node (connection "+ to_string(numConnections) + " trying to connect to node "+ to_string(nIndices.at(i)) + ", site "+ to_string(sIndices.at(i)) + ")");
					delete con;
					con = NULL;
					return BioNetError;
				}

				// Add to list
				terminals.push_back(nod->getChild(sIndices.at(i)));

			} else {

				// Add to list
				terminals.push_back(nod);
			}
		}

		// Now, apply protein template
		con->applyTemplate(bon);

		// Add to the network
		if(addConnection(con, terminals) == BioNetError) {
			fileLog->printError("ElasticNetwork::connections", "Unable to add connection " + to_string(numConnections) + " to the network :(");
			delete con;
			con = NULL;
			return BioNetError;			
		}
	}

	// Resize the memory properly
	connection.shrink_to_fit();

	if(numWarnings != 0) {
		fileLog->printWarning("For your connections, you attempted to specify approximately " + to_string(float(numWarnings) / numConnections) + " unrecognised parameters per connection.\n\t\tWe will attempt to continue.");
	}

	return BioNetSuccess;
}


int ElasticNetwork::buildForces(map<int, map<string,string>> forceMap, NetworkParameters *params) {

	// Delete, if necessary
	deleteForces();

	// Resize vector
	force.resize(forceMap.size());

	// Get some variables
	int i;
	fObject *fo;
	map<string, string> aForce;
	map<int, map<string,string>>::iterator itOut;
	map<string,string>::iterator itIn;
	
	string lValue, rValue;

	// Loop over map
	numUniqueForces = 0;

	int numWarnings = 0;
	int fIndex, offset = 0;
	string fTypeString;
	ForceType fType;

	vector<int> nIndices, sIndices;

	for(itOut = forceMap.begin(); itOut != forceMap.end(); ++itOut) {

		// Get a force
		fIndex = itOut->first;
		aForce = itOut->second;

		//
		// Build polymorphic object
		//

		// Get type
		fTypeString = aForce["type"];
		if(fTypeString == "") {
			fTypeString = aForce["name"];
			if(fTypeString == "") {
				fileLog->printError("fObject::type", "Required, but not provided");
				return BioNetError;
			}
		}
		
		// Set a type
		fType = getForceType(fTypeString);

		// Sort out the global ones
		if(fType == FSteric || fType == FVdW || fType == FWall) {
			offset += 1;
			continue;
		}

		// Use type to initialise properly
		if(fType == FConstant) {
			fo = new ConstantForce();
		} else if (fType == FConstantVelocity) {
			fo = new ConstantVelocityForce();
		} else {
			fileLog->printError("fObject::type", "'" + fTypeString + "' does not correspond to a recognised force type. Cannot build force object.");
			return BioNetError;
		}

		// Set the index
		if(fo->setParameter("index", to_string(fIndex - offset)) == BioNetWarning) {
			numWarnings++;
		}

		// Loop over the attributes
		for(itIn = aForce.begin(); itIn != aForce.end(); ++itIn) {

			lValue = itIn->first;
			rValue = itIn->second;
			try {
				if(fo->setParameter(lValue, rValue) == BioNetWarning) {
					fileLog->printWarning(lValue, "Unrecognised Parameter.");
					numWarnings++;
				}
			} catch (invalid_argument& e) {
				fileLog->printError(lValue, "Unable to parse parameter '" + rValue + "'");
				return BioNetError;
			}
		}

		// Now, get the target indices
		try {

			// Nodes (this is a limited 2 vector. Generalise in future to getNumTerminals())
			nIndices = stringToVectorI(aForce["nodes"]);
			
			// Check for consistency
			for(i = 0; i < nIndices.size(); ++i) {
				if(nIndices[i] >= getNumNodes() || nIndices[i] < 0) {
					fileLog->printError("Nodes indices provided out of bounds for number of defined nodes.");
					return BioNetError;
				}
			}

			
			if(localConnectivitySet) {

				// Children (this is a limited 2 vector. Generalise in future to getNumTerminals())
				sIndices = stringToVectorI(aForce["sites"]);

				// Should be same size
				if(sIndices.size() != nIndices.size()) {
					fileLog->printError("Number of Child vObjects not equal to number of parent vObjects.");
					return BioNetError;
				}

				// Check for consistency
				for(i = 0; i < sIndices.size(); ++i) {
					if(sIndices[i] >= node[nIndices[i]]->getNumChildren() || nIndices[i] < 0) {
						fileLog->printError("Child vObject indices provided out of bounds for number of defined children.");
						return BioNetError;
					}
				}
			}

		} catch (invalid_argument& e) {
			fileLog->printError("Unable to obtain vObject indices :(");
			return BioNetError;
		}

		// Attach the terminals and initialise
		for(i = 0; i < nIndices.size(); ++i) {
			if(localConnectivitySet) {
				fo->addTarget(node[nIndices[i]]->getChild(sIndices[i]));
			} else {
				fo->addTarget(node[nIndices[i]]);
			}
		}
		
		// Non primitives
		fo->calculateNonPrimitives();
		
		// Add to list
		force[numUniqueForces] = fo;
		numUniqueForces += 1;
	}

	if(numWarnings != 0) {
		fileLog->printWarning("For your forces, you attempted to specify " + to_string(numWarnings) + " unrecognised parameters.\n\t\tWe will attempt to continue.");
	}

	force.shrink_to_fit();
	return BioNetSuccess;
}

/*
void ElasticNetwork::addNode(vObject *v) {

	int i;

	// First, check for empties
	i = 0;
	if(freeNode.size() != 0) {
//		cout << "fc size: " << freeNode.size() << ", fc index: " << i << ", fc: " << freeNode[i] << endl;
		while(freeNode[i] != BioNetStop) {

			// Found an empty?
			if(freeNode[i] != BioNetContinue) {

				// Add the connection
				v->setIndex(freeNode[i]);
				node[freeNode[i]] = v;
				numNodes += 1;

				// Change frees list
				freeNode[i] = BioNetContinue;

				return;
			}

			i++;
		}

		// None left. Refil the freeNodes thing
		i = 0;
		while(freeNode[i] != BioNetStop) {
			freeNode[i] = BioNetStop;
			i++;
		}
	}

	// Add the connection
	v->setIndex(numNodes);
	node.push_back(v);
	numNodes += 1;

	// Update frees list
	freeNode.push_back(BioNetStop);
}
*/

int ElasticNetwork::addNode(vObject *v) {

	// When we add a node, all we need to do is add it to the lists

	// First, check for empty array positions
	if(!deletedNIndex.empty()) {

		// If not empty, add node in the first available position
		v->setIndex(*deletedNIndex.begin());
		node[*deletedNIndex.begin()] = v;

		// Add active node index
		activeNIndex.insert(*deletedNIndex.begin());

		// Remove deactive node index
		deletedNIndex.erase(deletedNIndex.begin());

		numNodes += 1;
	} else {

		// If empty, add a new node to all arrays

		// Add the node
		v->setIndex(numNodes);
		node.push_back(v);

		// Add active node index
		activeNIndex.insert(numNodes);
		numNodes += 1;
	}

	return BioNetSuccess;
}
/*
void ElasticNetwork::removeNode(int index) {

	int i, j;

	// Check for the index
	if(index >= node.size()) {
		return BioNetWarning;
	}

	// Remove all connected cObjects
	if(localConnectivitySet) {
		for(i = 0; i < node[index]->getNumChildren(); ++i) {
			for(j = 0; j < node[index]->getChild(i)->getNumConnections(); ++j) {
				removeConnection(node[index]->getChild(i)->getConnection(j)->getIndex());
			}
		}
	} else {
		for(i = 0; i < node[index]->getNumConnections(); ++i) {
			removeConnection(node[index]->getConnection(i)->getIndex());
		}
	}

	// Delete the memory
	delete *(node.begin() + index);

	// Replace in list with NULL
	node[index] = NULL;
	numNodes -= 1;

	// Update the free list
	for(i = 0; i < freeNode.size(); ++i) {
		if(freeNode[i] == BioNetContinue || freeNode[i] == BioNetStop) {
			freeNode[i] = index;
			break;
		}
	}
}


void ElasticNetwork::removeNode(vObject *v) {

	int i, j;

	// Iterate and remove
	vector<vObject*>::iterator it = node.begin();

	while(it != node.end()) {
		if((*it) == v) {

			// Update the free list (c index and list position should be the same!)
			for(i = 0; i < freeNode.size(); ++i) {
				if(freeNode[i] == BioNetContinue || freeNode[i] == BioNetStop) {
					freeNode[i] = (*it)->getIndex();
					break;
				}
			}

			// Remove from vObject
			if(localConnectivitySet) {
				for(i = 0; i < node[(*it)->getIndex()]->getNumChildren(); ++i) {
					for(j = 0; j < node[(*it)->getIndex()]->getChild(i)->getNumConnections(); ++j) {
						removeConnection(node[(*it)->getIndex()]->getChild(i)->getConnection(j)->getIndex());
					}
				}
			} else {
				for(i = 0; i < node[(*it)->getIndex()]->getNumConnections(); ++i) {
					removeConnection(node[(*it)->getIndex()]->getConnection(i)->getIndex());
				}
			}

			// Replace in list with NULL
			node[(*it)->getIndex()] = NULL;
			numNodes -= 1;

			// Delete the memory
			delete *it;

			return BioNetSuccess;
		}
		it++;
	}
}
*/

void ElasticNetwork::removeNode(int index) {

	//
	// When we remove a node, we need to remove it from the lists but only after we've completely disconnected it from everything and removed the associated connections.
	// To do this, we call the removeConnection method for every connection first
	//

	int i, j;

	// Remove all connected cObjects (this part may change if we do unfolding) 
	if(localConnectivitySet) {
		for(i = 0; i < node[index]->getNumChildren(); ++i) {
			for(j = 0; j < node[index]->getChild(i)->getNumConnections(); ++j) {
				removeConnection(node[index]->getChild(i)->getConnection(j)->getIndex());
			}
		}
	} else {
		for(i = 0; i < node[index]->getNumConnections(); ++i) {
			removeConnection(node[index]->getConnection(i)->getIndex());
		}
	}

	// Delete the memory
	delete *(node.begin() + index);

	// Replace in list with NULL
	node[index] = NULL;
	numNodes -= 1;

	// Flag the deleted node for lookup later
	activeNIndex.erase(activeNIndex.find(index));
	deletedNIndex.insert(index);
}


void ElasticNetwork::removeNode(vObject *v) {
	removeNode(v->getIndex());
}

int ElasticNetwork::addConnection(cObject *c, vector<vObject*> terminals) {

	//
	// When we add a connection, we need to add it to the lists but also connect it with the relevant nodes
	//
	int i;

	// Connect
	for(i = 0; i < terminals.size(); ++i) {
		if(c->addTerminal(terminals.at(i)) == BioNetError) {
			fileLog->printError("More nodes added to connection than it can support :(");
			return BioNetError;
		}
		if(terminals.at(i)->addConnection(c) == BioNetWarning) {
			fileLog->printError("More connections added to node than it can support :(");
			return BioNetError;
		}
	}	

	// Now, check for empty array positions and populate there
	if(!deletedCIndex.empty()) {

		// If not empty, add connection in the first available position
		c->setIndex(*deletedCIndex.begin());
		connection[*deletedCIndex.begin()] = c;

		// Add active connection index
		activeCIndex.insert(*deletedCIndex.begin());

		// Remove deactive connection index
		deletedCIndex.erase(deletedCIndex.begin());

		numConnections += 1;

	} else {

		// If empty, we are back to the number we started at. Add a new connection to all relevent arrays

		// Add the connection
		c->setIndex(numConnections);
		connection.push_back(c);

		// Add active connection index
		activeCIndex.insert(numConnections);
		numConnections += 1;
	}

	return BioNetSuccess;
}

int ElasticNetwork::addConnection(cObject *c) {


	// First, check for empty array positions and populate there
	if(!deletedCIndex.empty()) {

		// If not empty, add connection in the first available position
		c->setIndex(*deletedCIndex.begin());
		connection[*deletedCIndex.begin()] = c;

		// Add active connection index
		activeCIndex.insert(*deletedCIndex.begin());

		// Remove deactive connection index
		deletedCIndex.erase(deletedCIndex.begin());

		numConnections += 1;

	} else {

		// If empty, we are back to the number we started at. Add a new connection to all relevent arrays

		// Add the connection
		c->setIndex(numConnections);
		connection.push_back(c);

		// Add active connection index
		activeCIndex.insert(numConnections);
		numConnections += 1;
	}

	return BioNetSuccess;
}

void ElasticNetwork::removeConnection(int index) {

	//
	// When we remove a connection, we need to remove it from the lists but only after we've completely disconnected it from everything
	//

	int i;

	// Remove from vObject
	for(i = 0; i < connection[index]->getNumTerminals(); ++i) {
		connection[index]->getTerminal(i)->removeConnection(connection[index]);
	}

	// Delete the memory
	delete *(connection.begin() + index);

	// Replace in list with NULL
	connection[index] = NULL;
	numConnections -= 1;

	// Flag the deleted node for lookup later
	activeCIndex.erase(activeCIndex.find(index));
	deletedCIndex.insert(index);

	// Return the deleted index
//	return index;
}

void ElasticNetwork::removeConnection(cObject *c) {

	removeConnection(c->getIndex());
}

void ElasticNetwork::translate(Vector3 trans) {

	int i;
	for(i = 0; i < node.size(); ++i) {
		if(node[i] != NULL) {
			node[i]->translate(trans);
		}
	}

	pos += trans;
}

int ElasticNetwork::centraliseNetwork() {

	// Do we need to do this?
	if(!box->exists) {
		return BioNetSuccess;
	}
	
	// Calculate our own dimension
	calculateDimensions();

	// Get the lowest x,y,z point and translate
	translate(0.5 * (box->getDimensions() - dimension) - dimensionLL);
	
	// We need to reinitialise the unique forces
	for(int i = 0; i < numUniqueForces; ++i) {
		force[i]->calculateNonPrimitives();

	}
	return BioNetSuccess;
}

bool ElasticNetwork::validate(NetworkParameters *params) {
	if(validateStructure(params) == BioNetInvalid) {
		fileLog->printError("ElasticNetwork", "Structure invalid :(");
		return BioNetInvalid;
	}		

	return validateSimulation(params);

}

bool ElasticNetwork::validateStructure(NetworkParameters *params) {

	// Message
	fileLog->printMethodTask("Validating Network structure...");

	// Check whether connections have nodes, k (and perhaps l) given the simulation type
	// Check whether nodes have all required properties given the simulation type

	// This one looks at the structure only! Is this a properly connected network?
	int i;
	string errMessage;

	// Connections
	bool warnLength = false;
	for(i = 0; i < connection.size(); ++i) {
		
		if(connection[i] != NULL) {
			if(connection[i]->validate(params) == BioNetInvalid) {
				fileLog->printError("Connection " + to_string(i) + " has an invalid structure :(. Please fix and try again.");
				return BioNetInvalid;
			}
		}
	}

	// Nodes
	int countWarnPos = 0;
	bool warnPos = false;
	for(i = 0; i < node.size(); ++i) {
		if(node[i] != NULL) {
			if(node[i]->validate(params) == BioNetInvalid) {
				fileLog->printError("Node " + to_string(i) + " has an invalid structure :(. Please fix and try again.");
				return BioNetInvalid;
			}
		}
	}

	fileLog->printMethodSuccess();
	return BioNetValid;
}

// Some of this, anything that involve the external viscosity, needs to be ported to the ViscoElastic network
bool ElasticNetwork::validateSimulation(NetworkParameters *params) {
	
	// Message
	fileLog->printMethodTask("Validating Network simulation settings...");

	int i;
	string errMessage;

	// General first

	// What sim types are impossible (this should be the only time this refers to any other network type)
	if(isDynamicSimulation(simType)) {
		errMessage = "For the ElasticNetwork object, simType: '" + getSimTypeString(simType) + "' is unavailable. Try a ViscoElasticNetwork :)";
		fileLog->printError(errMessage);
		return BioNetInvalid;
	}

	// Sort parameters if neccessary
	if(!isDynamicSimulation(simType) || requiresInitialisation(initType)) {

		// Check we have appropriate objects
		for(i = 0; i < node.size(); ++i) {
			if(node[i] != NULL) {
				if(node[i]->getProteinType() == PPoint) {
					errMessage = "For the ElasticNetwork object, simType: '" + getSimTypeString(simType) + "' or initType: '" + getInitTypeString(initType) + "' requires dynamics of some form. Node " + to_string(i) + " cannot handle dynamics :(";
					fileLog->printError(errMessage);
					return BioNetInvalid;
				}
			}	
		}
	}

	// Eigendecomposition
	if(isNetworkModelSimulation(simType)) {
	
		// Will the eigensolver even work?
		if( simType == GNM || simType == VGNM) {
			if(params->numNMModes >= getNumNodes()) {
				fileLog->printError("NetworkParameters::numModes", "For a GNM / VGNM, we have an exclusive maximum of " + to_string(getNumNodes()) + " (numNodes) eigenvalues. You specified " + to_string(params->numNMModes) + " :(");
				return BioNetInvalid;
			}
		} else {
			if(params->numNMModes >= 3 * getNumNodes()) {
				fileLog->printError("NetworkParameters::numModes", "For a ANM / VANM / ENM / VENM, we have an exclusive maximum of " + to_string(3 * getNumNodes()) + " (3 * numNodes) eigenvalues. You specified " + to_string(params->numNMModes) + " :(");
				return BioNetInvalid;
			}
		}
	}
	
	fileLog->printMethodSuccess();
	return BioNetValid;
}

void ElasticNetwork::zeroForces() {

	for(int i = 0; i < node.size(); ++i) {
		if(node[i] != NULL) {
			node[i]->zeroTotalForce();
			node[i]->zeroTotalTorque();
		}
	}
}

void ElasticNetwork::applyForces(scalar dt) {

	int i, j;
//	cout << endl << node[50]->getTotalForce() << endl;
//	cout << endl << node[5000]->getTotalForce()[0] << ", " << node[5000]->getTotalForce()[1] << ", " << node[5000]->getTotalForce()[2] << endl;
	for(int i = 0; i < node.size(); ++i) {
		if(node[i] != NULL) {
			if(node[i]->isFrozen()) {
				node[i]->zeroTotalForce();
//				node[i]->zeroTotalTorque();
			} else {

				for(j = 0; j < 3; ++j) {
					if(node[i]->isFrozen(j)) {
						node[i]->zeroTotalForce(j);
					}

				}
				integrator->integrate(node[i], dt);
			}
		}
	}
}

int ElasticNetwork::randomise() {

	int i, j;
	scalar L;
	Vector3 trans;

	// To randomise the contents of the network without a simulation box, we need to know the system dispersity
	calculateDispersion();

	// And the current centroid
	calculateCentroid();

	// Now, get an effective box volume (assuming Cubic Crystaline packing fraction, about 50%)
	L = 2 * dispersion.getMaximum() * cbrt(getNumNodes());

	// Now, dump all the nodes in random locations in this box
	for(i = 0; i < node.size(); ++i) {
		if(node[i] != NULL) {
			for(j = 0; j < 3; ++j) {
				trans(j) = rngCont->getMinorUniformRN() * L;
			}

			node[i]->setPosition(getCentroid() + trans);
		}
	}

	fileLog->printMethodSuccess();
	return BioNetSuccess;
}

/*
int ElasticNetwork::relax(NetworkParameters *params) {


	// To relax, we need to apply elastic forces, and then integrate
	int n;
	clock_t t0, timeGone, t;
	scalar simTime, t2;
	scalar dE;

	// Message
	fileLog->printMethodTask("Mechanically relaxing the network...");
	cout << flush;

	// Open some files for writing
	ofstream tFout, mFout;
	tFout.open(params->tFname.string(), ofstream::app);
	mFout.open(params->mFname.string(), ofstream::app);

	// First, do we even need to do this?
	if(numConnections == 0 && !params->stericForcesActive) {

		// Calculate output stuff
		calculateMeasureables();

		// A frame, for completeness
		simTime = 0.0;
		fileOut->writeTrajectoryFrame(simTime, params, pSys, tFout);
		fileOut->writeMeasurementFrame(simTime, params, pSys, mFout);

		// Write footers
		fileOut->writeTrajectoryInitialisationFooter(tFout);
		fileOut->writeMeasurementInitialisationFooter(mFout);

		// Close output files 
		tFout.close();
		mFout.close();
		return BioNetSuccess;
	}

	// Now, relax forces appropriately
	cout << endl;
	if(params->stericForcesActive && box->exists) {
		if(relaxSteric(params, tFout, mFout) == BioNetError) {
			fileLog->printError("Unable to sterically relax the system :(");

			// Calculate output stuff
			calculateMeasureables();

			//
			// Final frame anyway
			//
			fileOut->writeTrajectoryFrame(pSys->simTime, params, pSys, tFout);
			fileOut->writeMeasurementFrame(pSys->simTime, params, pSys, mFout);

			// Write footers
			fileOut->writeTrajectoryInitialisationFooter(tFout);
			fileOut->writeMeasurementInitialisationFooter(mFout);

			// Close output files 
			tFout.close();
			mFout.close();

			return BioNetError;
		}

		// Repopulate the voxels
		if(box->exists) {
			box->repopulate();
		}
	}

	// Now, relax the vdw interactions
	if(params->vdwForcesActive) {
		if(relaxVdW(params, tFout, mFout) == BioNetError) {
			fileLog->printError("Unable to relax the system under VdW interactions :(");

			// Calculate output stuff
			calculateMeasureables();

			//
			// Final frame anyway
			//
			fileOut->writeTrajectoryFrame(pSys->simTime, params, pSys, tFout);
			fileOut->writeMeasurementFrame(pSys->simTime, params, pSys, mFout);

			// Write footers
			fileOut->writeTrajectoryInitialisationFooter(tFout);
			fileOut->writeMeasurementInitialisationFooter(mFout);

			// Close output files 
			tFout.close();
			mFout.close();

			return BioNetError;
		}

		// Repopulate the voxels
		if(box->exists) {
			box->repopulate();
		}
	}

	// Now, relax everything
	if(relaxAll(params, tFout, mFout) == BioNetError) {
		fileLog->printError("Unable to relax the system under all interactions :(");

		// Calculate output stuff
		calculateMeasureables();

		//
		// Final frame
		//
		fileOut->writeTrajectoryFrame(pSys->simTime, params, pSys, tFout);
		fileOut->writeMeasurementFrame(pSys->simTime, params, pSys, mFout);

		// Write footers
		fileOut->writeTrajectoryInitialisationFooter(tFout);
		fileOut->writeMeasurementInitialisationFooter(mFout);

		// Close output files 
		tFout.close();
		mFout.close();

		return BioNetError;
	}

	// Repopulate the voxels
	if(box->exists) {
		box->repopulate();
	}

	// Calculate output stuff
	calculateMeasureables();

	//
	// Final frame
	//
	fileOut->writeTrajectoryFrame(pSys->simTime, params, pSys, tFout);
	fileOut->writeMeasurementFrame(pSys->simTime, params, pSys, mFout);

	// Close output files 
	tFout.close();
	mFout.close();

	fileLog->printMethodSuccess();
	return BioNetSuccess;
}

int ElasticNetwork::relaxSteric(NetworkParameters *params, ofstream& tFout, ofstream& mFout) {

	// To relax, we need to apply steric forces, and then integrate
	int n, stepLimit;
	clock_t t0, timeGone, t;
	scalar simTime, timeRemaining, t2, eth;
	scalar dE;

	// Message
	fileLog->printMethodTask("Sterically relaxing the network...");
	cout << flush;

	// Get relaxation parameters (this method also calculates the energy)
	calculateStericRelaxationParameters(params);

	// Initial index
	n = 0;
	stepLimit = 10 * params->rNumSteps;

	// Zero forces
	zeroForces();

	// Single force type, so zero all energies as well
	zeroTotalEnergy();
	box->zeroTotalEnergy();

	// Time constant energy reduction factor
	eth = 1 - (1 / exp(1));

	// Initialise
	box->doStericInteractions();
	if(isInertialSimulation(simType)) {
//		calculateViscousForces();
	}

	dE = box->getStericEnergy();

	// Relax
	// For all steps, apply all forces, integrate, update, write (if necessary) and time it so the user
	// can get coffee or something
	cout << endl;
	t0 = clock();
	while((box->getStericEnergy() / getNumNodes()) * eth > params->rDE && (dE / getNumNodes()) * tauR(1) / params->rDt > params->rDE && n < stepLimit) {

		//
		// User Info (contains all forces and energies before the integration, not after)
		//
		if(n % params->rStepsPerFrame == 0) {

			// Write frame to file
			pSys->updateSimTime(params->rTimePerFrame);
			simTime = pSys->getSimTime();
			timeRemaining = -1;
			if(n != 0) {
				timeRemaining = (scalar)(((clock() - t0) / n) * (params->numSteps - n)) / CLOCKS_PER_SEC;
			}

			// Calculate output stuff
			calculateMeasureables();

			fileOut->writeTrajectoryFrame(simTime, params, pSys, tFout);
			fileOut->writeMeasurementFrame(simTime, params, pSys, mFout);
			fileLog->printProgressData(n, 0, params->rNumSteps, timeRemaining, params->rSimTime);
		}

		// Update
		applyForces(params->rDt);

		// And keep in box
		box->applyBoundaryConditions();

		// This is also the repopulation trigger
//		box->repopulate();

		// Start energy
		dE = box->getStericEnergy();

		// Do interactions to get current state
		box->doStericInteractions();

		// Energy change
		dE -= box->getStericEnergy();

		// Index
		n += 1;
	}

	// Zero forces again
	zeroForces();

	// Finalise sim time
	if(params->rStepsPerFrame != 0) {
		pSys->updateSimTime((n % params->rStepsPerFrame) * params->rDt);
	}
	simTime = pSys->getSimTime();

	// User info
	timeRemaining = 0;
	fileLog->printProgressData(n, 0, params->rNumSteps, timeRemaining, params->rSimTime);
	fileLog->printFinalProgressData((scalar)(clock() - t0) / CLOCKS_PER_SEC, simTime);
	if (n == stepLimit && n != 0) {
		fileLog->printError("Steric relaxation hit the time limit. Steric energy could not be minimised to zero :(");
		return BioNetError;
		
	}

	fileLog->printMethodSuccess();
	return BioNetSuccess;
}

int ElasticNetwork::relaxVdW(NetworkParameters *params, ofstream& tFout, ofstream& mFout) {
	
	// To relax, we need to apply steric forces, and then integrate
	int n, stepLimit;
	clock_t t0, timeGone, t;
	scalar simTime, timeRemaining, t2, eth;
	scalar dE;

	// Message
	fileLog->printMethodTask("Relaxing the network under VdW interactions...");
	cout << flush;

	// Get relaxation parameters (this method also calculates the energy)
	calculateVdWRelaxationParameters(params);

	// Initial index
	n = 0;
	stepLimit = 10 * params->rNumSteps;

	// Zero forces and energies first, to be certain
	zeroForces();

	// Single force type, so zero all energies as well
	zeroTotalEnergy();
	box->zeroTotalEnergy();

	// Time constant energy reduction factor
	eth = 1 - (1 / exp(1));

	// Initialise
	box->doVdWInteractions();
	if(isInertialSimulation(simType)) {
//		calculateViscousForces();
	}

	dE = box->getVdWEnergy();

	// Relax
	// For all steps, apply all forces, integrate, update, write (if necessary) and time it so the user
	// can get coffee or something
	cout << endl;
	t0 = clock();
	while((box->getRelativeVdWEnergy() / getNumNodes()) * eth > params->rDE && (dE / getNumNodes()) * tauR(1) / params->rDt > params->rDE && n < stepLimit) {

		//
		// User Info (contains all forces and energies before the integration, not after)
		//
		if(n % params->rStepsPerFrame == 0) {

			// Write frame to file
			pSys->updateSimTime(params->rTimePerFrame);
			simTime = pSys->getSimTime();

			timeRemaining = -1;
			if(n != 0) {
				timeRemaining = (scalar)(((clock() - t0) / n) * (params->numSteps - n)) / CLOCKS_PER_SEC;
			}

			// Calculate output stuff
			calculateMeasureables();

			fileOut->writeTrajectoryFrame(simTime, params, pSys, tFout);
			fileOut->writeMeasurementFrame(simTime, params, pSys, mFout);
			fileLog->printProgressData(n, 0, params->rNumSteps, timeRemaining, params->rSimTime);
		}

		// Update
		applyForces(params->rDt);

		// And keep in box
		box->applyBoundaryConditions();

		// This is also the repopulation trigger
//		box->repopulate();

		// Start energy
		dE = box->getVdWEnergy();

		// Do interactions to get current state
		box->doVdWInteractions();

		if(isInertialSimulation(simType)) {
//			calculateViscousForces();
		}

		// Energy change
		dE -= box->getVdWEnergy();

		// Index
		n += 1;
	}

	// Zero forces again
	zeroForces();

	// Finalise sim time
	if(params->rStepsPerFrame != 0) {
		pSys->updateSimTime((n % params->rStepsPerFrame) * params->rDt);
	}	
	simTime = pSys->getSimTime();

	// User info
	timeRemaining = 0;
	fileLog->printProgressData(n, 0, params->rNumSteps, timeRemaining, params->rSimTime);
	fileLog->printFinalProgressData((scalar)(clock() - t0) / CLOCKS_PER_SEC, simTime);
	if (n == stepLimit && n != 0) {
		fileLog->printWarning("VdW relaxation hit the time limit. Should be ok, but make sure to check!");
	}

	fileLog->printMethodSuccess();
	return BioNetSuccess;
}
*/
/*
int ElasticNetwork::relaxAll(NetworkParameters *params, ofstream& tFout, ofstream& mFout) {

	// To relax, we need to apply steric forces, and then integrate
	int n, stepLimit;
	clock_t t0, timeGone, t;
	scalar simTime, timeRemaining, t2, eth;
	scalar dE;

	// Message
	fileLog->printMethodTask("Relaxing the network under all interactions...");
	cout << flush;

	// Get relaxation parameters
	calculateRelaxationParameters(params);

	// Initial index
	n = 0;
	stepLimit = 10 * params->rNumSteps;

	// Zero forces and energies first, to be certain
	zeroForces();

	zeroTotalEnergy();
	box->zeroTotalEnergy();

	// Time constant energy reduction factor
	eth = 1 - (1 / exp(1));

	// Initialise
	box->doAllInteractions();

	// Last as restraints implicit
	calculateElasticForces();		
	if(isInertialSimulation(simType)) {
//		calculateViscousForces();
	}

	dE = box->getTotalRelativeEnergy() + getTotalEnergy();

	// Relax
	// For all steps, apply all forces, integrate, update, write (if necessary) and time it so the user
	// can get coffee or something
	cout << endl;
	t0 = clock();

//	while((fabs(dE) / getNumNodes()) * tauR(1) / params->rDt > params->rDE && n < stepLimit) {
//	while(n < stepLimit) {
	while(((box->getTotalRelativeEnergy() + getTotalEnergy()) / getNumNodes()) * eth > params->rDE && (dE / getNumNodes()) * tauR(1) / params->rDt > params->rDE && n < stepLimit) {

		//
		// User Info (contains all forces and energies before the integration, not after)
		//
		if(n % params->rStepsPerFrame == 0) {

			// Write frame to file
			//simTime = n * params->rDt;
			pSys->updateSimTime(params->rTimePerFrame);
			simTime = pSys->getSimTime();

			timeRemaining = -1;
			if(n != 0) {
				timeRemaining = (scalar)(((clock() - t0) / n) * (params->numSteps - n)) / CLOCKS_PER_SEC;
			}

			// Calculate output stuff
			calculateMeasureables();

			fileOut->writeTrajectoryFrame(simTime, params, pSys, tFout);
			fileOut->writeMeasurementFrame(simTime, params, pSys, mFout);
			fileLog->printProgressData(n, 0, params->rNumSteps, timeRemaining, params->rSimTime);

		}

		// Update
		applyForces(params->rDt);

		// And keep in box
		box->applyBoundaryConditions();

		// This is also the repopulation trigger
//		box->repopulate();

		// Start energy
		dE = box->getTotalEnergy() + getTotalEnergy();


		// Do all interactions to get current state
		box->doAllInteractions();
		
		// Last as restraints implicit
		calculateElasticForces();		
		if(isInertialSimulation(simType)) {
//			calculateViscousForces();
		}

		// Energy change
		dE -= box->getTotalEnergy() + getTotalEnergy();

		// Index
		n += 1;
	}

	// Finalise sim time
	if(params->rStepsPerFrame != 0) {
		pSys->updateSimTime((n % params->rStepsPerFrame) * params->rDt);
	}
	simTime = pSys->getSimTime();

	// User info
	timeRemaining = 0;
	fileLog->printProgressData(n, 0, params->rNumSteps, timeRemaining, params->rSimTime);
	fileLog->printFinalProgressData((scalar)(clock() - t0) / CLOCKS_PER_SEC, simTime);
	if (n == stepLimit && n != 0) {
		fileLog->printWarning("Total relaxation hit the time limit. Should be ok, but make sure to check!");
	}

	fileLog->printMethodSuccess();
	return BioNetSuccess;
}
*/

/*
int ElasticNetwork::relax(NetworkParameters *params) {


	// To relax, we need to apply elastic forces, and then integrate
	int n;
	clock_t t0, timeGone, timeRemaining, t;
	scalar simTime, t2;
	scalar dE;

	// Message
	fileLog->printMethodTask("Mechanically relaxing the network...");

	// Open some files for writing
	ofstream tFout, mFout;
	tFout.open(params->tFname, ofstream::app);
	mFout.open(params->mFname, ofstream::app);

	fileOut->writeTrajectoryInitialisationHeader(params, pSys, tFout);
	fileOut->writeMeasurementInitialisationHeader(params, pSys, mFout);

	// First, do we even need to do this?
	if(numConnections == 0 && !params->stericForcesActive) {

		// A frame, for completeness
		simTime = n * params->rDt;
		fileOut->writeTrajectoryFrame(simTime, params, pSys, tFout);
		fileOut->writeMeasurementFrame(simTime, params, pSys, mFout);

		// Write footers
		fileOut->writeTrajectoryInitialisationFooter(tFout);
		fileOut->writeMeasurementInitialisationFooter(mFout);

		// Close output files 
		tFout.close();
		mFout.close();
		return BioNetSuccess;
	}

	// Work out some parameters for relaxation

	// And now the parameters
	calculateRelaxationParameters(params);

	// Zero forces and energies first, to be certain
	zeroForces();

	// For all steps, apply all forces, integrate, update, write (if necessary) and time it so the user
	// can get coffee or something
	cout << endl;
	t0 = clock();

//	for(n = 0; n < params->rNumSteps; ++n) {

	// Initialise loop parameters (make energy huge so loop runs. Only calculate energies once per loop)
	n = 0;
	dE = sInf;
	setElasticEnergy(dE);
	

	// Loop until limit is reached
//	while(dE > params->rDE) {
	for(n = 0; n < params->rNumSteps; ++n) {

		// Get energy at start
		dE = getTotalEnergy() + box->getTotalEnergy();

		// Field forces (pair-pair, O(N^2) / O(Nln(N))
		if(params->stericForcesActive) {
			box->doStericInteractions(params);
		}

		if(params->vdwForcesActive) {
			box->doVdWInteractions();
		}

		// Internal forces (single object, O(N))
		calculateElasticForces();

		if(isInertialSimulation(simType)) {
			calculateViscousForces();
		}

		// Minus energy at end
		calculateTotalEnergy();
		dE -= getTotalEnergy() + box->getTotalEnergy();
//		cout << "E: " << getTotalEnergy() + box->getTotalEnergy() << endl;
//		cout << "dE: " << dE << endl;

		//
		// User Info (contains all forces and energies before the integration, not after)
		//
		if(n % params->rStepsPerFrame == 0) {

			// Write frame to file
			simTime = n * params->rDt;
			fileOut->writeTrajectoryFrame(simTime, params, pSys, tFout);
			fileOut->writeMeasurementFrame(simTime, params, pSys, mFout);

			if(n == 0) {
				cout << "\r\t\tStep " << n << " of " << params->rNumSteps << " (" << params->rSimTime << "ns) - ? hrs left" << flush;
			} else {
				timeRemaining = (float)(((clock() - t0) / n) * (params->rNumSteps - n)) / CLOCKS_PER_SEC;
				cout << "\r\t\t\t\t\t\t\t\t\t";
				cout << "\r\t\tStep " << n << " of ~" << params->rNumSteps << " (" << params->rSimTime << "ns) - " << timeRemaining / 3600.0 << " hrs left" << flush;
			}
		}

		// Update
		applyForces(params->rDt);

		// And keep in box
		box->applyBoundaryConditions();

		// Index
//		n += 1;
	}

	// User info
	cout << "\r                                                                                     ";
	cout << "\r\t\tStep " << n << " of " << n << " (" << params->rSimTime << "ns) - 0 hrs left" << endl << flush;

	//
	// Final frame
	//
	simTime = n * params->rDt;
	fileOut->writeTrajectoryFrame(simTime, params, pSys, tFout);
	fileOut->writeMeasurementFrame(simTime, params, pSys, mFout);

	// Write footers
	fileOut->writeTrajectoryInitialisationFooter(tFout);
	fileOut->writeMeasurementInitialisationFooter(mFout);

	// Close output files 
	tFout.close();
	mFout.close();

	fileLog->printMethodSuccess();
	return BioNetSuccess;
			
}
*/
/*
void ElasticNetwork::calculateStericRelaxationParameters(NetworkParameters *params) {

	// Calculate the timescales
//	box->calculateStericTimeConstants();

	// And set them
	tauR = box->getStericTimeConstants();

	// Firstly, if either time constants is inf, then the system is already relaxed
	if(tauR(0) == sInf || tauR(1) == sInf) {
		params->rDt = 0.0;
		params->rSimTime = 0.0;
		params->rTimePerFrame = 0.0;
		params->rNumSteps = 0;
		params->rStepsPerFrame = 0;
		params->rDE = sInf;
	} else {

		// We want relaxation to be defined by some proportion of kT. Therefore...

		// Fraction of kT
		params->rDE = 0.01 * params->kBT;

		// Current energy
		box->calculateStericEnergy();

		if(box->getStericEnergy() <= 0) {
			params->rDt = 0.0;
			params->rSimTime = 0.0;
			params->rTimePerFrame = 0.0;
			params->rNumSteps = 0;
			params->rStepsPerFrame = 0;
			params->rDE = sInf;	
		
		} else {

			// Relaxation parameters, assuming an exponential decay with the slowest time constant
			params->rDt = tauR(0) / 5.0;
			params->rSimTime = tauR(1) * log(box->getStericEnergy() / params->rDE);
			params->rNumSteps = int(params->rSimTime / params->rDt);

			// Round up to nearest 100 to give 100 frames
			params->rNumFrames = 30;
			params->rNumSteps = ((params->rNumSteps + params->rNumFrames) / params->rNumFrames) * params->rNumFrames;
			params->rSimTime = params->rDt * params->rNumSteps;

			params->rStepsPerFrame = params->rNumSteps / params->rNumFrames;
			params->rTimePerFrame = params->rStepsPerFrame * params->rDt;
		}
	}
}
*/
/*
void ElasticNetwork::calculateVdWRelaxationParameters(NetworkParameters *params) {

	// Calculate the timescales
//	box->calculateVdWTimeConstants();

	// And set them
	tauR = box->getVdWTimeConstants();

	// Firstly, if either time constants is inf, then the system is already relaxed
	if(tauR(0) == sInf || tauR(1) == sInf) {
		params->rDt = 0.0;
		params->rSimTime = 0.0;
		params->rTimePerFrame = 0.0;
		params->rNumSteps = 0;
		params->rStepsPerFrame = 0;
		params->rDE = sInf;

	} else {

		// We want relaxation to be defined by some proportion of kT. Therefore...

		// Fraction of kT
		params->rDE = 0.01 * params->kBT;

		// Current energy
		box->calculateVdWEnergy();

		if(box->getRelativeVdWEnergy() <= 0) {
			params->rDt = 0.0;
			params->rSimTime = 0.0;
			params->rTimePerFrame = 0.0;
			params->rNumSteps = 0;
			params->rStepsPerFrame = 0;
			params->rDE = sInf;	
		
		} else {

			// Relaxation parameters, assuming an exponential decay with the slowest time constant
			params->rDt = tauR(0) / 5.0;
			params->rSimTime = tauR(1) * log(box->getRelativeVdWEnergy() / params->rDE);
			params->rNumSteps = int(params->rSimTime / params->rDt);

			// Round up to nearest 100 to give 100 frames
			params->rNumFrames = 30;
			params->rNumSteps = ((params->rNumSteps + params->rNumFrames) / params->rNumFrames) * params->rNumFrames;
			params->rSimTime = params->rDt * params->rNumSteps;

			params->rStepsPerFrame = params->rNumSteps / params->rNumFrames;
			params->rTimePerFrame = params->rStepsPerFrame * params->rDt;
		}
	}
}
*/
/*
void ElasticNetwork::calculateRelaxationParameters(NetworkParameters *params) {

	//
	// Calculate relaxation simulation parameters, set them on the params object
	//
	
	// Default to the elastic ones
	tauR = tauE;

	// Inertial?
	if(isInertialSimulation(simType)) {
		if(tauI(0) < tauR(0)) {
			tauR(0) = tauI(0);
		}

		if(tauI(1) > tauR(1)) {
			tauR(1) = tauI(1);
		}
	}

	// Work out the relevent time constants in the whole world (if we need to)
	if(params->vdwForcesActive) {

		// Calculate them
//		box->calculateVdWTimeConstants();

		if(box->getVdWTimeConstants()(0) < tauR(0)) {
			tauR(0) = box->getVdWTimeConstants()(0);
		}

		// This is just a guess, so give the smallest of the big time constants
		// Check for infinity
		if(tauR(1) >= sInf) {
			tauR(1) = box->getVdWTimeConstants()(1);
		} else if(box->getVdWTimeConstants()(1) < tauR(1)) {
			tauR(1) = box->getVdWTimeConstants()(1);
		}
//		cout << "tauR VdW: " << tauR(0) << ", " << tauR(1) << endl;
		
	}

	if(params->stericForcesActive) {

		// Calculate them
//		box->calculateStericTimeConstants();

		if(box->getStericTimeConstants()(0) < tauR(0)) {
			tauR(0) = box->getStericTimeConstants()(0);
		}

		// This is just a guess, so give the smallest of the big time constants
		// Check for infinity
		if(tauR(1) >= sInf) {
			tauR(1) = box->getStericTimeConstants()(1);
		} else if(box->getStericTimeConstants()(1) < tauR(1)) {
			tauR(1) = box->getStericTimeConstants()(1);
		}
//		cout << "tauR Steric: " << tauR(0) << ", " << tauR(1) << endl;
	}

	// Firstly, if either time constants is inf, then the system is already relaxed
	if(tauR(0) == sInf || tauR(1) == sInf) {
		params->rDt = 0.0;
		params->rSimTime = 0.0;
		params->rTimePerFrame = 0.0;
		params->rNumSteps = 0;
		params->rStepsPerFrame = 0;
		params->rDE = sInf;
	} else {

		// We want relaxation to be defined by some proportion of kT. Therefore...

		// Fraction of kT
		params->rDE = 0.01 * params->kBT;

		// Current energy
		calculateTotalEnergy();
		box->calculateTotalEnergy();

		if(getTotalEnergy() + box->getTotalRelativeEnergy() <= 0) {
			params->rDt = 0.0;
			params->rSimTime = 0.0;
			params->rTimePerFrame = 0.0;
			params->rNumSteps = 0;
			params->rStepsPerFrame = 0;
			params->rDE = sInf;	
		} else {
			// Relaxation parameters, assuming an exponential decay with the slowest time constant
			params->rDt = tauR(0) / 5.0;
			params->rSimTime = tauR(1) * log(getTotalEnergy() + box->getTotalRelativeEnergy() / params->rDE);
			params->rNumSteps = int(params->rSimTime / params->rDt);

			// Round up to nearest 100 to give 100 frames
			params->rNumFrames = 30;
			params->rNumSteps = ((params->rNumSteps + params->rNumFrames) / params->rNumFrames) * params->rNumFrames;
			params->rSimTime = params->rDt * params->rNumSteps;

			params->rStepsPerFrame = params->rNumSteps / params->rNumFrames;
			params->rTimePerFrame = params->rStepsPerFrame * params->rDt;
		}
	}
}
*/
void ElasticNetwork::setInitialState() {

	int i;
	for(i = 0; i < node.size(); ++i) {
		if(node[i] != NULL) {
			node[i]->setInitialPosition(node[i]->getPosition());
			node[i]->setInitialVelocity(node[i]->getVelocity());
			node[i]->setInitialAcceleration(node[i]->getAcceleration());
		}
	}
}
 
int ElasticNetwork::preInitialise() {

	// Message
	fileLog->printMethodTask("Pre-preparing an ElasticNetwork...");

	// Energies (cannot cause error...)
	calculateTotalEnergy();
	calculateBondEnergy();

	if(params->kineticsActive) {
		initialiseKinetics();
	}

	fileLog->printMethodSuccess();
	return BioNetSuccess;
}

int ElasticNetwork::initialiseKinetics() {

	// Do do kinetics here, we will be linearising everything. Otherwise, the free energy integrals become infeasible
	// without their own monte carlo simulations

	// Linearlise elasticity
//	buildElasticityVector();

	// Calculate all of the initial probabilities for bonding / unbonding
	calculateReactionProbabilities();
	return BioNetSuccess;
}

int ElasticNetwork::initialise(NetworkParameters *params) {

	string errMessage;
	int index;

	// Message
	fileLog->printMethodTask("Initialising an ElasticNetwork ('" + getInitTypeString(initType) + "')...");

	// Write some file stuff
	ofstream tFout, mFout, tpFout;
	tFout.open(params->tFname.string(), ofstream::app);
	mFout.open(params->mFname.string(), ofstream::app);

	fileOut->writeTrajectoryInitialisationHeader(params, pSys, tFout);
	fileOut->writeMeasurementInitialisationHeader(params, pSys, mFout);

	// Close output files 
	tFout.close();
	mFout.close();

	// Maybe topology file
	if(params->bondKineticsActive) {
		tpFout.open(params->tpFname.string(), ofstream::app);
		fileOut->writeTopologyInitialisationHeader(params, pSys, tpFout);
		tpFout.close();
	}

	// We should have a list of initialisations
	index = 0;
	for(vector<InitType>::iterator it = initTypeSequence.begin(); it != initTypeSequence.end(); ++it) {
		cout << endl;
		fileLog->printMethodTask("Initialisation " + to_string(index) + ": " + getInitTypeString(*it) + "...");

		// Initialise
		if (*it == Random) {
			cout << endl;
			if(randomInitialise() == BioNetError) {
				errMessage = "'" + getInitTypeString(initType) + "' initialisation failed :(";
				fileLog->printError(errMessage);
				return BioNetError;	
			}
		} else if (*it == Reconnect) {
			cout << endl;
			if(reconnectionInitialise(params) == BioNetError) {
				errMessage = "'" + getInitTypeString(initType) + "' initialisation failed :(";
				fileLog->printError(errMessage);
				return BioNetError;	
			}
		} else if (*it == MonteCarloI) {
			cout << endl;
//			if(monteCarloInitialise(params) == BioNetError) {
//				errMessage = "'" + getInitTypeString(initType) + "' initialisation failed :(";
//				fileLog->printError(errMessage);
//				return BioNetError;	
//			}
		} else if (*it == Relax) {
			cout << endl;
			if(relaxationInitialise(params) == BioNetError) {
				errMessage = "'" + getInitTypeString(initType) + "' initialisation failed :(";
				fileLog->printError(errMessage);
				return BioNetError;	
			}

		}

		// Success for this method!
		fileLog->printMethodSuccess();
	}

	// Write final data out to a file
	tFout.open(params->tFname.string(), ofstream::app);
	mFout.open(params->mFname.string(), ofstream::app);

	// Write footers
	fileOut->writeTrajectoryInitialisationFooter(tFout);
	fileOut->writeMeasurementInitialisationFooter(mFout);

	// Close output files 
	tFout.close();
	mFout.close();

	// Maybe topology file
	if(params->bondKineticsActive) {
		tpFout.open(params->tpFname.string(), ofstream::app);
		fileOut->writeTopologyInitialisationFooter(tpFout);
		tpFout.close();
	}

	// Also, set initial state
	setInitialState();

	// Finalise
	fileLog->printMethodSuccess();
	return BioNetSuccess;
}

int ElasticNetwork::relaxationInitialise(NetworkParameters *params) {

	// The aim of this initialisation protocol is to relax the system. Easy
//	if(relax(params) == BioNetError) {
//		fileLog->printError("ElasticNetwork::relaxationInitialise", "Unable to mechanically relax the system :(");
//		return BioNetError;
//	}

	return BioNetSuccess;
}

int ElasticNetwork::randomInitialise() {

	// The aim of this initialisation protocol is to randomly shuffle the components around the box,
	// rotate them if they are >0D objects, then rebind if they are within range. Easy
	if(box->exists) {
		box->randomiseContents();
	} else {
		randomise();
	}

	return BioNetSuccess;
}

int ElasticNetwork::reconnectionInitialise(NetworkParameters *params) {

	fileLog->printError("ElasticNetwork::reconnectionInitialise", "Not yet supported :(");
	return BioNetError;

	/*
	// The aim of this initialisation protocol is to reconnect the network based on a cutoff distance
	// It will use the box if it can

	vector<PairPair> pairs;
	vector<PairPair>::iterator it;

	pairs = calculatePairPairList(params);

	// Now we have the pairs, sort them into order
	sort(pairs.begin(), pairs.end(), compareDistances);

	// And reconnect until saturated
	buildConnections(pairs);
	return BioNetSuccess;
	*/
}

// This method could be parallelised if we get rid of the individual methods...
void ElasticNetwork::calculateMeasureables() {

	// Quaternions (for orientations)
	calculateOrientationQuaternions();

	// Centroid (for general interest)
	calculateCentroid();

	// Energies
	if (isInertialSimulation(params->simType)) {
		calculateKineticEnergy();
	}

	// Coordination
	calculateCoordination();
}

void ElasticNetwork::calculateEulerAngles() {
	
	int i;
	for(i = 0; i < node.size(); ++i) {
		if(node[i] != NULL) {
			node[i]->calculateEulerAngles();
		}
	}
}

void ElasticNetwork::calculateOrientationQuaternions() {
	
	int i;
	for(i = 0; i < node.size(); ++i) {
		if(node[i] != NULL) {
			node[i]->calculateOrientationQuaternion();
		}
	}
}

void ElasticNetwork::calculateReactionProbabilities() {

	int i;

	for(i = 0; i < node.size(); ++i) {
		if(node[i] != NULL) {
			node[i]->calculateReactionProbability(params->dt);
		}
	}
}

void ElasticNetwork::calculateDimensions() {
	
	int i, j;

	// Loop over all nodes and get maximum and minimum

	// Initialise with first node
	for(i = 0; i < 3; ++i) {
		dimensionUL(i) = node[0]->getPosition()(i) + node[0]->getLengthscale();
		dimensionLL(i) = node[0]->getPosition()(i) - node[0]->getLengthscale();
	}

	// Loop over remaining nodes
	for(i = 1; i < node.size(); ++i) {
		if(node[i] != NULL) {
			for(j = 0; j < 3; ++j) {
				if(node[i]->getPosition()(j) + node[i]->getLengthscale() > dimensionUL(j)) {
					dimensionUL(j) = node[i]->getPosition()(j) + node[i]->getLengthscale();
				}
				if (node[i]->getPosition()(j) - node[i]->getLengthscale() < dimensionLL(j)) {
					dimensionLL(j) = node[i]->getPosition()(j) - node[i]->getLengthscale();
				}
			}
		}
	}

	// So dimensions are...
	dimension = dimensionUL - dimensionLL;
}

void ElasticNetwork::calculateDispersion() {

	// Get all particle sizes into an vector
	vector<scalar> sizes(getNumNodes());
	for(int i = 0; i < node.size(); ++i) {
		if(node[i] != NULL) {
			sizes.at(i) = node[i]->getLengthscale();
		}
	}

	// Make a distribution of it?
	dispersion.setData(sizes);

	// Calculate stuff we need
	dispersion.calculateMean();
	dispersion.calculateVariance();
	dispersion.calculateLimits();			
}

void ElasticNetwork::calculateCoordination() {

	int i, j;

	// Average coordination and distribution, so sum...
	coordination = 0.0;

	if(!localConnectivitySet) {
		for(i = 0; i < node.size(); ++i) {
			if(node[i] != NULL) {
				coordination += node[i]->getNumConnections();
			}
		}
	} else {
		for(i = 0; i < node.size(); ++i) {
			if(node[i] != NULL) {
				for(j = 0; j < node[i]->getNumChildren(); ++j) {
					coordination += node[i]->getChild(j)->getNumConnections();
				}
			}
		}
	}

	// And divide
	coordination /= numNodes;
}

scalar ElasticNetwork::getCoordination() {
	return coordination;
}

void ElasticNetwork::calculateCentroid() {

	int i;
	centroid.setZero();
	for(i = 0; i < node.size(); ++i) {
		if(node[i] != NULL) {
			centroid += node[i]->getPosition();	
		}
	}

	centroid /= getNumNodes();
}

vector<PairPair> ElasticNetwork::calculatePairPairList(NetworkParameters *params, bool availOnly) {

	// Variables
	vector<PairPair> pairs;

	// The box ought to exist if this method is being called
	box->calculatePairPairList(availOnly);
	pairs = box->getPairPairList();

	// Return
	return pairs;
}

int ElasticNetwork::simulate(NetworkParameters *params) {
	
	// Message
	fileLog->printMethodTask("Simulating an ElasticNetwork ('" + getSimTypeString(simType) + "')...");

	// Write some file stuff
	ofstream nmFout;

	// Headers
	nmFout.open(params->nmFname.string(), ofstream::app);
	fileOut->writeNormalModeSimulationHeader(params, pSys, nmFout);
	nmFout.close();

	string errMessage;

	if(simType == GNM) {

		fileLog->printMethodTask("Building (negative) laplacian matrix...");
		buildLaplacianMatrix();
		fileLog->printMethodSuccess();
//		exit(0);
		fileLog->printMethodTask("Diagonalising (may take a while)...");
		diagonaliseNetworkModelMatrix();
		fileLog->printMethodSuccess();
		
		fileLog->printMethodTask("Writing details to output files...");
//		diagonaliseNetworkModelMatrix();
		fileLog->printMethodSuccess();
		
	} else if (simType == ANM) {

		// Again, ignore any connection constants within the input file.
//		buildAnisotropicMatrix();
//		diagonaliseAnisotropicMatrix();

	} else if (simType == ENM) {

		// We can't just add the spring constants together. Move, calculateTotalEnergy, move back, calculateTotalEnergy, moveToCentre, calculateTotalEnergy. 2nd derivative
				
		// Use all data in the input file
//		buildElasticityMatrix();
//		diagonaliseElasticityMatrix();

	} else {
	
		// No idea how the user managed to get here, but we'll include it just in case
		errMessage = "Unrecognised simType: " + getSimTypeString(simType) +  ". Please contact development team.";
		fileLog->printError("ElasticNetwork::simulate", errMessage);
		return BioNetError;

	}

	// Write everything to file
	fileLog->printMethodTask("Writing eigensystem to file...");

	nmFout.open(params->nmFname.string(), ofstream::app);
	fileOut->writeEigensystem(params, pSys, nmFout);

	// And footer
	fileOut->writeNormalModeSimulationFooter(nmFout);
	nmFout.close();

	// Finalise
	fileLog->printMethodSuccess();
	return BioNetSuccess;
}

void ElasticNetwork::calculateElasticForces() {

	int i, j;
	Vector3 trans, sep;

	// Zero stuff
	zeroElasticEnergy();

	// Loop over all connections and apply force to nodes
//	#ifdef _OPENMP
//		#pragma omp parallel for reduction(+:elasticEnergy)
//	#endif


	// If this is kept serial, we can move the actual nodes! If not, it gets trickier. Developers beware...
	for(i = 0; i < connection.size(); ++i) {

		if(connection[i] != NULL) {

			// Maybe get periodic boundary conditions
			if(box->getBCType() == PBC) {

				// Use wrapped and offset get the translation
				trans = connection[i]->getTerminal(1)->getTopParent()->getWrap() - connection[i]->getTerminal(0)->getTopParent()->getWrap();
				trans = box->getDimensions().cwiseProduct(trans);
				trans -= connection[i]->getOffset();
				connection[i]->getTerminal(0)->getTopParent()->translate(-1 * trans);
			}

			sep = connection[i]->getTerminal(1)->getPosition() - connection[i]->getTerminal(0)->getPosition(); 
//			if(sep.norm() > box->getDimensions()[0] / 2.0) {
//				cout << endl << "Sep: " << sep.norm() << endl;
//				cout << "Wrap 0: " << endl << connection[i]->getTerminal(0)->getTopParent()->getWrap() << endl;
//				cout << "Wrap 1: " << endl << connection[i]->getTerminal(1)->getTopParent()->getWrap() << endl;
//				cout << "Offset: " << endl << connection[i]->getOffset() << endl;
//				exit(0);
//			}

			// Calculations
			connection[i]->calculateElasticForce();
			connection[i]->applyElasticForce();
	
			elasticEnergy += connection[i]->getElasticEnergy();

			// Maybe reverse periodic boundary conditions
			if(box->getBCType() == PBC) {
				connection[i]->getTerminal(0)->getTopParent()->translate(trans);
			}
		}
	}

	// Add to total
	totalEnergy += elasticEnergy;
//	exit(0);
}

void ElasticNetwork::calculateUniqueForces(scalar dt) {

	int i;

	for(i = 0; i < getNumForces(); ++i) {
		force[i]->apply(dt);
	}
}
/*
void ElasticNetwork::calculateViscousForces() {

	int i;

	// Loop over all node and apply force to nodes
	#ifdef _OPENMP
		#pragma omp parallel for
	#endif
	for(i = 0; i < node.size(); ++i) {

		// Force is added implicitly
		node[i]->calculateViscousForce();
	}	
}
*/
void ElasticNetwork::calculateElasticEnergy() {

	// Elastic Energy comes from all elastic connections
	int i;
	Vector3 trans;

	// Reset this energy type
	zeroElasticEnergy();

	// Initialise
	trans.setZero();

	for(int i = 0; i < connection.size(); ++i) {
		if(connection[i] != NULL) {

			// Maybe get periodic boundary conditions
			if(box->getBCType() == PBC) {

				// Use wrapped and offset get the translation
				trans = connection[i]->getTerminal(1)->getTopParent()->getWrap() - connection[i]->getTerminal(0)->getTopParent()->getWrap();
				trans = box->getDimensions().cwiseProduct(trans);
				trans -= connection[i]->getOffset();
				connection[i]->getTerminal(0)->getTopParent()->translate(-1 * trans);
			}

			connection[i]->calculateElasticEnergy();
			elasticEnergy += connection[i]->getElasticEnergy();

			// Maybe reverse periodic boundary conditions
			if(box->getBCType() == PBC) {
				connection[i]->getTerminal(0)->getTopParent()->translate(trans);

			}
		}
	}

	// And add back on
	totalEnergy += elasticEnergy;
}

void ElasticNetwork::calculateKineticEnergy() {

	// Kinetic Energy comes from all massive nodes
	int i;

	// Reset this energy type
	zeroKineticEnergy();

	for(int i = 0; i < node.size(); ++i) {
		if(node[i] != NULL) {
			node[i]->calculateKineticEnergy();
			kineticEnergy += node[i]->getKineticEnergy();
		}
	}

	// And add back on
	totalEnergy += kineticEnergy;
}

void ElasticNetwork::calculateBondEnergy() {

	// Bond Energy comes from all connections
	int i;

	// Reset this energy type
	zeroBondEnergy();

	for(int i = 0; i < connection.size(); ++i) {
		if(connection[i] != NULL) {
			bondEnergy += connection[i]->getBondEnergy();
		}	
	}
}

void ElasticNetwork::calculateTotalEnergy() {

	// Calculate
	calculateElasticEnergy();

	if(isInertialSimulation(simType)) {
		calculateKineticEnergy();
	}

	calculateBondEnergy();
}

int ElasticNetwork::doKinetics(NetworkParameters *params) {

	// Firstly, sort out the new bonds being formed
	if(params->bondKineticsActive) {
		if(doBondKinetics(params) == BioNetError) {
			fileLog->printError("Unable to calculate bond kinetics.");
			return BioNetError;
		}
	}
	
	if(params->proteinKineticsActive) {
		// Now, sort out protein kinetics
		if(doProteinKinetics(params) == BioNetError) {
			fileLog->printError("Unable to calculate protein kinetics.");
			return BioNetError;
		}
	}
	return BioNetSuccess;
}

int ElasticNetwork::doBondKinetics(NetworkParameters *params) {

	// This method looks for bonds that could form or break throughout the network
	int i, j, toAlterIndex;
	set<int> alteredIndex;
	set<int>::reverse_iterator sIt;
	vector<PairPair> pairs;
	vector<PairPair>::iterator it;
	cTObject *bon;
	cObject *con, *con2;
	vObject *nod;
	vector<vObject*> terminals;
	Vector3 trans;


	// Get a bond template
	bon = kineticBondTemplate;

	//
	// Remove connections first, if necessary
	//
		// Rewrite function

	
	//
	// Add new connections
	//
	
	// Get all possible pairs
	pairs = calculatePairPairList(params, true);

	// Sort into distance order
	sort(pairs.begin(), pairs.end(), compareDistances);

	// Initialise the memory (and shrink later for speed)
	connection.reserve(pairs.size() + connection.size());

	// Get first connection
	if(bon->getParameter("type") == bondTypeString[BRigid]) {
		con = new RigidRod1D();
	} else if(bon->getParameter("type") == bondTypeString[BSpring]) {
		con = new LinearSpring();
	} else if(bon->getParameter("type") == bondTypeString[BFENE]) {
		con = new FENESpring();
	} else if(bon->getParameter("type") == bondTypeString[BAngle]) {
		con = new AngularSpring();
	} else if(bon->getParameter("type") == bondTypeString[BWLC]) {
//		con = new WormLikeChain();
	} else {
		fileLog->printError("Unable to find bond type");
		return BioNetError;
	}

	// Apply bond template
	con->applyTemplate(bon);

	// Set reactivity
	if(!localConnectivitySet) {
		for(i = 0; i < node.size(); ++i) {
			if(node[i] != NULL) {
				if(rngCont->getMinorUniformRN() <= node[i]->getReactionProbability()) {
					node[i]->setReactive(true);
				} else {
					node[i]->setReactive(false);
				}
			}
		}
	} else {
		for(i = 0; i < node.size(); ++i) {
			if(node[i] != NULL) {
				if(node[i]->getProteinType() == PPoint) {
					if(rngCont->getMinorUniformRN() <= node[i]->getReactionProbability()) {
						node[i]->setReactive(true);
					} else {
						node[i]->setReactive(false);
					}
				} else {
					for(j = 0; j < node[i]->getNumChildren(); ++j) {
						if(rngCont->getMinorUniformRN() <= node[i]->getChild(j)->getReactionProbability()) {
							node[i]->getChild(j)->setReactive(true);
						} else {
							node[i]->getChild(j)->setReactive(false);
						}
					}
				}
			}
		}
	}

	// Test each pair, and add to the list
	bool toContinue;
	for(it = pairs.begin(); it != pairs.end(); ++it) {

//		cout << it->obj[0]->getTopParent()->getIndex() << ", " << it->obj[1]->getTopParent()->getIndex() << endl;
//		continue;
		
		// Check for saturation and reactivity
		if(isSaturated(*it) || !isReactive(*it)) {
			continue;
		}

		// Attach the terminals 1 way
		for(i = 0; i < 2; ++i) {
			con->addTerminal(it->obj[i]);
		}

		// Check bond doesn't already exist by looping existing connections (this shouldn't be a big amount)
		toContinue = false;
		for(i = 0; i < it->obj[0]->getNumConnections(); ++i) {
			con2 = it->obj[0]->getConnection(i);
			if(con2->getTemplate()->isKinetic()) {
				if(con2->getStartTerminal() == con->getStartTerminal() && con2->getEndTerminal() == con->getEndTerminal()) {
					toContinue = true;
					break;
				} else if (con2->getEndTerminal() == con->getStartTerminal() && con2->getStartTerminal() == con->getEndTerminal()) {
					toContinue = true;
					break;
				}
			}
		}

		if(toContinue) {
			con->removeTerminals();
			continue;
		}


		// Calculate the energy and the initial offset

		// Maybe get periodic boundary conditions
		if(box->getBCType() == PBC) {

			// See what should be interacting with what
			try {
				trans = box->calculatePeriodicTranslation(con->getTerminal(0)->getTopParent(), con->getTerminal(1)->getTopParent());
			} catch (exception &e) {
				return BioNetError;
			}

			con->getTerminal(0)->getTopParent()->translate(trans);
		}

		// Calculate the energy
		con->calculateElasticEnergy();

		// Maybe reverse periodic boundary conditions
		if(box->getBCType() == PBC) {
			con->getTerminal(0)->getTopParent()->translate(-1 * trans);

		}

		if(con->getElasticEnergy() + con->getBondEnergy() < 0) {

			// Attach the terminals the other way
			for(i = 0; i < 2; ++i) {
				it->obj[i]->addConnection(con);
			}

			// Set the offset for this thing
			trans += box->getDimensions().cwiseProduct(con->getTerminal(1)->getTopParent()->getWrap() - con->getTerminal(0)->getTopParent()->getWrap());
			con->setOffset(trans);

			// Add it
			addConnection(con);

			// Get new one
			if(bon->getParameter("type") == bondTypeString[BRigid]) {
				con = new RigidRod1D();
			} else if(bon->getParameter("type") == bondTypeString[BSpring]) {
				con = new LinearSpring();
			} else if(bon->getParameter("type") == bondTypeString[BFENE]) {
				con = new FENESpring();
			} else if(bon->getParameter("type") == bondTypeString[BAngle]) {
				con = new AngularSpring();
			} else if(bon->getParameter("type") == bondTypeString[BWLC]) {
		//		con = new WormLikeChain();
			}
			con->applyTemplate(bon);
		} else {
			con->removeTerminals();
		}
	}

	// Delete the final one
	delete con;
	con = NULL;

	// Resize the memory properly
	connection.shrink_to_fit();
//	exit(0);
	return BioNetSuccess;
}

int ElasticNetwork::doProteinKinetics(NetworkParameters *params) {

	// For the moment, these kinetics replace all spherenodes with forces > unfoldingForce with 'amino acid chains'
//	int i;
//	for(i = 0; i < node.size(); ++i) {
//		if(node[i] != NULL) {
//			if((node[i]->getTotalForce() - node[i]->getThermalForce()).norm() > node[i]->getUnfoldingForce()) {
//	//			removeNode(node[i]->getIndex());
//				unfoldNode(node[i]->getIndex());
//			}
//		}
//	}

	return BioNetSuccess;
}

void ElasticNetwork::buildLaplacianMatrix() {

	// This is actually the negative of the laplacian, because we want positive eigenvalues.
	// Connections are made at the parent level, not the child level
	// This does not consider any pair-pair interactions. If you want those, use ENM
	// This matrix is symmetric. Self-adjoint solvers in Eigen only use bottom triangle
	
	int i, n0, n1;
	vector<Triplet<int>> entryTriplets;

	// Must be re-initialised to dynamically allocate. No directional dependence
	stiffnessMatrix.resize(numNodes, numNodes);

	// Connections
	for(i = 0; i < numConnections; ++i) {
		n0 = connection[i]->getTerminal(0)->getTopParentIndex();
		n1 = connection[i]->getTerminal(1)->getTopParentIndex();
		entryTriplets.push_back(Triplet<int>(n1, n0, -1));
		entryTriplets.push_back(Triplet<int>(n0, n0, 1));
		entryTriplets.push_back(Triplet<int>(n1, n1, 1));
	}
	stiffnessMatrix.setFromTriplets(entryTriplets.begin(), entryTriplets.end());
}

void ElasticNetwork::diagonaliseNetworkModelMatrix() {

	// Initialise the solver objects (use Spectra libraries, which use the Lanczos algorithm)
	Spectra::SparseSymMatProd<scalar> symMatApply(stiffnessMatrix);
	Spectra::SymEigsSolver<scalar, Spectra::LARGEST_ALGE,Spectra::SparseSymMatProd<scalar>> symEigenSolver(&symMatApply, params->numNMModes, min(2 * params->numNMModes, int(stiffnessMatrix.rows())));

	// Simply do the built-in computation
	
	// Initialize and compute
	symEigenSolver.init();
	symEigenSolver.compute();

	// Assign to the eigensystem objects
	nmEigenvalues = symEigenSolver.eigenvalues();
	nmEigenvectors = symEigenSolver.eigenvectors();
}

void ElasticNetwork::printProteinDetails() {

	for(int i = 0; i < numProteins; ++i) {
		protein[i]->printDetails();
	}
}

void ElasticNetwork::printBondDetails() {

	for(int i = 0; i < numBonds; ++i) {
		bond[i]->printDetails();
	}
}

void ElasticNetwork::printNodeDetails() {

	for(int i = 0; i < node.size(); ++i) {
		if(node[i] != NULL) {
			node[i]->printDetails();
		}
	}
}

void ElasticNetwork::printConnectionDetails() {

	for(int i = 0; i < connection.size(); ++i) {
		if(connection[i] != NULL) {
			connection[i]->printDetails();
		}	
	}
}

void ElasticNetwork::printForceDetails() {

	for(int i = 0; i < numUniqueForces; ++i) {
		force[i]->printDetails();
	}
}

void ElasticNetwork::printDetails(bool verbose) {
	cout << endl;
	cout << "Class: ElasticNetwork" << endl;
	cout << "Parameters: " << endl;
	cout << "\tNumber of Protein Templates: " << numProteins << endl;
	cout << "\tNumber of Bond Templates: " << numBonds << endl;
	cout << "\tNumber of Nodes: " << numNodes << endl;
	cout << "\tNumber of Connections: " << numConnections << endl;

	cout << endl;

	// Energies
	cout << "Energies: " << endl;
	cout << "\tElastic Energy: " << getElasticEnergy() << endl;
	cout << "\tKinetic Energy: " << getKineticEnergy() << endl;
	cout << "\tBond Energy: " << getBondEnergy() << endl;
	cout << "\tTotal Energy: " << getTotalEnergy() << endl;
 
	if(verbose) {
		cout << endl << "Proteins:" << endl << endl;
		printProteinDetails();

		cout << endl << "Bonds:" << endl;
		printBondDetails();

		cout << endl << "Nodes:" << endl;
		printNodeDetails();

		cout << endl << "Connections:" << endl;
		printConnectionDetails();
	}
}

void ElasticNetwork::zeroKineticEnergy() {

	// Local zeroing only
	totalEnergy -= kineticEnergy;
	kineticEnergy = 0.0;
}

void ElasticNetwork::zeroElasticEnergy() {

	// Local zeroing only
	totalEnergy -= elasticEnergy;
	elasticEnergy = 0.0;
}
