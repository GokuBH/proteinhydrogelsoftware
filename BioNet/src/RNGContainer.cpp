#include "RNGContainer.hpp"

RNGContainer::RNGContainer() {

	numThreads = 1;

	// Distro ranges
	uniform_real_distribution<scalar> uniformDistro(-0.5, 0.5);
//	normal_distribution<scalar> normalDistro(0.0, 1.0);

	normalDistro = NULL;

	// RNGs
	thermalRNG = NULL;

	// Seeds
	thermalRNGSeed = NULL;
	minorRNGSeed = 0;
}

RNGContainer::~RNGContainer() {

	numThreads = 0;

	// Distributions
	delete[] normalDistro;
	normalDistro = NULL;

	// RNGs
	delete[] thermalRNG;
	thermalRNG = NULL;

	// Seeds
	delete[] thermalRNGSeed;
	thermalRNGSeed = NULL;
	minorRNGSeed = 0;
}

void RNGContainer::setNumThreads(int n) {
	numThreads = n;
}

int RNGContainer::getNumThreads() {
	return numThreads;
}

void RNGContainer::setThermalRNGSeed(int n, long int s) {
	thermalRNGSeed[n] = s;
}

long int RNGContainer::getThermalRNGSeed(int n) {
	return thermalRNGSeed[n];
}

void RNGContainer::setMinorRNGSeed(long int s) {
	minorRNGSeed = s;
}

long int RNGContainer::getMinorRNGSeed() {
	return minorRNGSeed;
}

void RNGContainer::setThermalRNGState(int n, stringstream &ss) {
	ss >> thermalRNG[n];
}

mt19937 RNGContainer::getThermalRNG(int n) {
	return thermalRNG[n];
}

void RNGContainer::setMinorRNGState(stringstream &ss) {
	ss >> minorRNG;
}

mt19937 RNGContainer::getMinorRNG() {
	return minorRNG;
}

void RNGContainer::setNormalDistributionState(int n, stringstream &ss) {
//	ss >> normalDistro[n];
	normalDistro[n].reset();
}

normal_distribution<scalar> RNGContainer::getNormalDistribution(int n) {
	return normalDistro[n];
}

void RNGContainer::setUniformDistributionState(stringstream &ss) {
//	ss >> uniformDistro;
	uniformDistro.reset();
}

uniform_real_distribution<scalar> RNGContainer::getUniformDistribution() {
	return uniformDistro;
}

scalar RNGContainer::getThermalRN(int n) {
	return normalDistro[n](thermalRNG[n]);
}

scalar RNGContainer::getMinorUniformRN() {
	return uniformDistro(minorRNG);
}

scalar RNGContainer::getMinorNormalRN(int n) {
	return normalDistro[n](minorRNG);
}

void RNGContainer::initialise(int n) {

	int i;

	// First, set num threads
	setNumThreads(n);

	// Build distributions and RNGs for each thread
	normalDistro = new normal_distribution<scalar>[numThreads];
	thermalRNG = new mt19937[numThreads];
	thermalRNGSeed = new long int[numThreads];

	for(i = 0; i < numThreads; ++i) {

		// Set parameters and reset
		normalDistro[i].param(normal_distribution<scalar>::param_type(0.0, 1.0));
		normalDistro[i].reset();

		// Seed and store seeds
		thermalRNGSeed[i] = (chrono::system_clock::now().time_since_epoch()).count() + i;
		thermalRNG[i].seed(thermalRNGSeed[i]);
	}

	// Get the single rng that isn't that important (but might be in the future)
	uniform_real_distribution<scalar> uniformDistro(-0.5, 0.5);
	minorRNGSeed = (chrono::system_clock::now().time_since_epoch()).count() + numThreads;
	minorRNG.seed(minorRNGSeed);

	// Test seeds!
/*
//	ofstream fout("seed.dat");
//	string lol;
//	cout << endl;
	cout << "Seed: " << "1" << endl;
	thermalRNGSeed[0] = 1;
	thermalRNG[0].seed(thermalRNGSeed[0]);
	cout << "RNG 1: " << normalDistro[0](thermalRNG[0]) << endl;
	cout << "RNG 2: " << normalDistro[0](thermalRNG[0]) << endl;
//	lol = thermalRNG[0];
//	fout <<  thermalRNG[0] << endl;
//	fout.close();
	cout << "RNG 3: " << normalDistro[0](thermalRNG[0]) << endl;
	cout << "RNG 4: " << normalDistro[0](thermalRNG[0]) << endl;
	cout << "RNG 5: " << normalDistro[0](thermalRNG[0]) << endl;

//	ifstream fin("seed.dat");
//	fin >> thermalRNG[0];
//	lol >> thermalRNG[0];
	normalDistro[0].reset();
//	fin.close();

	cout << "RNG 1: " << normalDistro[0](thermalRNG[0]) << endl;
	cout << "RNG 2: " << normalDistro[0](thermalRNG[0]) << endl;
	thermalRNGSeed[0] = normalDistro[0](thermalRNG[0]);
	cout << "RNG 3: " << thermalRNGSeed[0] << endl;
	cout << "RNG 4: " << normalDistro[0](thermalRNG[0]) << endl;
	cout << "RNG 5: " << normalDistro[0](thermalRNG[0]) << endl;
	exit(0);
*/


}
