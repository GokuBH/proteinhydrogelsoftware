#include "SystemParameters.hpp"

scalar stos(string s) {
	return stod(s);
}

InitType getInitType(int i) {

	// Manual loop (fix this in future!)
	InitType iT;
	switch(i) {
		case(0):
			iT = Relax;
			break;
		case(1):
			iT = Random;
			break;
		case(2):
			iT = MonteCarloI;
			break;
		default:
			iT = WrongInit;
			break;
	}
	
	return iT;
}

string getInitTypeString(InitType initType) {

	return initTypeString[initType];
}

bool initTypeSupported(InitType initType) {

	if(initType == NoInit || initType == WrongInit) {
		return BioNetInvalid;
	}
	return BioNetValid;
}

bool requiresInitialisation(InitType initType) {
	if(initType != NoInit) {
		return true;
	}
	return false;

}

SimType getSimType(int i) {

	// Manual loop (fix this in future!)
	switch(i) {
		case(0):
			return GNM;
		case(1):
			return ANM;
		case(2):
			return ENM;
		case(3):
			return VGNM;
		case(4):
			return VANM;
		case(5):
			return VENM;
		case(6):
			return Langevin;
		case(7):
			return Brownian;
		case(8):
			return MonteCarloS;
		default:
			return WrongSim;
	}
}

string getSimTypeString(SimType simType) {

	return simTypeString[simType];
}


bool simTypeSupported(SimType simType) {
	
	if(simType == ANM || simType == VGNM || simType == VANM || simType == VENM || simType == MonteCarloS) {
		return BioNetInvalid;
	}

	return BioNetValid;
}

bool requiresSimulation(SimType simType) {
	if(simType != NoSim) {
		return true;
	}
	return false;

}

bool isInertialSimulation(SimType simType) {
	if(simType == Langevin) {
		return true;
	}

	return false;
}

bool isDynamicSimulation(SimType simType) {

	if(simType == Brownian || simType == Langevin) {
		return true;
	}

	return false;
}

bool isNetworkModelSimulation(SimType simType) {

	if(simType == GNM || simType == ANM || simType == ENM || simType == VGNM || simType == VANM || simType == VENM) {
		return true;
	}

	return false;
}

bool isPureElasticSimulation(SimType simType) {

	if(simType == GNM || simType == ANM || simType == ENM) {
		return true;
	}

	return false;
}

