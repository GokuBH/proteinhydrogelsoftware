#include "NetworkFileLogger.hpp"

//
// Global Methods
//
void printIntro(ostream &ofs) {

	printHeader(ofs);
	printProgramDetails(ofs);
	printVersionInfo(ofs);
	printDevDetails(ofs);
}

void printHeader(ostream &ofs) {
	ofs << endl << endl;
	ofs << "******************************************" << endl;
	ofs << "***********                    ***********" << endl;
	ofs << "****** Biological Network Simulator ******" << endl;
	ofs << "***********                    ***********" << endl;
	ofs << "******************************************" << endl;
	ofs << endl;
}

void printProgramDetails(ostream &ofs) {

	ofs << "An open source software package designed to enable both static and dynamic";
	ofs << " simulations of biological network structures." << endl;
	ofs << endl;
}

void printVersionInfo(ostream &ofs) {
//	ofs << "Version -	" << VERSION_MAJOR << "." << VERSION_MINOR << "." << VERSION_PATCH << endl;
        ofs << "Version -       " << VERSION_MAJOR << "." << VERSION_MINOR << endl;
//        ofs << "Version -       " << VMAJOR << "." << VMINOR << "." << VPATCH << endl;
	#ifdef _OPENMP
		ofs << "\t-	OpenMP Active" << endl;
	#else
		ofs << "\t-	OpenMP Inactive" << endl;
	#endif

	ofs << endl;
}

void printDevDetails(ostream &ofs) {

	ofs << "Theory  -	Benjamin Hanson " << "(b.s.hanson@leeds.ac.uk)" << endl;
	ofs << "Coding  -	Benjamin Hanson " << "(b.s.hanson@leeds.ac.uk)" << endl;
	ofs << endl;
}

void printOutro(ostream &ofs) {

	ofs << endl;
	ofs << "Program completed. Thanks for playing!" << endl << endl;
}

void printFailedOutro(ostream &ofs) {
 	ofs << endl;
	ofs << "Program failed to complete. Please try again or contact support." << endl << endl;
}

// Tags for different issues
void printErrorTag(ostream& ofs) {
	if(&ofs == &cout) {
		ofs << "\033[1;31mERROR: \t\t\033[0m";
	} else {
		ofs << "ERROR:";
	}
}

void printWarningTag(ostream& ofs) {
	if(&ofs == &cout) {
		ofs << "\033[1;33mWARNING: \t\033[0m";
	} else {
		ofs << "WARNING:";
	}
}

void printInfoTag(ostream& ofs) {
	if(&ofs == &cout) {
		ofs << "\033[1;32mINFO: \033[0m";
	} else {
		ofs << "INFO: ";
	}
}

void printParamTag(ostream& ofs) {
	if(&ofs == &cout) {
		ofs << "\033[1;34mParameter: \033[0m";
	} else {
		ofs << "Parameter: ";
	}
}

void printMessageTag(ostream& ofs) {
	if(&ofs == &cout) {
		ofs << "\033[1;32mMessage: \033[0m";
	} else {
		ofs << "Message: ";
	}
}

//
// Logger methods
//
NetworkFileLogger::NetworkFileLogger() {
	verbosityLevel = 10;
	blockLevel = 0;
	decreasing = true;
	logFname = bfs::path{""};
	logging = false;
	applyTabs = false;
}

NetworkFileLogger::~NetworkFileLogger() {

	// Stop the logger
	stop();

	verbosityLevel = 0;
	blockLevel = 0;
	decreasing = false;
	logFname = bfs::path{""};
	logging = false;
	applyTabs = false;
}

void NetworkFileLogger::setVerbosityLevel(bool v) {
	if(v) {
		verbosityLevel = 5;
	} else {
		verbosityLevel = 0;
	}
}

int NetworkFileLogger::getVerbosityLevel() {
	return verbosityLevel;
}

void NetworkFileLogger::setBlockLevel(int b) {
	blockLevel = b;
}

int NetworkFileLogger::getBlockLevel() {
	return blockLevel;
}

bfs::path NetworkFileLogger::getLogFname() {
	return logFname;
}

void NetworkFileLogger::incrementBlockLevel() {
	blockLevel++;
}

void NetworkFileLogger::decrementBlockLevel() {
	blockLevel--;
}

void NetworkFileLogger::buildLogFname(bfs::path iFname) {

	// Filename is input name plus time .log (parent + "/" + stem + "_" + date + log
	logFname = bfs::path{iFname.parent_path().string() + "/" + iFname.stem().string() + "_" + getCurrentDateTime() + ".log"};
}

// Logging methods
void NetworkFileLogger::start() {

	// If closed, then open (and overwrite)
	if(!lFout.is_open()) {
		lFout.open(logFname.string());
	}

	logging = true;
}

void NetworkFileLogger::catchUp() {

	// If closed, then open (and overwrite)
	if(logging) {
		printIntro(lFout);
	}
}

void NetworkFileLogger::stop() {

	// If open, then close
	if(lFout.is_open()) {

		// Print the outro
		printOutro(lFout);
		lFout.close();
	}

	logging = false;
}

// Progress notifications
void NetworkFileLogger::printMethodTask(string message) {
	
	// Check verbosity first
	if(getVerbosityLevel() < getBlockLevel()) {

		// Set levels and flags
		incrementBlockLevel();
		decreasing = false;
		return;
	}

	// Always inset by the number of tabs required, even zero
	string tabs(getBlockLevel(), '\t');

	// If we're in a task, new line
	if(!decreasing) {
		tabs = "\n" + tabs;
	}

	// Message
	cout << tabs << "--> " << message << flush;
	if(logging) {
		lFout << tabs << "--> " << message << flush;
	}

	// Set levels and flags
	incrementBlockLevel();
	decreasing = false;
}

void NetworkFileLogger::printMethodSuccess() {

	// Check verbosity first
	if(getVerbosityLevel() < getBlockLevel() - 1) {

		// Set levels
		decrementBlockLevel();
		decreasing = true;

		return;
	}

	// Message (dependent on whether a task chain is completed)

	// Get tabs and newline
	string tabs(getBlockLevel() - 1, '\t');
	if(!decreasing) {
		tabs = "";
	}

	// Inset by the number of tabs required
	cout << tabs << "...done!" << endl << flush;
	if(logging) {
		lFout << tabs << "...done!" << endl << flush;
	}

	// Set levels
	decrementBlockLevel();
	decreasing = true;
}

void NetworkFileLogger::printProgressData(int step, int startStep, long numSteps, scalar timeRemaining, scalar simTime) {

	// This method doesn't care about the verbosity or the tab level changes
	
	// Get tabs and newline
	string tabs;
	if(getVerbosityLevel() < getBlockLevel()) {
		tabs = string(getVerbosityLevel() + 1, '\t');
	} else {
		tabs = string(getBlockLevel(), '\t');
	}

	// Sort out time
	string timeString;
	if(timeRemaining > 3600.0) {
		timeString = to_string(timeRemaining / 3600.0) + " hrs remaining";
	} else if (timeRemaining > 60) {
		timeString = to_string(timeRemaining / 60) + " mins remaining";
	} else {
		timeString = to_string(timeRemaining) + " secs remaining";
	}

	if(step - startStep == 0) {
		cout << "\n\r" << tabs << "Step " << step << " of " << numSteps << " (" << simTime << "ns) - ? hrs remaining" << flush;
	} else {

		if(timeRemaining > 0) {
			cout << "\33[2K\r" << tabs << "Step " << step << " of " << numSteps << " (" << simTime << "ns) - " << timeString << flush;
		} else if (step == numSteps) {
			cout << "\33[2K\r" << tabs << "Step " << step << " of " << numSteps << " (" << simTime << "ns) - " << timeString << endl << flush;
			decreasing = true;
		} else {
			cout << "\33[2K\r" << tabs << "Step " << step << " of " << numSteps << " (" << simTime << "ns, overrun) - ? hrs remaining" << flush;
		}	
	}
}

void NetworkFileLogger::printFinalProgressData(scalar timeElapsed, scalar simTime) {

	// Get tabs and newline
	string tabs;
	if(getVerbosityLevel() < getBlockLevel()) {
		tabs = string(getVerbosityLevel() + 1, '\t');
	} else {
		tabs = string(getBlockLevel(), '\t');
	}

	// Sort out time
	string timeString;
	if(timeElapsed > 3600.0) {
		timeString = to_string(timeElapsed / 3600.0) + " hours";
	} else if (timeElapsed > 60) {
		timeString = to_string(timeElapsed / 60) + " minutes";
	} else {
		timeString = to_string(timeElapsed) + " seconds";
	}

	cout << tabs << "Simulation Time Elapsed: " << simTime << " ns" << endl;
	cout << tabs << "Real Time Elapsed: " << timeString << endl;

	if(logging) {

		// Endline here to compensate for regular progress data not going into the logs
		lFout << endl << tabs << "Simulation Time Elapsed: " << simTime << " ns" << endl;
		lFout << tabs << "Real Time Elapsed: " << timeString << endl;
	}
}

// Status output
void NetworkFileLogger::printError(string errMessage) {

	// Message
	cout << endl;
	printErrorTag(cout);
	cout << errMessage << endl;

	if(logging) {
		lFout << endl;
		printErrorTag(lFout);
		lFout << errMessage << endl;
	}

	decreasing = true;
}

void NetworkFileLogger::printError(string param, string errMessage) {

	// Message
	cout << endl;
	printErrorTag(cout);
	printParamTag(cout);
	cout << param << endl << "\t\t";
	printMessageTag(cout);
	cout << errMessage << endl;

	if(logging) {
		lFout << endl;
		printErrorTag(lFout);
		printParamTag(lFout);
		lFout << param << endl << "\t\t";
		printMessageTag(lFout);
		lFout << errMessage << endl;

	}

	decreasing = true;
}

void NetworkFileLogger::printWarning(string warnMessage) {

	// Check verbosity
	if(getVerbosityLevel() == 0) {
		return;
	}

	// Message
	cout << endl;
	printWarningTag(cout);
	cout << warnMessage << endl;

	if(logging) {
		lFout << endl;
		printWarningTag(lFout);
		lFout << warnMessage << endl;
	}

	decreasing = true;
}

void NetworkFileLogger::printWarning(string param, string warnMessage) {

	// Check verbosity
	if(getVerbosityLevel() == 0) {
		return;
	}

	// Message
	cout << endl;
	printWarningTag(cout);
	printParamTag(cout);
	cout << param << endl << "\t\t";
	printMessageTag(cout);
	cout << warnMessage << endl;

	if(logging) {
		lFout << endl;
		printWarningTag(lFout);
		printParamTag(lFout);
		lFout << param << endl << "\t\t";
		printMessageTag(lFout);
		lFout << warnMessage << endl;
	}

	decreasing = true;
}

void NetworkFileLogger::printMessage(string message) {

	// Check verbosity
	if(getVerbosityLevel() == 0) {
		return;
	}

	// Message
	cout << endl;
	printMessageTag(cout);
	cout << message << endl;

	if(logging) {
		lFout << endl;
		printMessageTag(lFout);
		lFout << message << endl;
	}

	// Set task completed to reset the tabs
	decreasing = true;
}

void NetworkFileLogger::printMessage(string param, string message) {

	// Message
	cout << endl;
	printInfoTag(cout);
	printParamTag(cout);
	cout << param << endl << "\t";
	printMessageTag(cout);
	cout << message << endl;

	if(logging) {
		lFout << endl;
		printInfoTag(lFout);
		printParamTag(lFout);
		lFout << param << endl << "\t";
		printMessageTag(lFout);
		lFout << message << endl;
	}

	decreasing = true;
}
