#include "LexicalAnalyser.hpp"

scalar ProcessMaths(string expression) {
	
	int i;
	scalar value;
	vector<string> splitVector;

	// Replace the bit
	for(i = 0; i < numMathConstants; ++i) {
		boost::replace_all(expression, mathConstantStrings[i], to_string(mathConstants[i]));
	}

	// Try all operators
	for(i = 0; i < numMathOperators; ++i) {
		if(boost::contains(expression, mathOperators[i])) {
			boost::split(splitVector, expression, boost::is_any_of(mathOperators[i]));
			if(i == 0) {
				value = stos(splitVector.at(0)) * stos(splitVector.at(1));
			} else if(i == 1) {
				value = stos(splitVector.at(0)) / stos(splitVector.at(1));
			} else if(i == 2) {
				value = stos(splitVector.at(0)) + stos(splitVector.at(1));
			} else {
				if(splitVector.size() == 1 || boost::algorithm::trim_copy(splitVector.at(1)) == "") {
					value = -1 * stos(splitVector.at(0));
				} else if(boost::algorithm::trim_copy(splitVector.at(0)) == "") {
					value = -1 * stos(splitVector.at(1));
				} else {
					value = stos(splitVector.at(0)) - stos(splitVector.at(1));
				}	
			}
			return value;
		}
	}			

	// If it got here, just try to return the value. Or give up
	try {
		return stos(expression);
	} catch(exception& e) {
		throw;
	}
}
