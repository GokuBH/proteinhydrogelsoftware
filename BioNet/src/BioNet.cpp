#include <iostream>
#include <exception>
#include <string>

#include <boost/filesystem.hpp>

#ifdef _OPENMP
	#include <omp.h>
#endif

#include "GlobalObjects.hpp"
#include "SystemParameters.hpp"

#ifdef BIONETFORM
	#include "CommandLineArgsForm.hpp"
#else
	#include "CommandLineArgsSim.hpp"
#endif

#include "NetworkFileReader.hpp"
#include "NetworkFileWriter.hpp"
#include "NetworkFileLogger.hpp"

#include "PhysicalSystem.hpp"

using namespace std;

class MainObject {

	public:

		//
		// Methods
		//
		MainObject() {
			cmArgs = NULL;
			fileIn = NULL;
			fileOut = NULL;
			fileLog = NULL;
			pSystem = NULL;
			rngCont = NULL;
		}

		~MainObject() {
			if(cmArgs != NULL) {
				delete cmArgs;
			}
			if(fileIn != NULL) {
				delete fileIn;
			}
			if(fileOut != NULL) {
				delete fileOut;
			}
			if(fileLog != NULL) {
				delete fileLog;
			}
			if(pSystem != NULL) {
				delete pSystem;
			}
			if(rngCont != NULL) {
				delete rngCont;
			}

			cmArgs = NULL;
			fileIn = NULL;
			fileOut = NULL;
			fileLog = NULL;
			pSystem = NULL;
			rngCont = NULL;
		}

		//
		// Variables
		//
		CommandLineArgs *cmArgs;
		NetworkFileReader *fileIn;
		NetworkFileWriter *fileOut;
		NetworkFileLogger *fileLog;
		PhysicalSystem *pSystem;
		RNGContainer *rngCont;

};

#ifdef _OPENMP
void setParallelProperties(CommandLineArgs *cmArgs) {

	char *envVar;
	int maxNumThreads;
	fileLog->printMethodTask("Setting parallelisation parameters...");
	cout << endl;

	// Default
	maxNumThreads = 1;	
	envVar = getenv("OMP_NUM_THREADS");
	if(envVar != NULL) {
		maxNumThreads = max(atoi(envVar), 1);
	} else {
		cout << "\tDefaulting OMP_NUM_THREADS to 1" << endl;
	}

	// Conditionals
	if(cmArgs->getNumThreads() <= maxNumThreads && cmArgs->getNumThreads() > 0) {
		omp_set_num_threads(cmArgs->getNumThreads());
		cout << "\tNumber of Threads set to " << cmArgs->getNumThreads() << ", as requested" << endl;
	} else {
		omp_set_num_threads(maxNumThreads);
		cout << "\tNumber of Threads defaulting to maximum " << maxNumThreads << ", as defined by OMP_NUM_THREADS" << endl;
	}

	fileLog->printMethodSuccess();
}
#endif

void setLoggerProperties(NetworkFileLogger *fileLog, CommandLineArgs *cmArgs) {

	// Assign some properties
	fileLog->setVerbosityLevel(cmArgs->getVerbosityLevel());

	// Build a new log file
	fileLog->buildLogFname(cmArgs->getInputFname());

	// Start logging
	fileLog->start();

	// Catch up the log file
	fileLog->catchUp();
}

//
// Main Begins!
//
int main(int argc, char **argv) {

	// Give user a welcome
	printIntro(cout);

	// Get a string for writing errors
	string errString;

	//
	// Global stuff
	//
	
	// Get the object container for this method
	MainObject *mainObject = new MainObject();

	// Logger object
	mainObject->fileLog = new NetworkFileLogger();
	GlobalVariables::fileLog = mainObject->fileLog;

	// We're making a network model! Lets read in the structure from a file
	mainObject->cmArgs = new CommandLineArgs();
	if(mainObject->cmArgs->read(argc, argv) == BioNetError) {
		errString = "Could not read command line arguments :( ";
		fileLog->printError(errString);
		delete mainObject;
		return BioNetError;
	}

	// Validate command-line args immediately (Everything is dependent on them)
	if(mainObject->cmArgs->validate() == BioNetInvalid) {
		errString = "Command line arguments inconsistent or invalid :( ";
		fileLog->printError(errString);
		delete mainObject;
		return BioNetError;
	}

	// Set parallel stuff
	#ifdef _OPENMP
		setParallelProperties(mainObject->cmArgs);
	#endif

	// RNG object
	mainObject->rngCont = new RNGContainer();
	#ifdef _OPENMP
		mainObject->rngCont->initialise(omp_get_max_threads());
	#else
		mainObject->rngCont->initialise(1);
	#endif
	GlobalVariables::rngCont = mainObject->rngCont;

	// We have some filenames now

	// Build the logger object
	setLoggerProperties(mainObject->fileLog, mainObject->cmArgs);

	// Read in the input data
	mainObject->fileIn = new NetworkFileReader();
	mainObject->fileOut = new NetworkFileWriter();
	if(mainObject->fileIn->readInputFile(mainObject->cmArgs->getInputFname()) == BioNetError) {
		errString = "Unable to load data from '" + mainObject->cmArgs->getInputFname().string() + "' :( ";
		delete mainObject;
		fileLog->printError(errString);
		return BioNetError;
	}

	// Now we have all of th input data, we can build a physical system
	mainObject->pSystem = new PhysicalSystem();

	// Build from input
	if(mainObject->pSystem->build(mainObject->cmArgs, mainObject->fileIn, mainObject->fileOut) == BioNetError) {
		errString = "The physical system could not be built :( ";
		fileLog->printError(errString);
		delete mainObject;
		return BioNetError;
	}

	// Read restart data (maybe this should be in build?)
	if(mainObject->pSystem->getParameters()->restartSet) {
	
		cout << "Restarting..." << endl << flush;

		// Trajectory  data
		if(mainObject->fileIn->readTrajectoryFile(mainObject->pSystem->getParameters()->tFname.string()) == BioNetError) {
			errString = "Unable to load data from '" + mainObject->pSystem->getParameters()->tFname.string() + "' :( ";
			delete mainObject;
			fileLog->printError(errString);
			return BioNetError;
		}

		// Topology data
		if(mainObject->pSystem->getParameters()->kineticsActive) {
			if(boost::filesystem::exists(mainObject->pSystem->getParameters()->tpFname)) {
				if(mainObject->fileIn->readTopologyFile(mainObject->pSystem->getParameters()->tpFname.string()) == BioNetError) {
					errString = "Unable to load data from '" + mainObject->pSystem->getParameters()->tpFname.string() + "' :( ";
					delete mainObject;
					fileLog->printError(errString);
					return BioNetError;
				}
			} else {
				errString = "'" + mainObject->pSystem->getParameters()->tpFname.string() + "' does not exist";
				fileLog->printWarning(errString);
			}
		}

		// Constant force data
		if(mainObject->pSystem->getParameters()->constantForcesActive) {
			if(mainObject->fileIn->readConstantForceTrajectoryFile(mainObject->pSystem->getParameters()->cfFname.string()) == BioNetError) {
				errString = "Unable to load data from '" + mainObject->pSystem->getParameters()->cfFname.string() + "' :( ";
				delete mainObject;
				fileLog->printError(errString);
				return BioNetError;
			}
		}
		
		// Build from input
		if(mainObject->pSystem->rebuildFromTrajectory(mainObject->cmArgs, mainObject->fileIn, mainObject->fileOut) == BioNetError) {
			errString = "The physical system could not be rebuilt from existing trajectories :( ";
			fileLog->printError(errString);
			delete mainObject;
			return BioNetError;
		}
	}

	// Always pre-initialise
	if(mainObject->pSystem->preInitialise(mainObject->fileIn->simTime) == BioNetError) {
		errString = "The physical system could not be pre-prepared :(";
		fileLog->printError(errString);
		delete mainObject;
		return BioNetError;
	}

	// Initialise the simulation (maybe)
	if(mainObject->cmArgs->getInitType() != NoInit) {
		if(mainObject->pSystem->initialise() == BioNetError) {
			errString = "The physical system could not be initialised correctly :( ";

			// Try to write the structure file anyway
//			if(mainObject->fileOut->writeInputFile(mainObject->pSystem, mainObject->pSystem->getParameters()->sFname) == BioNetError) {
//				errString += "\nAlso, unable to write initialisation state to file :( ";
//			}
			fileLog->printError(errString);
			delete mainObject;
			return BioNetError;
		}

		// Always write the new initialisation state to file so we know what we've simulated
//		if(mainObject->fileOut->writeInputFile(mainObject->pSystem, mainObject->pSystem->getParameters()->sFname) == BioNetError) {
//
//			errString = "Unable to write initialisation state to file :( ";
//			delete mainObject;
//			fileLog->printError(errString);
//			return BioNetError;
//		}
	}

	// Run simulation
	if(mainObject->cmArgs->getSimType() != NoSim) {
		if(mainObject->pSystem->simulate() == BioNetError) {
			errString = "Unable to complete simulation :( ";
			fileLog->printError(errString);
			delete mainObject;
			return BioNetError;
		}

		// And write the static stuff to file (the network will deal with dynamic trajectories itself)
	/*	if(!isDynamicSimulation(mainObject->params->simType)) {
			fileOut->writeNetworkModelOutputFile(mainObject->params, net);
		}
	*/
	}

	// Complete programme :)	
	delete mainObject;

	printOutro(cout);
	return BioNetSuccess;
}
