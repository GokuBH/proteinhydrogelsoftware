#include "NetworkFileReader.hpp"

// Define static variables
const vector<string> NetworkFileReader::inputScriptTags {"params", "proteins", "bonds", "nodes", "connections", "box", "forces"};


const int NetworkFileReader::maxXMLLevel = 2;

/*
 * Methods
 */
NetworkFileReader::NetworkFileReader() {

	params.clear();
	proteins.clear();
	iSites.clear();
	nodes.clear();

	bonds.clear();
	connections.clear();

	box.clear();
	forces.clear();
	XMLLevel = 0;

	simTime = 0.0;
}

NetworkFileReader::~NetworkFileReader() {

	params.clear();

	proteins.clear();
	iSites.clear();
	nodes.clear();

	bonds.clear();
	connections.clear();

	box.clear();
	forces.clear();
	XMLLevel = 0;

	simTime = 0.0;
}


/*
 * Input Files
 */
int NetworkFileReader::readInputFile(bfs::path iFname) {

	// Message
	fileLog->printMethodTask("Reading input file (xml)...");

	// Open the file
	XMLDocument doc;
	doc.LoadFile(iFname.string().c_str());

	// Declare the variables we will need
	int secIndex;
	string errMessage;
	vector<string>::const_iterator it;
	XMLNode *docRoot, *sectionRoot;

	// Find top level
	docRoot = doc.FirstChild();
	if(docRoot == NULL) {
		errMessage = "XML Document incorrectly formatted.\n";
		errMessage += "\t\tCould not find any XML sections within the document.";
		fileLog->printError(errMessage);
		return BioNetError;
	}

	// Read required sections first (in the 'componenets' block, maybe). They come first in the vector object
	secIndex = 0;
	for(it = inputScriptTags.begin(); it != inputScriptTags.end(); ++it) {

		// Find the root (and check the section exists, although it doesn't matter if not. Look for dependencies later.)	
		sectionRoot = getXMLSection(docRoot, *it);
		if(sectionRoot == NULL) {
			continue;
/*
			if(*it == "box") {
				fileLog->printWarning("SimulationBox", "Object not defined. We'll try to compensate.");
				continue;
			} else if(*it == "connections") {
				fileLog->printWarning("Connections", "Object not defined. We should be ok with zero connections.");
				continue;
			} else if(*it == "forces") {
				fileLog->printWarning("Forces", "Object not defined. We should be able to default, if necessary.");
				continue;
			} else {
				errMessage = "XML Document incorrectly formatted.\n";
				errMessage += "\t\tCould not find required XML section.\n";
				errMessage += "\t\tMake sure the whole document is wrapped in <BioNet></BioNet> tags!";
				fileLog->printError(errMessage);
				return BioNetError;
			}
*/
		}

		// Read it into containers
		if(readXMLSection(sectionRoot) == BioNetError) {
			return BioNetError;
		}
		secIndex += 1;
	}

	fileLog->printMethodSuccess();
	return BioNetSuccess;
}

int NetworkFileReader::readTrajectoryFile(bfs::path tFname) {

	int count;

	// Message
	fileLog->printMethodTask("Reading trajectory file (bntrj)...");

	// Open the file
	ifstream fin;
	streampos trajBegin = -1, frameBegin = -1, frameEnd = -1;
	string line;

	fin.open(tFname.string(), ios::binary);

	// Skip header for now
	count = 0;
	while(true) {
		getline(fin, line);
		if (line == "#####") {
			count += 1;
			if(count == 2) {
				trajBegin = fin.tellg();
				frameBegin = trajBegin;
				frameEnd = trajBegin;
				break;
			}
		} else if (line == "") {
			break;
		}
	}

	// Check
	if(trajBegin == -1) {
		fileLog->printError("Trajectory file header incorrectly formatted, and could not be read :(");
		return BioNetError;	
	}


	// Seek to  where we need to be
	while(true) {
		getline(fin, line);
		if (line == "###") {
			frameBegin = frameEnd;
			frameEnd = fin.tellg();
		} else if (line == "#####" || line == "") {
			break;
		}
	}

	// Check
	if(frameBegin == frameEnd) {
		fileLog->printError("Trajectory file has no frames, and could not be read :(");
		return BioNetError;	
	}

	// Read frame
	fin.close();
	fin.open(tFname.string(), ios::binary);
	fin.seekg(frameBegin, ios::beg);

	if(readTrajectoryFrame(fin) == BioNetError) {
		fileLog->printError("Could not read trajectory frame :(");
		return BioNetError;
	}


	// Close
	fin.close();

	fileLog->printMethodSuccess();
	return BioNetSuccess;

}

int NetworkFileReader::readTopologyFile(bfs::path tFname) {

	int count;

	// Message
	fileLog->printMethodTask("Reading topology file (bntrj)...");

	// Open the file
	ifstream fin;
	streampos trajBegin = -1, frameBegin = -1, frameEnd = -1;
	string line;

	fin.open(tFname.string(), ios::binary);

	// Skip header for now
	count = 0;
	while(true) {
		getline(fin, line);
		if (line == "#####") {
			count += 1;
			if(count == 2) {
				trajBegin = fin.tellg();
				frameBegin = trajBegin;
				frameEnd = trajBegin;
				break;
			}
		} else if (line == "") {
			break;
		}
	}

	// Check
	if(trajBegin == -1) {
		fileLog->printError("Topology file header incorrectly formatted, and could not be read :(");
		return BioNetError;	
	}


	// Seek to  where we need to be
	while(true) {
		getline(fin, line);
		if (line == "###") {
			frameBegin = frameEnd;
			frameEnd = fin.tellg();
		} else if (line == "#####" || line == "") {
			break;
		}
	}

	// Check
	if(frameBegin == frameEnd) {
		fileLog->printError("Topology file has no frames, and could not be read :(");
		return BioNetError;	
	}

	// Read frame
	fin.close();
	fin.open(tFname.string(), ios::binary);
	fin.seekg(frameBegin, ios::beg);

	if(readTopologyFrame(fin) == BioNetError) {
		fileLog->printError("Could not read topology frame :(");
		return BioNetError;
	}


	// Close
	fin.close();

	fileLog->printMethodSuccess();
	return BioNetSuccess;

}

int NetworkFileReader::readConstantForceTrajectoryFile(bfs::path cfFname) {

	int count;

	// Message
	fileLog->printMethodTask("Reading constant force file (bncf)...");

	// Open the file
	ifstream fin;
	streampos trajBegin = -1, frameBegin = -1, frameEnd = -1;
	string line;

	fin.open(cfFname.string(), ios::binary);

	// Skip header for now
	count = 0;
	while(true) {
		getline(fin, line);
		if (line == "#####") {
			count += 1;
			if(count == 2) {
				trajBegin = fin.tellg();
				frameBegin = trajBegin;
				frameEnd = trajBegin;
				break;
			}
		} else if (line == "") {
			break;
		}
	}

	// Check
	if(trajBegin == -1) {
		fileLog->printError("Constant force trajectory file header incorrectly formatted, and could not be read :(");
		return BioNetError;	
	}


	// Seek to  where we need to be
	while(true) {
		getline(fin, line);
		if (line == "###") {
			frameBegin = frameEnd;
			frameEnd = fin.tellg();
		} else if (line == "#####" || line == "") {
			break;
		}
	}

	// Check
	if(frameBegin == frameEnd) {
		fileLog->printError("Constant force trajectory file has no frames, and could not be read :(");
		return BioNetError;	
	}

	// Read frame
	fin.close();
	fin.open(cfFname.string(), ios::binary);
	fin.seekg(frameBegin, ios::beg);

	if(readConstantForceTrajectoryFrame(fin) == BioNetError) {
		fileLog->printError("Could not read constant force trajectory frame :(");
		return BioNetError;
	}


	// Close
	fin.close();

	fileLog->printMethodSuccess();
	return BioNetSuccess;

}

// This is recursive and goes right down each branch first, rather than through each level
XMLNode * NetworkFileReader::getXMLSection(XMLNode *parentNode, string sectionTitle) {

	XMLNode *lvlRoot, *retRoot;

	// Immediately increase the level
	XMLLevel += 1;

	// Recursion break
	if(XMLLevel > maxXMLLevel) {
		XMLLevel -= 1;
		return NULL;
	}

	// Loop over all nodes to check
	for(lvlRoot = parentNode->FirstChild(); lvlRoot != NULL; lvlRoot = lvlRoot->NextSibling()) {
		if(strcmp(lvlRoot->Value(), sectionTitle.c_str()) == 0) {
			XMLLevel -= 1;
			return lvlRoot;
		} else {

			// Not found. Try next level
			retRoot = getXMLSection(lvlRoot, sectionTitle);
			if(retRoot != NULL) {
				XMLLevel -= 1;
				return retRoot;
			}
		}
	}

	// Return NULL if we're here (it doesn't exist anywhere below this level on this branch)
	XMLLevel -= 1;
	return NULL;

	
}

int NetworkFileReader::readXMLSection(XMLNode *sec) {

	// Just send to the appropriate method
	string secTitle = string(sec->Value());
	if(secTitle == "params") {
		if(readXMLParameters(sec) == BioNetError) {
			fileLog->printError("Unable to read XML Parameters");
			return BioNetError;
		}
	} else if (secTitle == "proteins") {
		if(readXMLProteins(sec) == BioNetError) {
			fileLog->printError("Unable to read XML Protein Components");
			return BioNetError;
		}
	} else if (secTitle == "bonds") {
		if(readXMLBonds(sec) == BioNetError) {
			fileLog->printError("Unable to read XML Bond Components");
			return BioNetError;
		}
	} else if (secTitle == "nodes") {
		if(readXMLNodes(sec) == BioNetError) {
			fileLog->printError("Unable to read XML System Nodes");
			return BioNetError;
		}
	} else if (secTitle == "connections" || secTitle == "springs") {
		if(readXMLConnections(sec) == BioNetError) {
			fileLog->printError("Unable to read XML System Connections");
			return BioNetError;
		}
	} else if (secTitle == "box") {
		if(readXMLBox(sec) == BioNetError) {
			fileLog->printError("Unable to read XML Simulation Box");
			return BioNetError;
		}
	} else if (secTitle == "forces") {
		if(readXMLForces(sec) == BioNetError) {
			fileLog->printError("Unable to read XML Force specifications");
			return BioNetError;
		}
	} else {
		fileLog->printError("Unrecognised XML section: '" + secTitle + "'");
	}
	return BioNetSuccess;
}

int NetworkFileReader::readXMLParameters(XMLNode *sec) {

	string errMessage;

	// Loop over all elements and all attributes
	XMLElement *el;

	string tag = "param";
	for(el = sec->FirstChildElement(tag.c_str()); el; el = el->NextSiblingElement()) {
		if (el->Attribute("name") == NULL || el->Attribute("value") == NULL) {
			
			errMessage = "XML Parameter Section incorrectly formatted.";
			errMessage += " <params> element should be formatted as follows:\n";
			errMessage += "\t<param name=\"paramName\" value=\"paramValue\"/>";
			fileLog->printError(errMessage);
			return BioNetError;
		}

		params[el->Attribute("name")] = el->Attribute("value");
	}

	return BioNetSuccess;	
}

int NetworkFileReader::readXMLProteins(XMLNode *sec) {

	// Loop over all elements and all attributes
	XMLElement *pEl, *iEl;
	const XMLAttribute *att;

	string pTag = "protein", iTag;
	int pIndex = 0;
	int iIndex;
	for(pEl = sec->FirstChildElement(pTag.c_str()); pEl; pEl = pEl->NextSiblingElement()) {

		// Loop over attributes
		for(att = pEl->FirstAttribute(); att != 0; att = att->Next()) {
			proteins[pIndex][att->Name()] = att->Value();
		}

		// Proteins also have interaction sites
		iIndex = 0;
		iTag = "site";
		for(iEl = pEl->FirstChildElement(iTag.c_str()); iEl; iEl = iEl->NextSiblingElement()) {
			for(att = iEl->FirstAttribute(); att != 0; att = att->Next()) {
				iSites[pIndex][iIndex][att->Name()] = att->Value();
			}			
			iIndex += 1;
		}

		// For backwards compatibility, try 'iSite'
		if(iIndex == 0) {
			iTag = "iSite";
			for(iEl = pEl->FirstChildElement(iTag.c_str()); iEl; iEl = iEl->NextSiblingElement()) {
				for(att = iEl->FirstAttribute(); att != 0; att = att->Next()) {
						iSites[pIndex][iIndex][att->Name()] = att->Value();
				}			
				iIndex += 1;
			}
		}
		pIndex += 1;


	}

	return BioNetSuccess;
}

int NetworkFileReader::readXMLBonds(XMLNode *sec) {

	// Loop over all elements and all attributes
	XMLElement *el;
	const XMLAttribute *att;

	string tag = "bond";
	int index = 0;
	for(el = sec->FirstChildElement(tag.c_str()); el; el = el->NextSiblingElement()) {

		// Loop over attributes
		for(att = el->FirstAttribute(); att != 0; att = att->Next()) {
			bonds[index][att->Name()] = att->Value();
		}

		index += 1;


	}

	return BioNetSuccess;
}

int NetworkFileReader::readXMLNodes(XMLNode *sec) {

	// Loop over all elements and all attributes
	XMLElement *el;
	const XMLAttribute *att;

	string tag = "node";
	int index = 0;
	for(el = sec->FirstChildElement(tag.c_str()); el; el = el->NextSiblingElement()) {

		// Loop over attributes
		for(att = el->FirstAttribute(); att != 0; att = att->Next()) {
			nodes[index][att->Name()] = att->Value();
		}

		index += 1;


	}
	return BioNetSuccess;
}

int NetworkFileReader::readXMLConnections(XMLNode *sec) {

	// Loop over all elements and all attributes
	XMLElement *el;
	const XMLAttribute *att;

	string tag;

	tag = "connection";
	int index = 0;

	for(el = sec->FirstChildElement(tag.c_str()); el; el = el->NextSiblingElement()) {

		// Loop over attributes
		for(att = el->FirstAttribute(); att != 0; att = att->Next()) {
			connections[index][att->Name()] = att->Value();
		}

		index += 1;


	}

	return BioNetSuccess;
}

int NetworkFileReader::readXMLBox(XMLNode *sec) {

	string errMessage;

	// Loop over all elements and all attributes
	XMLElement *el;

	string tag = "param";
	for(el = sec->FirstChildElement(tag.c_str()); el; el = el->NextSiblingElement()) {
		if (el->Attribute("name") == NULL || el->Attribute("value") == NULL) {
			
			errMessage = "XML Simulation Box Section incorrectly formatted.";
			errMessage += " <box> element should be formatted as follows:\n";
			errMessage += "\t<param name=\"paramName\" value=\"paramValue\"/>";
			fileLog->printError(errMessage);
			return BioNetError;
		}

		box[el->Attribute("name")] = el->Attribute("value");
	}
	return BioNetSuccess;
}

int NetworkFileReader::readXMLForces(XMLNode *sec) {

	// Loop over all elements and all attributes
	XMLElement *el;
	const XMLAttribute *att;

	string tag = "force";
	int index = 0;
	for(el = sec->FirstChildElement(tag.c_str()); el; el = el->NextSiblingElement()) {

		// Loop over attributes
		for(att = el->FirstAttribute(); att != 0; att = att->Next()) {
			forces[index][att->Name()] = att->Value();
			
		}

		index += 1;


	}
	return BioNetSuccess;
}

int NetworkFileReader::readTrajectoryFrame(ifstream& fin) {

	int i, index;
	string line, formatLine, templateLine;
	vector<string> lineVec, templateVec;

	// Use nodes container to do this

	// Get templates first
	map<int, map<string, string>>::iterator it;

	for (it = nodes.begin(); it != nodes.end(); ++it) {
		templateLine = it->second["template"];
		if(templateLine == "") {
			templateLine = it->second["protein"];
			if(templateLine == "") {
				fileLog->printError("vObject::template", "Required, but not provided");
				return BioNetError;
			}
		}

		templateVec.push_back(templateLine);
	}

	// Clear
	nodes.clear();

	// This line should be the time
	getline(fin, line);

	try {
		simTime = stos(line);
	} catch (exception &e) {
		fileLog->printError("Could not read time from trajectory frame :(");
		return BioNetError;
	}

	// Now read the entire frame's worth of data
	index = 0;

	// Initial line
	getline(fin, line);

	while(line != "###") {

		// Split with boost
		boost::split(lineVec, line, boost::is_any_of(","));

		// Format for the nodes container (some ignored as will be recalculated)
		nodes[index]["protein"] = templateVec.at(index);	// This will only work whilst number of nodes is constant (node kinetics will need new traj format)
		nodes[index]["position"] = lineVec.at(0) + ", " + lineVec.at(1) + ", " + lineVec.at(2);
		nodes[index]["velocity"] = lineVec.at(3) + ", " + lineVec.at(4) + ", " + lineVec.at(5);
//		nodes[index]["force"] = lineVec.at(6) + ", " + lineVec.at(7) + ", " + lineVec.at(8);

		if(lineVec.size() == 12) {
			nodes[index]["wrap"] = lineVec.at(9) + "," + lineVec.at(10) + "," + lineVec.at(11);
		} else {

			// Orientation matrix
			formatLine = lineVec.at(9);
			for(i = 0; i < 8; ++i) {
				formatLine += ", " + lineVec.at(10 + i);
			}

			nodes[index]["orientation"] = formatLine;
			nodes[index]["wrap"] = lineVec.at(18) + "," + lineVec.at(19) + "," + lineVec.at(20);
		}

		index += 1;

		// Get the line
		getline(fin, line);

	}

	// Success!
	return BioNetSuccess;
}

int NetworkFileReader::readTopologyFrame(ifstream& fin) {

	int i, index, numTerminals;
	string line, nodeFormatLine, siteFormatLine, templateLine;
	vector<string> lineVec, bitVec;

	// Use connections container to do this
	connections.clear();

	// This line should be the time
	getline(fin, line);

	try {
		simTime = stos(line);
	} catch (exception &e) {
		fileLog->printError("Could not read time from topology frame :(");
		return BioNetError;
	}

	// Now read the entire frame's worth of data
	index = 0;

	// Initial line
	getline(fin, line);
	while(line != "###") {

		// Split with boost
		boost::split(lineVec, line, boost::is_any_of(" "));
		numTerminals = lineVec.size() - 2;
		for(i = 0; i < numTerminals; ++i) {
			boost::split(bitVec, lineVec.at(i), boost::is_any_of(","));
			if(i == 0) {
				if(bitVec.size() == 1) {
					nodeFormatLine = bitVec.at(0);
					siteFormatLine = "0";
				} else {
					nodeFormatLine = bitVec.at(1);
					siteFormatLine = bitVec.at(0);
				}
			} else {
				if(bitVec.size() == 1) {
					nodeFormatLine += "," + bitVec.at(0);
					siteFormatLine += ",0";
				} else {
					nodeFormatLine += "," + bitVec.at(1);
					siteFormatLine += "," + bitVec.at(0);
				}
			}
		}

		// Format for the connections container (some ignored as will be recalculated)
		connections[index]["nodes"] = nodeFormatLine;
		connections[index]["sites"] = siteFormatLine;
		
		// Offset
		boost::split(bitVec, lineVec.at(lineVec.size() - 2), boost::is_any_of(","));
		templateLine = bitVec.at(0) + "," + bitVec.at(1) + "," + bitVec.at(2);		
		connections[index]["offset"] = templateLine;

		// Template
		templateLine = lineVec.at(lineVec.size() - 1);
		connections[index]["templateindex"] = templateLine;
//		connections[index]["templateindex"] = to_string(lineVec.at(lineVec.size() - 1));
		index += 1;

		// Get the line
		getline(fin, line);
	}

	// Success!
	return BioNetSuccess;
}

int NetworkFileReader::readConstantForceTrajectoryFrame(ifstream& fin) {

	int i, index;
	string line, formatLine, templateLine;
	vector<string> lineVec, templateVec;

	// Use nodes container to do this

	// Get templates first
	map<int, map<string, string>>::iterator it;

	/*
	for (it = forces.begin(); it != forces.end(); ++it) {
		templateLine = it->second["name"];
		if(templateLine == "") {
			fileLog->printError("fObject::name", "Required, but not provided");
		}

		templateVec.push_back(templateLine);
	}
	*/
	
	// Don't clear the forces. We still need the data
//	forces.clear();

	// This line should be the time
	getline(fin, line);

	// Now read the entire frame's worth of data
	index = 0;

	// Initial line
	getline(fin, line);

	while(line != "###") {

		// Split with boost
		boost::split(lineVec, line, boost::is_any_of(","));

		// Format for the forces container
//		forces[index]["name"] = templateVec.at(index);
		forces[index]["position"] = lineVec.at(0) + ", " + lineVec.at(1) + ", " + lineVec.at(2);
//		forces[index]["force"] = lineVec.at(3) + ", " + lineVec.at(4) + ", " + lineVec.at(5);

		index += 1;

		// Get the line
		getline(fin, line);

	}
	
	// Success!
	return BioNetSuccess;
}

string NetworkFileReader::getParam(string paramName) {
	return params[paramName];
}
