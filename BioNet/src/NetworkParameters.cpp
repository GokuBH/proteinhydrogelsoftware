#include "NetworkParameters.hpp"

const vector<string> NetworkParameters::paramList {"iFname", "oFname", "mFname", "tFname", "tpFname", "cFname", "nmFname", "simTime", "dt", "timePerFrame", "numSteps", "stepsPerFrame", "viscosity", "temperature"};

// Default all paramters to something valid
NetworkParameters::NetworkParameters() {

	iFname = bfs::path{""};
	oFname = bfs::path{""};
	mFname = bfs::path{""};
	tFname = bfs::path{""};
	tpFname = bfs::path{""};
	cFname = bfs::path{""};
	nmFname = bfs::path{""};
	cfFname = bfs::path{""};

	simType = NoSim;
	initType = NoInit;
	initTypeSequence.clear();

	dt = 0;
	numSteps = 0;
	numFrames = 0;
	simTime = 0;
	timePerFrame = 0.0;
	stepsPerFrame = 0;

	numNMModes = 1;
		
	localConnectivitySet = false;
	restartSet = false;

	viscosity = 1.0 / InternalUnits::Viscosity;
	temperature = 298 / InternalUnits::Temperature;
	kBT = temperature * kB;

	thermalForcesActive = true;
	vdwForcesActive = true;
	stericForcesActive = true;
	wallForcesActive = false;

	constantForcesActive = false;

	electrostaticForcesActive = false;

	kineticsActive = false;
	bondKineticsActive = false;
	proteinKineticsActive = false;
	
	boxActive = true;
	centraliseNetwork = true;
}

NetworkParameters::~NetworkParameters() {

	iFname = bfs::path{""};
	oFname = bfs::path{""};
	mFname = bfs::path{""};
	tFname = bfs::path{""};
	tpFname = bfs::path{""};
	cFname = bfs::path{""};
	cfFname = bfs::path{""};
	nmFname = bfs::path{""};
	
	simType = NoSim;
	initType = NoInit;
	initTypeSequence.clear();

	numSteps = 0;
	stepsPerFrame = 0;
	simTime = 0;
	timePerFrame = 0;
	dt = 0;

	numNMModes = 0;
		
	localConnectivitySet = false;
	restartSet = false;

	viscosity = 0;
	temperature = 0;
	kBT = 0;

	thermalForcesActive = false;
	vdwForcesActive = false;
	stericForcesActive = false;
	wallForcesActive = false;
	constantForcesActive = false;

	electrostaticForcesActive = false;

	kineticsActive = false;
	bondKineticsActive = false;
	proteinKineticsActive = false;
	
	boxActive = false;
	centraliseNetwork = false;
}

int NetworkParameters::setParameter(string name, string value) {

	boost::algorithm::to_lower(name);

	try {
		if(name == "simtime") {
			simTime = stos(value) / InternalUnits::Time;
		} else if(name == "timeperframe") {
			timePerFrame = stos(value) / InternalUnits::Time;
		} else if(name == "dt" || name == "timestep") {
			dt = stos(value) / InternalUnits::Time;
		} else if(name == "viscosity") {
			viscosity = stos(value) / InternalUnits::Viscosity;
		} else if(name == "temperature") {
			temperature = stos(value) / InternalUnits::Temperature;
			kBT = temperature * kB;
		} else {
			return BioNetWarning;
		}
	} catch (exception &e) {
		fileLog->printError(name, "Unable to set parameter '" + value + "'");
		return BioNetError;
	}
	return BioNetSuccess;
}

string NetworkParameters::getParameter(string name) {

	ostringstream sStream;
	if(name == "iFname" || name == "inputFname") {
		return iFname.string();
	} else if(name == "oFname" || name == "outputFname") {
		return oFname.string();
	} else if(name == "mFname") {
		return mFname.string();
	} else if(name == "tFname") {
		return tFname.string();
	} else if(name == "cFname") {
		return cFname.string();
	} else if(name == "cFname") {
		return cFname.string();
	} else if(name == "cfFname") {
		return cfFname.string();
	} else if(name == "tpFname") {
		return tpFname.string();
	} else if(name == "numSteps") {
		return to_string(numSteps);
	} else if(name == "stepsPerFrame") {
		return to_string(stepsPerFrame);
	} else if(name == "simTime") {
		sStream << simTime;
		return sStream.str();
	} else if(name == "timePerFrame") {
		sStream << timePerFrame;
		return sStream.str();
	} else if(name == "dt" || name == "timestep") {
		sStream << dt;
		return sStream.str();
	} else if(name == "viscosity") {
		sStream << viscosity;
		return sStream.str();
	} else if(name == "temperature") {
		sStream << temperature;
		return sStream.str();
	} else if(name == "kBT" || name == "kT") {
		sStream << kBT;
		return sStream.str();
	} else {
		return "";
	}
}

int NetworkParameters::assign(CommandLineArgs *cmArgs) {

	// Message
	fileLog->printMethodTask("Assigning cmd args as network parameters...");

	iFname = cmArgs->getInputFname();
	tFname = cmArgs->getTrajectoryFname();
	mFname = cmArgs->getMeasurementFname();
	tpFname = cmArgs->getTopologyFname();
	cFname = cmArgs->getCheckpointFname();
	cfFname = cmArgs->getConstantForceFname();
	nmFname = cmArgs->getNetworkModelFname();
	
	simType = cmArgs->getSimType();
	initType = cmArgs->getInitType();
	initTypeSequence = cmArgs->getInitTypeSequence();
	localConnectivitySet = cmArgs->getLocalConnectivity();
	restartSet = cmArgs->getRestart();

	numNMModes = cmArgs->getNumModes();
	
	// Forces
	thermalForcesActive = (bool)cmArgs->thermalForces;
	stericForcesActive = (bool)cmArgs->stericForces;
	vdwForcesActive = (bool)cmArgs->vdwForces;
	electrostaticForcesActive = (bool)cmArgs->electrostaticForces;
	wallForcesActive = (bool)cmArgs->wallForces;
	constantForcesActive = (bool)cmArgs->constantForces;
	boxActive = (bool)cmArgs->boxFlag;
	centraliseNetwork = (bool)cmArgs->centerFlag;

	
	// Specific kinetics (everything set false by default)
	if (cmArgs->kinetics == 1) {
		kineticsActive = true;
		bondKineticsActive = true;
		proteinKineticsActive = true;
	} else if (cmArgs->kinetics == 2) {
		kineticsActive = true;
		bondKineticsActive = true;
		proteinKineticsActive = false;
	} else if (cmArgs->kinetics == 3) {
		kineticsActive = true;
		bondKineticsActive = false;
		proteinKineticsActive = true;
	}
	
	fileLog->printMethodSuccess();
	return BioNetSuccess;

}

int NetworkParameters::assign(map<string,string> paramMap) {

	// Message
	fileLog->printMethodTask("Assigning network parameters...");

	// Try to build actual box first
	map<string, string>::iterator it;
	string lValue, rValue;
	int numWarnings = 0;

	for(it = paramMap.begin(); it != paramMap.end(); ++it) {
		
		lValue = it->first;
		rValue = it->second;

		try {
			if(setParameter(lValue, rValue) == BioNetWarning) {
				fileLog->printWarning(lValue, "Unrecognised Parameter.");
				numWarnings++;
			}
		} catch (invalid_argument& e) {
			fileLog->printError(lValue, "Unable to parse parameter '" + rValue + "'");
			return BioNetError;
		}
	}

	if(numWarnings != 0) {
		fileLog->printWarning("For your parameters, you attempted to specify an unrecognised parameter. We will attempt to continue.");
	}


	fileLog->printMethodSuccess();
	return BioNetSuccess;
}

bool NetworkParameters::validate() {

	// Message
	fileLog->printMethodTask("Validating network parameters...");

	string errMessage;

	//
	// Simulation Parameters (only if simulation is required)
	//	

	
	// Dynamic i.e numerical integration involved
	if(isDynamicSimulation(simType)) {

		if(dt <= 0.0) {
			fileLog->printError("NetworkParameters::dt", "Please specify a value (> 0)");
			return BioNetInvalid;
		}

		if(simTime <= 0) {
			errMessage = "Please specify a value (> 0)";
			fileLog->printError("NetworkParameters::simTime", errMessage);
			return BioNetInvalid;
		} else if(simTime < dt) {
			errMessage = "Please specify a value greater than the timestep 'dt'";
			fileLog->printError("NetworkParameters::simTime", errMessage);
			return BioNetInvalid;	
		}
		
		// Calculate numSteps
		if(timePerFrame < 0) {
			errMessage = "Please specify a value (> 0)";
			fileLog->printError("NetworkParameters::timePerFrame", errMessage);
			return BioNetInvalid;
		} else if(timePerFrame < dt) {
			errMessage = "Please specify a value greater than the timestep 'dt'.";
			fileLog->printError("NetworkParameters::timePerFrame", errMessage);
			return BioNetInvalid;
		} else if(timePerFrame > simTime) {
			errMessage = "Please specify a value less than the total time 'simTime'";
			fileLog->printError("NetworkParameters::timePerFrame", errMessage);
			return BioNetInvalid;	
		}
		
		// Calculate non-trivial stuff (and fix rounding errors)
		calculateNumericalIntegrationParameters();

		// Material parameters
		if(viscosity <= 0) {
			errMessage = "Please specify a value (> 0)";
			fileLog->printError("NetworkParameters::viscosity", errMessage);
			return BioNetInvalid;
		}

		if(thermalForcesActive) {
			if(temperature <= 0) {
				errMessage = "Please specify a value (> 0)";
				fileLog->printError("NetworkParameters::temperature", errMessage);
				return BioNetInvalid;
			}
		}

	}

//	if((initType == MonteCarloI || simType == MonteCarloS) && numSteps <= 0) {
//		fileLog->printError("NetworkParameters::numSteps", "Please specify a value (> 0)");
//		return BioNetInvalid;
//	}

	fileLog->printMethodSuccess();
	return BioNetValid;
}

void NetworkParameters::calculateNumericalIntegrationParameters() {

	// Non-trivial calculations
	string errMessage;

	// Get numSteps	
	numSteps = long(simTime / dt);

	// Get steps per frame
	stepsPerFrame = int(timePerFrame / dt);

	// Syncronise by increasing numSteps
	if(numSteps % stepsPerFrame != 0) {
		numSteps += stepsPerFrame - (numSteps % stepsPerFrame);
	}

	// Redo the times
	simTime = numSteps * dt;
	timePerFrame = stepsPerFrame * dt;

	// And finally
	numFrames = numSteps / stepsPerFrame;
}

void NetworkParameters::printDetails() {
	cout << "Class: NetworkParameters" << endl;
	cout << "Global Parameters: " << endl;
	cout << "\tiFname = " << iFname << endl;
	cout << "\toFname = " << oFname << endl;
	cout << "\ttFname = " << tFname << endl << endl;
	cout << "\tmFname = " << mFname << endl << endl;
	cout << "\ttpFname = " << tpFname << endl << endl;
	cout << "\tcFname = " << cFname << endl << endl;
	cout << "\tcfFname = " << cfFname << endl << endl;
	cout << "\tnmFname = " << nmFname << endl << endl;
	cout << "\tinitType = " <<  getInitTypeString(initType) << endl;
	cout << "\tsimType = " <<  getSimTypeString(simType) << endl;

	cout << "Simulation Parameters: " << endl;
	cout << "\tdt = " << dt << endl;
	cout << "\tnumSteps = " << numSteps << endl;
	cout << "\tnumFrames = " << numFrames << endl;
	cout << "\tsimTime = " << simTime << endl;
	cout << "\tstepsPerFrame = " << stepsPerFrame << endl;
	cout << "\ttimePerFrame = " << timePerFrame << endl << endl;

	cout << "Network Parameters: " << endl;
	cout << "\tLocal Connectivity Set = " << localConnectivitySet << endl;
	cout << "\tRestarting = " << restartSet << endl;
	cout << "\tviscosity = " << viscosity << endl;
	cout << "\ttemperature = " << temperature << endl;
	cout << "\tkBT = " << kBT << endl;

	cout << "Force Types: " << endl;
	cout << "\tThermal Forces Active = " << thermalForcesActive << endl;
	cout << "\tSteric Forces Active = " << stericForcesActive << endl;
	cout << "\tVan der Waals Forces Active = " << vdwForcesActive << endl;
	cout << "\tElectrostatic Forces Active = " << electrostaticForcesActive << endl;
	cout << "\tBox wall Forces Active = " << wallForcesActive << endl;
	cout << "\tConstant Forces Active = " << constantForcesActive << endl;
	cout << "Kinetics Active: " << endl;
	cout << "\t << Bond Kinetics = " << bondKineticsActive << endl;
	cout << "\t << Protein Kinetics = " << proteinKineticsActive << endl;
	
}
