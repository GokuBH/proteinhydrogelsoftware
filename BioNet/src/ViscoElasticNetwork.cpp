#include "ViscoElasticNetwork.hpp"

// Cyclic dependencies
#include "PhysicalSystem.hpp"
#include "SimulationBox.hpp"
#include "NetworkFileWriter.hpp"

ViscoElasticNetwork::ViscoElasticNetwork() : ElasticNetwork::ElasticNetwork() {
	viscosityMatrix.setZero();
	viscoElasticMatrix.setZero();
	viscoElasticMatrixPrecon.setZero();

	VNMEvals = NULL;
	VNMEvecs = NULL;

	VGNMEvals = NULL;
	VGNMEvecs = NULL;
	VANMEvals = NULL;
	VANMEvecs = NULL;
	VENMEvals = NULL;
	VENMEvecs = NULL;
}

ViscoElasticNetwork::~ViscoElasticNetwork() {

	if(VNMEvals != NULL) {
		delete[] VNMEvals;
		VNMEvals = NULL;
		delete[] VNMEvecs;
		VNMEvecs = NULL;
	}

	if(VGNMEvals != NULL) {
		delete[] VGNMEvals;
		VGNMEvals = NULL;
		delete[] VGNMEvecs;
		VGNMEvecs = NULL;
	}
	if(VANMEvals != NULL) {
		delete[] VANMEvals;
		VANMEvals = NULL;
		delete[] VANMEvecs;
		VANMEvecs = NULL;
	}
	if(VENMEvals != NULL) {
		delete[] VENMEvals;
		VENMEvals = NULL;
		delete[] VENMEvecs;
		VENMEvecs = NULL;
	}

	viscosityMatrix.setZero();
	viscoElasticMatrix.setZero();
	viscoElasticMatrixPrecon.setZero();
}

// We only need to overide the buildProteins method. Maybe there's a better way to do this

int ViscoElasticNetwork::build(NetworkParameters *params, NetworkFileReader *fileIn, NetworkFileWriter *fileOut) {

	// Parent first
	if(ElasticNetwork::build(params, fileIn, fileOut)) {
		return BioNetError;
	}

	fileLog->printMethodTask("Building ViscoElastic Network Components...");

	fileLog->printMethodSuccess();
	return BioNetSuccess;
}

int ViscoElasticNetwork::rebuildFromTrajectory(NetworkParameters *params, NetworkFileReader *fileIn, NetworkFileWriter *fileOut) {

	// Parent first
	if(ElasticNetwork::rebuildFromTrajectory(params, fileIn, fileOut)) {
		return BioNetError;
	}

	fileLog->printMethodTask("Rebuilding ViscoElastic Network Components...");

	fileLog->printMethodSuccess();
	return BioNetSuccess;
}

bool ViscoElasticNetwork::validate(NetworkParameters *params) {

	if(validateStructure(params) == BioNetInvalid) {
		fileLog->printError("ElasticNetwork", "Structure invalid :(");
		return BioNetInvalid;
	}		

	// This fails for ViscoElastic inheritance. Fix before uncommenting
	return validateSimulation(params);
}

bool ViscoElasticNetwork::validateStructure(NetworkParameters *params) {

	// This one looks at the structure only! Is this a properly connected network?

	// Parent is fine at this stage
	if(ElasticNetwork::validateStructure(params) == BioNetInvalid) {
		return BioNetInvalid;
	}
	
	return BioNetValid;
}

bool ViscoElasticNetwork::validateSimulation(NetworkParameters *params) {

	int i, checkInt;
	bool checkFlag;

	// If kinetics are on, we need to have at least one kinetic bond established
	if(params->bondKineticsActive) {
		checkInt = 0;
		for(i = 0; i < numBonds; ++i) {
			if(bond[i]->isKinetic()) {
				checkInt += 1;
			}
		}
	
		if(checkInt != 1) {
			fileLog->printError("ViscoElasticNetwork", "Only 1 kinetic bond allowed :(");
			return BioNetInvalid;
		}
	}

	return BioNetValid;
}


int ViscoElasticNetwork::preInitialise() {

	// Message
	fileLog->printMethodTask("Pre-preparing a ViscoElasticNetwork...");

	if(ElasticNetwork::preInitialise() == BioNetError) {
		fileLog->printError("Unable to pre-initialise ElasticNetwork components :(");
		return BioNetError;
	}

	fileLog->printMethodSuccess();
	return BioNetSuccess;
}

int ViscoElasticNetwork::simulate(NetworkParameters *params) {

	// If it's purely elastic, outsource work to the parent (this is not a life lesson!)
	if(!isDynamicSimulation(simType) || isPureElasticSimulation(simType)) {
		if(ElasticNetwork::simulate(params) == BioNetError) {
			return BioNetError;
		}
	} else {

		string errMessage;

		// Message
		fileLog->printMethodTask("Simulating a ViscoElasticNetwork ('" + getSimTypeString(simType) + "')...");

		// Write some file stuff
		ofstream tFout, mFout, cFout, tpFout, cfFout;
		if(!params->restartSet) {
			tFout.open(params->tFname.string(), ofstream::app);
			mFout.open(params->mFname.string(), ofstream::app);
//			cFout.open(params->cFname.string(), ofstream::app);

			fileOut->writeTrajectorySimulationHeader(params, pSys, tFout);
			fileOut->writeMeasurementSimulationHeader(params, pSys, mFout);
			fileOut->writeCheckpointSimulationHeader(params, pSys, cFout);

			// Close output files 
			tFout.close();
			mFout.close();
//			cFout.close();

			// Maybe topology file
			if(params->bondKineticsActive) {
				tpFout.open(params->tpFname.string(), ofstream::app);
				fileOut->writeTopologySimulationHeader(params, pSys, tpFout);
				tpFout.close();
			}
			
			// Maybe constant forces file
			if(params->constantForcesActive) {
				cfFout.open(params->cfFname.string(), ofstream::app);
				fileOut->writeForceSimulationHeader(params, pSys, cfFout);
				cfFout.close();
			}
		}

		if(simType == VGNM) {
			// Ignore any connection constants within the input file. Gaussian network model only uses connectivity, which was
			// rebuilt based on cutoff distance in Network::build()
//			buildLaplacianMatrix();
//			diagonaliseLaplacianMatrix();

		} else if (simType == VANM) {
			// Again, ignore any connection constants within the input file.
//			buildAnisotropicMatrix();
//			diagonaliseAnisotropicMatrix();

		} else if (simType == VENM) {

			// Use all data in the input file
		} else if (simType == Brownian) {
			if(brownianSimulate(params) == BioNetError) {
				fileLog->printError("Unable to complete Brownian simulation of the network :(");
				return BioNetError;
			}
		} else if (simType == Langevin) {
			if(langevinSimulate(params) == BioNetError) {
				fileLog->printError("Unable to complete Langevin simulation of the network :(");
				return BioNetError;
			}
		} else {
		
			// No idea how the user managed to get here, but we'll include it just in case
			errMessage = "Unrecognised simType: " + getSimTypeString(simType) +  ". Please contact development team.";
			fileLog->printError("ElasticNetwork::simulate", errMessage);
			return BioNetError;

		}

		// Write final data out to a file
		tFout.open(params->tFname.string(), ofstream::app);
		mFout.open(params->mFname.string(), ofstream::app);
//		cFout.open(params->cFname.string(), ofstream::app);

		// Write footers
		fileOut->writeTrajectorySimulationFooter(tFout);
		fileOut->writeMeasurementSimulationFooter(mFout);
//		fileOut->writeCheckpointSimulationFooter(cFout);

		// Close output files 
		tFout.close();
		mFout.close();
//		cFout.close();

		// Maybe topology file
		if(params->bondKineticsActive) {
			tpFout.open(params->tpFname.string(), ofstream::app);
			fileOut->writeTopologySimulationFooter(tpFout);
			tpFout.close();
		}

		// Maybe constant force file
		if(params->constantForcesActive) {
			cfFout.open(params->cfFname.string(), ofstream::app);
			fileOut->writeForceSimulationFooter(cfFout);
			cfFout.close();
		}
		
		fileLog->printMethodSuccess();
	}

	return BioNetSuccess;
}

int ViscoElasticNetwork::brownianSimulate(NetworkParameters *params) {

	int n, nStart;
	clock_t t0, timeGone, t;
	scalar simTime, timeRemaining;

	// Open some files for writing
	ofstream tFout, mFout, cFout, tpFout, cfFout;
	tFout.open(params->tFname.string(), ofstream::app);
	mFout.open(params->mFname.string(), ofstream::app);
//	cFout.open(params->cFname.string(), ofstream::app);

	if(params->bondKineticsActive) {
		tpFout.open(params->tpFname.string(), ofstream::app);
	}

	if(params->constantForcesActive) {
		cfFout.open(params->cfFname.string(), ofstream::app);
	}
	
	// Whether restarting or not, we need to do an initial set of calculations to get the state of the system at t == 0 / t = tStart
	// If restarting, we need to do everything. If starting, just need standard forces (no kinetics)
	if(params->restartSet) {

		// Check trajectory time
		nStart = (int)((pSys->getSimTime() / params->dt));
		if(nStart % params->stepsPerFrame != 0) {
			nStart += (params->stepsPerFrame - (nStart % params->stepsPerFrame));
		}

		if(nStart >= params->numSteps) {
			fileLog->printProgressData(params->numSteps, 0, params->numSteps, 0, params->simTime);
			#ifdef _OPENMP
				fileLog->printFinalProgressData(0.0, pSys->getSimTime());
			#else
				fileLog->printFinalProgressData(0.0, pSys->getSimTime());
			#endif
			return BioNetSuccess;
		}

		// Update for next loop
		pSys->updateSimTime(params->timePerFrame);
		simTime = pSys->getSimTime();
		nStart += 1;

	} else {
		nStart = 0;
		simTime = 0;
	}

	// Do all forces from 'last' turn (add thermal forces, which are not on the trajectory)
	if(params->thermalForcesActive) {
		calculateThermalForces(rngCont);
	}

	// Update
	applyForces(params->dt);

	// And keep in box
	box->applyBoundaryConditions();

	// Kinetics
	if(params->restartSet) {
		if(params->kineticsActive) {
			if(doKinetics(params) == BioNetError) {
				fileLog->printError("Unable to complete kinetics calculations");
				return BioNetError;
			}
		}
	}

	// Zero all forces
	zeroForces();

	// Do all interactions to get current state
	box->doAllInteractions();

	// Turn it into a simulation
	if(params->thermalForcesActive) {
		calculateThermalForces(rngCont);
	}

	// Unique forces
	calculateUniqueForces(params->dt);
	
	// Elastic forces last as they have restraints implicit
	calculateElasticForces();

	#ifdef _OPENMP
		t0 = omp_get_wtime();
	#else
		t0 = clock();
	#endif
	for(n = nStart; n < params->numSteps; ++n) {

		//
		// User Info (contains all forces and energies before the integration, not after)
		//
		if(n % params->stepsPerFrame == 0) {

			// Write frame to file

			// Time data
			timeRemaining = -1;
			if(n - nStart != 0) {
				#ifdef _OPENMP
					timeRemaining = (scalar)(((omp_get_wtime() - t0) / (n - nStart)) * (params->numSteps - n));
				#else
					timeRemaining = (scalar)(((clock() - t0) / (n - nStart)) * (params->numSteps - n)) / CLOCKS_PER_SEC;
				#endif
//				cout << endl << clock() << ", " << CLOCKS_PER_SEC << endl;
			}

			// Calculate output stuff
			calculateMeasureables();

			// Output to streams
			fileOut->writeTrajectoryFrame(simTime, params, pSys, tFout);
			fileOut->writeMeasurementFrame(simTime, params, pSys, mFout);

			if(params->bondKineticsActive) {
				fileOut->writeTopologyFrame(simTime, params, pSys, tpFout);
			}

			if(params->constantForcesActive) {
				fileOut->writeForceFrame(simTime, params, pSys, cfFout);
			}
			
			fileLog->printProgressData(n, nStart, params->numSteps, timeRemaining, params->simTime);

			// Update time
			pSys->updateSimTime(params->timePerFrame);
			simTime = pSys->getSimTime();
		}

		// Apply forces immediately (we have everything stored from preInitialise / previous step)
		applyForces(params->dt);
		
		// And keep in box
		box->applyBoundaryConditions();
		
		// Kinetics
		if(params->kineticsActive) {
			if(doKinetics(params) == BioNetError) {
				fileLog->printError("Unable to complete kinetics calculations");
				return BioNetError;
			}
		}
		
		// Zero all forces
		zeroForces();

		// Do all interactions to get current state
		box->doAllInteractions();

		// Turn it into a simulation
		if(params->thermalForcesActive) {
			calculateThermalForces(rngCont);
		}
				
		// Unique forces
		calculateUniqueForces(params->dt);
		
		// Elastic forces last as they have restraints implicit
		calculateElasticForces();
	}

	// Finalise
//	pSys->updateSimTime(params->timePerFrame);
	pSys->updateSimTime((n % params->stepsPerFrame) * params->timePerFrame / params->stepsPerFrame);
	simTime = pSys->getSimTime();
	timeRemaining = 0;

	// Calculate output stuff
	calculateMeasureables();

	fileOut->writeTrajectoryFrame(simTime, params, pSys, tFout);
	fileOut->writeMeasurementFrame(simTime, params, pSys, mFout);
//	fileOut->writeCheckpointFrame(simTime, params, pSys, cFout);

	// Close output files 
	tFout.close();
	mFout.close();
//	cFout.close();

	if(params->bondKineticsActive) {
		fileOut->writeTopologyFrame(simTime, params, pSys, tpFout);
		tpFout.close();
	}

	if(params->constantForcesActive) {
		fileOut->writeForceFrame(simTime, params, pSys, cfFout);
		cfFout.close();
	}

	fileLog->printProgressData(n, 0, params->numSteps, timeRemaining, params->simTime);
	#ifdef _OPENMP
		fileLog->printFinalProgressData((scalar)(omp_get_wtime() - t0), simTime);
	#else
		fileLog->printFinalProgressData((scalar)(clock() - t0) / CLOCKS_PER_SEC, simTime);
	#endif
	return BioNetSuccess;
}

int ViscoElasticNetwork::langevinSimulate(NetworkParameters *params) {

	int n, nStart;
	clock_t t0, timeGone, t;
	scalar simTime, timeRemaining;

	// Open some files for writing
	ofstream tFout, mFout, cFout, tpFout, cfFout;
	tFout.open(params->tFname.string(), ofstream::app);
	mFout.open(params->mFname.string(), ofstream::app);
//	cFout.open(params->cFname.string(), ofstream::app);

	if(params->bondKineticsActive) {
		tpFout.open(params->tpFname.string(), ofstream::app);
	}

	if(params->constantForcesActive) {
		cfFout.open(params->cfFname.string(), ofstream::app);
	}
	
	// Timing for the user (if restarting, get appropriate start step)
	if(params->restartSet) {

		// Check trajectory time
		nStart = (int)((pSys->getSimTime() / params->dt));
		cout << nStart << ", " << params->stepsPerFrame << endl;
		if(nStart % params->stepsPerFrame != 0) {
			nStart += (params->stepsPerFrame - nStart % params->stepsPerFrame);
		}

		// Do all forces from 'last' turn (add thermal forces, which are not on the trajectory)
		if(params->thermalForcesActive) {
			calculateThermalForces(rngCont);
		}

		// Update
		applyForces(params->dt);

		// And keep in box
		box->applyBoundaryConditions();

		// Kinetics
		if(params->kineticsActive) {
			if(doKinetics(params) == BioNetError) {
				fileLog->printError("Unable to complete kinetics calculations");
				return BioNetError;
			}
		}

		// Zero all forces
		zeroForces();

		// This is also the repopulation trigger
//		box->repopulate();

		// Do all interactions to get current state
		box->doAllInteractions();

		// Turn it into a simulation
		if(params->thermalForcesActive) {
			calculateThermalForces(rngCont);
		}

		// Unique forces
		calculateUniqueForces(params->dt);
		
		// Elastic forces last as they have restraints implicit
		calculateElasticForces();

		// Update for next loop
		pSys->updateSimTime(params->timePerFrame);
		simTime = pSys->getSimTime();
		nStart += 1;
		
	} else {
		nStart = 0;
	}

	#ifdef _OPENMP
		t0 = omp_get_wtime();
	#else
		t0 = clock();
	#endif
	for(n = nStart; n < params->numSteps; ++n) {

		//
		// User Info (contains all forces and energies before the integration, not after)
		//
		if(n % params->stepsPerFrame == 0) {

			// Write frame to file

			// Time data
			timeRemaining = -1;
			if(n - nStart != 0) {
				#ifdef _OPENMP
					timeRemaining = (scalar)(((omp_get_wtime() - t0) / (n - nStart)) * (params->numSteps - n));
				#else
					timeRemaining = (scalar)(((clock() - t0) / (n - nStart)) * (params->numSteps - n)) / CLOCKS_PER_SEC;
				#endif
//				cout << endl << clock() << ", " << CLOCKS_PER_SEC << endl;
			}

			// Calculate output stuff
			calculateMeasureables();

			// Output to streams
			fileOut->writeTrajectoryFrame(simTime, params, pSys, tFout);
			fileOut->writeMeasurementFrame(simTime, params, pSys, mFout);

			if(params->bondKineticsActive) {
				fileOut->writeTopologyFrame(simTime, params, pSys, tpFout);
			}

			if(params->constantForcesActive) {
				fileOut->writeForceFrame(simTime, params, pSys, cfFout);
			}
			
			fileLog->printProgressData(n, nStart, params->numSteps, timeRemaining, params->simTime);

			// Update time
			pSys->updateSimTime(params->timePerFrame);
			simTime = pSys->getSimTime();
		}

		// Apply forces immediately
		applyForces(params->dt);

		// And keep in box
		box->applyBoundaryConditions();

		// Kinetics
		if(params->kineticsActive) {
			if(doKinetics(params) == BioNetError) {
				fileLog->printError("Unable to complete kinetics calculations");
				return BioNetError;
			}
		}

		// Zero all forces
		zeroForces();

		// This is also the repopulation trigger
//		box->repopulate();

		// Do all interactions to get current state
		box->doAllInteractions();

		// Turn it into a simulation
		if(params->thermalForcesActive) {
			calculateThermalForces(rngCont);
		}

		// Unique forces
		calculateUniqueForces(params->dt);
		
		// Elastic forces last as they have restraints implicit
		calculateElasticForces();
		calculateViscousForces();
	}

	// Finalise
//	pSys->updateSimTime(params->timePerFrame);
	pSys->updateSimTime((n % params->stepsPerFrame) * params->timePerFrame / params->stepsPerFrame);
	simTime = pSys->getSimTime();
	timeRemaining = 0;

	// Calculate output stuff
	calculateMeasureables();

	fileOut->writeTrajectoryFrame(simTime, params, pSys, tFout);
	fileOut->writeMeasurementFrame(simTime, params, pSys, mFout);
//	fileOut->writeCheckpointFrame(simTime, params, pSys, cFout);

	// Close output files 
	tFout.close();
	mFout.close();
//	cFout.close();

	if(params->bondKineticsActive) {
		fileOut->writeTopologyFrame(simTime, params, pSys, tpFout);
		tpFout.close();
	}

	if(params->constantForcesActive) {
		fileOut->writeForceFrame(simTime, params, pSys, cfFout);
		cfFout.close();
	}
	
	fileLog->printProgressData(n, 0, params->numSteps, timeRemaining, params->simTime);
	#ifdef _OPENMP
		fileLog->printFinalProgressData((scalar)(omp_get_wtime() - t0), simTime);
	#else
		fileLog->printFinalProgressData((scalar)(clock() - t0) / CLOCKS_PER_SEC, simTime);
	#endif

	return BioNetSuccess;
}

void ViscoElasticNetwork::calculateViscousForces() {

	int i;

	// Loop over all node and apply force to nodes
	if(localConnectivitySet) {
//		#ifdef _OPENMP
//			#pragma omp parallel for
//		#endif
		for(i = 0; i < node.size(); ++i) {

			if(node[i] != NULL) {

				node[i]->calculateViscousForce();
				node[i]->calculateViscousTorque();
			}
		}
	} else {
//		#ifdef _OPENMP
//			#pragma omp parallel for
//		#endif
		for(i = 0; i < node.size(); ++i) {
			
			if(node[i] != NULL) {

				node[i]->calculateViscousForce();
			}
		}
	}
}


void ViscoElasticNetwork::calculateThermalForces(RNGContainer *r) {

	int i;

	// Loop over all nodes (differently depending on the state)
	if(localConnectivitySet) {

//		#ifdef _OPENMP
//			#pragma omp parallel for
//		#endif
		for(i = 0; i < node.size(); ++i) {
			
			if(node[i] != NULL) {

				// Calculations
//				#ifdef _OPENMP
//					node[i]->calculateThermalForce(r->getThermalRN(omp_get_thread_num()), r->getThermalRN(omp_get_thread_num()), r->getThermalRN(omp_get_thread_num()));
//					node[i]->calculateThermalTorque(r->getThermalRN(omp_get_thread_num()), r->getThermalRN(omp_get_thread_num()), r->getThermalRN(omp_get_thread_num()));
//				#else
					node[i]->calculateThermalForce(r->getThermalRN(0), r->getThermalRN(0), r->getThermalRN(0));
					node[i]->calculateThermalTorque(r->getThermalRN(0), r->getThermalRN(0), r->getThermalRN(0));
//				#endif
			}
		}
	} else {
//		#ifdef _OPENMP
//			#pragma omp parallel for
//		#endif
		for(i = 0; i < node.size(); ++i) {
	
			if(node[i] != NULL) {
	
				// Calculations
//				#ifdef _OPENMP
//					node[i]->calculateThermalForce(r->getThermalRN(omp_get_thread_num()), r->getThermalRN(omp_get_thread_num()), r->getThermalRN(omp_get_thread_num()));
//				#else
					node[i]->calculateThermalForce(r->getThermalRN(0), r->getThermalRN(0), r->getThermalRN(0));
//				#endif
			}
		}
	}
}

/*
void ViscoElasticNetwork::buildViscosityMatrix() {
	
	// Must be re-initialised to dynamically allocate. Directional dependence
	int numRows = 3 * numNodes;
	viscosityMatrix.resize(numRows, numRows);

	// Build it by scanning over the node list and building a triplet list
	int i, j, cIndex, rIndex;
	vector<Triplet<scalar>> vTriplets;
	for(i = 0; i < node.size(); ++i) {
		
		// Still 3 directions
		for(j = 0; j < 3; ++j) {
			cIndex = 3 * i + j;
			rIndex = cIndex;
	
			// Diagonal effect only
			vTriplets.push_back(Triplet<scalar>(rIndex, cIndex, node[i]->getDrag()));
		}
	}
-
	// Now build it
	viscosityMatrix.setFromTriplets(vTriplets.begin(), vTriplets.end());
}

void ViscoElasticNetwork::diagonaliseViscosityMatrix() {

	// Simply do the built-in computation
	VNMES.compute(viscosityMatrix);

	// Assign to the eigensystem objects
	VGNMEvals = assignEigenvalues(VNMES);
	VGNMEvecs = assignEigenvectors(VNMES);
}

void ViscoElasticNetwork::buildViscoElasticMatrix() {

	// Build dense by default
//	buildDenseViscoElasticMatrix();
	
	// Do some check and perhaps build sparse
	if(true) {
//		buildSparseViscoElasticMatrix();
	}
}

/*
void ViscoElasticNetwork::buildDenseViscoElasticMatrix() {

	// Requires the elastic and viscosity matrices to be built
	if(!elasticMatrixBuilt) {
		buildElasticityMatrix();
	}
	if(!viscosityMatrixBuilt) {
		buildViscosityMatrix();
	}

	// Requires viscosity matrix to be diagonalised
	if(!viscosityEigensystemSolved) {
		diagonaliseViscosityMatrix();
	}

	// These matrices are required for a coordinate transformation
	int i; 
	int numRows = 3 * numNodes;

	// Build diagonal trasform
	viscoElasticMatrixPrecon.resize(numRows, numRows);
	vector<Triplet<scalar>> vTriplets;
	for(i = 0; i < numRows; ++i) {
	
		// Diagonal effect only
		vTriplets.push_back(Triplet<scalar>(i, i, 1.0 / sqrt(VNMES.eigenvalues()[i])));
	}

	// Now build it
	viscoElasticMatrixPrecon.setFromTriplets(vTriplets.begin(), vTriplets.end());

	// Now we can build the matrix
	viscoElasticMatrixDense = viscoElasticMatrixPrecon.transpose() * VNMES.eigenvectors().transpose() * elasticityMatrix * VNMES.eigenvectors() * viscoElasticMatrixPrecon;	
}
*/
/*
void ViscoElasticNetwork::buildSparseViscoElasticMatrix() {

	// Requires the dense viscoelastic matrix to be built
	if(!viscoElasticDenseMatrixBuilt) {
		buildDenseViscoElasticMatrix();
	}

	// Loop over all elements and build sparse matrix based on some limiting value
	int i, j;
	int numRows = 3 * numNodes;
	vector<Triplet<scalar>> vTriplets;
	scalar lowerLimit = 1e-10;

	viscoElasticMatrix.resize(numRows, numRows);
	for(i = 0; i < numRows; ++i) {
		for(j = 0; j < numRows; ++j) {
			if(fabs(viscoElasticMatrixDense(i, j)) > lowerLimit) {
				vTriplets.push_back(Triplet<scalar>(i, j, viscoElasticMatrixDense(i, j)));
			} else {
				vTriplets.push_back(Triplet<scalar>(i, j, 0));
			}
		}
	}

	// Now build it
	viscoElasticMatrix.setFromTriplets(vTriplets.begin(), vTriplets.end());
}
*/
/*
void ViscoElasticNetwork::diagonaliseViscoElasticMatrix() {
	
	// Sparse takes priority
	cout << "Viscoelasticity" << endl;
	if(viscoElasticSparseMatrixBuilt) {

		// Assign evals
		VGNMES.compute(viscoElasticMatrix);
		VGNMEvals = ElasticNetwork::assignEigenvalues(VGNMES);

		// Build the mode matrix
		MatrixX R = VNMES.eigenvectors() * viscoElasticMatrixPrecon * VGNMES.eigenvectors();

		// Assign evecs
		VGNMEvecs = assignEigenvectors(R);

	} else {
		VGNMDenseES.compute(viscoElasticMatrixDense);
		VGNMEvals = assignEigenvalues(VGNMDenseES);

		// Build the mode matrix
		MatrixX R = VNMES.eigenvectors() * viscoElasticMatrixPrecon * VGNMDenseES.eigenvectors();

		// Assign evecs
		VGNMEvecs = assignEigenvectors(R);
	}

	viscoElasticEigensystemSolved = true;
}
*/

/*
// No idea why the compiler wasn't inheriting these methods
scalar* ViscoElasticNetwork::assignEigenvalues(SelfAdjointEigenSolver<SparseMatrix<scalar>> es) {

	return ElasticNetwork::assignEigenvalues(es);
}

scalar** ViscoElasticNetwork::assignEigenvectors(SelfAdjointEigenSolver<SparseMatrix<scalar>> es) {

	return ElasticNetwork::assignEigenvectors(es);
}

scalar* ViscoElasticNetwork::assignEigenvalues(SelfAdjointEigenSolver<MatrixX> es) {

	// Just stick them in a standardised container
	int i, j;
	int numRows = numNodes * 3;
	scalar *evals;
	evals = new scalar[numRows];
	for(i = 0; i < numRows; ++i) {
		evals[i] = es.eigenvalues()[i];
	}

	// By the way, because this returns a pointer, if it is called twice then the original memory is not deallocated
	// ...do something about it	
	return evals;
}

scalar** ViscoElasticNetwork::assignEigenvectors(MatrixX evecContainer) {

	// Just stick them in a standardised container
	int i, j;	
	int numRows = numNodes * 3;
	scalar **evecs;
	evecs = new scalar*[numRows];
	for(i = 0; i < numRows; ++i) {
		evecs[i] = new scalar[numRows];
		for(j = 0; j < numRows; ++j) {
			evecs[i][j] = evecContainer.col(i)[j];
		}
	}

	return evecs;
}

scalar* ViscoElasticNetwork::getEigenvalues(SimType simType) {
	if(!isViscousSimulation(simType)) {
		return ElasticNetwork::getEigenvalues(simType);
	}

	if(simType == VGNM) {
		return VGNMEvals;
	} else if (simType == VANM) {
		return VANMEvals;
	} else if (simType == VENM) {
		return VENMEvals;
	} else {
		return NULL;
	}
}

scalar** ViscoElasticNetwork::getEigenvectors(SimType simType) {
	if(!isViscousSimulation(simType)) {
		return ElasticNetwork::getEigenvectors(simType);
	}

	if(simType == VGNM) {
		return VGNMEvecs;
	} else if (simType == VANM) {
		return VANMEvecs;
	} else if (simType == VENM) {
		return VENMEvecs;
	} else {
		return NULL;
	}
}
*/

void ViscoElasticNetwork::printDetails(bool verbose) {
	cout << endl;
	cout << "Class: ViscoElasticNetwork" << endl;
	cout << "Parameters: " << endl;
	cout << "\tNumber of Proteins: " << numProteins << endl;
	cout << "\tNumber of Bonds: " << numBonds << endl;
	cout << "\tNumber of Nodes: " << numNodes << endl;
	cout << "\tNumber of Connections: " << numConnections << endl;

	cout << endl;
	cout << "Energies: " << endl;
	cout << "\tElastic Energy: " << getElasticEnergy() << endl;
	cout << "\tKinetic Energy: " << getKineticEnergy() << endl;
	cout << "\tBond Energy: " << getBondEnergy() << endl;
	cout << "\tTotal Energy: " << getTotalEnergy() << endl;

	if(verbose) {
		cout << endl << "Proteins:" << endl << endl;
		printProteinDetails();

		cout << endl << "Bonds:" << endl << endl;
		printBondDetails();

		cout << endl << "Nodes:" << endl << endl;
		printNodeDetails();

		cout << endl << "Connections:" << endl << endl;
		printConnectionDetails();
//		printSimulationBoxDetails();
	}
}
