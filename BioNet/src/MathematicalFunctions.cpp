#include "MathematicalFunctions.hpp"
Vector2 MathFunctions::cartesian(scalar theta) {
	
	Vector2 cart;
	cart[0] = cos(theta);
	cart[1] = sin(theta);

	return cart;
}

Vector3 MathFunctions::cartesian(scalar theta, scalar phi) {

	Vector3 cart;
	cart[0] = sin(theta) * cos(phi);
	cart[1] = sin(theta) * sin(phi);
	cart[2] = cos(theta);

	return cart;
}

Vector3 MathFunctions::cartesian(Vector2 sphere) {
	
	return cartesian(sphere(0), sphere(1));
}

Vector3 MathFunctions::cartesian(scalar r, scalar theta, scalar phi) {

	return r * cartesian(theta, phi);
}

Vector3 MathFunctions::cartesian(Vector3 sphere) {

	return cartesian(sphere(0), sphere(1), sphere(2));
}

Vector3 MathFunctions::cartesian(Quat q) {
	return q.vec();
}

Vector2 MathFunctions::spherical2(Vector3 cart) {

	Vector2 sphere;
	scalar radius;

	radius = cart.norm();

	// Try to acos
	sphere[0] = acos(cart[2]/radius);

	// Make sure cos is ok
	if(isnan(sphere[0])) {

		for(int i = 0; i < 2; ++i) {
			sphere[i] = 0;
		}
		return sphere;
	}

	// Try to atan (atan2 has quadrants)
	sphere[1] = atan2(cart[1], cart[0]);
	
	// Make sure tan is ok
	if(isnan(sphere[1])) {
		sphere[1] = 0.0;
	}

	return sphere;
}

Vector2 MathFunctions::spherical2(Quat q) {
	return spherical2(q.vec());
}

Vector3 MathFunctions::spherical3(Vector3 cart) {

	Vector2 bit;
	Vector3 sphere;

	// Get the anugular bit
	bit = spherical2(cart);
	
	// Copy into the other one
	sphere[0] = cart.norm();
	sphere[1] = bit[0];
	sphere[2] = bit[1];

	return sphere;
}

Vector3 MathFunctions::spherical3(Quat q) {
	return spherical3(q.vec());
}

Matrix3 MathFunctions::eulerToZXZ(Vector3 ori) {
	return (AngleAxisS(ori[2], Vector3::UnitZ()) * AngleAxisS(ori[1], Vector3::UnitX()) * AngleAxisS(ori[0], Vector3::UnitZ())).toRotationMatrix();
}

Vector3 MathFunctions::ZXZToEuler(Matrix3 mat) {

	Vector3 v;

	if(mat(2,2) == 1) {
	
		// No x rotation at all
		v(1) = 0;
		v(2) = 0;
		v(0) = atan2(mat(1,0), mat(0,0));

	} else {

		// First, assume standard angle
		v(1) = acos(mat(2,2));
		v(0) = atan2(mat(2,0) / sin(v(1)), mat(2,1) / sin(v(1)));
		v(2) = atan2(mat(0,2) / sin(v(1)), -1 * mat(1,2) / sin(v(1)));

		// May need to check in future against other elements in the matrix (in case arcc was the wrong direction)
	}

	return v;
}
