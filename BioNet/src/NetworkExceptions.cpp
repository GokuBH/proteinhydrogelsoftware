#include "NetworkExceptions.hpp"

ParameterException::ParameterException() {
	message = "System parameters are invalid :( ";
}

ParameterException::ParameterException(string newMessage) {
	message = newMessage;
}

ParameterException::~ParameterException() {

}

string ParameterException::error() {
	return message;
}

EigensystemException::EigensystemException() {
	message = "Eigensystem error :( ";
}

EigensystemException::EigensystemException(string newMessage) {
	message = newMessage;
}

EigensystemException::~EigensystemException() {

}

string EigensystemException::error() {
	return message;
}
