#include "Containers.hpp"

PairPair::PairPair() {

	obj[0] = NULL;
	obj[1] = NULL;

	metric = 0.0;
	energy = 0.0;
}

PairPair::PairPair(vObject *a, vObject *b) {

	obj[0] = a;
	obj[1] = b;

	metric = (a->getPosition() - b->getPosition()).norm();
	energy = 0.0;
}

PairPair::PairPair(vObject *a, vObject *b, scalar metric) {
	obj[0] = a;
	obj[1] = b;

	this->metric = metric;
	energy = 0.0;
}

PairPair::~PairPair() {

	obj[0] = NULL;
	obj[1] = NULL;

	metric = 0.0;
	energy = 0.0;
}
