#include "PhysicalSystem.hpp"

// Cyclic dependencies
#include "NetworkFileWriter.hpp"

string getNetworkTypeString(NetworkType netType) {
	return netTypeString[netType];
}

string getSimBoxTypeString(SimBoxType boxType) {
	return boxTypeString[boxType];
}

PhysicalSystem::PhysicalSystem() {
	params = NULL;
	net = NULL;
	box = NULL;

	fileOut = NULL;

	netType = NoNet;
	boxType = NoBox;

	built = false;
	preInitialised = false;
	initialised = false;
	simulated = false;

	simTime = 0.0;
}


PhysicalSystem::~PhysicalSystem() {
	if(params != NULL) {
		delete params;
	}
	params = NULL;
	if(net != NULL) {
		delete net;
	}
	net = NULL;
	if(box != NULL) {
		delete box;
	}
	box = NULL;

	// These are deleted elsewhere
	fileOut = NULL;

	netType = NoNet;
	boxType = NoBox;

	built = false;
	preInitialised = false;
	initialised = false;
	simulated = false;
	simTime = 0.0;
}

void PhysicalSystem::setBuilt(bool b) {
	built = b;
}

bool PhysicalSystem::isBuilt() {
	return built;
}

void PhysicalSystem::setPreInitialised(bool b) {
	preInitialised = b;
}

bool PhysicalSystem::isPreInitialised() {
	return preInitialised;
}

void PhysicalSystem::setInitialised(bool b) {
	initialised = b;
}

bool PhysicalSystem::isInitialised() {
	return initialised;
}

void PhysicalSystem::setSimulated(bool b) {
	simulated = b;
}

bool PhysicalSystem::isSimulated() {
	return simulated;
}

void PhysicalSystem::setSimTime(scalar time) {
	simTime = time;
}

void PhysicalSystem::updateSimTime(scalar time) {
	simTime += time;
}

scalar PhysicalSystem::getSimTime() {
	return simTime;
}

NetworkParameters * PhysicalSystem::getParameters() {
	return params;
}

Network * PhysicalSystem::getNetwork() {
	return net;
}

SimulationBox * PhysicalSystem::getSimulationBox() {
	return box;
}

int PhysicalSystem::build(CommandLineArgs *cmArgs, NetworkFileReader *fileIn, NetworkFileWriter *fileOut) {

	// User Info
	fileLog->printMethodTask("Building Physical System...");

	// Build the parameters container
	params = new NetworkParameters();

	// Assign command line arguments (already validated)
	if(params->assign(cmArgs) == BioNetError) {

		string errString = "Assignment of parameter data from command line arguments failed :( ";
		errString += "This shouldn't have happened. Please contact support.";		
		fileLog->printError(errString);
		return BioNetError;
	}

	// Assign and validate the parameters
	if(params->assign(fileIn->params) == BioNetError) {

		string errString = "Assignment of parameter data from '" + cmArgs->getInputFname().string() + "' failed :( ";
		fileLog->printError(errString);
		return BioNetError;
	}

	if(params->validate() == BioNetInvalid) {

		string errString = "Validation of parameter data from '" + cmArgs->getInputFname().string() + "' failed :( ";
		fileLog->printError(errString);
		return BioNetError;
	}

	// We have all the required parameters now. Initialise a network
	if(isDynamicSimulation(params->simType) || requiresInitialisation(params->initType)) {
		net = new ViscoElasticNetwork();
		netType = NViscoElastic;
	} else {
		net = new ElasticNetwork();
		netType = NElastic;
	}

	// Build the network
	if(net->build(params, fileIn, fileOut) == BioNetError) {

		string errString = "Unable to build Network model :( ";
		fileLog->printError(errString);
		return BioNetError;
	}

	// Set pointer to this
	net->setPhysicalSystem(this);

	if(net->validate(params) == BioNetInvalid) {

		string errString = "Validation of network data from '" + cmArgs->getInputFname().string() + "' failed :( ";
		fileLog->printError(errString);
		return BioNetError;
	}
	
	// Build the simulation box
	box = new SimulationBox();
	boxType = SBCube;

	if(box->build(params, net, fileIn) == BioNetError) {

		string errString = "Unable to build simulation box :( ";
		fileLog->printError(errString);
		return BioNetError;
	}

	if(box->validate() == BioNetInvalid) {

		string errString = "Validation of simulation box data from '" + cmArgs->getInputFname().string() + "' failed :( ";
		fileLog->printError(errString);
		return BioNetError;
	}

	// If all of this was successful, connect to the global NetworkFileWriter and NetworkFileLogger objects
	this->fileOut = fileOut;

	// Finalise system
	setBuilt(true);
	fileLog->printMethodSuccess();
//	net->printDetails(true);
//	return BioNetError;

	return BioNetSuccess;
}

int PhysicalSystem::rebuildFromTrajectory(CommandLineArgs *cmArgs, NetworkFileReader *fileIn, NetworkFileWriter *fileOut) {

	// User Info
	fileLog->printMethodTask("Rebuilding Physical System from trajectory data...");

	// Rebuild the network
	if(net->rebuildFromTrajectory(params, fileIn, fileOut) == BioNetError) {

		string errString = "Unable to build Network model :( ";
		fileLog->printError(errString);
		return BioNetError;
	}

	// Revalidate
	if(net->validate(params) == BioNetInvalid) {

		string errString = "Validation of network data from '" + cmArgs->getInputFname().string() + "' failed :( ";
		fileLog->printError(errString);
		return BioNetError;
	}
	
	 
	fileLog->printMethodSuccess();
	return BioNetSuccess;
}

// Validation methods
bool PhysicalSystem::validate() {

	if(net->validate(params) == BioNetInvalid) {

		string errString = "Validation of network data failed :( ";
		fileLog->printError(errString);
		return BioNetInvalid;
	}

	if(box->validate() == BioNetInvalid) {

		string errString = "Validation of simulation box data failed :( ";
		fileLog->printError(errString);
		return BioNetInvalid;
	}

	return BioNetValid;

}

bool PhysicalSystem::globalValidate() {

	// Simulation Box required
	if((params->initType == MonteCarloI || params->simType == MonteCarloS) && !box->exists) {
		fileLog->printError("InitType::MonteCarlo", "A simulation box is required for Monte Carlo simulations :(");
		return BioNetInvalid;
	}

	return BioNetValid;

}

int PhysicalSystem::preInitialise(scalar simTime) {

	// User Info
	fileLog->printMethodTask("Preparing Physical System for simulation...");

	if(!isBuilt()) {
		fileLog->printError("PhysicalSystem::preInitialise", "Unable to pre-initialise as the system has not yet been built! :(");
		return BioNetError;
	}

	// Now, do things differently depending on restart
	if(isDynamicSimulation(params->simType)) {
		if(!params->restartSet) {

			// Set time to zero
			setSimTime(0.0);

			// Open some files and write some headers
			ofstream fout;
			fout.open(params->tFname.string());
			fileOut->writeTrajectoryHeader(fout);
			fout.close();

			fout.open(params->mFname.string());
			fileOut->writeMeasurementHeader(fout);
			fout.close();

			fout.open(params->cFname.string());
			fileOut->writeCheckpointHeader(fout);
			fout.close();

			if(params->bondKineticsActive) {
				fout.open(params->tpFname.string());
				fileOut->writeTopologyHeader(fout);
				fout.close();
			}
			
			if(params->constantForcesActive) {
				fout.open(params->cfFname.string());
				fileOut->writeForceHeader(fout);
				fout.close();
			}
		} else {

			// Set sim time to whatever is on the file reader (from when it read the trajectory)
			setSimTime(simTime);
			
			// Truncate files
//			if(fileOut->truncateCheckpoint(params, this, params->cFname) == BioNetError) {
//				fileLog->printError("PhysicalSystem::preInitialise", "Unable to pre-initialise the system as we could not read and truncate the checkpoint file! :(");
//				return BioNetError;
//			}

			if(fileOut->truncateTrajectory(params, this, params->tFname) == BioNetError) {
				fileLog->printError("PhysicalSystem::preInitialise", "Unable to pre-initialise the system as we could not read and truncate the trajectory file! :(");
				return BioNetError;
			}


			if(fileOut->truncateMeasurement(params, this, params->mFname) == BioNetError) {
				fileLog->printError("PhysicalSystem::preInitialise", "Unable to pre-initialise the system as we could not read and truncate the measurement file! :(");
				return BioNetError;
			}


			if(params->bondKineticsActive) {
				
				if(boost::filesystem::exists(params->tpFname)) {
					if(fileOut->truncateTopology(params, this, params->tpFname) == BioNetError) {
						fileLog->printError("PhysicalSystem::preInitialise", "Unable to pre-initialise the system as we could not read and truncate the topology file! :(");
						return BioNetError;
					}
				} else {
				
					// Assume new file
					ofstream fout;
					fout.open(params->tpFname.string());
					fileOut->writeTopologyHeader(fout);
					fileOut->writeTopologyInitialisationHeader(params, this, fout);
					fout.close();					
				}
			}

			if(params->constantForcesActive) {
				if(fileOut->truncateForce(params, this, params->cfFname) == BioNetError) {
					fileLog->printError("PhysicalSystem::preInitialise", "Unable to pre-initialise the system as we could not read and truncate the forces file! :(");
					return BioNetError;
				}
			}
			
			if(box->exists) {
				box->repopulate();
			}
		}
	} else if (isNetworkModelSimulation(params->simType)) {

		// Open some files and write some headers
		ofstream fout;
		fout.open(params->nmFname.string());
		fileOut->writeNormalModeHeader(fout);
		fout.close();
	}

	// Do all individual things first
	if(net->preInitialise() == BioNetError) {
		fileLog->printError("PhysicalSystem::preInitialise", "Unable to pre-initialise as the network could not pre-initialise :(");
		return BioNetError;
	}
	
	if(box->exists) {
		if(box->preInitialise() == BioNetError) {
			fileLog->printError("PhysicalSystem::preInitialise", "Unable to pre-initialise as the simulation box could not pre-initialise :(");
			return BioNetError;
		}
	}

	// Finalise system
	setPreInitialised(true);
	fileLog->printMethodSuccess();
	return BioNetSuccess;
}

int PhysicalSystem::initialise() {

	// User Info
	fileLog->printMethodTask("Initialising Physical System...");
	if(!isPreInitialised()) {
		fileLog->printError("PhysicalSystem::initialise", "Unable to initialise as the system has not yet been pre-prepared! :(");
		return BioNetError;
	}

	// So we have a network. This network should be capable of what is asked of it (from net->validate()). 

	// Initialise the simulation
	if(net->initialise(params) == BioNetError) {	
		fileLog->printError("Unable to initialise network model / simulation :( ");
		return BioNetError;
	}

	// Validate the network again
	if(net->validate(params) == BioNetInvalid) {

		string errString = "Validation of network structure failed following initialisation procedure :( ";
		fileLog->printError(errString);
		return BioNetError;
	}

	// Finalise system
	setInitialised(true);

	fileLog->printMethodSuccess();
	return BioNetSuccess;
	
}

int PhysicalSystem::simulate() {

	// User Info
	fileLog->printMethodTask("Simulating Physical System...");

	// Needs to be a valid network, but not necessarily initialised check
	// Jump straight into the simulation

	// Initialise the simulation
	if(net->simulate(params) == BioNetError) {	
		fileLog->printError("Unable to simulated network model / simulation :( ");
		return BioNetError;
	}

	// Finalise system
	setSimulated(true);

	fileLog->printMethodSuccess();
	return BioNetSuccess;	
}

void PhysicalSystem::printDetails(bool verbose) {
	cout << endl;
	cout << "Class: PhysicalSystem" << endl;
	cout << "Components: " << endl;
	
	cout << "\tNetwork (" << getNetworkTypeString(netType) << ")" << endl;
//	cout << "\tSolvent (" << solType << ")" << endl;
	cout << "\tSimulation Box (" << getSimBoxTypeString(boxType) << ")" << endl;
	if(verbose) {
		cout << endl << "Network:" << endl << endl;
		net->printDetails(verbose);

//		cout << endl << "Solvent:" << endl << endl;
//		solvent->printDetails(verbose);

		cout << endl << "Simulation Box:" << endl << endl;
		box->printDetails();
	}
}
