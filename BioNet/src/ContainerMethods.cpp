#include "ContainerMethods.hpp"

bool compareDistances(PairPair a, PairPair b) {
	return a.metric < b.metric;
}

bool isSaturated(PairPair a) {
	return (a.obj[0]->isSaturated() || a.obj[1]->isSaturated());
}

bool isReactive(PairPair a) {
	return (a.obj[0]->isReactive() && a.obj[1]->isReactive());
}
