#include "SimulationBox.hpp"

const vector<string> SimulationBox::paramList {"BC", "dimensions", "numVoxels"};

string getBCTypeString(BCType type) {
	return bcTypeString[type];
}

string getEdgeTypeString(EdgeType type) {
	return edgeTypeString[type];
}

Voxel::Voxel() {

	int i;

	index = 0;
	indices.setZero();

	numObjects = 0;
	numNeighbours = 0;
	numInteractionNeighbours = 0;

	edge = false;
	edgeType(0) = NoEdge;
	edgeType(1) = NoEdge;
	edgeType(2) = NoEdge;

	coordinates.setZero();
	obj.clear();

	// Initialise these things to 27 (potential)
	neighbours.clear();
	interactionNeighbours.clear();

	neighbourTransLookup.clear();

	vdwEnergy = 0.0;
	stericEnergy = 0.0;
	wallEnergy = 0.0;
	
	electrostaticEnergy = 0.0;
	connectorEnergy = 0.0;
	totalEnergy = 0.0;

	checked.clear();
	resetThreadIndex();
}

Voxel::~Voxel() {

	index = 0;
	indices.setZero();

	numObjects = 0;
	numNeighbours = 0;
	numInteractionNeighbours = 0;

	edge = false;
	edgeType(0) = NoEdge;
	edgeType(1) = NoEdge;
	edgeType(2) = NoEdge;

	coordinates.setZero();
	obj.clear();
	neighbours.clear();
	interactionNeighbours.clear();

	neighbourTransLookup.clear();

	vdwEnergy = 0.0;
	stericEnergy = 0.0;
	wallEnergy = 0.0;
	
	electrostaticEnergy = 0.0;
	connectorEnergy = 0.0;
	totalEnergy = 0.0;

	checked.clear();
	threadIndex = 0;
}

void Voxel::setIndex(int i) {
	index = i;
}

void Voxel::setIndices(int i, int j, int k) {
	indices(0) = i;
	indices(1) = j;
	indices(2) = k;
}

int Voxel::getIndex() {
	return index;
}

Vector3i Voxel::getIndices() {
	return indices;
}

void Voxel::setThreadIndex(int i) {
	threadIndex = i;
}

void Voxel::resetThreadIndex() {
	threadIndex = -1;
}

int Voxel::getThreadIndex() {
	return threadIndex;
}

void Voxel::setChecked(Voxel *v, bool b) {
	checked[v] = b;
}

void Voxel::resetChecked() {

	map<Voxel*, bool>::iterator it;
	for(it = checked.begin(); it != checked.end(); ++it) {
		it->second = false;
	}
}

bool Voxel::isChecked(Voxel *v) {
	return checked[v];
}

void Voxel::setEdge(bool b) {
	edge = b;
}

bool Voxel::isEdge() {
	return edge;
}

void Voxel::setEdgeType(EdgeType type, AxisType XYZ) {

	if(XYZ==X) {
		edgeType(0) = type;
	} else if (XYZ==Y) {
		edgeType(1) = type;
	} else {
		edgeType(2) = type;
	}
}

Matrix<EdgeType, 3, 1> Voxel::getEdgeType() {
	return edgeType;
}

EdgeType Voxel::getEdgeType(AxisType XYZ) {

	if(XYZ==X) {
		return edgeType(0);
	} else if (XYZ==Y) {
		return edgeType(1);
	} else {
		return edgeType(2);
	}

}

string Voxel::getEdgeTypeString(AxisType XYZ) {
	return ::getEdgeTypeString(getEdgeType(XYZ));
}

int Voxel::getNumObjects() {
	return numObjects;
}

int Voxel::getNumNeighbours() {
	return numNeighbours;
}

int Voxel::getNumInteractionNeighbours() {
	return numInteractionNeighbours;
}

vObject * Voxel::getObject(int index) {
	if(index >= 0 && index < getNumObjects()) {
		set<vObject*>::iterator it = obj.begin();
		advance(it, index);
		return *it;
	}
	
	return NULL;
}

Voxel * Voxel::getNeighbour(int index) {
	if(index >= 0 && index < getNumNeighbours()) {
		set<Voxel*>::iterator it = neighbours.begin();
		advance(it, index);
		return *it;
	}
	
	return NULL;
}

Voxel * Voxel::getInteractionNeighbour(int index) {
	if(index >= 0 && index < getNumInteractionNeighbours()) {
		set<Voxel*>::iterator it = interactionNeighbours.begin();
		advance(it, index);
		return *it;
	}
	
	return NULL;
}

void Voxel::addObject(vObject *node) {
	obj.insert(node);
	numObjects = obj.size();
}

void Voxel::removeObject(vObject *node) {
	obj.erase(node);
	numObjects = obj.size();
}

bool Voxel::containsObject(vObject *node) {
	if(obj.find(node) != obj.end()) {
		return true;
	}
	
	return false;
}

void Voxel::addNeighbour(Voxel *v) {

	// Add the neighbour
	neighbours.insert(v);
	numNeighbours = neighbours.size();

	// Populate the 'checked' list
	checked.insert({v, false});

	// Add to the lookup
//	neighbourTransLookup.insert(pair<Voxel*,Vector3>(v,trans));
}

void Voxel::addInteractionNeighbour(Voxel *v) {

	// Add the neighbour
	interactionNeighbours.insert(v);
	numInteractionNeighbours = interactionNeighbours.size();

	// Populate the 'checked' list
//	checked.insert({v, false});
}

void Voxel::removeNeighbour(Voxel *v) {

	// Depopulate the 'checked' list
	checked.erase(v);

	// Remove the neighbour
	neighbours.erase(v);
	numNeighbours = neighbours.size();
}

Vector3 Voxel::getNeighbourTranslation(Voxel *v) {

	return neighbourTransLookup.find(v)->second;
}

void Voxel::setNeighbourTranslation(Voxel *v, Vector3 trans) {
	
//	neighbourTransLookup[v] = trans;
	neighbourTransLookup.insert(pair<Voxel *,Vector3>(v,trans));
}

void Voxel::removeNeighbourTranslation(Voxel *v) {
	
	neighbourTransLookup.erase(neighbourTransLookup.find(v), neighbourTransLookup.end());
}

void Voxel::addStericEnergy(scalar e) {
	stericEnergy += e;
	totalEnergy += e;
}

void Voxel::addVdWEnergy(scalar e) {
	vdwEnergy += e;
	totalEnergy += e;
}

void Voxel::addWallEnergy(scalar e) {
	wallEnergy += e;
	totalEnergy += e;
}

void Voxel::addConnectorEnergy(scalar e) {
	connectorEnergy += e;
	totalEnergy += e;
}

void Voxel::zeroStericEnergy() {
	totalEnergy -= stericEnergy;
	stericEnergy = 0;
}

void Voxel::zeroVdWEnergy() {
	totalEnergy -= vdwEnergy;
	vdwEnergy = 0;
}

void Voxel::zeroWallEnergy() {
	totalEnergy -= wallEnergy;
	wallEnergy = 0;
}

void Voxel::zeroConnectorEnergy() {
	totalEnergy -= connectorEnergy;
	connectorEnergy = 0;
}

scalar Voxel::getStericEnergy() {
	return stericEnergy;
}

scalar Voxel::getVdWEnergy() {
	return vdwEnergy;
}

scalar Voxel::getWallEnergy() {
	return wallEnergy;
}

scalar Voxel::getConnectorEnergy() {
	return connectorEnergy;
}

scalar Voxel::getTotalEnergy() {
	return totalEnergy;
}

void Voxel::clear() {
	obj.clear();
	numObjects = 0;
}

PairPairMethod::PairPairMethod() {
	energy = 0;
	totalEnergy = 0;
	cutoff = 0.3;
	k.setZero();
	tau.setZero();
	pairs.clear();
}

PairPairMethod::~PairPairMethod() {
	energy = 0;
	totalEnergy = 0;
	cutoff = 0.0;
	k.setZero();
	tau.setZero();
	pairs.clear();
}

int PairPairMethod::setParameter(string name, string value) {
	if(name == "energy") {
		setEnergy(stos(value));
	} else {
		return BioNetWarning;
	}
	return BioNetSuccess;
}

string PairPairMethod::getParameter(string name) {
	if (toLower(name) == "energy") {
		return to_string(getEnergy());
	} else {
		return "";
	}
}

void PairPairMethod::setCutoff(scalar c) {
	cutoff = c;
}

scalar PairPairMethod::getCutoff() {
	return cutoff;
}

scalar PairPairMethod::getEnergy() {
	return energy;
}

void PairPairMethod::setEnergy(scalar e) {
	energy = e;
}

scalar PairPairMethod::getTotalEnergy() {
	return totalEnergy;
}

void PairPairMethod::setTotalEnergy(scalar e) {
	totalEnergy = e;
}

Vector2 PairPairMethod::getLinearForceConstants() {
	return k;
}

Vector2 PairPairMethod::getTau() {
	return tau;
}

void PairPairMethod::setTau(Vector2 v) {
	tau = v;
}

vector<PairPair> PairPairMethod::getPairs() {
	return pairs;
}

void PairPairMethod::clearPairs() {
	pairs.clear();
}

void PairPairMethod::setAssemblyMethod(bool availOnly) {
	this->availOnly = availOnly;
}

bool PairPairMethod::getAssemblyMethod() {
	return availOnly;
}

void PairPairMethod::zeroEnergy() {
	energy = 0;
	totalEnergy = 0;
}

PairPairSteric::PairPairSteric() : PairPairMethod() {

	kst = 1560.5196694 * 1e-3 / StandardUnits::ElasticConstant;	// 10kbt for r = 1.5R, R=1nm (arbitrary, but user can set)
	eMax = (1 / 72.0) * kst * pow(4,2) * pow(2,4);		// r = 0
}

PairPairSteric::~PairPairSteric() {
	eMax = 0.0;
	kst = 0.0;
}

void PairPairSteric::operator() (vObject *a, vObject *b, Vector3 trans, Network *net) {
	
	// Conditional on maximum overlap separation
	if((a->getPosition() - (b->getPosition() + trans)).norm() < (a->getLengthscale() + b->getLengthscale())) {
		energy = a->calculateStericInteraction(kst, b, trans);

		totalEnergy += energy;
	}
}

int PairPairSteric::setParameter(string name, string value) {
	if (toLower(name) == "k" || toLower(name) == "kst") {
		setStericConstant(stos(value) / InternalUnits::ElasticConstant);
	} else {
		return PairPairMethod::setParameter(name, value);
	}

	return BioNetSuccess;
}

string PairPairSteric::getParameter(string name) {
	if (toLower(name) == "emax" || toLower(name) == "e") {
		return to_string(getEMax());
	} else if (toLower(name) == "k" || toLower(name) == "kst") {
		return to_string(getStericConstant());
	} else {
		return PairPairMethod::getParameter(name);
	}
}


void PairPairSteric::setEMax(scalar e) {
	eMax = e;
}

scalar PairPairSteric::getEMax() {
	return eMax;
}

void PairPairSteric::setStericConstant(scalar k) {
	kst = k;
}

scalar PairPairSteric::getStericConstant() {
	return kst;
}


void PairPairSteric::updateFieldEnergy(Voxel *a, Voxel *b) {
	
	// Half to each voxel (for now, maybe make specific later)
	a->addStericEnergy(0.5 * energy);
	b->addStericEnergy(0.5 * energy);
}

void PairPairSteric::initialise(Network *net) {

}

bool PairPairSteric::validate() {

	if(kst <= 0) {
		fileLog->printError("PairPairSteric::kst", "Must be > 0");
		return BioNetInvalid;
	}
	return BioNetValid;
}

void PairPairSteric::printDetails() {
	cout << endl;
	cout << "Class: PairPairSteric" << endl;
	cout << "Parameters: " << endl;
	cout << "\tkst: " << getStericConstant() << endl;
}

PairPairVdW::PairPairVdW() {

	// Less than a kT at room temperature, approx bond length separation
	rEq = 5 * 1e-10 / StandardUnits::Length;
	rCutoff = 3 * rEq;
	eps = 1 * 1e-21 / StandardUnits::Energy;
	alpha = 6.0;
}

PairPairVdW::~PairPairVdW() {
	rEq = 0.0;
	rCutoff = 0.0;
	eps = 0.0;
	alpha = 0.0;
}

void PairPairVdW::operator() (vObject *a, vObject *b, Vector3 trans, Network *net) {
	energy = a->calculateVdWInteraction(rEq, eps, alpha, rCutoff, b, trans);
	totalEnergy += energy;
}

int PairPairVdW::setParameter(string name, string value) {
	if (toLower(name) == "req" || toLower(name) == "r") {
		setREq(stos(value) / InternalUnits::Length);
	} else if (toLower(name) == "eps") {
		setEps(stos(value) / InternalUnits::Energy);
	} else if (toLower(name) == "rc" || toLower(name) == "rcutoff" || toLower(name) == "cutoff") {
		setVdWCutoff(stos(value) / InternalUnits::Energy);
	} else if (toLower(name) == "alpha" || toLower(name) == "a") {
		setAlpha(stos(value));
	} else {
		return PairPairMethod::setParameter(name, value);
	}

	return BioNetSuccess;
}

string PairPairVdW::getParameter(string name) {
	if (toLower(name) == "req") {
		return to_string(getREq());
	} else if(toLower(name) == "eps") {
		return to_string(getEps());
	} else if(toLower(name) == "cutoff") {
		return to_string(getVdWCutoff());
	} else if(toLower(name) == "alpha") {
		return to_string(getAlpha());
	} else {
		return PairPairMethod::getParameter(name);
	}
}

void PairPairVdW::setREq(scalar r) {
	rEq = r;
}

scalar PairPairVdW::getREq() {
	return rEq;
}

void PairPairVdW::setEps(scalar e) {
	eps = e;
}

scalar PairPairVdW::getEps() {
	return eps;
}

void PairPairVdW::setVdWCutoff(scalar r) {
	rCutoff = r;
}

scalar PairPairVdW::getVdWCutoff() {
	return rCutoff;
}

void PairPairVdW::setAlpha(scalar a) {
	alpha = a;
}

scalar PairPairVdW::getAlpha() {

	return alpha;
}

void PairPairVdW::updateFieldEnergy(Voxel *a, Voxel *b) {
	
	// Half to each voxel (for now, maybe make specific later)
	a->addVdWEnergy(0.5 * energy);
	b->addVdWEnergy(0.5 * energy);
}

void PairPairVdW::initialise(Network *net) {

	// Set the non-trivial values

	// Spring constants
	k(0) = 2 * getEps() / (getREq() * getREq());
	k(1) = (2 * getAlpha() + 1) * (getAlpha() + 1) * k(0) / 2.0;
}

bool PairPairVdW::validate() {

	if(rEq <= 0) {
		fileLog->printError("PairPairVdW::rEq", "Must be > 0");
		return BioNetInvalid;
	}

	if (alpha <= 0) {
		fileLog->printError("PairPairVdW::alpha", "Must be > 0");
		return BioNetInvalid;
	
	}
	
	if(eps < 0) {
		fileLog->printError("PairPairVdW::eps", "Must be >= 0");
		return BioNetInvalid;
	}


	return BioNetValid;
}

void PairPairVdW::printDetails() {
	cout << endl;
	cout << "Class: PairPairVdW" << endl;
	cout << "Parameters: " << endl;
	cout << "\trEq: " << getREq() << endl;
	cout << "\teps: " << getEps() << endl;
	cout << "\tcutoff: " << getVdWCutoff() << endl;
	cout << "\talpha: " << getAlpha() << endl;
}

PairPairConnect::PairPairConnect() {
	localConnectivitySet = false;
	availOnly = true;
}

PairPairConnect::PairPairConnect(bool lcs) {
	localConnectivitySet = lcs;
	setCutoff(0.3);
	availOnly = true;
}

PairPairConnect::PairPairConnect(bool lcs, scalar c) {
	localConnectivitySet = lcs;
	setCutoff(c);
	availOnly = true;
}

PairPairConnect::~PairPairConnect() {
	localConnectivitySet = false;
	availOnly = false;
}

void PairPairConnect::operator() (vObject *a, vObject *b, Vector3 trans, Network *net) {
	
	scalar sep, l;
	sep = (a->getPosition() - (b->getPosition() + trans)).norm();
	l = net->getKineticBond()->getEquilibriumLength();
	
	// Initial check
	if(sep - (a->getLengthscale() + b->getLengthscale()) >= getCutoff()) {
		return;
	}

	// This operator adds pairs objects to the list
	if(!localConnectivitySet) {

		// Parents
//		sep = (a->getPosition() - (b->getPosition() + trans)).norm(); // Sep was calculated above

		// Assembly method
//		if (getAssemblyMethod() && (a->isSaturated() || b->isSaturated())) {
//			return;
//		}
						
		if(sep < getCutoff()) {
//			pairs.push_back(PairPair(a,b, sep));
			pairs.push_back(PairPair(a,b, fabs(sep-l)));
		}
	} else {

		// Children
		int i, j;
		Vector3 o[2];

		// What if they're points?
		if(a->getProteinType() == PPoint && b->getProteinType() == PPoint) {

			// Same as before
//			sep = (a->getPosition() - (b->getPosition() + trans)).norm(); // Sep was calculated above

			// Assembly method
//			if (getAssemblyMethod() && (a->isSaturated() || b->isSaturated())) {
//				return;
//			}

			if(sep < getCutoff()) {
//				pairs.push_back(PairPair(a,b, sep));
				pairs.push_back(PairPair(a,b, fabs(sep-l)));
			}

		} else if(a->getProteinType() == PPoint && b->getProteinType() != PPoint) {

			// Loop over b's children only
			for(j = 0; j < b->getNumChildren(); ++j) {

				// Assembly method
//				if (getAssemblyMethod() && (a->isSaturated() || b->getChild(j)->isSaturated())) {
//					return;
//				}

				// Check separation
				sep = (a->getPosition() - (b->getChild(j)->getPosition() + trans)).norm();
				if(sep < getCutoff()) {
//					pairs.push_back(PairPair(a, b->getChild(j), sep));
					pairs.push_back(PairPair(a,b->getChild(j), fabs(sep-l)));
				}
			}

		} else if(a->getProteinType() != PPoint && b->getProteinType() == PPoint) {

			// Loop over a's children only
			for(i = 0; i < a->getNumChildren(); ++i) {

				// Assembly method
//				if (getAssemblyMethod() && (a->getChild(i)->isSaturated() || b->isSaturated())) {
//					return;
//				}
				
				// Check separation
				sep = (a->getChild(i)->getPosition() - (b->getPosition() + trans)).norm();
				if(sep < getCutoff()) {
//					pairs.push_back(PairPair(a->getChild(i), b, sep));
					pairs.push_back(PairPair(a->getChild(i), b, fabs(sep-l)));
				}
			}
		} else {

			// Loop over everything
			for(i = 0; i < a->getNumChildren(); ++i) {
				for(j = 0; j < b->getNumChildren(); ++j) {

					// Assembly method
//					if (getAssemblyMethod() && (a->getChild(i)->isSaturated() || b->getChild(j)->isSaturated())) {
//						return;
//					}
					
					// Check separation
					sep = (a->getChild(i)->getPosition() - (b->getChild(j)->getPosition() + trans)).norm();
					if(sep < getCutoff()) {

						// Extra check orientation (no trans needed here, it cancels)
						o[0] = a->getChild(i)->getPosition() - a->getPosition();
						o[1] = b->getChild(j)->getPosition() - b->getPosition();
						if(o[0].dot(o[1]) < 0) {
//							pairs.push_back(PairPair(a->getChild(i), b->getChild(j), sep));
							pairs.push_back(PairPair(a->getChild(i), b->getChild(j), fabs(sep- l)));
						}
					}
				}
			}
		}
	}
}

int PairPairConnect::setParameter(string name, string value) {
	if (toLower(name) == "cutoff") {
		setCutoff(stos(value) / InternalUnits::Length);
	} else {
		return PairPairMethod::setParameter(name, value);
	}

	return BioNetSuccess;
}

string PairPairConnect::getParameter(string name) {
	return PairPairMethod::getParameter(name);
}

void PairPairConnect::updateFieldEnergy(Voxel *a, Voxel *b) {
	// Do nothing
}


// Initialisation
void PairPairConnect::initialise(Network *net) {

	setCutoff(net->getKineticBond()->getCutoff());
}

// Validation methods
bool PairPairConnect::validate() {

	// Do nothing
	return true;
}

// User info
void PairPairConnect::printDetails() {
	cout << endl;
	cout << "Class: PairPairConnect" << endl;
	cout << "Parameters: " << endl;
	cout << "\tNum Pairs: " << pairs.size() << endl;
}

WallMethod::WallMethod() {
	energy = 0;
	totalEnergy = 0;
	cutoff = 0.3;
}

WallMethod::~WallMethod() {
	energy = 0;
	totalEnergy = 0;
	cutoff = 0.0;
}

int WallMethod::setParameter(string name, string value) {
//	if(name == "energy") {
//		setEnergy(stos(value));
//	} else {
//		return BioNetWarning;
//	}
	return BioNetSuccess;
}

string WallMethod::getParameter(string name) {
	if (toLower(name) == "energy") {
		return to_string(getEnergy());
	} else {
		return "";
	}
}

void WallMethod::zeroEnergy() {
	energy = 0;
	totalEnergy = 0;
}

scalar WallMethod::getEnergy() {
	return energy;
}

scalar WallMethod::getTotalEnergy() {
	return totalEnergy;
}

WallVdW::WallVdW() {

	// Less than a kT at room temperature, approx bond length separation
	rEq = 5 * 1e-10 / StandardUnits::Length;
	rCutoff = 3 * rEq;
	eps = 1 * 1e-21 / StandardUnits::Energy;
	alpha = 6.0;
}

WallVdW::~WallVdW() {
	rEq = 0.0;
	rCutoff = 0.0;
	eps = 0.0;
	alpha = 0.0;
}

int WallVdW::setParameter(string name, string value) {
	if (toLower(name) == "req" || toLower(name) == "r") {
		setREq(stos(value) / InternalUnits::Length);
	} else if (toLower(name) == "eps") {
		setEps(stos(value) / InternalUnits::Energy);
	} else if (toLower(name) == "rc" || toLower(name) == "rcutoff" || toLower(name) == "cutoff") {
		setVdWCutoff(stos(value) / InternalUnits::Energy);
	} else if (toLower(name) == "alpha" || toLower(name) == "a") {
		setAlpha(stos(value));
	} else {
		return WallMethod::setParameter(name, value);
	}

	return BioNetSuccess;
}

string WallVdW::getParameter(string name) {
	if (toLower(name) == "req") {
		return to_string(getREq());
	} else if(toLower(name) == "eps") {
		return to_string(getEps());
	} else if(toLower(name) == "cutoff") {
		return to_string(getVdWCutoff());
	} else if(toLower(name) == "alpha") {
		return to_string(getAlpha());
	} else {
		return WallMethod::getParameter(name);
	}
}

void WallVdW::setREq(scalar r) {
	rEq = r;
}

scalar WallVdW::getREq() {
	return rEq;
}

void WallVdW::setEps(scalar e) {
	eps = e;
}

scalar WallVdW::getEps() {
	return eps;
}

void WallVdW::setVdWCutoff(scalar r) {
	rCutoff = r;
}

scalar WallVdW::getVdWCutoff() {
	return rCutoff;
}

void WallVdW::setAlpha(scalar a) {
	alpha = a;
}

scalar WallVdW::getAlpha() {

	return alpha;
}

void WallVdW::updateFieldEnergy(Voxel *a) {
	
	// Half to each voxel (for now, maybe make specific later)
	a->addVdWEnergy(energy);
}

void WallVdW::initialise(Network *net) {

	// Set the non-trivial values

}

bool WallVdW::validate() {

	if(rEq <= 0) {
		fileLog->printError("WallVdW::rEq", "Must be > 0");
		return BioNetInvalid;
	}

	if(eps < 0) {
		fileLog->printError("WallVdW::eps", "Must be >= 0");
		return BioNetInvalid;
	}

	if(alpha < 0) {
		fileLog->printError("WallVdW::alpha", "Must be >= 0");
		return BioNetInvalid;
	}
	
	return BioNetValid;
}

void WallVdW::printDetails() {
	cout << endl;
	cout << "Class: WallVdW" << endl;
	cout << "Parameters: " << endl;
	cout << "\trEq: " << getREq() << endl;
	cout << "\teps: " << getEps() << endl;
	cout << "\tcutoff: " << getVdWCutoff() << endl;
}

void WallVdW::operator() (vObject *a, EdgeType et, scalar zBoxDim) {

	// This can be done here. Doesn't need loads of virtual pointers
	// Force is directly between the two nearest surface points
	Vector3 rHat;
	scalar r, req, eps;
	scalar fMag, energy;

	// Set params for quick access
	req = getREq();
	eps = getEps();
	
	// We need the scalar distance between the node surface and the wall and a direction to apply the force
	rHat(0) = 0.0;
	rHat(2) = 0.0;
	if (et == TEdge) {
		r = zBoxDim - a->getPosition()[1];
		rHat(1) = 1.0;
	} else {
		r = a->getPosition()[1];
		rHat(1) = -1.0;
	}
	
	r -= a->getLengthscale();

	// Cutoff check	
	if (r > getVdWCutoff()) {
		energy = 0.0;
		return;
	}
	

	if(r <= req) {

		//
		// Interpolation (spring potential through origin sets all soft parameters)
		//

		scalar k;

		// Spring constant
		k = 2 * eps / (req * req);

		// Force
		fMag = k * (req - r);

		energy = 0.5 * k * pow(req - r, 2) - eps;

	} else {

		//
		// Lennard-Jones
		//
		scalar reqonralpha, reqonr2alpha;

		// Force
		reqonralpha = pow(req / r, getAlpha());
		reqonr2alpha = reqonralpha * reqonralpha;

		fMag = (2 * alpha) * eps * (reqonr2alpha - reqonralpha) / r;

		// Energy
		energy = eps * (reqonr2alpha - 2 * reqonralpha);
	}

	// Add (directional) forces to nodes
	rHat *= -fMag;
	a->addExternalForce(rHat);

	// Energy (return value)
	totalEnergy += energy;
}

SimulationBox::SimulationBox() {
	
	net = NULL;

	voxel = NULL;
	numVoxels.setZero();
	totalNumVoxels = 0;
	
	edgeVoxel.clear();
	numEdgeVoxels = 0;

	interiorVoxel.clear();
	numInteriorVoxels = 0;

	yMinusVoxel.clear();
	numYMinusVoxels = 0;
	
	yPlusVoxel.clear();
	numYPlusVoxels = 0;
	
	dimension.setZero();
	voxelDimension.setZero();

	diagonal = 0.0;
	volume = 0.0;
	centroid.setZero();

	pairPairList.clear();

	bcType = PBC;

	exists = false;
	defaulted = false;

	// Get a Mersenne Twister RNG (seed using time)
	RNGGen.seed(chrono::system_clock::now().time_since_epoch().count());
	uniform_real_distribution<scalar> uniformDistro(0.0, 1.0);

	stericForcesActive = false;
	vdwForcesActive = false;
	electrostaticForcesActive = false;
	wallForcesActive = false;
	bothWallForcesActive = false;
		
	connectingActive = false;
	shearForcesActive = false;
	shearStress = 0.0;
	shearForces.clear();

	stericCalculator = NULL;
	vdwCalculator = NULL;
	wallCalculator = NULL;
	
	connector = NULL;

	vdwEnergy = 0.0;
	stericEnergy = 0.0;
	wallEnergy = 0.0;
	electrostaticEnergy = 0.0;
	connectorEnergy = 0.0;

	totalEnergy = 0.0;

	numThreads = 1;
}

SimulationBox::~SimulationBox() {
	
	net = NULL;

	if(voxel != NULL) {
		delete[] voxel;
		voxel = NULL;
	}
	totalNumVoxels = 0;
	numVoxels.setZero();

	edgeVoxel.clear();
	numEdgeVoxels = 0;

	interiorVoxel.clear();
	numInteriorVoxels = 0;

	yMinusVoxel.clear();
	numYMinusVoxels = 0;
	
	yPlusVoxel.clear();
	numYPlusVoxels = 0;
	
	dimension.setZero();
	voxelDimension.setZero();

	diagonal = 0.0;
	volume = 0.0;
	centroid.setZero();

	pairPairList.clear();

	bcType = NoBC;

	exists = false;
	defaulted = false;

	stericForcesActive = false;
	vdwForcesActive = false;
	electrostaticForcesActive = false;
	wallForcesActive = false;
	bothWallForcesActive = false;
	
	connectingActive = false;
	shearForcesActive = false;
	shearStress = 0.0;
	shearForces.clear();

	deleteCalculators();

	vdwEnergy = 0.0;
	stericEnergy = 0.0;
	electrostaticEnergy = 0.0;
	wallEnergy = 0.0;
	
	connectorEnergy = 0.0;

	totalEnergy = 0.0;

	numThreads = 0;
}

void SimulationBox::deleteCalculators() {

	if(stericCalculator != NULL) {
		for(int i = 0; i < numThreads; ++i) {
			delete stericCalculator[i];
			stericCalculator[i] = NULL;
		}
		delete[] stericCalculator;
		stericCalculator = NULL;
	}

	if(vdwCalculator != NULL) {
		for(int i = 0; i < numThreads; ++i) {
			delete vdwCalculator[i];
			vdwCalculator[i] = NULL;
		}
		delete[] vdwCalculator;
		vdwCalculator = NULL;
	}

	if(wallCalculator != NULL) {
		for(int i = 0; i < numThreads; ++i) {
			delete wallCalculator[i];
			wallCalculator[i] = NULL;
		}
		delete[] wallCalculator;
		wallCalculator = NULL;
	}

	if(connector != NULL) {
		for(int i = 0; i < numThreads; ++i) {
			delete connector[i];
			connector[i] = NULL;
		}
		delete[] connector;
		connector = NULL;
	}
}

int SimulationBox::setParameter(string name, string value) {

	boost::algorithm::to_lower(name);
	try {
		if(name == "totalNumVoxels") {
			totalNumVoxels = stoi(value);
		} else if(name == "voxels" || name == "numvoxels") {
			setNumVoxels(stringToVector3i(value));
		} else if(name == "dimensions" || name == "dimension") {
			setDimensions(stringToVector3(value) / InternalUnits::Length);
		} else if(name == "voxeldimensions") {
			setVoxelDimensions(stringToVector3(value) / InternalUnits::Length);
		} else if(name == "voxeldimension" || name == "cutoff") {
			setVoxelDimensions(stos(value) / InternalUnits::Length);
		} else if(name == "diagonal") {
			setDiagonal(stos(value) / InternalUnits::Length);
		} else if(name == "volume") {
			setVolume(stos(value) / InternalUnits::Volume);
		} else if(name == "centroid") {
			setCentroid(stringToVector3(value) / InternalUnits::Length);
		} else if(name == "stress" || name == "shearstress") {
			setShearStress(stos(value) / InternalUnits::Pressure);
		} else if(name == "bc" || name == "boundary") {
			return setBCType(value);
		} else {
			return BioNetWarning;
		}
	} catch (exception &e) {
		fileLog->printError(name, "Could not assign value '" + value + "' to parameter");
		return BioNetError;
	}
	return BioNetSuccess;
}

string SimulationBox::getParameter(string name) {

	ostringstream sStream;
	if(name == "totalNumVoxels") {
		return to_string(getTotalNumVoxels());
	} else if(name == "voxels" || name == "numVoxels") {
		return Vector3iToString(getNumVoxels());
	} else if(name == "dimensions" || name == "dimension") {
		return Vector3ToString(getDimensions());
	} else if(name == "voxelDimensions") {
		return Vector3ToString(getVoxelDimensions());
	} else if(name == "diagonal") {
		sStream << getDiagonal();
		return sStream.str();
	} else if(name == "volume") {
		sStream << getVolume();
		return sStream.str();
	} else if(name == "centroid") {
		return Vector3ToString(getCentroid());
	} else if(name == "type" || name == "BC" || name == "BCType") {
		return getBCTypeString();
	} else {
		return "";
	}
}

void SimulationBox::setTotalNumVoxels(int n) {
	totalNumVoxels = n;
}

int SimulationBox::getTotalNumVoxels() {
	return totalNumVoxels;
}

void SimulationBox::setNumVoxels(Vector3i n) {
	numVoxels = n;
}

Vector3i SimulationBox::getNumVoxels() {
	return numVoxels;
}

void SimulationBox::setDimensions(Vector3 d) {
	dimension = d;
}

Vector3 SimulationBox::getDimensions() {
	return dimension;
}

void SimulationBox::setVoxelDimensions(scalar d) {
	voxelDimension(0) = d;
	voxelDimension(1) = d;
	voxelDimension(2) = d;
}

void SimulationBox::setVoxelDimensions(Vector3 d) {
	voxelDimension = d;
}

Vector3 SimulationBox::getVoxelDimensions() {
	return voxelDimension;
}

Voxel * SimulationBox::getVoxel(int index) {
	if(index >= 0 && index < totalNumVoxels) {
		return &voxel[index];
	}
	
	return NULL;
}

Voxel * SimulationBox::getVoxel(int i, int j, int k) {
	return getVoxel(voxelIndexConversion(i, j, k)); 
}

Voxel * SimulationBox::getVoxel(Vector3i indices) {
	return getVoxel(voxelIndexConversion(indices)); 
}

void SimulationBox::setShearStress(scalar s) {
	shearStress = s;
}

scalar SimulationBox::getShearStress() {
	return shearStress;
}

void SimulationBox::setDiagonal(scalar d) {
	diagonal = d;
}

scalar SimulationBox::getDiagonal() {
	return diagonal;
}

void SimulationBox::setVolume(scalar v) {
	volume = v;
}

scalar SimulationBox::getVolume() {
	return volume;
}

void SimulationBox::setCentroid(Vector3 c) {
	centroid = c;
}
Vector3 SimulationBox::getCentroid() {
	return centroid;
}

int SimulationBox::setBCType(string s) {
	boost::algorithm::to_lower(s);
	if(s == "nobc" || s == "none") {
		setBCType(NoBC);
	} else if(s == "pbc" || s == "periodic") {
		setBCType(PBC);
	} else if (s == "hbc" || s == "hard") {
		setBCType(HBC);
	} else if (s == "cbc" || s == "cushion" || s == "cusion") {
		setBCType(CBC);
        } else if (s == "rbc" || s == "radial") {
                setBCType(RBC);
        } else if (s == "sbc" || s == "shear") {
		setBCType(PBC);
		shearForcesActive = true;
	} else {
		return BioNetError;
	}

	return BioNetSuccess;
}

void SimulationBox::setBCType(BCType type) {
	bcType = type;
}

BCType SimulationBox::getBCType() {
	return bcType;
}

string SimulationBox::getBCTypeString() {
	if (shearForcesActive) {
		return ::getBCTypeString(SBC);
	}
	return ::getBCTypeString(getBCType());
}

void SimulationBox::setStericEnergy(scalar e) {
	totalEnergy += e - stericEnergy;
	stericEnergy = e;
}

void SimulationBox::addStericEnergy(scalar e) {
	stericEnergy += e;
	totalEnergy += e;
}

scalar SimulationBox::getStericEnergy() {
	return stericEnergy;
}

void SimulationBox::setVdWEnergy(scalar e) {
	totalEnergy += e - vdwEnergy;
	vdwEnergy = e;
}

void SimulationBox::addVdWEnergy(scalar e) {
	vdwEnergy += e;
	totalEnergy += e;
}

scalar SimulationBox::getVdWEnergy() {
	return vdwEnergy;
}


void SimulationBox::setWallEnergy(scalar e) {
	totalEnergy += e - wallEnergy;
	wallEnergy = e;
}

void SimulationBox::addWallEnergy(scalar e) {
	wallEnergy += e;
	totalEnergy += e;
}

scalar SimulationBox::getWallEnergy() {
	return wallEnergy;
}

scalar SimulationBox::getTotalEnergy() {
	return totalEnergy;
}


Vector2 SimulationBox::getStericTimeConstants() {
	return stericCalculator[0]->getTau();
}

Vector2 SimulationBox::getVdWTimeConstants() {
	return vdwCalculator[0]->getTau();
}

void SimulationBox::setNumThreads(int n) {
	numThreads = n;
}

int SimulationBox::getNumThreads() {
	return numThreads;
}

void SimulationBox::resetChecked() {

	for(int i = 0; i < totalNumVoxels; ++i) {
		voxel[i].resetChecked();
	}
}

void SimulationBox::zeroTotalEnergy() {
	if(stericForcesActive) {
		zeroStericEnergy();
	}

	if(vdwForcesActive) {
		zeroVdWEnergy();
	}

	if(wallForcesActive) {
		zeroWallEnergy();
	}
}

void SimulationBox::zeroStericEnergy() {

	int i;
	
	// Take off the total
	totalEnergy -= stericEnergy;

	// Zero in all voxels
	for(i = 0; i < totalNumVoxels; ++i) {
		getVoxel(i)->zeroStericEnergy();
	}
	
	// And the totals
	stericEnergy = 0.0;
	for(i = 0; i < numThreads; ++i) {
		stericCalculator[i]->zeroEnergy();
	}
}

void SimulationBox::zeroVdWEnergy() {

	int i;

	// Take off the total
	totalEnergy -= vdwEnergy;

	// Zero in all voxels
	for(i = 0; i < totalNumVoxels; ++i) {
		getVoxel(i)->zeroVdWEnergy();
	}

	// And the total
	vdwEnergy = 0.0;
	for(i = 0; i < numThreads; ++i) {
		vdwCalculator[i]->zeroEnergy();
	}
}

void SimulationBox::zeroWallEnergy() {

	int i;

	// Take off the total
	totalEnergy -= wallEnergy;

	// Zero in all voxels
	for(i = 0; i < totalNumVoxels; ++i) {
		getVoxel(i)->zeroWallEnergy();
	}

	// And the total
	wallEnergy = 0.0;
	for(i = 0; i < numThreads; ++i) {
		wallCalculator[i]->zeroEnergy();
	}
}

void SimulationBox::zeroConnectorEnergy() {

	int i;

	// Take off the total
	totalEnergy -= connectorEnergy;

	// Zero in all voxels
	for(i = 0; i < totalNumVoxels; ++i) {
		getVoxel(i)->zeroConnectorEnergy();
	}

	// And the total
	connectorEnergy = 0.0;
	for(i = 0; i < numThreads; ++i) {
		connector[i]->zeroEnergy();
	}
}

void SimulationBox::clearConnectors() {

	int i;

	// Remove pairs
	for(i = 0; i < numThreads; ++i) {
		connector[i]->clearPairs();
	}
}

// Calculation methods
int SimulationBox::calculateNumVoxels() {
	try{
		for(int i = 0; i < 3; ++i) {

			// numVoxels must be integer, so
			if(fmod(dimension(i), voxelDimension(i)) != 0) {
				dimension(i) = ceil(dimension(i) / voxelDimension(i)) * voxelDimension(i);
			}
			numVoxels(i) = dimension(i) / voxelDimension(i);
			if(numVoxels(i) < 0) {
				fileLog->printError("SimulationBox::numVoxels", "numVoxels calculated to be negative :(");
				return BioNetError;
			} else if (numVoxels(i) == 0) {
				fileLog->printError("SimulationBox::numVoxels", "numVoxels calculated to be 0. Please edit box size");
				return BioNetError;
			}
		}
	} catch (exception& e) {
		fileLog->printError("SimulationBox::numVoxels", "Could not calculate numVoxels.");
		return BioNetError;
	}

	totalNumVoxels = numVoxels(0) * numVoxels(1) * numVoxels(2);
	
	return BioNetSuccess;
}

int SimulationBox::calculateVoxelDimensions() {
	for(int i = 0; i < 3; ++i) {
		voxelDimension(i) = dimension(i) / numVoxels(i);
		if(voxelDimension(i) <= 0) {
			fileLog->printError("SimulationBox::voxelDimension", "voxelDimension calculated to be negative :(");
			return BioNetError;
		}
	}

	return BioNetSuccess;
}

void SimulationBox::calculateDiagonal() {
	diagonal = dimension.norm();
}

void SimulationBox::calculateCentroid() {
	centroid = dimension * 0.5;
}

void SimulationBox::calculateVolume() {
	volume = dimension(0) * dimension(1) * dimension(2);
}

void SimulationBox::calculateStericEnergy() {
	doStericInteractions();
}

void SimulationBox::calculateVdWEnergy() {
	doVdWInteractions();
}

void SimulationBox::calculateWallEnergy() {
	doWallInteractions();
}

/*
void SimulationBox::calculateTotalEnergySum() {
	totalEnergy = 0.0;
	if(vdwForcesActive) {
		totalEnergy += getVdWEnergy();
	}

	if(stericForcesActive) {
		totalEnergy += getStericEnergy();
	}
}
*/

void SimulationBox::calculateTotalEnergy() {

	if(vdwForcesActive) {
		calculateVdWEnergy();
	}

	if(stericForcesActive) {
		calculateStericEnergy();
	}

	if(wallForcesActive) {
		calculateWallEnergy();
	}
}

void SimulationBox::pairPairInteractions(PairPairMethod **ppM) {

	//
	// Loop over all voxels and their neighbours, and do the function
	//

	int i, j;
	Voxel *cV, *nV;

	// Checking now unnecessary thanks you unique neighbour lists
//	resetChecked();

	// Zero the energy of the calculator
	for(i = 0; i < numThreads; ++i) {
		ppM[i]->zeroEnergy();
	}

	//
	// Begin loop
	//

	// Loop over all voxels
	#ifdef _OPENMP
		#pragma omp parallel for private(j, cV,nV)
	#endif
	for(i = 0; i < getTotalNumVoxels(); ++i) {

		// Get the voxel
		cV = getVoxel(i);

		// Interact with all in same voxel
		try {
			#ifdef _OPENMP
				pairPairInteractions(ppM[omp_get_thread_num()], cV);
			#else
				pairPairInteractions(ppM[0], cV);
			#endif
		} catch (exception &e) {
			throw;	
		}

		//
		// Loop over neighbouring voxels
		//
		for(j = 0; j < cV->getNumInteractionNeighbours(); ++j) {

			// Get a neighbour
			nV = cV->getInteractionNeighbour(j);

			// Interact with all neighbours
			try {
				#ifdef _OPENMP
					pairPairInteractions(ppM[omp_get_thread_num()], cV, nV);
				#else
					pairPairInteractions(ppM[0], cV, nV);
				#endif
			} catch (exception &e) {
				throw;	
			}
		}
	}
}

void SimulationBox::pairPairInteractions(PairPairMethod *ppM, Voxel *cV) {

	//
	// All interactions within the same voxel
	//

	int i, j;
	vObject *vO[2];
	Vector3 trans;
	trans.setZero();
	for(i = 0; i < cV->getNumObjects(); ++i) {
		vO[0] = cV->getObject(i);
		for(j = i + 1; j < cV->getNumObjects(); ++j) {
			vO[1] = cV->getObject(j);

			try {

				// Do the function
				(*ppM)(vO[0], vO[1], trans, net);

			} catch (exception &e) {
				throw;
			}
		}
	}
}

void SimulationBox::pairPairInteractions(PairPairMethod *ppM, Voxel *cV, Voxel *nV) {

	//
	// All interactions within different voxels
	//

	int i, j;
	vObject *vO[2];
	Vector3i adj;
	Vector3 trans;

	// First, sort the edges stuff (and consider parallel i.e. don't move the actual objects!)
	trans.setZero();

	// Boundary condition specific stuff
	if(bcType == PBC) {

		// Move second particle relative (i.e. trans should be applied to nV particles)
		trans = nV->getNeighbourTranslation(cV);
	}

	for(i = 0; i < cV->getNumObjects(); ++i) {
		vO[0] = cV->getObject(i);
		for(j = 0; j < nV->getNumObjects(); ++j) {
			
			vO[1] = nV->getObject(j);
			try {
				// Do the function
				(*ppM)(vO[0], vO[1], trans, net);

			} catch (exception &e) {
				throw;
			}
		}
	}
}

void SimulationBox::pairPairInteractions(PairPairMethod *ppM, vObject *v) {

	// Get the containing voxel, and do interactions with every object in it except itself
	
	// Variables
	int i, j;
	Voxel *cV, *nV;
	Vector3i adj;
	Vector3 trans;
	trans.setZero();

	// Get voxel
	cV = locateContainingVoxel(v);

	// Loop contents
	for(i = 0; i < cV->getNumObjects(); ++i) {
		try {

			// Do the function
			if(v != cV->getObject(i)) {
				(*ppM)(v, cV->getObject(i), trans, net);
			}
		} catch (exception &e) {
			throw;
		}
	}

	// Now, loop neighbours
	for(i = 0; i < cV->getNumNeighbours(); ++i) {
		nV = cV->getNeighbour(i);

		// First, sort the edges stuff (and consider parallel i.e. don't move the actual objects!)
		trans.setZero();
		if(cV->isEdge() and nV->isEdge()) {
			if(bcType == PBC) {

				// The translation will always be applied to whatever is in the nV voxel
//				adj = cV->getEdgeIndices() - nV->getEdgeIndices();
//				for(j = 0; j < 3; ++j) {
//					trans(j) = getDimensions()(j) * adj(j);
//				}
			}

			// Lees-Edward will introduce the strain here?
		}

		// Do interactions
		pairPairInteractions(ppM, v, nV, trans);
	}
}

void SimulationBox::pairPairInteractions(PairPairMethod *ppM, vObject *v, Voxel *nV, Vector3 trans) {

	// Do interactions with every object in other voxel

	int i;

	// Loop contents
	for(i = 0; i < nV->getNumObjects(); ++i) {
		try {

			// Do the function
			(*ppM)(v, nV->getObject(i), trans, net);
		} catch (exception &e) {
			throw;
		}
	}
}

void SimulationBox::wallInteractions(WallMethod **wM) {

	//
	// Loop over all voxels and their neighbours, and do the function
	//
	int i;
	Voxel *cV;
	set<Voxel*>::iterator it;

	// Checking now unnecessary thanks you unique neighbour lists
//	resetChecked();

	// Zero the energy of the calculator
	for(i = 0; i < numThreads; ++i) {
		wM[i]->zeroEnergy();
	}

	//
	// Begin loop
	//

	// Loop over bottom voxels
//	#ifdef _OPENMP
//		#pragma omp parallel for private(it, cV)
//	#endif
	for(it = yMinusVoxel.begin(); it != yMinusVoxel.end(); ++it) {
		
		// Get the voxel
		cV = *it;

		// Interact with all in same voxel
		try {
			#ifdef _OPENMP
				wallInteractions(wM[omp_get_thread_num()], cV);
			#else
				wallInteractions(wM[0], cV);
			#endif
		} catch (exception &e) {
			throw;	
		}
	}
	
	// Loop over top voxels only if both walls active
	if (bothWallForcesActive) {
//		#ifdef _OPENMP
//			#pragma omp parallel for private(it, cV)
//		#endif
		for(it = yPlusVoxel.begin(); it != yPlusVoxel.end(); ++it) {

			// Get the voxel
			cV = *it;

			// Interact with all in same voxel
			try {
				#ifdef _OPENMP
					wallInteractions(wM[omp_get_thread_num()], cV);
				#else
					wallInteractions(wM[0], cV);
				#endif
			} catch (exception &e) {
				throw;	
			}
		}
	}
}

void SimulationBox::wallInteractions(WallMethod *wM, Voxel *cV) {

	//
	// All interactions from this voxel to the wall
	//

	int i, j;
	vObject *vO;
	Vector3 trans;
	trans.setZero();
	for(i = 0; i < cV->getNumObjects(); ++i) {

		vO = cV->getObject(i);

		try {

			// Do the function
			(*wM)(vO, cV->getEdgeType(Y), getDimensions()[1]);

		} catch (exception &e) {
			throw;
		}
	}
}

void SimulationBox::doStericInteractions() {

	// Get current steric & total energies
	scalar stericE = getStericEnergy();
	scalar totalE = getTotalEnergy();
	scalar newEnergy = 0.0;

	// Reset this energy type
	zeroStericEnergy();

	// Voxel loop (which does total energy)
	pairPairInteractions(stericCalculator);

	// Set new energy
//	setStericEnergy(stericCalculator[0]->getTotalEnergy());

	// Set new energy
	for(int i = 0; i < numThreads; ++i) {
		newEnergy += stericCalculator[i]->getTotalEnergy();
	}
	setStericEnergy(newEnergy);
		
//	stericEnergy = stericE + getTotalEnergy() - totalE;
}

void SimulationBox::doVdWInteractions() {

	// Get current vdw & total energies
	scalar vdwE = getVdWEnergy();
	scalar totalE = getTotalEnergy();
	scalar newEnergy = 0.0;

	// Reset this energy type
	zeroVdWEnergy();

	// Voxel loop (which does total energy)
	pairPairInteractions(vdwCalculator);

	// Set new energy
	for(int i = 0; i < numThreads; ++i) {
		newEnergy += vdwCalculator[i]->getTotalEnergy();
	}
	setVdWEnergy(newEnergy);

	// Get new energy
//	vdwEnergy = vdwE + getTotalEnergy() - totalE;
}

void SimulationBox::doWallInteractions() {

	// Get current wall & total energies
	scalar wallE = getWallEnergy();
	scalar totalE = getTotalEnergy();
	scalar newEnergy = 0.0;

	// Reset this energy type
	zeroWallEnergy();

	// Voxel loop (which does total energy)
	wallInteractions(wallCalculator);

	// Set new energy
	for(int i = 0; i < numThreads; ++i) {
		newEnergy += wallCalculator[i]->getTotalEnergy();
	}
	setWallEnergy(newEnergy);
}

void SimulationBox::applyShearForces() {

	int i, j, k;
	int numParticles;
	scalar fMag = 0.0;
	Voxel *cV;

	// Get number of particles (in top layer) first
	numParticles = 0;
	for(i = 0; i < getNumVoxels()[0]; ++i) {
		for(j = 0; j < getNumVoxels()[2]; ++j) {

			numParticles += getVoxel(i, getNumVoxels()[1] - 1, j)->getNumObjects();
		}
	}

	// Now work out the required force for each particle to apply a constant stress
	fMag = getShearStress() * getDimensions()[0] * getDimensions()[2] / numParticles;

	// Apply the force (positive x)
	for(i = 0; i < getNumVoxels()[0]; ++i) {
		for(j = 0; j < getNumVoxels()[2]; ++j) {

			// Get voxel
			cV = getVoxel(i, getNumVoxels()[1] - 1, j);

			// Loop nodes in voxel and freeze
			for(k = 0; k < cV->getNumObjects(); ++k) {
				cV->getObject(k)->addExternalForce(fMag, 0);
			}
		}
	}
		
}

void SimulationBox::doAllInteractions() {
	
	// Go to the appropriate methods (they are additive)
	if(stericForcesActive) {
		doStericInteractions();
	}

	if(vdwForcesActive) {
		doVdWInteractions();
	}

	if(wallForcesActive) {
		doWallInteractions();
	}
	
	if(shearForcesActive) {
		applyShearForces();
	}
}

void SimulationBox::calculatePairPairList(bool availOnly) {

	int i, j, index, totalSize;
	vector<PairPair>::iterator it;
	scalar startTime;

	// Reset the connectors and the list
	clearConnectors();
	pairPairList.clear();

	for(i = 0; i < numThreads; ++i) {
		connector[i]->setAssemblyMethod(availOnly);
	}
	
	// Loop over the box to get them all
	pairPairInteractions(connector);

	// Assemble big list
	totalSize = 0;
	for(i = 0; i < numThreads; ++i) {
		totalSize += connector[i]->getPairs().size();
	}
	pairPairList.reserve(totalSize);

	for(i = 0; i < numThreads; ++i) {
		for(j = 0; j < connector[i]->getPairs().size(); ++j) {
			pairPairList.push_back(connector[i]->getPairs().at(j));
//			pairPairList.at(index) = connector[i]->getPairs().at(j);
		}
	}
}

vector<PairPair> SimulationBox::getPairPairList() {
	return pairPairList;
}

int SimulationBox::voxelIndexConversion(int i, int j, int k) {

	// index = i * numLayers (size numVoxels(1)*numVoxels(2)) + j * numLines (size numVoxels(1)) + k
	// index = (i * numVoxels(1) + j) * numVoxels(2) + k
	return (i * numVoxels(1) + j) * numVoxels(2) + k;
}

int SimulationBox::voxelIndexConversion(Vector3i indices) {
	return voxelIndexConversion(indices(0), indices(1), indices(2));
}

Vector3i SimulationBox::voxelIndexConversion(int index) {

	// index = (i * numVoxels(1) + j) * numVoxels(2) + k
	
	Vector3i indices;

	indices(2) = index % numVoxels(2);
	indices(1) = ((index - indices(2)) / numVoxels(2)) % numVoxels(1);
	indices(0) = (((index - indices(2)) / numVoxels(2)) - indices(1)) / numVoxels(1);
	return indices;
}

Vector3i SimulationBox::getPotentialVoxelCoordinates(vObject *v) {
	
	int i;
	Vector3i vIndices;
	
	// Use node position to get the potential voxel
	for(i = 0; i < 3; ++i) {
		vIndices(i) = floor(v->getPosition()(i) / voxelDimension(i));

		// Check for limiting case
		if(vIndices(i) == numVoxels(i)) {
			vIndices(i) -= 1;
		}
	}

	return vIndices;
}

Voxel * SimulationBox::locateContainingVoxel(vObject *v) {

	int i;
	Vector3i vIndices;
	Voxel *cV;
	
	// Get the potential voxel and search around it
	cV = locatePotentialVoxel(v);

	// Problem
	if(cV == NULL) {
		return cV;
	}

	// Check for containment
	if(cV->containsObject(v)) {
		return cV;
	}

	// Check neighbours
	for(i = 0; i < cV->getNumNeighbours(); ++i) {
		if(cV->getNeighbour(i)->containsObject(v)) {
			return cV->getNeighbour(i);
		}
	}

	// Check everywhere
	for(i = 0; i < getTotalNumVoxels(); ++i) {
		if(getVoxel(i)->containsObject(v)) {
			return getVoxel(i);
		}
	}

	// If it's here, node isn't in the box
	return NULL;
}

Voxel * SimulationBox::locatePotentialVoxel(vObject *v) {

	int i;
	Vector3i vIndices;
	Voxel *cV;
	
	// Get coordinates
	vIndices = getPotentialVoxelCoordinates(v);

	// Get this voxel
	cV = getVoxel(vIndices);

	return cV;
}

int SimulationBox::build(NetworkParameters *params, Network *n, NetworkFileReader *fileIn) {

	// Message
	fileLog->printMethodTask("Building Simulation Box...");

	// Add pointer to network
	net = n;

	// Add pointer to box to network
	net->setBox(this);

	// Flag forces straight away
	stericForcesActive = params->stericForcesActive;
	vdwForcesActive = params->vdwForcesActive;
	electrostaticForcesActive = params->electrostaticForcesActive;
	wallForcesActive = params->wallForcesActive;
	
	// Set num threads locally immediately
	#ifdef _OPENMP
		setNumThreads(omp_get_max_threads());
	#endif

	// Check if we need a box
	if(!params->boxActive) {
		exists = false;
		fileLog->printMethodSuccess();
		return BioNetSuccess;
	}

	// If not defined at all, build a default one
	if(fileIn->box.size() == 0) {
		if(buildDefaultSimulationBox(getBCType()) == BioNetError) {
			fileLog->printError("SimulationBox", "Unable to build default box :(");
			return BioNetError;
		}
	} else {

		// Now build defined box first
		map<string, string>::iterator it;

		string lValue, rValue;
		int numWarnings = 0, numSet = 0;

		// We absolutely need box size and numVoxels
		int returnValue;
		bool dimensionSet = false, voxelDimensionSet = false, voxelsSet = false, bcSet = false, cutoffSet = false;
		for(it = fileIn->box.begin(); it != fileIn->box.end(); ++it) {
			
			lValue = it->first;
			rValue = it->second;

			try {
				returnValue = setParameter(lValue, rValue);
				if(returnValue == BioNetWarning) {
					fileLog->printWarning(lValue, "Unrecognised Parameter.");
					numWarnings++;
				} else if (returnValue == BioNetError) {
					fileLog->printError("Unable to set parameter :(");
					return BioNetError;
				}

				if(toLower(lValue) == "voxeldimensions" || toLower(lValue) == "voxeldimension") {
					voxelDimensionSet = true;
				} else if (toLower(lValue) == "cutoff") {
					
					// Cutoff is equivalent to voxel dimensions, but shouldn't be dual-set
					cutoffSet = true;
				} else if (toLower(lValue) == "dimensions" || toLower(lValue) == "dimension") {
					dimensionSet = true;
				} else if (toLower(lValue) == "voxels" || toLower(lValue) == "numvoxels") {
					voxelsSet = true;
				} else if (toLower(lValue) == "bc") {
					bcSet = true;
				}
				
				numSet += 1;

			} catch (invalid_argument& e) {
				fileLog->printError(lValue, "Unable to parse parameter '" + rValue + "'");
				return BioNetError;
			}
		}

		// If only BC, we can build default box
		if(numSet == 1 && bcSet == true) {
			if(buildDefaultSimulationBox(getBCType()) == BioNetError) {
				fileLog->printError("SimulationBox", "Unable to build default box :(");
				return BioNetError;
			}
		} else {

			// Check for confusion
			if(voxelDimensionSet && cutoffSet) {
				fileLog->printError("Both cutoff and voxelDimensions were set. These are equivalent keywords :(");
				return BioNetError;

			} else if (cutoffSet) {

				// This is set within the 
				voxelDimensionSet = true;
			}

			// Check we can continue
			if(!dimensionSet || (!voxelDimensionSet && !voxelsSet)) {
				fileLog->printError("Unable to build simulation box :(\nWe need to know the box dimensions, and either a 'numVoxels' or 'voxelDimensions' to build a structure.");
				return BioNetError;		
			}

			if(voxelDimensionSet && voxelsSet) {
				fileLog->printError("Both numVoxels and voxelDimensions (cutoff) were set. These may be inconsistent :(");
				return BioNetError;
			}

			// Non-trivial calculations
			try {
				if(voxelDimensionSet) {
					fileLog->printWarning("We will proceed using voxelDimensions.");
					if(calculateNumVoxels() == BioNetError) {
						return BioNetError;
					}
				} else {
					fileLog->printWarning("We will proceed using numVoxels.");
					setTotalNumVoxels(numVoxels(0) * numVoxels(1) * numVoxels(2));
					calculateVoxelDimensions();
				}
			} catch (exception &e) {
				fileLog->printError("Unable to calculate simulation box dimensions. Please try again!");
				return BioNetError;
			}

			if(numWarnings != 0) {
				fileLog->printWarning("For your simulation box, you attempted to specify an unrecognised parameter. We will attempt to continue.");
			}
		}
	}

	// Check whether the box exists (default is PBC, so default is that it exists)
	if(getBCType() == NoBC) {

		fileLog->printMessage("SimulationBox::build", "No boundary conditions requested, so box will not be built.");
		
		if(params->stericForcesActive) {
			params->stericForcesActive = false;
			fileLog->printWarning("SimulationBox::build", "Steric forces will not be calculated!");
		}

		if(params->vdwForcesActive) {
			params->vdwForcesActive = false;
			fileLog->printWarning("SimulationBox::build", "Van der Waals forces will not be calculated!");
		}

		if(params->wallForcesActive) {
			params->wallForcesActive = false;
			fileLog->printWarning("SimulationBox::build", "Wall forces will not be calculated!");
		}
		
		// Set box status
		exists = false;
		fileLog->printMethodSuccess();
		return BioNetSuccess;

	} else {
		exists = true;
	}

	// Non-trivial stuff
	calculateCentroid();
	calculateDiagonal();
	calculateVolume();

	// Network reconnections (for kinetics)
	if(params->bondKineticsActive) {
		if(buildConnector(params, n) == BioNetError) {
			fileLog->printError("Unable to build network reconnector :(");
			return BioNetError;
		}
	}

	// Forces
	if(buildForces(params, fileIn) == BioNetError) {
		fileLog->printError("Unable to build forces :(");
		return BioNetError;
	}

	// Assign memory
	voxel = new Voxel[totalNumVoxels];

	// Assign neighbours
	linkVoxels();

	// Translate the network to the center of the box
	if(params->centraliseNetwork) {
		net->centraliseNetwork();
	}

	// Now, assign nodes to voxels
	if(!params->restartSet) {
		if(addNetworkToSimulationBox() == BioNetError) {
			fileLog->printError("Unable to add nodes to voxels.");
			return BioNetError;
		}
	} else {
		if(repopulate() == BioNetError) {
			fileLog->printError("Unable to add nodes to voxels.");
			return BioNetError;
		}
	}

	// Now, if we are doing something to the network, do it here
	if(shearForcesActive) {

		int i, j, k;
		Voxel *cV;

		// Freeze everything at the bottom layer
		for(i = 0; i < getNumVoxels()[0]; ++i) {
			for(j = 0; j < getNumVoxels()[2]; ++j) {

				// Get voxel
				cV = getVoxel(i, 0, j);

				// Loop nodes in voxel and freeze
				for(k = 0; k < cV->getNumObjects(); ++k) {
					cV->getObject(k)->setFrozen(true);
				}
			}
		}

		// Freeze everything at the top layer in the y direction
		for(i = 0; i < getNumVoxels()[0]; ++i) {
			for(j = 0; j < getNumVoxels()[2]; ++j) {

				// Get voxel
				cV = getVoxel(i, getNumVoxels()[1] - 1, j);

				// Loop nodes in voxel and freeze
				for(k = 0; k < cV->getNumObjects(); ++k) {
					cV->getObject(k)->setFrozen(true, 1);
				}
			}
		}
	}

	fileLog->printMethodSuccess();
	return BioNetSuccess;
}

int SimulationBox::buildForces(NetworkParameters *params, NetworkFileReader *fileIn) {

	// User info
	fileLog->printMethodTask("Assigning forces (within Simulation Box)...");

	int i, index;
	map<string, string> aForce;
	map<int, map<string, string>>::iterator fIt;
	map<string, string>::iterator it;
	string lValue, rValue, fName, fType;

	//
	// Sort the forces into functor objects
	//

	// Get vdw first (steric is semi-dependent on it)
	for(fIt = fileIn->forces.begin(); fIt != fileIn->forces.end(); ++fIt) {

		// Get force
		index = fIt->first;
		aForce = fIt->second;
		fName = aForce["name"];

		// Check for error
		if(toLower(fName) == "") {
			fileLog->printError("NetworkFileReader::forces", "No name provided for defined force " + to_string(index));
			return BioNetError;
		}

		// Do we need to do anything?
		if(toLower(fName) == "vdw" && vdwForcesActive) {

			// Build the calculators
			vdwCalculator = new PairPairMethod*[numThreads];

			for(i = 0; i < numThreads; ++i) {
				vdwCalculator[i] = new PairPairVdW();

				// Load parameters
				try {
					for(it = aForce.begin(); it != aForce.end(); ++it) {
						lValue = it->first;
						rValue = it->second;
						
						vdwCalculator[i]->setParameter(lValue, rValue);
					}
				} catch (exception &e) {
					fileLog->printError(lValue, "Unable to parse parameter '" + rValue + "'");
					return BioNetError;
				}

				// Initialise the calculator
				vdwCalculator[i]->initialise(net);
			}

			// Turn on steric
			params->stericForcesActive = true;
			stericForcesActive = params->stericForcesActive;
		}
	}

	// Now the rest
	for(fIt = fileIn->forces.begin(); fIt != fileIn->forces.end(); ++fIt) {

		// Get force
		index = fIt->first;
		aForce = fIt->second;
		fName = aForce["name"];

		// Check for error
		if(toLower(fName) == "") {
			fileLog->printError("NetworkFileReader::forces", "No name provided for defined force " + to_string(index));
			return BioNetError;
		}

		// Do we need to do anything?
		if(toLower(fName) == "steric" && stericForcesActive) {

			// Build the calculators
			stericCalculator = new PairPairMethod*[numThreads];

			for(i = 0; i < numThreads; ++i) {
				stericCalculator[i] = new PairPairSteric();
			
				// Load parameters
				try {
					for(it = aForce.begin(); it != aForce.end(); ++it) {
						lValue = it->first;
						rValue = it->second;
						
						stericCalculator[i]->setParameter(lValue, rValue);
					}
				} catch (exception &e) {
					fileLog->printError(lValue, "Unable to parse parameter '" + rValue + "'");
					return BioNetError;
				}

				// Initialise the calculator
				stericCalculator[i]->initialise(net);
			}
		}
		

		// Do we need to do anything?
		if((toLower(fName) == "wall" || toLower(fName) == "wallone" || toLower(fName) == "walltwo") && wallForcesActive) {

			// How many walls?
			if (toLower(fName) == "walltwo") {
				bothWallForcesActive = true;
			}
			
			// We also need a type
			fType = aForce["type"];

			// Check for error
			if(toLower(fType) == "") {
				fileLog->printError("NetworkFileReader::forces", "No type provided for wall force " + to_string(index));
				return BioNetError;
			}
			
			// Build the calculators
			wallCalculator = new WallMethod*[numThreads];

			for(i = 0; i < numThreads; ++i) {
			
				if (toLower(fType) == "vdw") {
					wallCalculator[i] = new WallVdW();
				}
				
				// Load parameters
				try {
					for(it = aForce.begin(); it != aForce.end(); ++it) {
						lValue = it->first;
						rValue = it->second;
						
						wallCalculator[i]->setParameter(lValue, rValue);
					}
				} catch (exception &e) {
					fileLog->printError(lValue, "Unable to parse parameter '" + rValue + "'");
					return BioNetError;
				}

				// Initialise the calculator
				wallCalculator[i]->initialise(net);
			}
		}

	}

	// What if user just wants default parameters?
	if(vdwForcesActive && vdwCalculator == NULL) {

		// Build the calculators
		vdwCalculator = new PairPairMethod*[numThreads];

		for(i = 0; i < numThreads; ++i) {
			vdwCalculator[i] = new PairPairVdW();
			vdwCalculator[i]->initialise(net);
		}

	}

	if(stericForcesActive && stericCalculator == NULL) {

		// Build the calculators
		stericCalculator = new PairPairMethod*[numThreads];

		for(i = 0; i < numThreads; ++i) {
			stericCalculator[i] = new PairPairSteric();
			stericCalculator[i]->initialise(net);
		}
	}

	fileLog->printMethodSuccess();
	return BioNetSuccess;
}

int SimulationBox::buildConnector(NetworkParameters *params, Network *n) {

	// We need to build the connector object, which does the calculations to determine reconnection 'kinetics'

	// Cutoff is simply obtained from the top bond, which will be the kinetic one
	connector = new PairPairMethod*[numThreads];
	for(int i = 0; i < numThreads; ++i) {
		connector[i] = new PairPairConnect(params->localConnectivitySet);
		connector[i]->initialise(n);
		if(connector[i]->getCutoff() < 0.0) {
			fileLog->printError("Bond cutoff has been incorrectly set. It must be greater than 0 :(");
			return BioNetError;
		}
	}
	return BioNetSuccess;
}

int SimulationBox::buildDefaultSimulationBox(BCType bc) {

	// Method details
	cout << endl;
	fileLog->printMethodTask("Building Default Simulation Box...");

	// Set type immeidately to supplied boundary conditions
	setBCType(bc);

	// Get system size and make that the dimensions
	net->calculateDimensions();
	dimension = net->getDimensions();
	
	if(!electrostaticForcesActive && !vdwForcesActive && !stericForcesActive and !wallForcesActive) {
		
		// Box is a single voxel as there are no long-range interactions
		setVoxelDimensions(dimension);

	} else {

		// If it isn't, then it should be dependent upon the particle size (dispersion) or interaction size, whichever is biggest
		Distribution *d;
		d = net->getDispersion();

		// Set the distribution maximum as the voxel size (isotropic)
		setVoxelDimensions(2 * d->getMaximum());
	}


	if(calculateNumVoxels() == BioNetError) {
		return BioNetError;
	}

	// Set flag
	defaulted = true;

	fileLog->printMethodSuccess();
	return BioNetSuccess;
}

void SimulationBox::linkVoxels() {

	int i, j, k, l;
	int iEdge, jEdge, kEdge;
	int vIndex;
	scalar diff;
	Vector3i cIndices, nIndices;
	Vector3 trans;
	bool linkVoxel;
	Voxel *cV, *nV;

	// This is a crazy loop, but it only happens once and will hugely speed things up in future calculations

	// Initialise
	vIndex = 0;
	numEdgeVoxels = 0;
	numInteriorVoxels = 0;
	numYMinusVoxels = 0;
	numYPlusVoxels = 0;
	
	edgeVoxel.clear();
	interiorVoxel.clear();
	yPlusVoxel.clear();
	yMinusVoxel.clear();


	// Loop over all voxels and set local properties (indices, edge status etc)
	for(cIndices(0) = 0; cIndices(0) < numVoxels(0); ++cIndices(0)) {
		for(cIndices(1) = 0; cIndices(1) < numVoxels(1); ++cIndices(1)) {
			for(cIndices(2) = 0; cIndices(2) < numVoxels(2); ++cIndices(2)) {

				// Set the indices
				voxel[vIndex].setIndex(vIndex);
				voxel[vIndex].setIndices(cIndices(0), cIndices(1), cIndices(2));

				// Set edge voxels and edge types
				if(cIndices(0) == 0 || cIndices(0) == numVoxels(0) - 1 || cIndices(1) == 0 || cIndices(1) == numVoxels(1) - 1 || cIndices(2) == 0 || cIndices(2) == numVoxels(2) - 1) {

					voxel[vIndex].setEdge(true);

					if(cIndices(0) == 0) {
						voxel[vIndex].setEdgeType(BEdge, X);
					} else if (cIndices(0) == numVoxels(0) - 1) {
						voxel[vIndex].setEdgeType(TEdge, X);
					} else {
						voxel[vIndex].setEdgeType(NoEdge, X);
					}

					if(cIndices(1) == 0) {
						voxel[vIndex].setEdgeType(BEdge, Y);
						yMinusVoxel.insert(&voxel[vIndex]);
						numYMinusVoxels += 1;
					} else if (cIndices(1) == numVoxels(1) - 1) {
						voxel[vIndex].setEdgeType(TEdge, Y);
						yPlusVoxel.insert(&voxel[vIndex]);
						numYPlusVoxels += 1;
					} else {
						voxel[vIndex].setEdgeType(NoEdge, Y);
					}

					if(cIndices(2) == 0) {
						voxel[vIndex].setEdgeType(BEdge, Z);
					} else if (cIndices(2) == numVoxels(2) - 1) {
						voxel[vIndex].setEdgeType(TEdge, Z);
					} else {
						voxel[vIndex].setEdgeType(NoEdge, Z);
					}

					edgeVoxel.insert(&voxel[vIndex]);
					numEdgeVoxels += 1;

				} else {
					voxel[vIndex].setEdge(false);

					voxel[vIndex].setEdgeType(NoEdge, X);
					voxel[vIndex].setEdgeType(NoEdge, Y);
					voxel[vIndex].setEdgeType(NoEdge, Z);

					interiorVoxel.insert(&voxel[vIndex]);
					numInteriorVoxels += 1;
				}

				vIndex += 1;
			}
		}
	}

	// Link voxels
	for(cIndices(0) = 0; cIndices(0) < numVoxels(0); ++cIndices(0)) {
		for(cIndices(1) = 0; cIndices(1) < numVoxels(1); ++cIndices(1)) {
			for(cIndices(2) = 0; cIndices(2) < numVoxels(2); ++cIndices(2)) {

				cV = getVoxel(cIndices);

				// And all neighbours, adding them to the neighbour lists
				for(i = -1; i < 2; ++i) {
					for(j = -1; j < 2; ++j) {
						for(k = -1; k < 2; ++k) {
						
							nIndices(0) = cIndices(0) + i;
							nIndices(1) = cIndices(1) + j;
							nIndices(2) = cIndices(2) + k;

							if(bcType == PBC) {

								// Wall interactions turn off the periodic y neighbours
								if(wallForcesActive and j != 0) {
									if(nIndices(1) < 0 || nIndices(1) >= numVoxels(1)) {
										continue;
									}
								}

								// Clear negatives by adding 1 box length first
								for(l = 0; l < 3; ++l) {
									nIndices(l) = (nIndices(l) + numVoxels(l)) % numVoxels(l);
								}

								nV = getVoxel(nIndices);

								// Are they the same?
								if(nV == cV) {
									continue;
								}


								// Add voxel as a neighbour
								cV->addNeighbour(nV);

								// Calculate the translation to move from this voxel to the 
								// appropriate location as a neighbour
								trans.setZero();

								// If cV is 'in front', translation is backwards and vice versa
								if(cV->getEdgeType(X) == TEdge && nV->getEdgeType(X) == BEdge) {
									trans(0) -= getDimensions()[0];
								} else if(cV->getEdgeType(X) == BEdge && nV->getEdgeType(X) == TEdge) {
									trans(0) += getDimensions()[0];
								}

								if(cV->getEdgeType(Y) == TEdge && nV->getEdgeType(Y) == BEdge) {
									trans(1) -= getDimensions()[1];
								} else if(cV->getEdgeType(Y) == BEdge && nV->getEdgeType(Y) == TEdge) {
									trans(1) += getDimensions()[1];
								}

								if(cV->getEdgeType(Z) == TEdge && nV->getEdgeType(Z) == BEdge) {
									trans(2) -= getDimensions()[2];
								} else if(cV->getEdgeType(Z) == BEdge && nV->getEdgeType(Z) == TEdge) {
									trans(2) += getDimensions()[2];
								}

								// Add translation to lookup
								cV->setNeighbourTranslation(nV, trans);

							} else if (bcType == HBC) {
								linkVoxel = true;
								for(l = 0; l < 3; ++l) {
									if(nIndices(l) < 0 || nIndices(l) >= numVoxels(l)) {
										linkVoxel = false;
										break;
									}
								}

								if(linkVoxel) {
									nV = getVoxel(nIndices);

									// Are they the same?
									if(nV == cV) {
										continue;
									}

									cV->addNeighbour(nV);
								}
							}
						}
					}
				}

				// Remove self from neighbour list
//				voxel[vIndex].removeNeighbour(&voxel[vIndex]);

				// Remove self from translations
//				voxel[vIndex].removeNeighbourTranslation(&voxel[vIndex]);

				// Move forward
				vIndex += 1;
			}
		}
	}

	// Loop over all voxels to get all possible pairs
	set<pair<int,int>> pairs;
	set<pair<int,int>>::iterator it;
	for(i = 0; i < totalNumVoxels; ++i) {

		// Get all neighbour pairs into a set
		cV = getVoxel(i);
		for(j = 0; j < cV->getNumNeighbours(); ++j) {
			nV = cV->getNeighbour(j);
	
			if(nV->getIndex() < cV->getIndex()) {
				pairs.insert(make_pair(nV->getIndex(),cV->getIndex()));
			} else {
				pairs.insert(make_pair(cV->getIndex(),nV->getIndex()));
			}
		}
	}

	// Loop over all pairs to assign unique neighbours to each voxel
	// Alternate first and second voxel in pair to keep it approximately equally weighted per thread
	int count = 0;
	for(it = pairs.begin(); it != pairs.end(); ++it) {
		if(count % 2 == 0) {
			getVoxel(it->first)->addInteractionNeighbour(getVoxel(it->second));
		} else {
			getVoxel(it->second)->addInteractionNeighbour(getVoxel(it->first));
		}
		count += 1;
	}
}

int SimulationBox::preInitialise() {

	// Message
	fileLog->printMethodTask("Pre-preparing a Simulation Box...");

	doAllInteractions();

	// Energies (cannot cause error...)
//	calculateTotalEnergy();

	// Time constants
//	calculateTimeConstants();

	fileLog->printMethodSuccess();
	return BioNetSuccess;
}

int SimulationBox::repopulate() {

	int i, numWarnings, numNodes;
	vector<vObject*> nodes;

	// Clear the box
	clear();

	// Get the nodes
	nodes = net->getNodes();
	numNodes = net->getNumNodes();
	
	// We know the exact dimensions of the box, so we can loop the nodes once and add to box
	numWarnings = 0;
	for(i = 0; i < nodes.size(); ++i) {
//		cout << nodes[i]->getTemplate()->getTypeString() << endl;
//		if(nodes[i] != NULL && nodes[i]->getTemplate()->getTypeString() != "Point") {
		if(nodes[i] != NULL) {
			// Apply boundary conditions to make sure indices are positive (negative indices don't flatten properly) 
//			applyBoundaryConditions(nodes[i]);

			// Try to add
			if(addObjectToVoxels(nodes[i]) == BioNetError) {
	
				fileLog->printError("SimulationBox::voxel", "Unable to add node " + to_string(i) + " to any voxel.");
				return BioNetError;
			}
		}
	}

	return BioNetSuccess;
}

int SimulationBox::addNetworkToSimulationBox() {

	// Message
	fileLog->printMethodTask("Populating Simulation Box voxels with nodes...");

	// Apply boundary conditions to get all objects in the box
	applyBoundaryConditions();

	// Fill box object
	if(repopulate() == BioNetError) {
		fileLog->printError("Unable to populate simulation box with nodes :(");
		return BioNetError;
	}

	fileLog->printMethodSuccess();
	return BioNetSuccess;
}

int SimulationBox::addObjectToVoxels(vObject *node) {
	
	int i;
	Vector3 p;
	Vector3i vIndices;	
	p = node->getPosition();

	vIndices = getPotentialVoxelCoordinates(node);

	return addObjectToVoxel(node, vIndices);
}

int SimulationBox::removeObjectFromVoxels(vObject *node) {
	
	int i;
	Voxel *v;

	// Find the voxel
	v = locateContainingVoxel(node);
	if (v == NULL) {
		fileLog->printWarning("Could not find voxel to remove node from :(");
		return BioNetWarning;
	}

	// Remove it
	v->removeObject(node);

	return BioNetSuccess;
}


int SimulationBox::addObjectToVoxel(vObject *node, Vector3i vIndices) {
	Voxel *v = getVoxel(vIndices);

	// Does voxel exist?
	if(v == NULL) {
		return BioNetError;
	}
	
	// Add node to list
	v->addObject(node);

	return BioNetSuccess;
}

void SimulationBox::clear() {
	for(int i = 0; i < totalNumVoxels; ++i) {
		voxel[i].clear();
	}
}

void SimulationBox::printEnergies() {

	int i;
	Voxel *V;
	cout << endl;
	for(i = 0; i < totalNumVoxels; ++i) {
		V = getVoxel(i);
		cout << "Voxel " << i << ", Steric: " << V->getStericEnergy() << ", VdW: " << V->getVdWEnergy() << ", Wall: " << V->getWallEnergy() << ", Total: " << V->getTotalEnergy() << endl; 
	}
	cout << "Steric: " << stericEnergy << endl;
	cout << "VdW: " << vdwEnergy << endl;
	cout << "Wall: " << wallEnergy << endl;
	cout << "Total: " << totalEnergy << endl;
}

bool SimulationBox::validate() {

	// User info
	fileLog->printMethodTask("Validating Simulation Box structure...");

	// Super easy stuff
	if(!exists) {
		return BioNetValid;
	}

	// Check all nodes are in box
	int i, numNodes, checkNumNodes = 0;
	vector<vObject*> nodes;

	// Easy stuff first
	numNodes = net->getNumNodes();
	nodes = net->getNodes();

	for(i = 0; i < nodes.size(); ++i) {

		if(nodes[i] != NULL) {
			if(!containsObject(nodes[i])) {
				fileLog->printError("Node " + to_string(i) + " not in SimulationBox limits :(");
			}
		}	
	}

	// Now, check all nodes are within voxels
	for(i = 0; i < totalNumVoxels; ++i) {
		checkNumNodes += voxel[i].getNumObjects();

		// And check correct number of neighbour voxels (for PBC. HBC is complicated)
		if(bcType == PBC and !wallForcesActive) {
			if(totalNumVoxels >= 27 && voxel[i].getNumNeighbours() != 26) {
				fileLog->printError("Voxel " + to_string(i) + " only has " + to_string(voxel[i].getNumNeighbours()) + " neighbours instead of 26.");
				return BioNetInvalid;
			} else if(totalNumVoxels < 27 && voxel[i].getNumNeighbours() != totalNumVoxels - 1) {
				fileLog->printError("Voxel " + to_string(i) + " only has " + to_string(voxel[i].getNumNeighbours()) + " neighbours instead of " + to_string(totalNumVoxels - 1) + ".");
				return BioNetInvalid;
			}
		}
	}

//	if(checkNumNodes != numNodes) {
//		fileLog->printError("Of " + to_string(numNodes) + ", only " + to_string(checkNumNodes) + " are contained in SimulationBox structure.");
//		return BioNetInvalid;
//	}

	fileLog->printMethodSuccess();

	//
	// Forces
	//
	fileLog->printMethodTask("Validating forces (within Simulation Box)...");
	if(vdwCalculator != NULL) {
		for(i = 0; i < numThreads; ++i) {
			if(vdwCalculator[i]->validate() == BioNetInvalid) {
				return BioNetInvalid;
			}
		}
	}

	if(stericCalculator != NULL) {
		for(i = 0; i < numThreads; ++i) {
			if(stericCalculator[i]->validate() == BioNetInvalid) {
				return BioNetInvalid;
			}
		}
	}

	if(wallCalculator != NULL) {
		for(i = 0; i < numThreads; ++i) {
			if(wallCalculator[i]->validate() == BioNetInvalid) {
				return BioNetInvalid;
			}
		}
	}
	
	// Dependencies
	if(shearForcesActive and getShearStress() == 0.0) {
		fileLog->printError("SimulationBox::shearStress", "If 'SBC' is set, we also require a non-zero shear stress value.");
		return BioNetInvalid;
	}

	fileLog->printMethodSuccess();
	return BioNetValid;
}

bool SimulationBox::containsObject(vObject *node) {
	
	for(int i = 0; i < 3; ++i) {
		if(node->pos[i] < 0 || node->pos[i] > dimension(i)) {
			return false;
		}
	}
	return true;
}

void SimulationBox::applyBoundaryConditions() {

	if(!exists) {
		return;
	}

	int i, numNodes;
	vector<vObject*> nodes;

	numNodes = net->getNumNodes();
	nodes = net->getNodes();

	for(int i = 0; i < nodes.size(); ++i) {
		if(nodes[i] != NULL) {
			applyBoundaryConditions(nodes[i]);
		}
	}

	// Always repopulate
	repopulate();
}

void SimulationBox::applyBoundaryConditions(vObject *node) {

	if(bcType == PBC) {
		applyPeriodicBoundaryConditions(node);

	} else if (bcType == HBC) {
		applyHardBoundaryConditions(node);
	} else if (bcType == CBC) {
		applyCushionBoundaryConditions(node);
	} else if (bcType == RBC) {
		applyRadialBoundaryConditions(node);
	}	
}

void SimulationBox::applyPeriodicBoundaryConditions(vObject *node) {

	Vector3 p = node->getPosition();
	Vector3 p2;

	// Y is different if walls are active. Do that first
	if (wallForcesActive) {
		applyHardBoundaryConditions(node, 1);
	}

	// Wrap around with modular algebra (add the box length to deal with negative numbers)
	for(int i = 0; i < 3; ++i) {
		p2(i) = fmod(p(i) + dimension(i), dimension(i));
		if(p2(i) - p(i) < -getDimensions()[i] / 2.0) {
			node->addWrap(i, 1);
		} else if(p2(i) - p(i) > getDimensions()[i] / 2.0) {
			node->addWrap(i, -1);
		}
	}

	node->setPosition(p2);
}

void SimulationBox::applyHardBoundaryConditions(vObject *node) {

	Vector3 p = node->getPosition();
	scalar r = node->getLengthscale();
	Vector3 v = node->getVelocity();
	scalar diff1, diff2;
	
	// Stop it moving and reverse direction
	for(int i = 0; i < 3; ++i) {

		// Position locked into box
		diff1 = (p(i) + r) - dimension(i);
		diff2 = p(i) - r;
		if(diff1 > 0.0) {
			p(i) -= diff1 * 2;

			// Reverse the velocity
			v(i) *= -1;

		} else if (diff2 < 0.0) {
			p(i) -= diff2 * 2;

			// Reverse the velocity
			v(i) *= -1;
		}
	}
	node->setVelocity(v);
	node->setPosition(p);
}

void SimulationBox::applyHardBoundaryConditions(vObject *node, int i) {

	Vector3 p = node->getPosition();
	scalar r = node->getLengthscale();
	Vector3 v = node->getVelocity();
	scalar diff1, diff2;
	
	// Stop it moving and reverse direction


	// Position locked into box
	diff1 = (p(i) + r) - dimension(i);
	diff2 = p(i) - r;
	if(diff1 > 0.0) {
		p(i) -= diff1 * 2;

		// Reverse the velocity
		v(i) *= -1;

	} else if (diff2 < 0.0) {
		p(i) -= diff2 * 2;

		// Reverse the velocity
		v(i) *= -1;
	}

	node->setVelocity(v);
	node->setPosition(p);
}

void SimulationBox::applyCushionBoundaryConditions(vObject *node) {

	Vector3 p = node->getPosition();
	scalar r = node->getLengthscale();
	Vector3 v = node->getVelocity();

	// Stop it moving outright
	for(int i = 0; i < 3; ++i) {

		// Position locked into box 
		if(p(i) + r > dimension(i)) {
			p(i) = dimension(i) - r;

			// Momentum absorbed by wall
			v(i) = 0;

		} else if (p(i) - r < 0) {
			p(i) = r;

			// Momentum absorbed by wall
			v(i) = 0;
		}
	}
	node->setVelocity(v);
	node->setPosition(p);
}

void SimulationBox::applyRadialBoundaryConditions(vObject *node) {

	Vector3 p = node->getPosition();
	scalar r = node->getLengthscale();
	Vector3 v = node->getVelocity();
        
        scalar Rsph;
        Rsph = dimension(0)/2;

	// Stop it moving outright
	/*for(int i = 0; i < 3; ++i) {cout<< p(i) << "," << r << "," << Rsph << endl;

		// Position locked into box and momentum absorbed by wall
		if(p(i) + r > dimension(i)/2 + Rsph) {
			p(i) = (dimension(i)/2 + Rsph) - r;
			v(i) = -1 * v(i);
                        
		} else if (p(i) - r < dimension(i)/2 - Rsph) {
			p(i) = -1 * (dimension(i)/2 - Rsph) + r;
			v(i) = -1 * v(i);
		}
	}*/

        Vector3 boxRadial = p - getCentroid();
        Vector3 UnitVector = boxRadial/boxRadial.norm();

        if(boxRadial.norm() + r > Rsph) {
            p = getCentroid() + (UnitVector * (Rsph-r));
            v = v - 2 * ((UnitVector.dot(v)) * UnitVector);
        }

	node->setVelocity(v);
	node->setPosition(p);
}

Vector3 SimulationBox::calculatePeriodicTranslation(vObject *n1, vObject *n2) {

	Voxel *v1, *v2;

	// Get effective voxels and return the neighbour translation
	v1 = locatePotentialVoxel(n1);
	v2 = locatePotentialVoxel(n2);

	if(v1 == NULL || v2 == NULL) {
		fileLog->printError("SimulationBox::calculatePeriodicTranslation", "Voxels not found :(");
		throw exception();
	}

	return locatePotentialVoxel(n1)->getNeighbourTranslation(locatePotentialVoxel(n2));
}

void SimulationBox::randomiseContents() {

	int i, numNodes;
	vector<vObject*> nodes;

	numNodes = net->getNumNodes();
	nodes = net->getNodes();

	for(int i = 0; i < nodes.size(); ++i) {
		if(nodes[i] != NULL) {
			randomMove(nodes[i]);
		}
	}

	// Re-introduce the network
	addNetworkToSimulationBox();
}

void SimulationBox::randomMove(vObject *node) {

	// Move randomly around the box using the distributions
	int i;
	Vector3 nPos;
	Voxel *oldV, *newV;

	// Find the old voxel
	oldV = locateContainingVoxel(node);

	// Get position
	for(i = 0; i < 3; ++i) {
		nPos(i) = uniformDistro(RNGGen)  * dimension(i);
	}

	// Translate
	node->setPosition(nPos);

	// Get new voxel
	newV = locatePotentialVoxel(node);

	// If not same, then change location
	if(newV != oldV) {
		oldV->removeObject(node);
		newV->addObject(node);
	}

	// And a random rotation (if 3D geometry exists)
//	if(node->getStructureDimension() > 0) {
//		Vector2 rot;
//		Quat q;
//
//		for(i = 0; i < 2; ++i) {
//			rot(i) = uniformDistro(RNGGen) * 2 * M_PI;
//		}
//
//		q.w() = 0.0;
//		q.vec() = MathFunctions::cartesian(rot);
//		node->rotate(q);
//	}

	// Sort new box position
}

/*

void SimulationBox::randomMove(vObject *node, set<int> nodeList) {
	
	// First, can we be quicker about this?
	if(nodeList.size() == 1) {
		randomMove(&node[*nodeList.begin()]);
	} else {

		// Move the whole set of nodes randomly around
		int i;
		set<int>::iterator it;

		// Firstly, get the minimum and maximum positions of the nodes
		Vector3 minVals, maxVals;
		for(i = 0; i < 3; ++i) {
			minVals(i) = sInf;
			maxVals(i) = -1 * sInf;
		}

		for(it = nodeList.begin(); it != nodeList.end(); ++it) {
			for(i = 0; i < 3; ++i) {
				if(node[*it].pos(i) < minVals(i)) {
					minVals(i) = node[*it].pos(i);
				}
				if (node[*it].pos(i) > maxVals(i)) {
					maxVals(i) = node[*it].pos(i);
				}
			}	
		}

		// To keep the node group approximately connected, only move them around the box, not loop aoud it
		Vector3 trans;
		for(i = 0; i < 3; ++i) {
			trans(i) = uniformDistro(RNGGen) * (length(i) - (maxVals(i) - minVals(i)) );
		}

		// Translate the whole lot in the same way
		for(it = nodeList.begin(); it != nodeList.end(); ++it) {
			node[*it].translate(trans);
		}

		// Now for the rotation
		Vector3 centroid;
		centroid.setZero();

		// Get centroid
		for(it = nodeList.begin(); it != nodeList.end(); ++it) {
			centroid += node[*it].pos;
		}
		centroid /= nodeList.size();

		// Translate
		for(it = nodeList.begin(); it != nodeList.end(); ++it) {
			node[*it].translate(-1 * centroid);
		}

		// Rotate the system
		Vector2 rot;
		for(i = 0; i < 2; ++i) {
			rot(i) = uniformDistro(RNGGen) * 2 * M_PI;
		}

		for(it = nodeList.begin(); it != nodeList.end(); ++it) {
			node[*it].rotateAboutOrigin(rot);
		}

		// Translate back, and apply boundary conditions
		for(it = nodeList.begin(); it != nodeList.end(); ++it) {
			node[*it].translate(centroid);
			applyBoundaryConditions(&node[*it]);
		}
	}
}

*/
void SimulationBox::printDetails() {
	cout << endl;
	cout << "Class: SimulationBox" << endl;
	cout << "Parameters: " << endl;
	cout << "\tExists: " << exists << endl;
	if(exists) {
		cout << "\tBoundary Conditions: " << getBCTypeString() << endl;
		cout << "\tTotal Number of Voxels = " << totalNumVoxels << ", Num Edge Voxels = " << numEdgeVoxels << endl;
		cout << "\tNumber of Voxels = (" << getNumVoxels()(0) << ", " << getNumVoxels()(1) << ", " << getNumVoxels()(2) << ")" << endl;
		cout << "\tLength Dimensions = (" << dimension(0) << ", " << dimension(1) << ", " << dimension(2) << ")" << endl;
		cout << "\tVoxel Dimensions = (" << voxelDimension(0) << ", " << voxelDimension(1) << ", " << voxelDimension(2) << ")" << endl;
		cout << "\tCentroid = (" << centroid(0) << ", " << centroid(1) << ", " << centroid(2) << ")" << endl;
		cout << "\tDiagonal Length = " << getDiagonal() << ", Volume = " << getVolume() << endl;
		cout << "\tShear = " << toString(shearForcesActive) << endl;

		// What if user just wants default parameters?
		if(vdwForcesActive) {
			vdwCalculator[0]->printDetails();
		}

		if(stericForcesActive) {
			stericCalculator[0]->printDetails();
		}

		if(wallForcesActive) {
			wallCalculator[0]->printDetails();
		}
		
	}
}
