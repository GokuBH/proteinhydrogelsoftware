#ifndef PHYSYS_H_INCLUDED
#define PHYSYS_H_INCLUDED

#include <iostream>
#include <string>

#include "SystemParameters.hpp"
#include "MathematicalFunctions.hpp"
#include "LexicalAnalyser.hpp"
#include "NetworkFileReader.hpp"
#include "GlobalObjects.hpp"

class Solvent {

	public:

		//
		// Methods
		//
		int build(NetworkFileReader *fileIn);
		void printDetails();
	private:

		
};
#endif
