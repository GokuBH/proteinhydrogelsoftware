// 
//  This file is part of the BioNet simulation package
//  
//  Copyright (c) by the Dougan Theoretical & Experimental Biophysics group,
//  as they appear in the README.md file.
// 
//  BioNet is free and open source software. You can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  should you wish, any later version.
// 
//  BioNet is distributed to help both the scientific community and those,
//  without financial backing.
//  But we distribute WITHOUT ANY WARRANTY, the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//  GNU General Public License for more details.
// 
//  The GNU General Public License is contained within the BioNet
//  package. Further details can be found at <http://www.gnu.org/licenses/>.
// 
//  To help us fund BioNet development, we do ask that you cite 
//  the relevent research papers and associated software release papers.
//

#ifndef CMCONF_H_INCLUDED
#define CMCONF_H_INCLUDED

// Grab some data from CMake

// Versions
#define VERSION_MAJOR 0
#define VERSION_MINOR 5
#define VERSION_PATCH 0

#endif
