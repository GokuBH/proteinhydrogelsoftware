#ifndef RNGCONT_H_INCLUDED
#define RNGCONT_H_INCLUDED

#include <chrono>
#include <random>
#include <fstream>
#include <sstream>
#include "SystemParameters.hpp"

using namespace std;

class RNGContainer {

	public:

		//
		// Methods
		//

		// Constructors / Destructors
		RNGContainer();
		~RNGContainer();

		// Setters & Getters
		void setNumThreads(int n);
		int getNumThreads();

		void setThermalRNGSeed(int n, long int s);
		long int getThermalRNGSeed(int n);

		void setMinorRNGSeed(long int s);
		long int getMinorRNGSeed();

		void setThermalRNGState(int n, stringstream &ss);
		mt19937 getThermalRNG(int n);

		void setMinorRNGState(stringstream &ss);
		mt19937 getMinorRNG();

		void setNormalDistributionState(int n, stringstream &ss);
		normal_distribution<scalar> getNormalDistribution(int n);

		void setUniformDistributionState(stringstream &ss);
		uniform_real_distribution<scalar> getUniformDistribution();

		scalar getThermalRN(int n);
		scalar getMinorUniformRN();
		scalar getMinorNormalRN(int n);

		// Initialisation methods
		void initialise(int n);

	private:

		//
		// Variables
		//
		int numThreads;

		// Distributions
		normal_distribution<scalar> *normalDistro;
		uniform_real_distribution<scalar> uniformDistro;

		// RNGs
		mt19937 *thermalRNG, minorRNG;

		// Seeds
		long int *thermalRNGSeed, minorRNGSeed;
};
#endif
