#ifndef NETWRITER_H_INCLUDED
#define NETWRITER_H_INCLUDED

#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include <exception>
#include <list>
#include <set>
#include <vector>
#include <map>
#include <ctime>
#include <boost/algorithm/string.hpp>
#include <boost/filesystem.hpp>
#include <boost/format.hpp>
#include "tinyxml2.h"

#include "GlobalObjects.hpp"
#include "Utilities.hpp"
#include "SystemParameters.hpp"
#include "NetworkExceptions.hpp"

#include "NetworkParameters.hpp"
#include "NetworkPrimitives.hpp"
#include "Forces.hpp"

#include "PhysicalSystem.hpp"
#include "Network.hpp"
#include "ElasticNetwork.hpp"
#include "ViscoElasticNetwork.hpp"

using namespace std;
using namespace tinyxml2;
namespace bfs = boost::filesystem;
using boost::format;


class NetworkFileWriter {
	
	public:

		//
		//  Methods
		//
		NetworkFileWriter();
		~NetworkFileWriter();
		
		//
		// Input
		//
		int writeInputFile(PhysicalSystem *pSystem, bfs::path oFname);

		//
		// Output
		//

		// Output files should only be written (they might be massive so will not be fully stored in memory)
		void writeTrajectoryHeader(ofstream& fout);
		void writeMeasurementHeader(ofstream& fout);
		void writeTopologyHeader(ofstream& fout);
		void writeCheckpointHeader(ofstream& fout);
		void writeForceHeader(ofstream& fout);
		void writeNormalModeHeader(ofstream& fout);

		void writeTrajectoryInitialisationHeader(NetworkParameters *params, PhysicalSystem *pSys, ofstream& fout);
		void writeMeasurementInitialisationHeader(NetworkParameters *params, PhysicalSystem *pSys, ofstream& fout);
		void writeTopologyInitialisationHeader(NetworkParameters *params, PhysicalSystem *pSys, ofstream& fout);
		void writeCheckpointInitialisationHeader(NetworkParameters *params, PhysicalSystem *pSys, ofstream& fout);
		
		void writeTrajectoryInitialisationFooter(ofstream& fout);
		void writeMeasurementInitialisationFooter(ofstream& fout);
		void writeTopologyInitialisationFooter(ofstream& fout);
		void writeCheckpointInitialisationFooter(ofstream& fout);
		
		void writeTrajectorySimulationHeader(NetworkParameters *params, PhysicalSystem *pSys, ofstream& fout);
		void writeMeasurementSimulationHeader(NetworkParameters *params, PhysicalSystem *pSys, ofstream& fout);
		void writeTopologySimulationHeader(NetworkParameters *params, PhysicalSystem *pSys, ofstream& fout);
		void writeCheckpointSimulationHeader(NetworkParameters *params, PhysicalSystem *pSys, ofstream& fout);
		void writeForceSimulationHeader(NetworkParameters *params, PhysicalSystem *pSys, ofstream& fout);
		void writeNormalModeSimulationHeader(NetworkParameters *params, PhysicalSystem *pSys, ofstream& fout);

		void writeTrajectorySimulationFooter(ofstream& fout);
		void writeMeasurementSimulationFooter(ofstream& fout);
		void writeTopologySimulationFooter(ofstream& fout);
		void writeCheckpointSimulationFooter(ofstream& fout);
		void writeForceSimulationFooter(ofstream& fout);
		void writeNormalModeSimulationFooter(ofstream& fout);

		void writeTrajectoryFrame(scalar time, NetworkParameters *params, PhysicalSystem *pSys, ofstream& fout);
		void writeMeasurementFrame(scalar time, NetworkParameters *params, PhysicalSystem *pSys, ofstream& fout);
		void writeTopologyFrame(scalar time, NetworkParameters *params, PhysicalSystem *pSys, ofstream& fout);
		void writeCheckpointFrame(scalar time, NetworkParameters *params, PhysicalSystem *pSys, ofstream& fout);
		void writeForceFrame(scalar time, NetworkParameters *params, PhysicalSystem *pSys, ofstream& fout);
		void writeEigensystem(NetworkParameters *params, PhysicalSystem *pSys, ofstream& fout);

		int truncateCheckpoint(NetworkParameters *params, PhysicalSystem *pSys, bfs::path cFname);
		int truncateTrajectory(NetworkParameters *params, PhysicalSystem *pSys, bfs::path tFname);
		int truncateMeasurement(NetworkParameters *params, PhysicalSystem *pSys, bfs::path mFname);
		int truncateTopology(NetworkParameters *params, PhysicalSystem *pSys, bfs::path tpFname);
		int truncateForce(NetworkParameters *params, PhysicalSystem *pSys, bfs::path tpFname);
		
	private:

		// These write methods can maybe be refactored?
		int writeProteinsToInputFile(XMLDocument *doc, XMLElement *parent, Network *net);
		int writeBondsToInputFile(XMLDocument *doc, XMLElement *parent, Network *net);
		int writeParametersToInputFile(XMLDocument *doc, XMLElement *parent, NetworkParameters *params);
		int writeNodesToInputFile(XMLDocument *doc, XMLElement *parent, Network *net);
		int writeConnectionsToInputFile(XMLDocument *doc, XMLElement *parent, Network *net, NetworkParameters *params);
		int writeSimulationBoxToInputFile(XMLDocument *doc, XMLElement *parent, SimulationBox *box);

		int writeVTObjectToInputFile(XMLDocument *doc, XMLElement *parent, string title, const vector<string> att, vTObject *obj);
		int writeCTObjectToInputFile(XMLDocument *doc, XMLElement *parent, string title, const vector<string> att, cTObject *obj);
		int writeVObjectToInputFile(XMLDocument *doc, XMLElement *parent, string title, const vector<string> att, vObject *obj);
		int writeCObjectToInputFile(XMLDocument *doc, XMLElement *parent, string title, const vector<string> att, cObject *obj, NetworkParameters *params);

		void writeVObjectToTrajectoryFile(vObject *n, ofstream& fout, bool local);
		void writeCObjectToTopologyFile(cObject *c, ofstream& fout);
		void writeFObjectToTrajectoryFile(fObject *f, ofstream& fout, bool local);
		
		int readFrameFromCheckpointFile(NetworkParameters *params, PhysicalSystem *pSys, ifstream& fin);
		int readFrameFromTrajectoryFile(NetworkParameters *params, PhysicalSystem *pSys, ifstream& fin);
		int readFrameFromTopologyFile(NetworkParameters *params, PhysicalSystem *pSys, ifstream& fin);
		int readFrameFromForceFile(NetworkParameters *params, PhysicalSystem *pSys, ifstream& fin);
		
		//
		// Variables
		//
		int cNumThreads;
		int XMLLevel;
		static const int maxXMLLevel;
		static const vector<string> proteinAttributes, siteAttributes, bondAttributes, nodeAttributes, connectionAttributes;

		// Pointers to objects
		Network *net;
		SimulationBox *box;
};

#endif
