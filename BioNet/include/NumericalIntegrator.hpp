#ifndef NUMINT_H_INCLUDED
#define NUMINT_H_INCLUDED

#include <Eigen/Core>

#include "SystemParameters.hpp"
#include "NetworkPrimitives.hpp"

/*
 * This class is a pure virtual (abstract) class. All methods defined must be implemented in any derived class.
 * The idea here is that the integrator should be initialised based on whether mass exists in the system at the start
 * and then just go straight into the method
 */
class NumericalIntegrator {

	public:

		// Destructor
		virtual ~NumericalIntegrator();

		// Methods
		virtual void integrate(vObject *v, scalar dt) = 0;

};

class InertialEulerIntegrator : public NumericalIntegrator {

	public:

		// Destructor
		InertialEulerIntegrator();
		~InertialEulerIntegrator();

		// Methods
		void integrate(vObject *v, scalar dt);

};

class ViscousEulerIntegrator : public NumericalIntegrator {

	public:

		// Destructor
		ViscousEulerIntegrator();
		~ViscousEulerIntegrator();

		// Methods
		void integrate(vObject *v, scalar dt);

};

#endif
