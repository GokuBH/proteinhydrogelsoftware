#ifndef FORCES_H_INCLUDED
#define FORCES_H_INCLUDED

#include <iostream>
#include <string>
#include <sstream>
#include <cctype>
#include <cmath>
#include <boost/algorithm/string.hpp>
#include <Eigen/Core>

#include "MathematicalFunctions.hpp"
#include "LexicalAnalyser.hpp"

#include "PhysicalUnits.hpp"
#include "PhysicalConstants.hpp"
#include "NetworkPrimitives.hpp"

enum ForceType {NoForce, FConstant, FConstantVelocity, FSteric, FVdW, FWall};
const string forceTypeString[] = {"NoForce", "Constant", "ConstantVelocity", "Steric", "VdW", "Wall"};

string getForceTypeString(ForceType forceType);
ForceType getForceType(string forceTypeString);

class fObject {

	public:

		//
		// Methods
		//

		// Constructor / Destructor
		fObject();
		virtual ~fObject();

		// Setters, Getters & Updaters
		virtual int setParameter(string name, string value) = 0;
		virtual string getParameter(string name) = 0;

		void setPosition(scalar a, scalar b, scalar c);
		void setPosition(Vector3 p);
		Vector3 getPosition();

		void setVelocity(scalar a, scalar b, scalar c);
		void setVelocity(Vector3 v);
		Vector3 getVelocity();
		
		void setIndex(int i);
		int getIndex();

		int getNumTargets();

		void addTarget(vObject* v);
		vObject * getTarget(int index);

		void setForce(Vector3 v);
		void addForce(Vector3 v);
		void zeroForce(Vector3 v);
		Vector3 getForce();

		void setMagnitude(scalar m);
		scalar getMagnitude();

		int setOrientation(string val);
		virtual void setOrientation(scalar theta, scalar phi);
		virtual void setOrientation(scalar x, scalar y, scalar z);

		virtual Vector3 getOrientation();

		virtual void calculateNonPrimitives() = 0;
		
		// Functional methods
		void apply(scalar dt);
		virtual void apply(vObject *v, scalar dt) = 0;

		virtual void printDetails() = 0;

		//
		// Variables
		//
		int index;

	protected:

		//
		// Variables
		//
		int numTargets;
		vector<vObject*> target;
		scalar magnitude;
		Vector3 pos, vel, force;
};

class ConstantForce : public fObject {


	public:

		//
		// Methods
		//

		// Constructor / Destructor
		ConstantForce();
		virtual ~ConstantForce();

		// Setters, Getters & Updaters
		virtual int setParameter(string name, string value);
		virtual string getParameter(string name);

		virtual void calculateNonPrimitives();
		
		virtual void apply(vObject *v, scalar dt);

		virtual void printDetails();
};

class ConstantVelocityForce : public fObject {


	public:

		//
		// Methods
		//

		// Constructor / Destructor
		ConstantVelocityForce();
		virtual ~ConstantVelocityForce();

		// Setters, Getters & Updaters
		virtual int setParameter(string name, string value);
		virtual string getParameter(string name);
		
		virtual void setOrientation(scalar theta, scalar phi);
		virtual void setOrientation(scalar x, scalar y, scalar z);
		virtual Vector3 getOrientation();
				
		void setElasticConstant(scalar k);
		scalar getElasticConstant();
		
		void setSpeed(scalar v);
		scalar getSpeed();

		virtual void calculateNonPrimitives();
		
		virtual void apply(vObject *v, scalar dt);

		virtual void printDetails();
		
	protected:

		// These should be lists / vectors, 1 for each target. Fix if found	
		Vector3 orientation;
		scalar k, v;
		bool posSet;
};
#endif
