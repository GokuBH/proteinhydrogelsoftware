#ifndef GLOBOBJ_H_INCLUDED
#define GLOBOBJ_H_INCLUDED

#include <string>

#include "SystemParameters.hpp"
#include "NetworkFileLogger.hpp"
#include "RNGContainer.hpp"

using namespace std;

namespace GlobalVariables {
	extern NetworkFileLogger *fileLog;
	extern RNGContainer *rngCont;
}

#endif
