#ifndef SIMBOX_H_INCLUDED
#define SIMBOX_H_INCLUDED

#include <string>
#include <sstream>
#include <random>
#include <chrono>
#include <vector>
#include <set>
#include <cmath>
#include <limits>

#include <Eigen/Dense>
#include <Eigen/SparseCore>

#include <exception>

#ifdef _OPENMP
	#include <omp.h>
#endif

#include "NetworkParameters.hpp"
#include "SystemParameters.hpp"
#include "NetworkPrimitives.hpp"
#include "Containers.hpp"
#include "Utilities.hpp"

#include "NetworkFileLogger.hpp"
#include "StatisticalMethods.hpp"
#include "InteractionTypes.hpp"

#include "Network.hpp"

#include "GlobalObjects.hpp"

using namespace std;
using namespace Eigen;

// Enums (for global types of things)
enum BCType {NoBC, PBC, HBC, CBC, RBC, SBC};
const string bcTypeString[] = {"No Boundary Conditions", "Periodic", "Hard", "Cushion", "Radial", "Shear"};


string getBCTypeString(BCType type);

enum EdgeType {NoEdge, BEdge, TEdge};
const string edgeTypeString[] = {"No Edge", "Bottom Edge", "Top Edge"};

string getEdgeTypeString(EdgeType type);

// Voxels make up the box. These contain objects, the box contains voxels
class Voxel {

	public:

		//
		// Methods
		//

		// Constructors / Destructors
		Voxel();
		~Voxel();

		// Setters & Getters
		void setIndex(int i);
		void setIndices(int i, int j, int k);
		int getIndex();
		Vector3i getIndices();

		void setThreadIndex(int i);
		void resetThreadIndex();
		int getThreadIndex();

		void setChecked(Voxel* v, bool b);
		void resetChecked();
		bool isChecked(Voxel* v);
		void setEdge(bool b);
		bool isEdge();

		void setEdgeType(EdgeType type, AxisType XYZ);
		Matrix<EdgeType, 3, 1> getEdgeType();
		EdgeType getEdgeType(AxisType XYZ);
		string getEdgeTypeString(AxisType XYZ);

		int getNumObjects();
		int getNumNeighbours();
		int getNumInteractionNeighbours();

		vObject * getObject(int index);
		Voxel * getNeighbour(int index);
		Voxel * getInteractionNeighbour(int index);

		scalar getStericEnergy();
		scalar getVdWEnergy();
		scalar getWallEnergy();
		
		scalar getConnectorEnergy();
		scalar getTotalEnergy();

		// Energy Methods
		void addStericEnergy(scalar e);
		void addVdWEnergy(scalar e);
		void addWallEnergy(scalar e);
		void addConnectorEnergy(scalar e);

		void zeroStericEnergy();
		void zeroVdWEnergy();
		void zeroWallEnergy();
		void zeroConnectorEnergy();

		// Build methods
		void addObject(vObject *node);
		void removeObject(vObject *node);
		bool containsObject(vObject *node);

		void addNeighbour(Voxel *v);	
		void removeNeighbour(Voxel *v);

		void addInteractionNeighbour(Voxel *v);

		Vector3 getNeighbourTranslation(Voxel *v);
		void setNeighbourTranslation(Voxel *v, Vector3 trans);
		void removeNeighbourTranslation(Voxel *v);

		void clear();

		//
		// Variables
		//
		map<Voxel*, bool> checked;

		int index;
		Vector3i indices;

	private:

		//
		// Methods
		//

		//
		// Variables
		//

		int numObjects;
		int numNeighbours, numInteractionNeighbours;

		// Is it an edge voxel (for boundary condition stuff)
		bool edge;
		Matrix<EdgeType,3,1> edgeType;

		// List of objects in the voxel
		set<vObject*> obj; 

		// Pointers to neighbouring voxels
		set<Voxel*> neighbours, interactionNeighbours;
		map<Voxel*,Vector3> neighbourTransLookup;

		// Coordinates of the voxel
		Vector3 coordinates;

		// Field variables
		scalar vdwEnergy, stericEnergy, wallEnergy, electrostaticEnergy, connectorEnergy;
		scalar totalEnergy;

		// Has this voxel been scanned?
		int threadIndex;
};

//
// Functors
//

// Pure abstract top-level
class PairPairMethod {

	public:

		PairPairMethod();
		virtual ~PairPairMethod();

		// Abstract Functor method
		virtual void operator() (vObject *a, vObject *b, Vector3 trans, Network *net) = 0;

		// Setters & Getters
		virtual int setParameter(string name, string value) = 0;
		virtual string getParameter(string name) = 0;

		void setCutoff(scalar c);
		scalar getCutoff();

		scalar getEnergy();
		scalar getTotalEnergy();

		void setEnergy(scalar e);
		void setTotalEnergy(scalar e);

		vector<PairPair> getPairs();
		void clearPairs();
		void setAssemblyMethod(bool availOnly);
		bool getAssemblyMethod();
		
		virtual void updateFieldEnergy(Voxel *a, Voxel *b) = 0;


		Vector2 getLinearForceConstants();
		Vector2 getTau();
		void setTau(Vector2 v);

		// Initialisation
		virtual void initialise(Network *net) = 0;

		// Validation methods
		virtual bool validate() = 0;

		// Calculation methods
		void zeroEnergy();

		// User info
		virtual void printDetails() = 0;

	protected:

		scalar energy, totalEnergy;
		scalar cutoff;
		Vector2 k;
		Vector2 tau;
		vector<PairPair> pairs;
		bool availOnly;
};

class PairPairSteric : public PairPairMethod {

	public:

		PairPairSteric();
		~PairPairSteric();

		virtual void operator() (vObject *a, vObject *b, Vector3 trans, Network *net);

		// Setters & Getters
		int setParameter(string name, string value);
		string getParameter(string name);
	
		void setEMax(scalar e);
		scalar getEMax();

		void setStericConstant(scalar k);
		scalar getStericConstant();

		// Pass energy to the voxels
		void updateFieldEnergy(Voxel *a, Voxel *b);

		// Initialisation
		void initialise(Network *net);

		// Validation methods
		bool validate();

		// User info
		void printDetails();

	protected:
		
		scalar kst, eMax;
};

class PairPairVdW : public PairPairMethod {

	public:

		PairPairVdW();
		~PairPairVdW();

		virtual void operator() (vObject *a, vObject *b, Vector3 trans, Network *net);

		// Setters & Getters
		int setParameter(string name, string value);
		string getParameter(string name);

		void setREq(scalar r);
		scalar getREq();
		void setEps(scalar e);
		scalar getEps();
		void setVdWCutoff(scalar r);
		scalar getVdWCutoff();
		void setAlpha(scalar a);
		scalar getAlpha();
		
		// Pass energy to the voxels
		void updateFieldEnergy(Voxel *a, Voxel *b);

		// Initialisation
		void initialise(Network *net);

		// Validation methods
		bool validate();

		// User info
		void printDetails();

	protected:

		scalar rEq, rCutoff;
		scalar eps;
		scalar alpha;
};

class PairPairConnect : public PairPairMethod {

	public:

		PairPairConnect();
		PairPairConnect(bool lcs);
		PairPairConnect(bool lcs, scalar c);
		~PairPairConnect();

		virtual void operator() (vObject *a, vObject *b, Vector3 trans, Network *net);

		// Setters & Getters
		int setParameter(string name, string value);
		string getParameter(string name);

		// Pass energy to the voxels
		void updateFieldEnergy(Voxel *a, Voxel *b);

		// Initialisation
		void initialise(Network *net);

		// Validation methods
		bool validate();

		// User info
		void printDetails();

	private:

		bool localConnectivitySet;
};

// Pure abstract top-level
class WallMethod {

	public:

		WallMethod();
		virtual ~WallMethod();

		// Abstract Functor method
		virtual void operator() (vObject *a, EdgeType et, scalar zBoxDim) = 0;

		// Setters & Getters
		virtual int setParameter(string name, string value) = 0;
		virtual string getParameter(string name) = 0;

		// Initialisation
		virtual void initialise(Network *net) = 0;

		// Validation methods
		virtual bool validate() = 0;

		// Calculation methods
		void zeroEnergy();
		scalar getEnergy();
		scalar getTotalEnergy();

		// User info
		virtual void printDetails() = 0;

	protected:

		scalar energy, totalEnergy;
		scalar cutoff;
};

class WallVdW : public WallMethod {

	public:

		WallVdW();
		~WallVdW();

		virtual void operator() (vObject *a, EdgeType et, scalar zBoxDim);

		// Setters & Getters
		int setParameter(string name, string value);
		string getParameter(string name);

		void setREq(scalar r);
		scalar getREq();
		void setEps(scalar e);
		scalar getEps();
		void setVdWCutoff(scalar r);
		scalar getVdWCutoff();
		void setAlpha(scalar a);
		scalar getAlpha();
	
		// Pass energy to the voxels
		void updateFieldEnergy(Voxel *a);

		// Initialisation
		void initialise(Network *net);

		// Validation methods
		bool validate();

		// User info
		void printDetails();

	protected:

		scalar rEq, rCutoff;
		scalar eps;
		scalar alpha;
};

class SimulationBox {

	public:

		//
		// Methods
		//

		// Constructors / Destructors
		SimulationBox();
		~SimulationBox();

		// Setters & getters
		int setParameter(string name, string value);
		string getParameter(string name);

		void setNumVoxels(Vector3i n);
		Vector3i getNumVoxels();

		void setTotalNumVoxels(int n);
		int getTotalNumVoxels();

		void setDimensions(Vector3 d);
		Vector3 getDimensions();
		void setVoxelDimensions(Vector3 d);
		void setVoxelDimensions(scalar d);
		Vector3 getVoxelDimensions();
		
		Voxel * getVoxel(int index);
		Voxel * getVoxel(int i, int j, int k);
		Voxel * getVoxel(Vector3i indices);

		void setShearStress(scalar s);
		scalar getShearStress();

		void setDiagonal(scalar d);
		scalar getDiagonal();

		void setVolume(scalar v);
		scalar getVolume();

		void setCentroid(Vector3 c);
		Vector3 getCentroid();

		int setBCType(string s);
		void setBCType(BCType type);
		BCType getBCType();
		string getBCTypeString();

		void setStericEnergy(scalar e);
		void addStericEnergy(scalar e);
		scalar getStericEnergy();

		void setVdWEnergy(scalar e);
		void addVdWEnergy(scalar e);
		scalar getVdWEnergy();

		void setWallEnergy(scalar e);
		void addWallEnergy(scalar e);
		scalar getWallEnergy();

		scalar getTotalEnergy();

		Vector2 getStericTimeConstants();
		Vector2 getVdWTimeConstants();

		void setNumThreads(int n);
		int getNumThreads();

		// Reset methods
		void resetChecked();

		void zeroStericEnergy();
		void zeroVdWEnergy();
		void zeroWallEnergy();
		void zeroConnectorEnergy();
		void clearConnectors();

		// The main pair-pair interaction methods
		void pairPairInteractions(PairPairMethod **ppM);
		void pairPairInteractions(PairPairMethod *ppM, Voxel *cV);
		void pairPairInteractions(PairPairMethod *ppM, Voxel *cV, Voxel *nV);

		void pairPairInteractions(PairPairMethod *ppM, vObject *v);
		void pairPairInteractions(PairPairMethod *ppM, vObject *v, Voxel *nV, Vector3 trans);

		void wallInteractions(WallMethod **wM);
		void wallInteractions(WallMethod *wM, Voxel *cV);
				
		// And everything that can be passed to it
		void doStericInteractions();
		void doVdWInteractions();
		void doWallInteractions();
		void applyShearForces();
		void doAllInteractions();
		void calculatePairPairList(bool availOnly);
		vector<PairPair> getPairPairList();
//		void reconnectNodes();
		

		//
		// Calculation Methods
		//

		// Structure
		int calculateNumVoxels();
		int calculateVoxelDimensions();
		void calculateDiagonal();
		void calculateCentroid();
		void calculateVolume();

		// Energies
		void zeroTotalEnergy();
		void calculateStericEnergy();
		void calculateVdWEnergy();
		void calculateWallEnergy();
		void calculateTotalEnergy();
		void calculateTotalEnergySum();

		// Coordinates
		int voxelIndexConversion(int i, int j, int k);
		int voxelIndexConversion(Vector3i indices);
		Vector3i voxelIndexConversion(int index);
		Vector3i getPotentialVoxelCoordinates(vObject *v);

		// Build methods
		int build(NetworkParameters *params, Network *n, NetworkFileReader *fileIn);
		int preInitialise();
		int repopulate();
		Voxel * locateContainingVoxel(vObject *v);
		Voxel * locatePotentialVoxel(vObject *v);
		int addObjectToVoxels(vObject *node);
		int removeObjectFromVoxels(vObject *node);
		void clear();

		// Validation methods
		bool validate();
		bool containsObject(vObject *node);

		// Mechanics
		void applyBoundaryConditions();
		Vector3 calculatePeriodicTranslation(vObject *n1, vObject *n2);
		void randomiseContents();
		void randomMove(vObject *node);

		// User info
		void printEnergies();
		void printDetails();

		//
		// Variables
		//
		int numThreads;
		bool exists;
		bool defaulted;

		static const vector<string> paramList;

	private:

		//
		// Methods
		//

		// Deletion methods
		void deleteCalculators();

		// Build methods
		int buildForces(NetworkParameters *params, NetworkFileReader *fileIn);
		int buildConnector(NetworkParameters *params, Network *n);

		int buildDefaultSimulationBox(BCType bc);
		void linkVoxels();
		int addNetworkToSimulationBox();
		int addObjectToVoxel(vObject *node, Vector3i vIndices);
		int removeObjectFromVoxel(vObject *node, Vector3i vIndices);

		// Mechanics
		void applyBoundaryConditions(vObject *node);
		void applyPeriodicBoundaryConditions(vObject *node);
		void applyHardBoundaryConditions(vObject *node);
		void applyHardBoundaryConditions(vObject *node, int i);
		void applyCushionBoundaryConditions(vObject *node);
                void applyRadialBoundaryConditions(vObject *node);

		//
		// Variables
		//

		// A pointer to the contained network
		Network *net;

		Voxel *voxel;

		Vector3i numVoxels;
		int totalNumVoxels;

		set<Voxel*> edgeVoxel;
		set<Voxel*> interiorVoxel;
		set<Voxel*> yMinusVoxel;
		set<Voxel*> yPlusVoxel;
						
		int numEdgeVoxels;
		int numInteriorVoxels;
		int numYMinusVoxels, numYPlusVoxels;
		
		Vector3 dimension;	
		Vector3 voxelDimension;

		BCType bcType;
		bool shear;
		scalar shearStress;
		vector<pair<vObject*,scalar>> shearForces;

		scalar diagonal;
		scalar volume;
		Vector3 centroid;

		vector<PairPair> pairPairList;

		// RNG stuff
		uniform_real_distribution<scalar> uniformDistro;
		mt19937 RNGGen;
		
		// Field variables
		scalar vdwEnergy, stericEnergy, wallEnergy, electrostaticEnergy, connectorEnergy;
		scalar totalEnergy;

		// Flags
		bool stericForcesActive, vdwForcesActive, wallForcesActive, 	bothWallForcesActive, electrostaticForcesActive, connectingActive, shearForcesActive;

		// Functor objects
		PairPairMethod **stericCalculator, **vdwCalculator, **connector;
		WallMethod **wallCalculator;
		
};
#endif
