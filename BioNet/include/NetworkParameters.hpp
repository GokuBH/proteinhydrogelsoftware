#ifndef NETPARAM_H_INCLUDED
#define NETPARAM_H_INCLUDED

#include <string>
#include <sstream>
#include <map>

#include <boost/filesystem.hpp>

#include "SystemParameters.hpp"

#ifdef BIONETFORM
	#include "CommandLineArgsForm.hpp"
#else
	#include "CommandLineArgsSim.hpp"
#endif

#include "Utilities.hpp"
#include "PhysicalConstants.hpp"
#include "PhysicalUnits.hpp"

using namespace PhysicalConstants;
using namespace std;

namespace bfs = boost::filesystem;

class NetworkParameters {
	
	public:

		//
		// Methods
		//

		// Constructors / Destructors
		NetworkParameters();
		~NetworkParameters();

		// Setters & getters
		int setParameter(string name, string value);
		string getParameter(string name);

		int assign(CommandLineArgs *cmArgs);
		int assign(map<string,string> paramMap);

		// Calculation Methods
		void buildFilenames();

		// Validation Methods
		bool validate();

		// User Info
		void printDetails();

		//
		// Parameters
		//

		// Filenames & Global Parameters
		bfs::path iFname;
		bfs::path mFname;
		bfs::path tFname;
		bfs::path cfFname;
		bfs::path tpFname;
		bfs::path cFname;
		bfs::path nmFname;
		bfs::path oFname;

		SimType simType;
		InitType initType;
		vector<InitType> initTypeSequence;

		// Simulation Parameters
		scalar dt;
		long numSteps;
		int numFrames;
		scalar simTime;
		int stepsPerFrame;
		scalar timePerFrame;

		int numNMModes;
		
		// Network Parameters
		bool localConnectivitySet, restartSet;
		scalar viscosity;
		scalar temperature;
		scalar kBT;

		// Forces
		bool thermalForcesActive;
		bool stericForcesActive;
		bool vdwForcesActive;
		bool electrostaticForcesActive;
		bool wallForcesActive;
		bool constantForcesActive;
		bool kineticsActive, bondKineticsActive, proteinKineticsActive;
		bool boxActive;
		bool centraliseNetwork;

		// A list of writables for the NetworkFileWriter
		static const vector<string> paramList; 

	private:

		//
		// Methods
		//
		void calculateNumericalIntegrationParameters();
};
#endif
