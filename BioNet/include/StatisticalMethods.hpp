#ifndef STATMETHODS_H_INCLUDED
#define STATMETHODS_H_INCLUDED

#include <cmath>
#include <algorithm>
#include <vector>
#include <iostream>

#include "GlobalObjects.hpp"

#include "Utilities.hpp"
#include "SystemParameters.hpp" 

#define MAX_NUM_MOMENTS 4

using namespace std;
using namespace GlobalVariables;

class Distribution {

	public:

		//
		// Methods
		//

		// Constructurs / Destructors
		Distribution();
		~Distribution();

		// Setters & Getters
		void setData(vector<scalar> d);
		vector<scalar> getData();

		scalar getMean();
		scalar getVariance();
		scalar getStandardDeviation();
		scalar getMaximum();
		scalar getMinimum();

		// Calculation methods
		void calculateMoments(int n);
		void calculateMoment(int n);
		void calculateCentralMoments(int n);
		void calculateCentralMoment(int n);

		void calculateMean();
		void calculateVariance();
		void calculateLimits();

		//
		// Variables
		//

		bool meanSet;
		vector<scalar> data;
		scalar moments[MAX_NUM_MOMENTS];
		scalar cMoments[MAX_NUM_MOMENTS];

		scalar maximum, minimum;
		scalar *mean;
		scalar *variance;

};
#endif
