#ifndef PHYSCONST_H_INCLUDED
#define PHYSCONST_H_INCLUDED

#include "PhysicalUnits.hpp"

namespace PhysicalConstants {
	static const scalar kB = 1.38064852e-23 / (StandardUnits::Energy / StandardUnits::Temperature);
}
#endif
