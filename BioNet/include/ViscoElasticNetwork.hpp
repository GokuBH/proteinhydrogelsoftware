#ifndef VISCELNET_H_INCLUDED
#define VISCELNET_H_INCLUDED

#include "ElasticNetwork.hpp"

using namespace std;

class ViscoElasticNetwork : public ElasticNetwork {

	public:
		
		ViscoElasticNetwork();
		~ViscoElasticNetwork();

		// Setters & Getters
		scalar* getEigenvalues(SimType simType);
		scalar** getEigenvectors(SimType simType);

		// Construction Methods
		int build(NetworkParameters *params, NetworkFileReader *fileIn, NetworkFileWriter *fileOut);
		int rebuildFromTrajectory(NetworkParameters *params, NetworkFileReader *fileIn, NetworkFileWriter *fileOut);

		// Validation methods
		bool validate(NetworkParameters *params);

		// Dynamic Matrix Methods
		void buildViscosityMatrix();
		void diagonaliseViscosityMatrix();
		void buildViscoElasticMatrix();
		void diagonaliseViscoElasticMatrix();

		// Initialisation Methods
		int preInitialise();
//		int initialise(NetworkParameters *params);

		// Modelling Methods
		int simulate(NetworkParameters *params);
		int brownianSimulate(NetworkParameters *params);
		int langevinSimulate(NetworkParameters *params);

		// Utility Methods
		void printDetails(bool verbose);

	protected:

		// Validation methods 
		bool validateStructure(NetworkParameters *params);
		bool validateSimulation(NetworkParameters *params);

		// Calculation methods
		void calculateViscousForces();
		void calculateThermalForces(RNGContainer *r);

		// Utility Methods
		void buildDenseViscoElasticMatrix();
		void buildSparseViscoElasticMatrix();

		scalar* assignEigenvalues(SelfAdjointEigenSolver<SparseMatrix<scalar>> es);
		scalar* assignEigenvalues(SelfAdjointEigenSolver<MatrixX> es);
		scalar** assignEigenvectors(SelfAdjointEigenSolver<SparseMatrix<scalar>> es);
		scalar** assignEigenvectors(MatrixX evecContainer);

		SparseMatrix<scalar> viscosityMatrix;
		SparseMatrix<scalar> viscoElasticMatrix;
		SparseMatrix<scalar> viscoElasticMatrixPrecon;
		MatrixX viscoElasticMatrixDense;

		SelfAdjointEigenSolver<SparseMatrix<scalar>> VNMES;

		// Is this self adjoint? Perhaps, perhaps not after all the manipulation
		SelfAdjointEigenSolver<SparseMatrix<scalar>> VGNMES;

		SelfAdjointEigenSolver<MatrixX> VGNMDenseES;

		scalar *VNMEvals, **VNMEvecs;
		scalar *VGNMEvals, **VGNMEvecs, *VANMEvals, **VANMEvecs, *VENMEvals, **VENMEvecs;
};
#endif
