#ifndef NETREADER_H_INCLUDED
#define NETREADER_H_INCLUDED

#include <string>
#include <iostream>
#include <fstream>
#include <exception>
#include <list>
#include <set>
#include <vector>
#include <map>
#include <ctime>
#include <boost/algorithm/string.hpp>
#include <boost/filesystem.hpp>
#include "tinyxml2.h"

#include "GlobalObjects.hpp"
#include "SystemParameters.hpp"
#include "NetworkExceptions.hpp"

#include "NetworkParameters.hpp"
#include "NetworkPrimitives.hpp"

using namespace std;
using namespace tinyxml2;
namespace bfs = boost::filesystem;

class NetworkFileReader {

	public:

		/*
		 *  Methods
		 */
		NetworkFileReader();
		~NetworkFileReader();

		// Files that can be read
		int readInputFile(bfs::path iFname);
		int readTrajectoryFile(bfs::path tFname);
		int readTopologyFile(bfs::path tFname);
		int readConstantForceTrajectoryFile(bfs::path cfFname);

		string getParam(string paramName);

		/*
		 *  Containers
		 */
		map<string, string> params;

		map<int, map<string, string>> proteins, bonds;
		map<int, map<int, map<string, string>>> iSites;
		map<int, map<string, string>> nodes, connections;
		map<int, map<string, string>> forces;

		map<string, string> box;

		scalar simTime;

	private:

		/*
		 *  Methods
		 */
		XMLNode * getXMLSection(XMLNode *parentNode, string sectionTitle);

		int readXMLSection(XMLNode *sec);
		int readXMLParameters(XMLNode *sec);
		int readXMLProteins(XMLNode *sec);
		int readXMLBonds(XMLNode *sec);
		int readXMLNodes(XMLNode *sec);
		int readXMLConnections(XMLNode *sec);
		int readXMLBox(XMLNode *sec);
		int readXMLForces(XMLNode *sec);

		int readTrajectoryFrame(ifstream& fin);
		int readTopologyFrame(ifstream& fin);
		int readConstantForceTrajectoryFrame(ifstream& fin);
		
		/*
		 *  Containers
		 */
		static const vector<string> inputScriptTags;

		// Variables
		int XMLLevel;
		static const int maxXMLLevel;
};
#endif
