#ifndef UTILS_H_INCLUDED
#define UTILS_H_INCLUDED

//#include <iostream>
#include <fstream>
#include <cstdlib>
#include <ctime>
#include <string>
#include <sstream>
#include <utility>
#include <vector>
#include <algorithm>

#include <boost/algorithm/string.hpp>
#include <boost/filesystem.hpp>
#include <boost/predef.h>

#include "SystemParameters.hpp"
#include "InteractionTypes.hpp"
#include "LexicalAnalyser.hpp"

using namespace std;
namespace bfs = boost::filesystem;

/*
 * Methods
 */

// Time stuff
string getCurrentTime();
string getCurrentDateTime();

// File stuff
bfs::path getRelativePath(bfs::path a, bfs::path b);
string getAbsolutePath(bfs::path p);
string incrementFileName(bfs::path p);

bool fileExists(string oFname);
int findStringInFile(ifstream &fin, string toFind);
int findSubtringInString(string fullString, string subString);
string readFileAsString(ifstream &fin);
vector<string> convertStringToLines(string fullString);

// String stuff
Vector2 stringToVector2(string s);
Vector2i stringToVector2i(string s);
Vector3 stringToVector3(string s);
Vector3i stringToVector3i(string s);
Matrix3 stringToMatrix3(string s);
vector<int> stringToVectorI(string s);
vector<int> stringToVectorIHard(string s);

string Vector2ToString(Vector2 v);
string Vector2iToString(Vector2i v);
string Vector3ToString(Vector3 v);
string Vector3iToString(Vector3i v);
string VectorIToString(vector<int> v);

string toLower(string s);
bool toBool(string s);
string toString(bool b);

// Sort stuff
bool compareInteractions(Interaction a, Interaction b);

#endif
