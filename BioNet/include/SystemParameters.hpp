#ifndef SYSPARAM_H_INCLUDED
#define SYSPARAM_H_INCLUDED

#include <iostream>
#include <string>
#include <limits>

#include <Eigen/Dense>

using namespace std;
using namespace Eigen;

//
// Return Values
//
const int BioNetError = -1;
const int BioNetSuccess = 0;
const int BioNetWarning = 1;

const bool BioNetValid = true;
const bool BioNetInvalid = false;

// Iterator values
const int BioNetStop = -1;
const int BioNetContinue = -2;

// Typedefs (mostly for memory management)

// So these conditionally, somehow
typedef double scalar;
scalar stos(string s);

// Shorthand infinities
const int iInf = numeric_limits<int>::infinity();
const scalar sInf = numeric_limits<scalar>::infinity();

typedef Matrix<scalar, 3, 1> Vector3;
typedef Matrix<scalar, 2, 1> Vector2;
typedef Matrix<scalar, Dynamic, 1> VectorX;
typedef Matrix<bool, 3, 1> Vector3b;

typedef Matrix<scalar, 3, 3> Matrix3;
typedef Matrix<scalar, Dynamic, Dynamic> MatrixX;
typedef Quaternion<scalar> Quat;
typedef AngleAxis<scalar> AngleAxisS;

// Enums (for global types of things)
enum InitType {NoInit, Relax, Random, Reconnect, MonteCarloI, Sequence, WrongInit};
const int numInitTypes = 4;
const string initTypeString[] = {"NoInitialisation", "Relax", "Random", "Reconnect", "MonteCarlo", "Sequence", "WrongInitialisation"};

enum SimType {NoSim, GNM, ANM, ENM, VGNM, VANM, VENM, Langevin, Brownian, MonteCarloS, WrongSim};
const int numSimTypes = 9;
const string simTypeString[] = {"NoSimulation", "GNM", "ANM", "ENM", "VGNM", "VANM", "VENM", "Langevin", "Brownian", "MonteCarlo", "WrongSimulation"};

enum AxisType {X, Y, Z};
const string axisTypeString[] = {"x", "y", "z"};

// Stand-Alone Methods
InitType getInitType(int i);
string getInitTypeString(InitType initType);

bool initTypeSupported(InitType initType);
bool requiresInitialisation(InitType initType);

SimType getSimType(int i);
string getSimTypeString(SimType simType);

bool simTypeSupported(SimType simType);
bool requiresSimulation(SimType simType);
bool isInertialSimulation(SimType simType);
bool isDynamicSimulation(SimType simType);
bool isNetworkModelSimulation(SimType simType);
bool isPureElasticSimulation(SimType simType);

#endif
