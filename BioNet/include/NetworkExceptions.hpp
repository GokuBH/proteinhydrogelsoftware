#ifndef NETEXCEPT_H_INCLUDED
#define NETEXCEPT_H_INCLUDED

#include <string>
#include <iostream>
#include <exception>

using namespace std;

class ParameterException : public exception {

	public:
		ParameterException();
		ParameterException(string message);
		~ParameterException();
		string error();

	private:

		string message;
};

class EigensystemException : public exception {

	public:
		EigensystemException();
		EigensystemException(string message);
		~EigensystemException();
		string error();

	private:

		string message;
};

#endif
