#ifndef PHYSUNITS_H_INCLUDED
#define PHYSUNITS_H_INCLUDED

// Anything set by the user should be divided by these values
// Anything given to the user should be multiplied by these values
// Anything internal should ignore these values
namespace InternalUnits {

	// Basic
	static const scalar Length = 1;			// User - nm, Internal - nm
	static const scalar Velocity = 1;			// User - nm/ns, Internal - nm/ns

	static const scalar Angle = 1;			// User - Radians, Internal - Radians
	static const scalar AngularVelocity = 1;		// User - Radians / ns, Internal - Radians / ns

	static const scalar Time = 1;				// User - ns, Internal - ns
	static const scalar Frequency = 1;			// User - GHz, Internal - GHz
	static const scalar Temperature = 1;			// User - K, Internal - K
	static const scalar Mass = 1;				// User - zkg = pNns^2/nm, Internal - zkg = pNns^2/nm
	static const scalar MomentOfInertia = 1;		// User - pNns^2.nm, Internal - pNns^2.nm

	static const scalar Charge = 1;			// User - Electron charges, Internal - Electron charges

	// Composite values
	static const scalar Area = 1;				// User - nm^2, Internal - nm^2
	static const scalar Volume = 1;			// User - nm^3, Internal - nm^3
	static const scalar Energy = 1;			// User - pN.nm, Internal - pN.nm
	static const scalar Force = 1;			// User - pN, Internal - pN
	static const scalar Torque = 1;			// User - pN.nm, Internal - pN.nm
	static const scalar Density = 1;			// User - zkg/nm^3, Internal - zkg/nm^3
	static const scalar Viscosity = 1;			// User - pN.nS / nm^2, Internal - pN.nS / nm^2
	static const scalar Drag = 1;				// User - pN.nS/nm, Internal - pN.nS/nm
	static const scalar AngularDrag = 1;			// User - pN.nm.ns, Internal - pN.nm.ns
	static const scalar ElasticConstant = 1;		// User - pN/nm, Internal - pN/nm
	static const scalar AngularElasticConstant = 1;	// User - pN.nm, Internal - pN.nm

	// Unlikely values
	static const scalar Pressure = 1;			// User - pN/nm^2, Internal - pN/nm^2
	static const scalar ChargeDensity = 1.602176e8;	// User - C/m^3, Internal - e/nm^3
	static const scalar Acceleration = 1;			// User - nm/ns^2, Internal - nm/ns^2, hopefully we won't need this
	static const scalar AngularAcceleration = 1;		// User - Radians / ns^2, Internal - Radians / ns^2, hopefully we won't need this
}

// SI Unit conversions
namespace StandardUnits {

	// Basic
	static const scalar Length = 1e-9;			// SI - m, Internal - nm
	static const scalar Velocity = 1;			// SI - m/s, Internal - nm/ns

	static const scalar Angle = 1;			// SI - Radians, Internal - Radians
	static const scalar AngularVelocity = 1e9;		// SI - Radians / s, Internal - Radians / ns

	static const scalar Time = 1e-9;			// SI - s, Internal - ns
	static const scalar Frequency = 1e9;			// SI - Hz, Internal - GHz
	static const scalar Temperature = 1;			// SI - K, Internal - K
	static const scalar Mass = 1e-21;			// SI - kg = Ns^2/m, Internal - pNns^2/nm - zkg

	static const scalar Charge = 1.602176e-19;		// SI - Coulombs, Internal - Electron charges

	// Composite values
	static const scalar Area = 1e-18;			// SI - m^2, Internal - nm^2
	static const scalar Volume = 1e-27;			// SI - m^3, Internal - nm^3
	static const scalar Energy = 1e-21;			// SI - N.m, Internal - pN.nm
	static const scalar Force = 1e-12;			// SI - N, Internal - pN
	static const scalar Torque = 1e-21;			// SI - N, Internal - pN
	static const scalar Density = 1e6;			// SI - kg/m^3, Internal - zkg/nm^3
	static const scalar Viscosity = 1e-3;			// SI - Pa.S = Ns/m^2, Internal - pN.nS / nm^2 = mPa.S
	static const scalar Drag = 1e-12;			// SI - Ns/m, Internal - pN.nS/nm
	static const scalar AngularDrag = 1e-30;		// SI - Nms, Internal - pN.nS.nm
	static const scalar ElasticConstant = 1e-3;		// SI - N/m, Internal - pN/nm
	static const scalar AngularElasticConstant = 1e-21;	// SI - N.m, Internal - pN.nm
	
	// Unlikely values
	static const scalar Pressure = 1e6;			// SI - N/m^2, Internal - pN/nm^2
	static const scalar ChargeDensity = 1.602176e8;	// SI - C/m^3, Internal - e/nm^3
	static const scalar Acceleration = 1e9;		// SI - m/s^2, Internal - nm/ns^2, hopefully we won't need this
	static const scalar AngularAcceleration = 1e18;	// SI - Radians / s^2, Internal - Radians / ns^2
}
#endif
