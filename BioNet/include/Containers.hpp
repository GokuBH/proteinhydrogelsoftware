#ifndef CONTAINERS_H_INCLUDED
#define CONTAINERS_H_INCLUDED

#include "NetworkPrimitives.hpp"

class PairPair {

	public:

		//
		// Methods
		//
		PairPair();
		PairPair(vObject *a, vObject *b);
		PairPair(vObject *a, vObject *b, scalar metric);
		~PairPair();

		//
		// Variables
		//
		vObject* obj[2];
		scalar metric;
		scalar equilibrium;
		scalar energy;
};

#endif
