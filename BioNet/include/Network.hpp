#ifndef NET_H_INCLUDED
#define NET_H_INCLUDED

#include <string>
#include <cmath>
#include <iostream>
#include <map>
#include <ctime>

#include <exception>

#ifdef _OPENMP
	#include <omp.h>
#endif

#include "NetworkExceptions.hpp"
#include "Utilities.hpp"

#include "SystemParameters.hpp"
#include "NetworkFileReader.hpp"
#include "NetworkParameters.hpp"
#include "NetworkPrimitives.hpp"
#include "Forces.hpp"
#include "Containers.hpp"
#include "ContainerMethods.hpp"
#include "StatisticalMethods.hpp"

#include "NumericalIntegrator.hpp"

#include "GlobalObjects.hpp"
#include "RNGContainer.hpp"

// Forward declarations of cyclic objects
class PhysicalSystem;
class SimulationBox;
class NetworkFileWriter;

using namespace std;

/*
 * This class is a pure virtual (abstract) class. All methods defined must be implemented in any derived class.
 * The idea here is that BioNet, or any future network model, can just replace the defined network with any new
 * network type with little effort. The same should be true of all network primitives. Then we can move to any biological network
 * we like.
 */
class Network {

	public:

		// Constructor / Destructor (virtual so all polymorphic classes delete properly)
		virtual ~Network() {
			box = NULL;
			fileOut = NULL;
		};

		// Getters and setters
		virtual int setParameter(string name, string value) = 0;
		virtual string getParameter(string name) = 0;

		virtual void setCentroid(Vector3 p) = 0;
		virtual Vector3 getDimensions() = 0;
		virtual Distribution * getDispersion() = 0;

		virtual void setBox(SimulationBox *b) = 0;
		virtual void setPhysicalSystem(PhysicalSystem *s) = 0;

		// Get object arrays
		virtual int getNumProteins() = 0;
		virtual vTObject ** getProteins() = 0;

		virtual int getNumBonds() = 0;
		virtual cTObject ** getBonds() = 0;
		virtual cTObject * getKineticBond() = 0;

		virtual int getNumNodes() = 0;
		virtual vector<vObject*> getNodes() = 0;

		virtual int getNumConnections() = 0;
		virtual vector<cObject*> getConnections() = 0;

		virtual VectorXd getEigenvalues() = 0;
		virtual MatrixXd getEigenvectors() = 0;

		virtual void resetSaturation() = 0;

		virtual int getNumForces() = 0;
		virtual vector<fObject*> getUniqueForces() = 0;
		
		// Construction functions
		virtual int build(NetworkParameters *params, NetworkFileReader *fileIn, NetworkFileWriter *fileOut) = 0;
		virtual int rebuildFromTrajectory(NetworkParameters *params, NetworkFileReader *fileIn, NetworkFileWriter *fileOut) = 0;

		// Mechanical methods
		virtual int centraliseNetwork() = 0;

		// Validation Methods
		virtual bool validate(NetworkParameters *params) = 0;

		// Initialisation Methods
		virtual int preInitialise() = 0;
		virtual int initialise(NetworkParameters *params) = 0;

		// Modelling Methods
		virtual int simulate(NetworkParameters *params) = 0;

		// Calculation methods

		// This method should be used every time you output to streams
		virtual void calculateMeasureables() = 0;

		virtual void calculateDimensions() = 0;
		virtual void calculateDispersion() = 0;
		virtual void calculateCoordination() = 0;
		virtual scalar getCoordination() = 0;
		
		virtual void calculateCentroid() = 0;
		virtual Vector3 getCentroid() = 0;

		virtual void calculateElasticEnergy() = 0;
		virtual void calculateKineticEnergy() = 0;
		virtual void calculateTotalEnergy() = 0;

		virtual scalar getElasticEnergy() = 0;
		virtual scalar getKineticEnergy() = 0;
		virtual scalar getTotalEnergy() = 0;

		// Debuging methods
		virtual void printDetails(bool verbose) = 0;

	protected:

		NetworkParameters *params;
		PhysicalSystem *pSys;
		SimulationBox *box;
		NetworkFileWriter *fileOut;
};
#endif
