#ifndef CMARGS_H_INCLUDED
#define CMARGS_H_INCLUDED

#include <iostream>
#include <string>
#include <locale>
#include <exception>
#include <getopt.h>

#include <boost/filesystem.hpp>

#include "GlobalObjects.hpp"
#include "SystemParameters.hpp"
#include "Utilities.hpp"
#include "PhysicalConstants.hpp"
#include "PhysicalUnits.hpp"

using namespace std;
using namespace GlobalVariables;
namespace bfs = boost::filesystem;

class CommandLineArgs {
	
	public:
		
		// Constructors / Destructors
		CommandLineArgs();
		~CommandLineArgs();

		// Setters & Getters
		void setInputFname(string fname);
		bfs::path getInputFname();

		void setOutputFname(string fname);
		bfs::path getOutputFname();

		void setTrajectoryFname(string fname);
		bfs::path getTrajectoryFname();

		void setMeasurementFname(string fname);
		bfs::path getMeasurementFname();

		void setTopologyFname(string fname);
		bfs::path getTopologyFname();

		void setCheckpointFname(string fname);
		bfs::path getCheckpointFname();

		void setConstantForceFname(string fname);
		bfs::path getConstantForceFname();
		
		void setNetworkModelFname(string fname);
		bfs::path getNetworkModelFname();
		
		void setLocalConnectivity(bool lCon);
		bool getLocalConnectivity();

		void setRestart(bool res);
		bool getRestart();

		void setVerbosityLevel(bool vLevel);
		bool getVerbosityLevel();

		void setSimType(string simTypeString);
		SimType getSimType();

		void setInitType(string type);
		InitType getInitType();
		vector<InitType> getInitTypeSequence();

		void setNumModes(int n);
		int getNumModes();
		
		void setNumThreads(int n);
		int getNumThreads();

		// Build methods
		int read(int argc, char** argv);

		// Validation Methods		
		bool validate();

		// User info
		void printDetails();

		// Make setters & getters if you can be arsed, for neatness
		int thermalForces, stericForces;
		int electrostaticForces, vdwForces, wallForces, constantForces;
		int kinetics, bondKinetics, proteinKinetics;
		int boxFlag, centerFlag;
	private:

		bool verbosity;
		bool iFnameSet, oFnameSet, tFnameSet, mFnameSet, tpFnameSet, cFnameSet, cfFnameSet, nmFnameSet;
		bfs::path iFname, oFname, tFname, mFname, tpFname, cFname, cfFname, nmFname;
		bool localConnectivity;
		SimType simType;
		InitType initType;
		vector<InitType> initTypeSequence;

		int numNMModes;
				
		int numThreads;

		// Command Line stuff
		void printHelp();

		// Validation Methods		
		bool validateAndBuildFilenames();

};

#endif
