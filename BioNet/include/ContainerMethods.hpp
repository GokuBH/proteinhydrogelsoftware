#ifndef CONTAINERMETHODS_H_INCLUDED
#define CONTAINERMETHODS_H_INCLUDED

#include "Containers.hpp"
#include "NetworkPrimitives.hpp"

bool compareDistances(PairPair a, PairPair b);
bool isSaturated(PairPair a);
bool isReactive(PairPair a);

#endif
