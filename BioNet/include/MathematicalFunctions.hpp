#ifndef MATHFUNC_H_INCLUDED
#define MATHFUNC_H_INCLUDED

#include <cmath>
#include <Eigen/Core>

#include "SystemParameters.hpp"

// Stick all methods in a namespace
namespace MathFunctions {

	Vector2 cartesian(scalar theta);

	Vector3 cartesian(scalar theta, scalar phi);
	Vector3 cartesian(Vector2 sphere);

	Vector3 cartesian(Vector3 sphere);
	Vector3 cartesian(scalar r, scalar theta, scalar phi);

	Vector3 cartesian(Quat q);

	Vector2 spherical2(Vector3 cart);
	Vector2 spherical2(Quat q);
	Vector3 spherical3(Vector3 cart);
	Vector3 spherical3(Quat q);

	Matrix3 eulerToZXZ(Vector3 ori);
	Vector3 ZXZToEuler(Matrix3 mat);
}

#endif
