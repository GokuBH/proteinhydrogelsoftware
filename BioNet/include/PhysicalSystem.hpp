#ifndef PHYSYS_H_INCLUDED
#define PHYSYS_H_INCLUDED

#include <iostream>
#include <string>
#include <fstream>

#include <boost/filesystem.hpp>

#ifdef _OPENMP
	#include <omp.h>
#endif

#include "SystemParameters.hpp"
#include "MathematicalFunctions.hpp"
#include "LexicalAnalyser.hpp"

#ifdef BIONETFORM
	#include "CommandLineArgsForm.hpp"
#else
	#include "CommandLineArgsSim.hpp"
#endif

#include "GlobalObjects.hpp"

#include "Network.hpp"
#include "ElasticNetwork.hpp"
#include "ViscoElasticNetwork.hpp"

#include "SimulationBox.hpp"

// Forward declarations of cyclic objects
class NetworkFileWriter;

enum NetworkType {NoNet, NElastic, NViscous, NViscoElastic};
const string netTypeString[] = {"NoNet", "Elastic", "Viscous", "ViscoElastic"};
enum SimBoxType {NoBox, SBCube};
const string boxTypeString[] = {"NoBox", "Cubic"};

string getNetworkTypeString(NetworkType netType);
string getSimBoxTypeString(NetworkType netType);

class PhysicalSystem {

	public:

		//
		// Methods
		//

		// Constructors / Destructors
		PhysicalSystem();
		~PhysicalSystem();

		// Setters & Getters
		void setBuilt(bool b);
		bool isBuilt();

		void setSimTime(scalar time);
		void updateSimTime(scalar time);

		scalar getSimTime();

		void setPreInitialised(bool b);
		bool isPreInitialised();

		void setInitialised(bool b);
		bool isInitialised();

		void setSimulated(bool b);
		bool isSimulated();

		NetworkParameters * getParameters();
		Network * getNetwork();
		SimulationBox * getSimulationBox();

		// Build methods
		int build(CommandLineArgs *cmArgs, NetworkFileReader *fileIn, NetworkFileWriter *fileOut);
		int rebuildFromTrajectory(CommandLineArgs *cmArgs, NetworkFileReader *fileIn, NetworkFileWriter *fileOut);

		// Validation methods
		bool validate();
		bool globalValidate();

		// Initialisation methods
		int preInitialise(scalar simTime);
		int initialise();

		// Simulaton methods
		int simulate();

		void printDetails(bool verbose);

		//
		// Variables
		//
		scalar simTime;

	private:

		//
		// Methods
		//

		//
		// Variables
		//
		
		bool built;
		bool preInitialised, initialised, simulated;

		NetworkType netType;
		SimBoxType boxType;

		NetworkParameters *params;
		Network *net;
		SimulationBox *box;

		NetworkFileWriter *fileOut;

		
};
#endif
