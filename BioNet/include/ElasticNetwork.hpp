#ifndef ELNET_H_INCLUDED
#define ELNET_H_INCLUDED

#include <fstream>
#include <set>
#include <set>
#include <vector>
#include <utility>
#include <algorithm>
#include <random>
#include <chrono>
#include <ctime>
#include <Eigen/Sparse>
#include <Eigen/Dense>
#include <Eigen/Eigenvalues>

#include <Spectra/SymEigsSolver.h>
#include <Spectra/MatOp/SparseSymMatProd.h>

#include "Network.hpp"

using namespace std;

class ElasticNetwork : public Network {

	public:
		//
		// Methods
		//

		ElasticNetwork();
		~ElasticNetwork();

		// Setters, Getters & Updaters
		int setParameter(string name, string value);
		string getParameter(string name);

		void setNumThreads(int n);
		int getNumThreads();

		void setNumProteins(int n);
		int getNumProteins();
		void setNumBonds(int n);
		int getNumBonds();

		void setNumNodes(int n);
		int getNumNodes();
		void setNumConnections(int n);
		int getNumConnections();
		int getNumForces();

		void setNumChildren(int n);
		int getNumChildren();

		void setPosition(scalar x, scalar y, scalar z);
		void setPosition(Vector3 p);
		Vector3 getPosition();

		void setCentroid(scalar x, scalar y, scalar z);
		void setCentroid(Vector3 p);
		Vector3 getCentroid();

		Vector3 getDimensions();
		Distribution * getDispersion();

		void setOrientation(scalar theta, scalar phi);
		void setOrientation(Vector2 o);
		void setOrientation(Quat q);
		Quat getOrientation();

		// Get objects arrays and individual objects
		vTObject * getProtein(string pName);
		vTObject * getProtein(int index);
		vTObject ** getProteins();

		cTObject * getBond(string bName);
		cTObject * getBond(int index);
		cTObject ** getBonds();
		cTObject * getKineticBond();

		vObject * getNode(int index);
		vector<vObject*> getNodes();

		cObject * getConnection(int index);
		vector<cObject*> getConnections();

		vector<fObject*> getUniqueForces();
		
		VectorXd getEigenvalues();
		MatrixXd getEigenvectors();

		scalar getElasticEnergy();
		scalar getKineticEnergy();

		scalar getBondEnergy();
		void zeroBondEnergy();

		scalar getTotalEnergy();
		void zeroTotalEnergy();

		void setBox(SimulationBox *b);
		void setPhysicalSystem(PhysicalSystem *s);
		
		void resetSaturation();
		void recalculateSaturation();

		//
		// Construction functions
		//
		int build(NetworkParameters *params, NetworkFileReader *fileIn, NetworkFileWriter *fileOut);
		int rebuildFromTrajectory(NetworkParameters *params, NetworkFileReader *fileIn, NetworkFileWriter *fileOut);

		// Template objects
		int buildProteins(map<int, map<string,string>> proteinMap, map<int, map<int, map<string, string>>> iSiteMap, NetworkParameters* params);
		int buildBonds(map<int, map<string,string>> bondMap, NetworkParameters* params);

		// Actual objects
		int buildNodes(map<int, map<string,string>> nodeMap, NetworkParameters *params);
		int buildConnections(map<int, map<string,string>> connectionMap, NetworkParameters *params);

		int buildForces(map<int, map<string,string>> forceMap, NetworkParameters *params);

		// Validation
		bool validate(NetworkParameters *params);

		// Record all initial positions and stuff
		void setInitialState();

		//
		// Initialisation Methods
		//
		void initialiseRNG();
		int preInitialise();
		int initialise(NetworkParameters *params);

		int randomInitialise();
		int reconnectionInitialise(NetworkParameters *params);
		int relaxationInitialise(NetworkParameters *params);

		//
		// Solution Methods
		//

		// Network Diagonalisation		
		void buildLaplacianMatrix();
		void buildAnisotropicMatrix();
		void buildElasticityMatrix();

		void diagonaliseNetworkModelMatrix();

		// Explicit Modelling
		int simulate(NetworkParameters *params);

		// Extrinsic / Compound properties
		void calculateElasticForces();
//		void calculateViscousForces();
		void calculateUniqueForces(scalar dt);
		void calculateElasticEnergy();
		void calculateKineticEnergy();
		void calculateBondEnergy();
		void calculateTotalEnergy();

		int doKinetics(NetworkParameters *params);
		int doBondKinetics(NetworkParameters *params);
		int doProteinKinetics(NetworkParameters *params);

		// Mechanical Methods
		int centraliseNetwork();
		void translate(Vector3 trans);

		// Calculation methods
		virtual void calculateMeasureables();
		void calculateEulerAngles();
		void calculateOrientationQuaternions();
		void calculateReactionProbabilities();
		void calculateDimensions();
		void calculateDispersion();
		void calculateCoordination();
		scalar getCoordination();

		void calculateCentroid();

		scalar calculateEntropy();
		scalar calculateFreeEnergy(NetworkParameters *params);

		void findConnectedNodes(int baseIndex, set<int>* connectedNodes);

		// Info methods
		void printDetails(bool verbose);
		void printProteinDetails();
		void printBondDetails();
		void printNodeDetails();
		void printConnectionDetails();
		void printForceDetails();

		// Eigen hack
		EIGEN_MAKE_ALIGNED_OPERATOR_NEW

	protected:
		
		//
		// Methods
		//

		// Setters, Getters & Updaters
		void zeroElasticEnergy();
		void zeroKineticEnergy();

		// Initialisation methods
		int initialiseKinetics();

		// Build methods
		int addNode(vObject *v);
		void removeNode(int index);
		void removeNode(vObject *v);

		int addConnection(cObject *c, vector<vObject*> terminals);
		int addConnection(cObject *c);

		void removeConnection(int index);
		void removeConnection(cObject *c);

		// Deletion methods
		void deleteProteins();
		void deleteBonds();
		void deleteNodes();
		void deleteConnections();
		void deleteForces();
		
		// Validation methods
		bool validateStructure(NetworkParameters *params);
		bool validateSimulation(NetworkParameters *params);

		// Extrinsic / Compound properties
		void applyForces(scalar dt);
		void zeroForces();

		vector<PairPair> calculatePairPairList(NetworkParameters *params, bool availOnly);

		//
		// Simulation methods
		//
		int randomise();

		//
		// Relaxation Methods
		//
//		int relax(NetworkParameters *params);
//		int relaxSteric(NetworkParameters *params, ofstream& tFout, ofstream& mFout);
//		int relaxVdW(NetworkParameters *params, ofstream& tFout, ofstream& mFout);
//		int relaxElastic(NetworkParameters *params, ofstream& tFout, ofstream& mFout);
//		int relaxAll(NetworkParameters *params, ofstream& tFout, ofstream& mFout);

//		void calculateRelaxationParameters(NetworkParameters *params);
//		void calculateStericRelaxationParameters(NetworkParameters *params);
//		void calculateVdWRelaxationParameters(NetworkParameters *params);

		//
		// Variables
		//

		// Software stuff
		int numThreads;

		// Mechanical stuff
		Vector3 pos;

		Vector3 centroid, dimension, dimensionLL, dimensionUL;

		Quat orientation;

		// Polymorphic objects so we can easily add new objects, templated by the virtual classes
		int numProteins, numBonds, numNodes, numConnections, numUniqueForces;

		vTObject **protein;
		cTObject **bond;

		vector<vObject*> node;
		set<int> activeNIndex, deletedNIndex;
		vector<int> freeNode;

		vector<cObject*> connection;
		set<int> activeCIndex, deletedCIndex;

		cTObject *kineticBondTemplate;
		vTObject *kineticProteinTemplate;

		vector<fObject*> force;
		NumericalIntegrator *integrator;

		SparseMatrix<scalar> stiffnessMatrix;
		VectorXd nmEigenvalues;
		MatrixXd nmEigenvectors;
		
		scalar coordination;
		Vector2 tauE, tauI, tauR;
		Distribution dispersion;

		scalar elasticEnergy, kineticEnergy, bondEnergy, totalEnergy;

		InitType initType;
		vector<InitType> initTypeSequence;
		SimType simType;

		bool localConnectivitySet;
};
#endif
