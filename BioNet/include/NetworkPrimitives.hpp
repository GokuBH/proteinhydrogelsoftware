#ifndef NETPRIM_H_INCLUDED
#define NETPRIM_H_INCLUDED

#include <iostream>
#include <string>
#include <sstream>
#include <cctype>
#include <cmath>
#include <algorithm>
#include <boost/algorithm/string.hpp>
#include <Eigen/Core>

#include "NetworkParameters.hpp"
#include "SystemParameters.hpp"
#include "MathematicalFunctions.hpp"
#include "LexicalAnalyser.hpp"
#include "Utilities.hpp"
#include "PhysicalUnits.hpp"
#include "PhysicalConstants.hpp"

#include "GlobalObjects.hpp"

using namespace std;
using namespace Eigen;
using namespace PhysicalConstants;
using namespace GlobalVariables;

enum ProteinType {NoProt, PPoint, PSphere, PEllipsoid, PBlob};
const string proteinTypeString[] = {"NoProt", "Point", "Sphere", "Ellipsoid", "Blob"};
enum BondType {NoBond, BRigid, BSpring, BFENE, BAngle, BWLC};
const string bondTypeString[] = {"NoBond", "Rigid", "Spring", "FENE", "Angular", "WLC"};
enum ISiteType {NoISite, ISPoint, ISCircle};
const string iSiteTypeString[] = {"NoISite", "Point", "Circle"};

string getProteinTypeString(ProteinType proteinType);
ProteinType getProteinType(string pTypeString);

string getBondTypeString(BondType bondType);
BondType getBondType(string bTypeString);

string getISiteTypeString(ISiteType iSiteType);
ISiteType getISiteType(string iTypeString);

// Forward declarations for circular pointers
class vObject;
class cObject;
class PointNode;
class SphereNode;
class EllipsoidNode;

// Pair-pair methods for all possible interaction types
scalar calculateVolumeOverlap(PointNode *a, PointNode *b);
scalar calculateVolumeOverlap(PointNode *a, SphereNode *b);
scalar calculateVolumeOverlap(PointNode *a, EllipsoidNode *b);
scalar calculateVolumeOverlap(SphereNode *a, SphereNode *b);
scalar calculateVolumeOverlap(SphereNode *a, EllipsoidNode *b);
scalar calculateVolumeOverlap(EllipsoidNode *a, EllipsoidNode *b);

scalar calculateVolumeOverlapGradient(PointNode *a, PointNode *b);
scalar calculateVolumeOverlapGradient(PointNode *a, SphereNode *b);
scalar calculateVolumeOverlapGradient(PointNode *a, EllipsoidNode *b);
scalar calculateVolumeOverlapGradient(SphereNode *a, SphereNode *b);
scalar calculateVolumeOverlapGradient(SphereNode *a, EllipsoidNode *b);
scalar calculateVolumeOverlapGradient(EllipsoidNode *a, EllipsoidNode *b);

Vector3 calculateVolumeOverlapParameters(PointNode *a, PointNode *b);
Vector3 calculateVolumeOverlapParameters(PointNode *a, SphereNode *b);
Vector3 calculateVolumeOverlapParameters(PointNode *a, EllipsoidNode *b);
Vector3 calculateVolumeOverlapParameters(SphereNode *a, SphereNode *b, Vector3 trans);
Vector3 calculateVolumeOverlapParameters(SphereNode *a, EllipsoidNode *b, Vector3 trans);
Vector3 calculateVolumeOverlapParameters(EllipsoidNode *a, EllipsoidNode *b, Vector3 trans);

scalar calculateStericInteraction(scalar kst, PointNode *a, PointNode *b, Vector3 trans);
scalar calculateStericInteraction(scalar kst, PointNode *a, SphereNode *b, Vector3 trans);
scalar calculateStericInteraction(scalar kst, PointNode *a, EllipsoidNode *b, Vector3 trans);
scalar calculateStericInteraction(scalar kst, SphereNode *a, SphereNode *b, Vector3 trans);
scalar calculateStericInteraction(scalar kst, SphereNode *a, EllipsoidNode *b, Vector3 trans);
scalar calculateStericInteraction(scalar kst, EllipsoidNode *a, EllipsoidNode *b, Vector3 trans);

scalar calculateVdWInteraction(scalar req, scalar eps, scalar alpha, scalar cutoff, PointNode *a, PointNode *b, Vector3 trans);
scalar calculateVdWInteraction(scalar req, scalar eps, scalar alpha, scalar cutoff, PointNode *a, SphereNode *b, Vector3 trans);
scalar calculateVdWInteraction(scalar req, scalar eps, scalar alpha, scalar cutoff, PointNode *a, EllipsoidNode *b, Vector3 trans);
scalar calculateVdWInteraction(scalar req, scalar eps, scalar alpha, scalar cutoff, SphereNode *a, SphereNode *b, Vector3 trans);
scalar calculateVdWInteraction(scalar req, scalar eps, scalar alpha, scalar cutoff, SphereNode *a, EllipsoidNode *b, Vector3 trans);
scalar calculateVdWInteraction(scalar req, scalar eps, scalar alpha, scalar cutoff, EllipsoidNode *a, EllipsoidNode *b, Vector3 trans);



//
// Note for devs. Standard method signitures are used here on dervied level objects, and on parent objects only if we don't want them overridden. virtual is used here when they can be overridden, but it isn't strictly necessary. virtual = 0 is used when the method must 100% be overridden.
//

// Top level object. Contains parameters and methods signatures which are common to all template objects
// Getters & Setters are held within the 'getParameter' method, which loops over all objects
// Cannot be instantiated
class tObject {

	public:

		//
		// Methods
		//

		// Constructor / Destructor
		tObject();
		virtual ~tObject();

		// Setters, Getters & Updaters
		virtual int setParameter(string name, string value) = 0;
		virtual string getParameter(string name) = 0;

		void setIndex(int i);
		int getIndex();

		void setName(string s);
		string getName();

		virtual string getTypeString() = 0;

		// Validation methods
		virtual bool validate(NetworkParameters *params) = 0;

		// Info
		virtual void printDetails() = 0;

		//
		// Variables
		//
		int tIndex;
		string name;
};

// First level object. Contains parameters and methods signatures which are common to all vertex template objects
// Cannot be instantiated
class vTObject : public tObject {

	public:

		//
		// Methods
		//

		// Constructor / Destructor
		vTObject();
		virtual ~vTObject();

		//
		// Pure overrides from nObject
		//

		// Setters, Getters & Updaters
		virtual int setParameter(string name, string value);
		virtual string getParameter(string name);

		//
		// Methods
		//

		// Setters, Getters & Updaters
		void setRadius(scalar r);
		scalar getRadius();

		int getNumTSites();

		void setDensity(scalar d);
		scalar getDensity();

		void setMass(scalar m);
		scalar getMass();
		
		void setViscosity(scalar v);
		scalar getViscosity();

		void setDrag(scalar d);
		scalar getDrag();

		void setRotationalDrag(scalar d);
		scalar getRotationalDrag();

		void setMomentOfInertia(scalar I);
		scalar getMomentOfInertia();

		void setChargeDensity(scalar d);
		scalar getChargeDensity();
		void setVdWPotential(scalar p);
		scalar getVdWPotential();
		void setVdWEquilibrium(scalar l);
		scalar getVdWEquilibrium();

		void setReactionRate(scalar r);
		scalar getReactionRate();

		int setRelativePosition(string val, bool normalise);
		void setRelativePosition(scalar theta, scalar phi);
		void setRelativePosition(scalar x, scalar y, scalar z);
		Vector3 getRelativePosition();

		void setMaxNumConnections(int n);
		int getMaxNumConnections();

		vTObject * getTSite(int index);
		int addTSite(vTObject *n);
		int removeTSite(int index);
		int removeTSite(vTObject *n);
		void removeTSites();

		//
		// Construction functions
		//
		virtual int buildSites(map<int, map<string, string>> siteMap, NetworkParameters* params) = 0;

		// Validation methods
		virtual bool validate(NetworkParameters *params) = 0;

		// Info
		virtual void printDetails();

		// Eigen Hack
		EIGEN_MAKE_ALIGNED_OPERATOR_NEW

	protected:

		//
		// Variables
		//
		scalar radius, density, mass, viscosity, drag, aDrag, momentOfInertia;
		int maxNumConnections;

		scalar chargeDensity, vdwPotential, vdwEquilibrium;
		scalar reactionRate;

		Vector3 relPos;

		int numTSites;
		vector<vTObject*> tSite;
};

// First level object. Contains parameters and methods signatures which are common to all connection template objects
// Cannot be instantiated
class cTObject : public tObject {

	public:

		//
		// Methods
		//

		// Constructor / Destructor
		cTObject();
		virtual ~cTObject();

		// Setters, Getters & Updaters
		virtual int setParameter(string name, string value) = 0;
		virtual string getParameter(string name) = 0;

		//
		// Methods
		//

		// Setters, Getters & Updaters
		void setElasticConstant(scalar k);
		scalar getElasticConstant();
		void setEquilibriumLength(scalar l);
		scalar getEquilibriumLength();

		void setElasticConstantLimit(scalar k);
		scalar getElasticConstantLimit();
		void setMaximumLength(scalar l);
		scalar getMaximumLength();
		
		void setCutoff(scalar c);
		scalar getCutoff();

		void setBondEnergy(scalar E);
		scalar getBondEnergy();

		void setKinetic(bool b);
		bool isKinetic();

		// Validation methods
		virtual bool validate(NetworkParameters *params) = 0;

		// Info
		virtual void printDetails();

	protected:

		//
		// Variables
		//

		bool kineticBond;
		scalar k, l, kLim, lm, cutoff, bondEnergy;
};

// First level object. Defines the protein template, containing network and geometry independent protein properties
// Cannot be instantiated
class Protein : public vTObject {

	public:

		//
		// Methods
		//

		// Setters, Getters & Updaters
		Protein();
		~Protein();

		//
		// Pure overrides from tObject
		//

		// Setters, Getters & Updaters
		virtual int setParameter(string name, string value);
		virtual string getParameter(string name);
		
		virtual string getTypeString();

		//
		// Pure overrides from vTObject
		//
		int buildSites(map<int, map<string, string>> siteMap, NetworkParameters* params);

		//
		// Methods
		//

		// Setters, Getters & Updaters
		int setType(string s);
		void setType(ProteinType type);
		ProteinType getType();

		// Validation methods
		bool validate(NetworkParameters *params);

		// Info methods
		void printDetails();

		//
		// Variables
		//


	protected:

		//
		// Variables
		//
		ProteinType type;
};

// First level object. Defines the bond template, containing network and geometry independent interaction properties
// Cannot be instantiated
class Bond : public cTObject {

	public:

		//
		// Methods
		//

		// Setters, Getters & Updaters
		Bond();
		~Bond();

		//
		// Pure overrides from tObject
		//
		virtual int setParameter(string name, string value);
		virtual string getParameter(string name);

		virtual string getTypeString();

		//
		// Methods
		//

		// Setters, Getters & Updaters
		int setType(string s);
		void setType(BondType type);
		BondType getType();

		// Validation methods
		bool validate(NetworkParameters *params);

		// Info methods
		void printDetails();

		//
		// Variables
		//

	protected:

		//
		// Variables
		//
		BondType type;
};

// First level object. Defines the interaction site template, containing network and geometry independent interaction properties
// Cannot be instantiated
class InteractionSite : public vTObject {

	public:

		//
		// Methods
		//

		// Constructor / Destructor
		InteractionSite();
		~InteractionSite();

		//
		// Pure overrides from tObject
		//

		virtual int setParameter(string name, string value);
		virtual string getParameter(string name);

		virtual string getTypeString();

		//
		// Overrides from vTObject
		//

		//
		// Methods
		//

		// Setters, Getters & Updaters
		int setType(string s);
		void setType(ISiteType type);
		ISiteType getType();

		//
		// Construction functions
		//
		int buildSites(map<int, map<string, string>> siteMap, NetworkParameters* params);

		// Validation methods
		bool validate(NetworkParameters *params);

		// Info methods
		void printDetails();


	protected:

		//
		// Variables
		//

		ISiteType type;

};

// Top level object. Contains parameters and method signatures which are common to all objects within a network
// Cannot be instantiated 
class nObject {

	public:

		//
		// Methods
		//

		// Constructor / Destructor
		nObject();
		virtual ~nObject();

		// Setters, Getters & Updaters
		virtual int setParameter(string name, string value) = 0;
		virtual string getParameter(string name) = 0;

		void setIndex(int i);
		int getIndex();

		int getStructureDimension();

		// Validation methods
		virtual bool validate(NetworkParameters *params) = 0;

		// Info
		virtual void printDetails() = 0;

		//
		// Variables
		//
		int index;

	protected:

		//
		// Variables
		//
		int structureDimension;
};

// First level object. Contains parameters and method signatures which are common to all vertex / node objects within a network
// Cannot be instantiated 
class vObject : public nObject {

	public:

		//
		// Methods
		//

		// Constructor / Destructor
		vObject();
		virtual ~vObject();

		//
		// Pure overrides from nObject
		//

		// Setters, Getters & Updaters
		virtual int setParameter(string name, string value) = 0;
		virtual string getParameter(string name) = 0;

		// Validation methods
		virtual bool validate(NetworkParameters *params) = 0;

		// Info methods
		virtual void printDetails();

		//
		// Methods
		//

		// Setters, Getters & Updaters
		int getTemplateIndex();
		vTObject * getTemplate();

		int getParentIndex(int i);
		int getParentIndex();
		int getTopParentIndex();

		void setParent(vObject *p);
		vObject * getParent();
		vObject * getParent(int i);
		vObject * getTopParent();

		virtual void setPosition(scalar x, scalar y, scalar z);
		virtual void setPosition(Vector3 v);
		virtual void setInitialPosition(Vector3 v);
		Vector3 getPosition();

		virtual void setRelativePosition(scalar x, scalar y, scalar z);
		virtual void setRelativePosition(Vector3 v);
		virtual void setInitialRelativePosition(Vector3 v);
		Vector3 getRelativePosition();
		Vector3 getInitialRelativePosition();

		virtual void setVelocity(scalar x, scalar y, scalar z);
		virtual void setVelocity(Vector3 v);
		virtual void setInitialVelocity(Vector3 v);
		Vector3 getVelocity();

		virtual void setAcceleration(scalar x, scalar y, scalar z);
		virtual void setAcceleration(Vector3 v);
		virtual void setInitialAcceleration(Vector3 v);
		Vector3 getAcceleration();

		virtual void setOrientation(scalar alpha, scalar beta, scalar gamma);
		virtual void setOrientation(Vector3 v);
		virtual void setInitialOrientation(Vector3 v);
		Vector3 getOrientation();

		void setOrientationMatrix(Matrix3 m);
		Matrix3 getOrientationMatrix();
		virtual void applyOrientationMatrix(Matrix3 m);
		Quat getOrientationQuaternion();

		virtual int setAngularVelocity(string val);
		virtual void setAngularVelocity(scalar x, scalar y, scalar z);
		virtual void setAngularVelocity(Vector3 o);
		virtual void setAngularVelocity(Quat q);
		Quat getAngularVelocity();

		virtual int setAngularAcceleration(string val);
		virtual void setAngularAcceleration(scalar x, scalar y, scalar z);
		virtual void setAngularAcceleration(Vector3 o);
		virtual void setAngularAcceleration(Quat q);
		Quat getAngularAcceleration();

		Vector3 getViscousForce();
		Vector3 getThermalForce();

		void setExternalForce(scalar x, scalar y, scalar z);
		void setExternalForce(Vector3 v);
		Vector3 getExternalForce();
		virtual void addExternalForce(Vector3 v);
		virtual void addExternalForce(scalar s, int direction); 

		Vector3 getTotalForce();
		void zeroTotalForce();
		void zeroTotalForce(int direction);

		Vector3 getViscousTorque();
		Vector3 getThermalTorque();

		Vector3 getExternalTorque();
		virtual void addExternalTorque(Vector3 v);

		Vector3 getTotalTorque();
		void zeroTotalTorque();

		scalar getKineticEnergy();
		scalar getTotalEnergy();
		void zeroTotalEnergy();

		void setMass(scalar m);
		scalar getMass();
		
		void setDrag(scalar d);
		scalar getDrag();

		void setRotationalDrag(scalar d);
		scalar getRotationalDrag();

		void setMomentOfInertia(scalar I);
		scalar getMomentOfInertia();

		void setMaxNumConnections(int n);
		int getMaxNumConnections();

		void setWrap(Vector3 v);
		void setWrap(int direction, int w);
		void addWrap(int direction, int w);
		Vector3 getWrap();
		scalar getWrap(int direction);

		void setFrozen(bool f);
		bool isFrozen();

		void setFrozen(bool f, int direction);
		bool isFrozen(int direction);

		void setReactionRate(scalar r);
		scalar getReactionRate();
		virtual void calculateReactionProbability(scalar dt);
		scalar getReactionProbability();
		void setReactive(bool r);
		bool isReactive();

		int getNumConnections();

		cObject * getConnection(int index);
		virtual int addConnection(cObject *c);
		virtual int removeConnection(int index);
		virtual int removeConnection(cObject *c);
		virtual void removeConnections();
		
		void setSaturated(bool b);
		bool isSaturated();

		ProteinType getProteinType();

		// Extrinsic / Compound properties
		virtual void calculateNoiseMagnitude(scalar kBT, scalar dt);
		virtual void calculateRotationalNoiseMagnitude(scalar kBT, scalar dt);

		virtual void calculateViscousForce();
		virtual void calculateThermalForce(scalar rn1, scalar rn2, scalar rn3);
		virtual void calculateTotalForce();

		virtual void calculateViscousTorque();
		virtual void calculateThermalTorque(scalar rn1, scalar rn2, scalar rn3);
		virtual void calculateTotalTorque();

		virtual void calculateKineticEnergy();
		virtual void calculateTotalEnergy();

		void calculateEulerAngles();
		void calculateOrientationQuaternion();

		// Mechanical methods
		virtual void translate(Vector3 t);
		virtual void rotate(Matrix3 m);
		virtual void rotate(Quat q);


		//
		// Must be overridden for any object inheriting from vObject
		//

		// Setters, Getters & Updaters
		virtual scalar getLengthscale() = 0;
		virtual int getNumChildren() = 0;

		virtual vObject * getChild(int index) = 0;		

		// Calculation methods
		virtual void calculateExtrinsicProperties(NetworkParameters *params) = 0;

		//
		// Pair-Pair Interactions
		//
		virtual scalar calculateVolumeOverlap(vObject *v) = 0;
		virtual scalar calculateStericInteraction(scalar kst, vObject *v, Vector3 trans) = 0;
		virtual scalar calculateVdWInteraction(scalar req, scalar eps, scalar alpha, scalar cutoff, vObject *v, Vector3 trans) = 0;

		virtual scalar calculateVolumeOverlap(PointNode *p) = 0;
		virtual scalar calculateVolumeOverlap(SphereNode *s) = 0;
		virtual scalar calculateVolumeOverlap(EllipsoidNode *e) = 0;

		virtual scalar calculateStericInteraction(scalar kst, PointNode *p, Vector3 trans) = 0;
		virtual scalar calculateStericInteraction(scalar kst, SphereNode *s, Vector3 trans) = 0;
		virtual scalar calculateStericInteraction(scalar kst, EllipsoidNode *e, Vector3 trans) = 0;

		virtual scalar calculateVdWInteraction(scalar req, scalar eps, scalar alpha, scalar cutoff, PointNode *p, Vector3 trans) = 0;
		virtual scalar calculateVdWInteraction(scalar req, scalar eps, scalar alpha, scalar cutoff, SphereNode *s, Vector3 trans) = 0;
		virtual scalar calculateVdWInteraction(scalar req, scalar eps, scalar alpha, scalar cutoff, EllipsoidNode *s, Vector3 trans) = 0;

		// Applies template object details to the vObject
		virtual void applyTemplate(vTObject *vTO, NetworkParameters *params) = 0;

		//
		// Variables
		//

		// Mechanical Stuff
		Vector3 pos, relPos, vel, acc;
		Vector3 pos0, relPos0, vel0, acc0;

		// Extrinsic ZXZ Euler angles
		Vector3 orientation, orientation0;
		Matrix3 orientationMatrix;
		Quat orientationQuaternion;

		// Angular mechanics
		Quat aVel, aAcc;
		Quat aVel0, aAcc0;

		// Eigen Hack
		EIGEN_MAKE_ALIGNED_OPERATOR_NEW
		
	protected:

		//
		// Methods
		//
		void setViscousForce(scalar x, scalar y, scalar z);
		void setViscousForce(Vector3 v);
		void zeroViscousForce();
		void addViscousForce(Vector3 v);

		void setThermalForce(scalar x, scalar y, scalar z);
		void setThermalForce(Vector3 v);
		void zeroThermalForce();
		void addThermalForce(Vector3 v);

		void zeroExternalForce();

		void setTotalForce(scalar x, scalar y, scalar z);
		void setTotalForce(Vector3 v);
		void addTotalForce(Vector3 v);

		void setViscousTorque(scalar x, scalar y, scalar z);
		void setViscousTorque(Vector3 v);
		void zeroViscousTorque();
		void addViscousTorque(Vector3 v);

		void setThermalTorque(scalar x, scalar y, scalar z);
		void setThermalTorque(Vector3 v);
		void zeroThermalTorque();
		void addThermalTorque(Vector3 v);

		void setExternalTorque(scalar x, scalar y, scalar z);
		void setExternalTorque(Vector3 v);
		void zeroExternalTorque();

		void setTotalTorque(scalar x, scalar y, scalar z);
		void setTotalTorque(Vector3 v);
		void addTotalTorque(Vector3 v);

		void setKineticEnergy(scalar E);
		void zeroKineticEnergy();
		void addKineticEnergy(scalar E);

		void setTotalEnergy(scalar E);
		void addTotalEnergy(scalar E);

		//
		// Variables
		//

		// Connection details
		int numConnections, maxNumConnections;

		vector<cObject*> connection;
		bool saturated;

		// Mechanical Stuff
		scalar mass, drag, noiseMagnitude, aDrag, aNoiseMagnitude, momentOfInertia;

		// Kinetic stuff
		scalar reactionRate, reactionProbability;
		bool reactive;

		// Forces, Torques and Energies
		bool frozen;
		bool frozenXYZ[3];

		Vector3 viscousForce, thermalForce, externalForce, totalForce;
		Vector3 viscousTorque, thermalTorque, externalTorque, totalTorque;
		scalar kineticEnergy, totalEnergy;

		// Peridodic stuff (this is an integer, really, but we need to do multiplication in Eigen)
		Vector3 wrapped;

		// Template
		vTObject *vTO;

		// Connected vObjects
		vObject *parent;

		int numChildren;
		vObject **child;
};


// First level object. Contains parameters and method signatures which are common to all connection objects within a network
// Cannot be instantiated 
class cObject : public nObject {

	public:

		//
		// Methods
		//

		// Constructor / Destructor
		cObject();
		virtual ~cObject();

		//
		// Pure overrides from nObject
		//

		// Setters, Getters & Updaters
		virtual int setParameter(string name, string value) = 0;
		virtual string getParameter(string name) = 0;

		// Validation methods
		virtual bool validate(NetworkParameters *params) = 0;

		// Info methods
		virtual void printDetails();

		//
		// New Methods
		//
		
		// Setters, Getters & Updaters
		int getTemplateIndex();
		cTObject * getTemplate();

		int getNumAttachedTerminals();
		int getNumTerminals();

		vObject * getTerminal(int index);
		vObject * getStartTerminal();
		vObject * getEndTerminal();

		int addTerminal(vObject *n);
		int removeTerminal(int index);
		int removeTerminal(vObject *n);
		void removeTerminals();

		void setElasticConstant(scalar k);
		scalar getElasticConstant();

		void setEquilibriumLength(scalar l);
		scalar getEquilibriumLength();

		void setParentNodeIndices(int i, int j);
		void setChildNodeIndices(int i, int j);

		Vector3 getElasticForce();
		Vector3 getTotalForce();		
		scalar getElasticEnergy();
		scalar getTotalEnergy();

		void setBondEnergy(scalar E);
		scalar getBondEnergy();

		void setSaturated(bool b);
		bool isSaturated();

		void setOffset(Vector3 v);
		Vector3 getOffset();

		BondType getBondType();

		// Extrinsic / Compound properties
		virtual void zeroElasticForce();
		virtual void applyElasticForce() = 0;
		virtual void calculateElasticForce() = 0;
		
		virtual void zeroTotalForce();
		virtual void applyTotalForce() = 0;
		virtual void calculateTotalForce() = 0;

		virtual void zeroElasticEnergy();
		virtual void calculateElasticEnergy() = 0;
		
		virtual void zeroTotalEnergy();
		virtual void calculateTotalEnergy() = 0;

		// Calculation methods
		virtual void calculateExtrinsicProperties(NetworkParameters *params) = 0;

		// Applies template object details to the cObject
		virtual void applyTemplate(cTObject *cTO) = 0;

	protected:

		//
		// Methods
		//

		// Setters, Getters & Updaters
		void setElasticForce(scalar x, scalar y, scalar z);
		void setElasticForce(Vector3 v);

		void setTotalForce(scalar x, scalar y, scalar z);
		void setTotalForce(Vector3 v);

		void setElasticEnergy(scalar E);
		void addElasticEnergy(scalar E);

		void setTotalEnergy(scalar E);

		void zeroBondEnergy();

		//
		// Variables
		//

		// Terminal details
		int numAttachedTerminals, numTerminals;
		vector<vObject*> terminal;
		bool saturated;

		// Periodic stuff
		Vector3 offset;

		// Mechanical Stuff
		scalar k, l;

		// Energies and Forces
		scalar elasticEnergy, totalEnergy, bondEnergy;
		Vector3 elasticForce, totalForce;

		// Template
		cTObject *cTO;	
};

// Second level object. Contains 0D internal and 3D external properties for a vObject
class Point : public vObject {

	public:

		//
		// Methods
		//

		// Constructors / Destructors
		Point();
		~Point();

		//
		// Pure overrides from vObject
		//

		// Setters, Getters & Updaters
		virtual int setParameter(string name, string value);
		virtual string getParameter(string name);

		virtual scalar getLengthscale();
		virtual int getNumChildren();
		virtual vObject * getChild(int index);

		// Extrinsic / Compound properties
		virtual void calculateExtrinsicProperties(NetworkParameters *params);

		// Validation methods
		virtual bool validate(NetworkParameters *params);
};

// Third level object. Contains 3D internal and 3D external properties for a vObject
class Sphere : public Point {

	public:

		//
		// Methods
		//

		// Constructors / Destructors
		Sphere();
		~Sphere();

		//
		// Overridden methods
		//

		// Setters, Getters & Updaters
		virtual int setParameter(string name, string value);
		virtual string getParameter(string name);

		virtual scalar getLengthscale();

		// Mechanics methods
		virtual void translate(Vector3 t);
		virtual void rotate(Matrix3 m);
		virtual void applyOrientationMatrix(Matrix3 m);

		// Non-trivial properties
		virtual void calculateExtrinsicProperties(NetworkParameters *params);
		virtual void calculateKineticEnergy();
		virtual void calculateTotalEnergy();

		// Validation methods
		virtual bool validate(NetworkParameters *params);

		//
		// Further methods
		//

		void setRadius(scalar r);
		scalar getRadius();

		void setDensity(scalar d);
		scalar getDensity();

		void setViscosity(scalar v);
		scalar getViscosity();

		scalar getMeanFreePath();
		scalar getCrossSection();
		scalar getVolume();

		virtual int getNumChildren();
		virtual vObject * getChild(int index);

		// Extrinsic / Compound properties
		void calculateCOrientation();
		virtual void calculateReactionProbability(scalar dt);

		//
		// Variables
		//

	protected:

		//
		// Methods
		//

		// Extrinsic / Compound properties
		void calculateCrossSection();
		void calculateMomentOfInertia();
		void calculateVolume();
		void calculateMass();
		void calculateDrag(scalar extVisc);
		void calculateRotationalDrag(scalar extVisc);

		//
		// Variables
		//
		scalar radius, density, viscosity, mfp, volume, crossSection;
};

// Third level object. Contains 3D internal and 3D external properties for a vObject
class Ellipsoid : public Point {

	public:

		//
		// Methods
		//

		// Constructors / Destructors
		Ellipsoid();
		~Ellipsoid();

		//
		// Overridden methods
		//

		// Setters, Getters & Updaters
		virtual int setParameter(string name, string value);
		virtual string getParameter(string name);

		virtual scalar getLengthscale();

		// Mechanics methods
		virtual void translate(Vector3 t);
		virtual void rotate(Matrix3 m);
		virtual void applyOrientationMatrix(Matrix3 m);

		// Non-trivial properties
		virtual void calculateExtrinsicProperties(NetworkParameters *params);
		virtual void calculateKineticEnergy();
		virtual void calculateTotalEnergy();

		// Validation methods
		virtual bool validate(NetworkParameters *params);

		//
		// Further methods
		//

		void setRadius(scalar r);
		scalar getRadius();

		void setDensity(scalar d);
		scalar getDensity();

		void setViscosity(scalar v);
		scalar getViscosity();

		scalar getMeanFreePath();
		scalar getCrossSection();
		scalar getVolume();

		virtual int getNumChildren();
		virtual vObject * getChild(int index);

		// Extrinsic / Compound properties
		void calculateCOrientation();
		virtual void calculateReactionProbability(scalar dt);

		//
		// Variables
		//

	protected:

		//
		// Methods
		//

		// Extrinsic / Compound properties
		void calculateCrossSection();
		void calculateMomentOfInertia();
		void calculateVolume();
		void calculateMass();
		void calculateDrag(scalar extVisc);
		void calculateRotationalDrag(scalar extVisc);

		//
		// Variables
		//
		scalar radius, density, viscosity, mfp, volume, crossSection;
//		Vector3 radii;
};

// Second level object. Contains a simple linear exeForce of the cObject
class TwoNodeCObject : public cObject {

	public:

		//
		// Methods
		//

		// Constructors / Destructors
		TwoNodeCObject();
		~TwoNodeCObject();

		//
		// Pure overrides from nObject
		//
		virtual int setParameter(string name, string value);
		virtual string getParameter(string name);

		virtual void applyElasticForce() = 0;
		virtual void calculateElasticForce() = 0;
		
		virtual void applyTotalForce() = 0;
		virtual void calculateTotalForce() = 0;

		virtual void calculateElasticEnergy() = 0;
		virtual void calculateTotalEnergy() = 0;


		// Validation methods
		virtual bool validate(NetworkParameters *params) = 0;

		//
		// Pure overrides from cObject
		//
		virtual void calculateExtrinsicProperties(NetworkParameters *params);
};

// Second level object. Contains a simple linear exeForce of the cObject
class ThreeNodeCObject : public cObject {

	public:

		//
		// Methods
		//

		// Constructors / Destructors
		ThreeNodeCObject();
		~ThreeNodeCObject();

		//
		// Pure overrides from nObject
		//
		virtual int setParameter(string name, string value);
		virtual string getParameter(string name);

		virtual void applyElasticForce() = 0;
		virtual void calculateElasticForce() = 0;
		
		virtual void applyTotalForce() = 0;
		virtual void calculateTotalForce() = 0;

		virtual void calculateElasticEnergy() = 0;
		virtual void calculateTotalEnergy() = 0;


		// Validation methods
		virtual bool validate(NetworkParameters *params) = 0;

		//
		// Pure overrides from cObject
		//
		virtual void calculateExtrinsicProperties(NetworkParameters *params);

	protected:

		//
		// Methods
		//
		vObject * getMidTerminal();

		void setElasticTorque(Vector3 v);
		void setElasticForces(Vector3 v1, Vector3 v2);

		//
		// Variables
		//
		Vector3 elasticTorque;
		Vector3 ef1, ef2;
};

//
// Instantiatable objects (override the applyTemplate methods, PairPair methods, and also any virtual methods you like)
//
class PointNode : public Point {

	public:

		//
		// Methods
		//

		// Constructors / Destructors
		PointNode();
		~PointNode();

		//
		// Pure overrides from vObject
		//

		// Apply template, to make this object instantiatable within the network
		virtual void applyTemplate(vTObject *vTO, NetworkParameters *params);

		// Pair-pair interactions
		virtual scalar calculateVolumeOverlap(vObject *v);
		virtual scalar calculateStericInteraction(scalar kst, vObject *v, Vector3 trans);
		virtual scalar calculateVdWInteraction(scalar req, scalar eps, scalar alpha, scalar cutoff, vObject *v, Vector3 trans);

		// Validation methods
		virtual bool validate(NetworkParameters *params);

		// Info methods
		virtual void printDetails();

		//
		// Variables
		//

	protected:

		//
		// Methods
		//

		// Pair-pair interactions
		virtual scalar calculateVolumeOverlap(PointNode *p);
		virtual scalar calculateVolumeOverlap(SphereNode *s);
		virtual scalar calculateVolumeOverlap(EllipsoidNode *e);

		virtual scalar calculateStericInteraction(scalar kst, PointNode *p, Vector3 trans);
		virtual scalar calculateStericInteraction(scalar kst, SphereNode *s, Vector3 trans);
		virtual scalar calculateStericInteraction(scalar kst, EllipsoidNode *e, Vector3 trans);

		virtual scalar calculateVdWInteraction(scalar req, scalar eps, scalar alpha, scalar cutoff, PointNode *p, Vector3 trans);
		virtual scalar calculateVdWInteraction(scalar req, scalar eps, scalar alpha, scalar cutoff, SphereNode *s, Vector3 trans);
		virtual scalar calculateVdWInteraction(scalar req, scalar eps, scalar alpha, scalar cutoff, EllipsoidNode *e, Vector3 trans);

};

// 3rd level object. Specific exeForce of Point which allows the site to be on the object surface
// Can be instantiated 
class PointInteractionSite : public Point {

	public:

		//
		// Methods
		//

		// Constructors / Destructors
		PointInteractionSite();
		~PointInteractionSite();

		//
		// Pure overrides from vObject
		//

		// Setters, Getters & Updaters
		virtual int setParameter(string name, string value);
		virtual void addExternalForce(Vector3 v);

		// Apply template, to make this object instantiatable within the network
		virtual void applyTemplate(vTObject *vTO, NetworkParameters *params);

		// Pair-pair interactions
		virtual scalar calculateVolumeOverlap(vObject *v);
		virtual scalar calculateStericInteraction(scalar kst, vObject *v, Vector3 trans);
		virtual scalar calculateVdWInteraction(scalar req, scalar eps, scalar alpha, scalar cutoff, vObject *v, Vector3 trans);

		// Validation methods
		virtual bool validate(NetworkParameters *params);

		// Info
		virtual void printDetails();

		// Mechanics
		virtual void rotate(Matrix3 m);

/*
		string getParameter(string name);

		int addConnection(cObject *c);
		int removeConnection(int index);

		// Non-trivial calculations
		void calculateSaturation();
*/

	protected:

		//
		// Methods
		//

		// Pair-pair interactions
		virtual scalar calculateVolumeOverlap(PointNode *p);
		virtual scalar calculateVolumeOverlap(SphereNode *s);
		virtual scalar calculateVolumeOverlap(EllipsoidNode *e);

		virtual scalar calculateStericInteraction(scalar kst, PointNode *p, Vector3 trans);
		virtual scalar calculateStericInteraction(scalar kst, SphereNode *s, Vector3 trans);
		virtual scalar calculateStericInteraction(scalar kst, EllipsoidNode *e, Vector3 trans);

		virtual scalar calculateVdWInteraction(scalar req, scalar eps, scalar alpha, scalar cutoff, PointNode *p, Vector3 trans);
		virtual scalar calculateVdWInteraction(scalar req, scalar eps, scalar alpha, scalar cutoff, SphereNode *s, Vector3 trans);
		virtual scalar calculateVdWInteraction(scalar req, scalar eps, scalar alpha, scalar cutoff, EllipsoidNode *e, Vector3 trans);

};

class SphereNode : public Sphere {

	public:

		//
		// Methods
		//

		// Constructors / Destructors
		SphereNode();
		~SphereNode();

		// Setters, Getters & Updaters
		virtual int setParameter(string name, string value);
		virtual string getParameter(string name);

		void setPosition(Vector3 v);

		//
		// Pure overrides from nObject
		//

		// Info methods
		virtual void printDetails();

		//
		// Pure overrides from vObject
		//

		// Apply template, to make this object instantiatable within the network
		virtual void applyTemplate(vTObject *vTO, NetworkParameters *params);

		// Pair-pair interactions
		virtual scalar calculateVolumeOverlap(vObject *v);
		virtual scalar calculateStericInteraction(scalar kst, vObject *v, Vector3 trans);
		virtual scalar calculateVdWInteraction(scalar req, scalar eps, scalar alpha, scalar cutoff, vObject *v, Vector3 trans);


		// Validation methods
		bool validate(NetworkParameters *params);


	protected:

		//
		// Methods
		//

		// Pair-pair interactions
		virtual scalar calculateVolumeOverlap(PointNode *p);
		virtual scalar calculateVolumeOverlap(SphereNode *s);
		virtual scalar calculateVolumeOverlap(EllipsoidNode *e);

		virtual scalar calculateStericInteraction(scalar kst, PointNode *p, Vector3 trans);
		virtual scalar calculateStericInteraction(scalar kst, SphereNode *s, Vector3 trans);
		virtual scalar calculateStericInteraction(scalar kst, EllipsoidNode *e, Vector3 trans);

		virtual scalar calculateVdWInteraction(scalar req, scalar eps, scalar alpha, scalar cutoff, PointNode *p, Vector3 trans);
		virtual scalar calculateVdWInteraction(scalar req, scalar eps, scalar alpha, scalar cutoff, SphereNode *s, Vector3 trans);
		virtual scalar calculateVdWInteraction(scalar req, scalar eps, scalar alpha, scalar cutoff, EllipsoidNode *e, Vector3 trans);

};

class EllipsoidNode : public Ellipsoid {

	public:

		//
		// Methods
		//

		// Constructors / Destructors
		EllipsoidNode();
		~EllipsoidNode();

		// Setters, Getters & Updaters
		virtual int setParameter(string name, string value);
		virtual string getParameter(string name);

		void setPosition(Vector3 v);

		//
		// Pure overrides from nObject
		//

		// Info methods
		virtual void printDetails();

		//
		// Pure overrides from vObject
		//

		// Apply template, to make this object instantiatable within the network
		virtual void applyTemplate(vTObject *vTO, NetworkParameters *params);

		// Pair-pair interactions
		virtual scalar calculateVolumeOverlap(vObject *v);
		virtual scalar calculateStericInteraction(scalar kst, vObject *v, Vector3 trans);
		virtual scalar calculateVdWInteraction(scalar req, scalar eps, scalar alpha, scalar cutoff, vObject *v, Vector3 trans);


		// Validation methods
		bool validate(NetworkParameters *params);


	protected:

		//
		// Methods
		//

		// Pair-pair interactions
		virtual scalar calculateVolumeOverlap(PointNode *p);
		virtual scalar calculateVolumeOverlap(SphereNode *s);
		virtual scalar calculateVolumeOverlap(EllipsoidNode *e);

		virtual scalar calculateStericInteraction(scalar kst, PointNode *p, Vector3 trans);
		virtual scalar calculateStericInteraction(scalar kst, SphereNode *s, Vector3 trans);
		virtual scalar calculateStericInteraction(scalar kst, EllipsoidNode *e, Vector3 trans);

		virtual scalar calculateVdWInteraction(scalar req, scalar eps, scalar alpha, scalar cutoff, PointNode *p, Vector3 trans);
		virtual scalar calculateVdWInteraction(scalar req, scalar eps, scalar alpha, scalar cutoff, SphereNode *s, Vector3 trans);
		virtual scalar calculateVdWInteraction(scalar req, scalar eps, scalar alpha, scalar cutoff, EllipsoidNode *e, Vector3 trans);

};


// Second level object. Contains a 1D rigid rod instance of the cObject
class RigidRod1D : public TwoNodeCObject {

	public:

		//
		// Methods
		//

		// Constructors / Destructors
		RigidRod1D();
		~RigidRod1D();

		//
		// Pure overrides from cObject
		//

		virtual void applyElasticForce();
		virtual void calculateElasticForce();
		
		virtual void applyTotalForce();
		virtual void calculateTotalForce();

		virtual void calculateElasticEnergy();
		virtual void calculateTotalEnergy();

		// Info methods
		virtual void printDetails();

		//
		// Pure overrides from cObject
		//

		// Validation methods
		virtual bool validate(NetworkParameters *params);
		
		// Apply template, to make this object instantiatable within the network
		virtual void applyTemplate(cTObject *cTO);
};

// Second level object. Contains a simple linear force instance of the cObject
class LinearSpring : public TwoNodeCObject {

	public:

		//
		// Methods
		//

		// Constructors / Destructors
		LinearSpring();
		~LinearSpring();

		
		//
		// Pure overrides from cObject
		//

		virtual void applyElasticForce();
		virtual void calculateElasticForce();
		
		virtual void applyTotalForce();
		virtual void calculateTotalForce();

		virtual void calculateElasticEnergy();
		virtual void calculateTotalEnergy();
		
		// Info methods
		virtual void printDetails();

		// Validation methods
		virtual bool validate(NetworkParameters *params);
		
		// Apply template, to make this object instantiatable within the network
		virtual void applyTemplate(cTObject *cTO);
};

class FENESpring : public TwoNodeCObject {

	public:

		//
		// Methods
		//

		// Constructors / Destructors
		FENESpring();
		~FENESpring();

		// Setters, Getters & Updaters
		void setMaximumLength(scalar value);
		scalar getMaximumLength();
		
		void setElasticConstantLimit(scalar value);
		scalar getElasticConstantLimit();		

		//
		// Pure overrides from cObject
		//

		virtual void applyElasticForce();
		virtual void calculateElasticForce();
		
		virtual void applyTotalForce();
		virtual void calculateTotalForce();

		virtual void calculateElasticEnergy();
		virtual void calculateTotalEnergy();
		
		// Info methods
		virtual void printDetails();

		// Validation methods
		virtual bool validate(NetworkParameters *params);
		
		// Apply template, to make this object instantiatable within the network
		virtual void applyTemplate(cTObject *cTO);
	
	protected:
	
		// Setters, Getters & Updaters
		scalar getLengthLimit();
		void setLengthLimit(scalar r);
		void setEquilibriumLengthLimit(scalar l);
		scalar getEquilibriumLengthLimit();

		// Calculation methods
		void calculateLengthLimit();
		scalar calculateEffectiveElasticConstant();
		scalar calculateEffectiveElasticConstant(scalar r);
	private:
	
		scalar lm, rLim, lLim, kLim;
	
};

// Second level object. Contains a simple linear angular force instance of the cObject
class AngularSpring : public ThreeNodeCObject {

	public:

		//
		// Methods
		//

		// Constructors / Destructors
		AngularSpring();
		~AngularSpring();

		
		//
		// Pure overrides from cObject
		//

		virtual void applyElasticForce();
		virtual void calculateElasticForce();
		
		virtual void applyTotalForce();
		virtual void calculateTotalForce();

		virtual void calculateElasticEnergy();
		virtual void calculateTotalEnergy();
		
		// Info methods
		virtual void printDetails();

		// Validation methods
		virtual bool validate(NetworkParameters *params);
		
		// Apply template, to make this object instantiatable within the network
		virtual void applyTemplate(cTObject *cTO);
};
#endif
