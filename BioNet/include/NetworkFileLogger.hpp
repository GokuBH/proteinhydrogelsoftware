#ifndef NETLOGGER_H_INCLUDED
#define NETLOGGER_H_INCLUDED

#include <string>
#include <iostream>
#include <fstream>
#include <ctime>

#include <boost/filesystem.hpp>

#include "CMakeConfig.hpp"
#include "Utilities.hpp"

using namespace std;

namespace bfs = boost::filesystem;

//
// Global Methods
//

// Intro and exit notifications
void printIntro(ostream& ofs);
void printHeader(ostream& ofs);
void printProgramDetails(ostream& ofs);
void printVersionInfo(ostream& ofs);
void printDevDetails(ostream& ofs);
void printOutro(ostream& ofs);
void printFailedOutro(ostream& ofs);

// Tags for different issues
void printErrorTag(ostream& ofs);
void printWarningTag(ostream& ofs);
void printParamTag(ostream& ofs);
void printInfoTag(ostream& ofs);
void printMessageTag(ostream& ofs);

class NetworkFileLogger {

	public:

		// Constructors / Destructors
		NetworkFileLogger();
		~NetworkFileLogger();

		// Getters & Setters
		void setVerbosityLevel(bool v);
		int getVerbosityLevel();
		void setBlockLevel(int b);
		int getBlockLevel();
		bfs::path getLogFname();

		// Calculations
		void incrementBlockLevel();
		void decrementBlockLevel();
		void buildLogFname(bfs::path iFname);

		// Logging
		void start();
		void catchUp();
		void stop();

		// Progress notifications
		void printMethodTask(string message);
		void printMethodSuccess();
		void printProgressData(int step, int startStep, long numSteps, scalar timeRemaining, scalar simTime);
		void printFinalProgressData(scalar timeElapsed, scalar simTime);

		// Status methods
		void printError(string errMessage);
		void printError(string param, string errMessage);
		void printWarning(string warnMessage);
		void printWarning(string param, string errMessage);
		void printMessage(string message);
		void printMessage(string param, string message);

	private:

		bool verbosityLevel;
		int blockLevel;
		bool decreasing;
		bool logging;
		bool applyTabs;
		bfs::path logFname;
		ofstream lFout;
};

#endif
