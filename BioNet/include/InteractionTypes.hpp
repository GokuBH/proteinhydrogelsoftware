#ifndef INTTYPES_H_INCLUDED
#define INTTYPES_H_INCLUDED

#include <string>

#include "SystemParameters.hpp"

// This may become a generalised class eventually
class Interaction {
	
	public:
		//
		// Methods
		//

		// Constructors / Destructors
		Interaction();
		~Interaction();

		void printDetails();

		//
		// Variables
		//
		scalar energy;
		int nIndex[2];
		int bIndex;
};

#endif
