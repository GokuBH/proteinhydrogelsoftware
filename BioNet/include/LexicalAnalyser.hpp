#ifndef LEXANAL_H_INCLUDED
#define LEXANAL_H_INCLUDED

#include <string>
#include <cmath>
#include <exception>
#include <vector>
#include <boost/algorithm/string.hpp>

#include "SystemParameters.hpp"

using namespace std;

const int numMathOperators = 4;
const int numMathConstants = 4;
const string mathOperators[] = {"*", "/", "+","-"};
const scalar mathConstants[] = {M_PI, M_PI, 2*M_PI, 2*M_PI};
const string mathConstantStrings[] = {"pi", "PI", "tau","TAU"};

// Methods
scalar ProcessMaths(string expression);

#endif
