from sys import stdout
from os import path
import xml.etree.ElementTree as ET
import numpy as np

from UserInfo import *
from SystemParameters import *
from LexicalAnalyser import *

class BioNetInputParameters:

	def __init__(self):

		# Simulation parameters
		self.dt = 0.0
		self.numSteps = 0
		self.numFrames = 0
		self.simTime = 0.0
		self.stepsPerFrame = 0
		self.timePerFrame = 0.0

		# Network parameters
		self.localConnectivitySet = False
		self.viscosity = 1
		self.temperature = 298
		self.kbt = 0.0138 * 298

		# Forces
		self.thermalForcesActive = False
		self.stericForcesActive = False
		self.vdwForcesActive = False
		self.wallForcesActive = False
		self.electrostaticForcesActive = False
		self.constantForcesActive = False
		self.kineticsActive = False
		self.boxActive = False

	def setParameter(self, key, val):
		key = key.lower()
		try:
			if(key == "dt"):
				self.dt = float(val)
			elif(key == "numsteps"):
				self.numSteps = int(val)
			elif(key == "numframes"):
				self.numFrames = int(val)
			elif(key == "simtime"):
				self.simTime = float(val)
			elif(key == "stepsperframe"):
				self.stepsPerFrame = int(val)
			elif(key == "timeperframe"):
				self.timePerFrame = float(val)

			elif(key == "localconnectivityset"):
				self.localConnectivitySet = bool(val)
			elif(key == "viscosity"):
				self.viscosity = float(val)
			elif(key == "temperature" or key == "temp"):
				self.temperature = float(val)
				self.kbt = 0.0138 * self.temperature
			elif(key == "thermalforcesactive"):
				self.thermalForcesActive = bool(val)
			elif(key == "stericforcesactive"):
				self.stericForcesActive = bool(val)
			elif(key == "vdwforcesactive"):
				self.vdwForcesActive = bool(val)
			elif(key == "wallforcesactive"):
				self.wallForcesActive = bool(val)
			elif(key == "electrostaticforcesactive"):
				self.electrostaticForcesActive = bool(val)
			elif(key == "constantforcesactive"):
				self.constantForcesActive = bool(val)
			elif(key == "kineticsactive"):
				self.kineticsActive = bool(val)
			elif(key == "boxactive"):
				self.boxActive = bool(val)

		except:
			printError("Unable to set %s,%s :(" % (key, val))
			return BioNetError

	def write(self, f):

		f.write('\t\t\t<param name="dt" value="%f"/>\n' % (self.dt))
		f.write('\t\t\t<param name="simTime" value="%f"/>\n' % (self.simTime))
		f.write('\t\t\t<param name="timePerFrame" value="%f"/>\n' % (self.timePerFrame))
		if(self.temperature != 298):
			f.write('\t\t\t<param name="temperature" value="%.2f"/>\n' % (self.temperature))

		if self.viscosity != 1:
			f.write('\t\t\t<param name="viscosity" value="%.2e"/>\n' % (self.viscosity))


class BioNetInputSite:

	def __init__(self):

		self.name = ""
		self.type = ""
		self.position = np.array([1.0,0.0,0.0])
		self.rate = "inf"

	def setParameter(self, key, val):

		key = key.lower()
		try:
			if(key == "name"):
				self.name = val
			elif(key == "type"):
				self.type = val
			elif(key == "position"):
				self.position = np.array([ProcessMaths(bit) for bit in val.split(",")])
			elif(key == "rate"):
				self.rate = float(val)
		except:
			printError("Unable to set %s,%s :(" % (key, val))
			return BioNetError

	def write(self, f):

		f.write("\t\t\t\t<site")
		f.write(' name="%s"' % (self.name))
		f.write(' type="%s"' % (self.type))
		f.write(' position="%.3f,%.3f,%.3f"' % (self.position[0], self.position[1], self.position[2]))

		if self.rate != "inf":
			f.write(' rate="%.2f"' % (self.rate))

		f.write('/>\n')

class BioNetInputProtein:
	
	def __init__(self):

		self.name = ""
		self.type = ""
		self.radius = 0.0
		self.density = 0.0
		self.viscosity = 0.0
		self.drag = 0.0
		self.mass = 0.0
		self.maxBonds = 0
		self.rate = "inf"
		
		self.numSites = 0
		self.site = []

	def setParameter(self, key, val):

		key = key.lower()
		try:
			if(key == "name"):
				self.name = val
			elif(key == "type"):
				self.type = val
			elif(key == "density"):
				self.density = float(val)
			elif(key == "drag"):
				self.drag = float(val)
			elif(key == "mass"):
				self.mass = float(val)
			elif(key == "viscosity"):
				self.viscosity = float(val)
			elif(key == "radius"):
				self.radius = float(val)
			elif(key == "maxbonds" or key == "maxnumconnections"):
				self.maxBonds = int(val)
			elif(key == "rate"):
				self.rate = float(val)
		except:
			printError("Unable to set %s,%s :(" % (key, val))
			return BioNetError

	def write(self, f):

		f.write("\t\t\t<protein")
		f.write(' name="%s"' % (self.name))
		f.write(' type="%s"' % (self.type))

		if self.density != 0:
			f.write(' density="%.2f"' % (self.density))
		if self.radius != 0:
			f.write(' radius="%.2f"' % (self.radius))

		if self.drag != 0:
			f.write(' drag="%.2f"' % (self.drag))

#		f.write(' viscosity="%.3f"' % (self.viscosity))
		if self.rate != "inf":
			f.write(' rate="%.2f"' % (self.rate))

		if self.numSites == 0:
			f.write(' maxbonds="%d"' % (self.maxBonds))
			f.write("/>\n")
		else:
			f.write(">\n")
			for s in self.site:
				s.write(f)
			f.write("\t\t\t</protein>\n")

class BioNetInputBond:

	def __init__(self):

		self.name = ""
		self.type = ""
		self.k = 0.0
		self.l = 0.0
		self.klim = 0.0
		self.lm = 0.0
		self.E = 0
		self.cutoff = "inf"
		self.kinetics = False

	def setParameter(self, key, val):

		key = key.lower()
		try:
			if(key == "name"):
				self.name = val
			elif(key == "type"):
				self.type = val
			elif(key == "k"):
				self.k = float(val)
			elif(key == "l"):
				self.l = float(val)
			elif(key == "klim"):
				self.klim = float(val)
			elif(key == "lm"):
				self.lm = float(val)
			elif(key == "e"):
				self.E = float(val)
			elif(key == "kinetics"):
				self.kinetics = bool(val)
			elif(key == "cutoff"):
				self.cutoff = float(val)
		except:
			printError("Unable to set %s,%s :(" % (key, val))
			return BioNetError

	def write(self, f):

		f.write("\t\t\t<bond")
		f.write(' name="%s"' % (self.name))
		f.write(' type="%s"' % (self.type))
		f.write(' k="%.2f"' % (self.k))
		f.write(' l="%.2f"' % (self.l))

		if(self.E != 0):
			f.write(' E="%.2f"' % (self.E))
		if(self.cutoff != "inf"):
			f.write(' cutoff="%.2f"' % (self.cutoff))
		if(self.kinetics):
			f.write(' kinetics="1"')
		f.write("/>\n")

class BioNetInputNode:

	def __init__(self):

		self.protein = None
		self.pos = np.array([0.0,0.0,0.0])
		self.vel = np.array([0.0,0.0,0.0])
		self.orientation = np.array([0.0,0.0,0.0])

		self.force = np.array([0.0,0.0,0.0])
		self.wrap = np.array([0,0,0])

	def setParameter(self, key, val):

		key = key.lower()
		try:
			if(key == "pos" or key == "position"):
				self.pos = np.array([float(bit) for bit in val.split(",")])
			elif(key == "vel" or key == "velocity"):
				self.vel = np.array([float(bit) for bit in val.split(",")])
			elif(key == "orientation"):
				self.orientation = np.array([ProcessMaths(bit) for bit in val.split(",")])
			elif(key == "force"):
				self.force = np.array([float(bit) for bit in val.split(",")])
			elif(key == "wrap"):
				self.wrap = np.array([int(bit) for bit in val.split(",")])
		except:
			printError("Unable to set %s,%s :(" % (key, val))
			return BioNetError

	def write(self, f):
		f.write("\t\t\t<node")
		f.write(' protein="%s"' % (self.protein.name))
		f.write(' pos="%.2f,%.2f,%.2f"' % (self.pos[0], self.pos[1], self.pos[2]))
		f.write(' vel="%.2f,%.2f,%.2f"' % (self.vel[0], self.vel[1], self.vel[2]))

		if len(self.orientation) == 3:
			if(np.all(self.orientation != 0.0)):
				f.write(' orientation="%.2f,%.2f,%.2f"' % (self.orientation[0], self.orientation[1], self.orientation[2]))
		else:
			if(not(self.orientation[2] == 1.0 and self.orientation[5] == 1.0 and self.orientation[8] == 1.0)):
				f.write(' orientation="%.2f' % (self.orientation[0]))
				for o in self.orientation[1:]:
					f.write(',%.2f' % (o))
				f.write('"')

		if(np.any(self.wrap != 0)):
			f.write(' wrap="%d,%d,%d"' % (self.wrap[0], self.wrap[1], self.wrap[2]))

		if(np.any(self.force != 0)):
			f.write(' force="%f,%f,%f"' % (self.force[0], self.force[1], self.force[2]))
		f.write("/>\n")

class BioNetInputConnection:

	def __init__(self):

		self.bond = None
		self.nIndex = [-1,-1]
		self.sIndex = [-1,-1]
		self.tIndex = [-1,-1]

		self.node = [None, None]
		self.site = [None, None]
		self.terminal = [None, None]

		self.offset = np.array([0.0,0.0,0.0])

	def setParameter(self, key, val):

		key = key.lower()
		try:
			if(key == "nodes"):
				self.nIndex = np.array([int(bit) for bit in val.split(",")])
			elif(key == "sites"):
				self.sIndex = np.array([int(bit) for bit in val.split(",")])
			elif(key == "offset"):
				self.offset = np.array([float(bit) for bit in val.split(",")])
		except:
			printError("Unable to set %s,%s :(" % (key, val))
			return BioNetError

	def write(self, f):

		f.write("\t\t\t<connection")
		f.write(' bond="%s"' % (self.bond.name))
		f.write(' nodes="%d,%d"' % (self.nIndex[0], self.nIndex[1]))

		if(self.sIndex[0] != -1 and self.sIndex[1] != -1):

			f.write(' sites="')
			if(self.sIndex[0] == -1):
				f.write('0,')
			else:
				f.write('%d,' % (self.sIndex[0]))

			if(self.sIndex[1] == -1):
				f.write('0"')
			else:
				f.write('%d"' % (self.sIndex[1]))

		if not np.all(self.offset == 0.0):
			f.write(' offset="%.3f,%.3f,%.3f"' % (self.offset[0], self.offset[1], self.offset[2]))

		f.write("/>\n")

	def setTerminals(self, nodes):

		try:
			self.node = [nodes[i] for i in self.nIndex]
		except:
			printError("Could not assign node objects to connection :(")
			return BioNetError

		"""
		# Sort the sIndex list
		i = 0

		oldSIndex = self.sIndex
		self.sIndex = []
		for n in self.node:
			if n.protein.type.lower() == "point":
				self.sIndex.append(0)
			else:
				self.sIndex.append(oldSIndex[i])
	
			i += 1
		"""

		# Sort the sIndex list
		i = 0

		for n in self.node:
			if n.protein.type.lower() == "point":
				self.sIndex[i] = 0
	
			i += 1

		
		# Build the connections
		self.site = []
		self.terminal = []
		self.tIndex = []

		i = 0
		for n in self.node:
			
			if n.protein.type.lower() != "point" and n.protein.numSites != 0:
				self.site.append(n.protein.site[self.sIndex[i]])
				self.terminal.append(self.site[-1])
				self.tIndex.append(self.sIndex[i])
			else:
				self.site.append(None)
				self.terminal.append(n)
				self.tIndex.append(self.nIndex[i])

			i += 1

		return BioNetSuccess

class BioNetInputSimulationBox:

	def __init__(self):

		self.BC = ""
		self.dimension = np.array([0.0,0.0,0.0])
		self.numVoxels = np.array([0,0,0])
		self.cutoff = ""
		self.stress = 0.0

	def setParameter(self, key, val):

		key = key.lower()
		try:
			if(key == "bc"):
				self.BC = val.lower()
			elif(key == "dimension" or key == "dimensions"):
				self.dimension = np.array([float(bit) for bit in val.split(",")])
			elif(key == "numvoxels"):
				self.numVoxels = np.array([int(bit) for bit in val.split(",")])
			elif(key == "viscosity"):
				self.viscosity = float(val)
			elif(key == "cutoff"):
				self.cutoff = float(val)
			elif(key == "stress"):
				self.stress = float(val)
		except:
			printError("Unable to set %s,%s :(" % (key, val))
			return BioNetError

	def write(self, f):

		f.write('\t\t\t<param name="BC" value="%s"/>\n' % (self.BC))
		f.write('\t\t\t<param name="dimension" value="%.2f,%.2f,%.2f"/>\n' % (self.dimension[0], self.dimension[1], self.dimension[2]))
		f.write('\t\t\t<param name="numVoxels" value="%d,%d,%d"/>\n' % (self.numVoxels[0], self.numVoxels[1], self.numVoxels[2]))
		if(self.cutoff != ""):
			f.write('\t\t\t<param name="cutoff" value="%.2f"/>\n' % (self.cutoff))

		if self.BC.lower() == "sbc":
			f.write('\t\t\t<param name="stress" value="%.6f"/>\n' % (self.stress))

class BioNetInputForce:

	def __init__(self):

		self.name = ""
		self.attributes = {}

	def setParameter(self, key, val):

		key = key.lower()
		try:
			if(key == "name"):
				self.name = val
		except:
			printError("Unable to set %s,%s :(" % (key, val))
			return BioNetError

	def write(self, f):

		f.write('\t\t\t<force name="%s"' % (self.name))
		for key, val in self.attributes.items():
			f.write(' %s="%s"' % (key, val))
		f.write('/>\n')

class BioNetInput:

	def __init__(self):

		self.params = None

		self.numProteins = 0
		self.protein = []

		self.numBonds = 0
		self.bond = []

		self.numNodes = 0
		self.node = []

		self.numConnections = 0
		self.connection = []

		self.box = None

		self.numForces = 0
		self.force = []

	def load(self, iFname):

		# Check existence
		if(not path.exists(iFname)):

			printError(path.basename(iFname) + " could not be found :(")
			return BioNetError
	
		# Try to read it
		try:
			tree = ET.parse(iFname)
		except(ET.ParseError):
			printError("Unable to parse input file :(")
			return BioNetError

		#
		# Top level
		#		
		root = tree.getroot()

		#
		# Components
		#		
		cEl = root.find("components")
		if(cEl == None):
			printError("Could not find the <components><components/> block :(")
			return BioNetError
		
		#
		# Proteins
		#		
		pEl = cEl.find("proteins")
		if(pEl == None):
			printError("Could not find the <proteins><proteins/> block :(")
			return BioNetError
		
		if(self.readProteins(pEl) == BioNetError):
			printError("Unable to parse the <proteins><proteins/> block :(")
			return BioNetError


		#
		# Bonds
		#		
		bEl = cEl.find("bonds")
		if(bEl == None):
			printError("Could not find the <bonds><bonds/> block :(")
			return BioNetError
		
		if(self.readBonds(bEl) == BioNetError):
			printError("Unable to parse the <bonds><bonds/> block :(")
			return BioNetError


		#
		# System
		#		
		sEl = root.find("system")
		if(sEl == None):
			printError("Could not find the <system><system/> block :(")
			return BioNetError
		
		#
		# Parameters
		#		
		paEl = sEl.find("params")
		if(paEl == None):
			printError("Could not find the <params><params/> block :(")
			return BioNetError
		
		if(self.readParameters(paEl) == BioNetError):
			printError("Unable to parse the <params><params/> block :(")
			return BioNetError

		
		#
		# Nodes
		#		
		nEl = sEl.find("nodes")
		if(nEl == None):
			printError("Could not find the <nodes><nodes/> block :(")
			return BioNetError
		
		if(self.readNodes(nEl) == BioNetError):
			printError("Unable to parse the <nodes><nodes/> block :(")
			return BioNetError

		
		#
		# Connections
		#		
		cEl = sEl.find("connections")
		if(cEl != None and len(cEl) != 0):
			if(self.readConnections(cEl) == BioNetError):
				printError("Unable to parse the <connections><connections/> block :(")
				return BioNetError

		#
		# Box
		#		
		boEl = sEl.find("box")
		if boEl != None:
			if(self.readSimulationBox(boEl) == BioNetError):
				printError("Unable to parse the <box><box/> block :(")
				return BioNetError

		#
		# Forces
		#		
		fEl = sEl.find("forces")
		if(fEl != None):
			if(self.readForces(fEl) == BioNetError):
				printError("Unable to parse the <forces><forces/> block :(")
				return BioNetError

		return BioNetSuccess

	def readParameters(self, el):

		someParams = BioNetInputParameters()

		# For each parameter
		for c in el:

			# Every one should have a name and a value
			try:
				if(someParams.setParameter(c.attrib["name"], c.attrib["value"]) == BioNetError):
					printError("Unable to set parameters :(")
					return BioNetError

			except(KeyError):
				printError("Expected a 'name' and a 'value' in <param /> tag :(")
				return BioNerError

		self.params = someParams

	def readProteins(self, el):

		# For each protein in the file
		for c in el:

			# Get a protein
			anObj = BioNetInputProtein()

			# Get name first
			try:
				anObj.name = c.attrib["name"]
			except(KeyError):
				printError("Expected a 'name' in <protein /> tag :(")
				return BioNetError

			# Loop over attributes
			for key in c.attrib:
				if(anObj.setParameter(key, c.attrib[key]) == BioNetError):
					printError("Unable to set parameters in protein %d :(" % (self.numProteins))
					return BioNetError

			# Sites
			if len(c) != 0:
				for subC in c:

					aSite = BioNetInputSite()

					# Get name first
					try:
						aSite.name = subC.attrib["name"]

					except(KeyError):
						printError("Expected a 'name' in <site /> tag :(")
						return BioNetError

					# Loop over attributes
					for key in subC.attrib:
						if(aSite.setParameter(key, subC.attrib[key]) == BioNetError):
							printError("Unable to set parameters in protein %d, site %d :(" % (self.numProteins, anObj.numSites))
							return BioNetError

					anObj.site.append(aSite)
					anObj.numSites += 1

			# Append to the list
			self.protein.append(anObj)
			self.numProteins += 1

	def readBonds(self, el):

		# For each bond in the file
		for c in el:

			# Get a bond
			anObj = BioNetInputBond()

			# Get name first
			try:
				anObj.name = c.attrib["name"]
			except(KeyError):
				printError("Expected a 'name' in <bond /> tag :(")
				return BioNetError

			# Loop over attributes
			for key in c.attrib:
				if(anObj.setParameter(key, c.attrib[key]) == BioNetError):
					printError("Unable to set parameters in bond %d :(" % (self.numBonds))
					return BioNetError


			# Now assign nodes and sites
			
			# Append to the list
			self.bond.append(anObj)
			self.numBonds += 1

	def readNodes(self, el):

		# For each node in the file
		for c in el:

			# Get a node
			anObj = BioNetInputNode()

			# Get protein first
			try:
				proteinName = c.attrib["template"] 

			except(KeyError):
				try:
					proteinName = c.attrib["protein"]
				except(KeyError):
					printError("Expected a 'template' in <node /> tag :(")
					return BioNetError

			pSet = False
			for p in self.protein:
				if p.name == proteinName:
					anObj.protein = p
					pSet = True
					break

			if not pSet:
				printError("Unable to find protein template '%s' for node %d :(" % (proteinName, self.numNodes))

			# Loop over attributes
			for key in c.attrib:
				if(anObj.setParameter(key, c.attrib[key]) == BioNetError):
					printError("Unable to set parameters in node %d :(" % (self.numNodes))
					return BioNetError


			# Append to the list
			self.node.append(anObj)
			self.numNodes += 1

	def readConnections(self, el):

		# For each connection in the file
		for c in el:

			# Get a connection
			anObj = BioNetInputConnection()

			# Get bond first
			try:
				bondName = c.attrib["template"] 
			except(KeyError):
				try:
					bondName = c.attrib["bond"] 
				except(KeyError):
					printError("Expected a 'template' in <connection /> tag :(")
					return BioNetError

			bSet = False
			for b in self.bond:
				if b.name == bondName:
					anObj.bond = b
					bSet = True
					break

			if not bSet:
				printError("Unable to find bond template '%s' for connection %d :(" % (bondName, self.numConnections))

			# Loop over attributes
			for key in c.attrib:
				if(anObj.setParameter(key, c.attrib[key]) == BioNetError):
					printError("Unable to set parameters in connection %d :(" % (self.numConnections))
					return BioNetError


			# Append to the list
			self.connection.append(anObj)
			self.numConnections += 1

		# Now set sites
		for c in self.connection:
			c.setTerminals(self.node)

	def readSimulationBox(self, el):

		aBox = BioNetInputSimulationBox()

		# For each parameter
		for c in el:

			# Every one should have a name and a value
			try:
				if(aBox.setParameter(c.attrib["name"], c.attrib["value"]) == BioNetError):
					printError("Unable to set box parameters :(")
					return BioNetError

			except(KeyError):
				printError("Expected a 'name' and a 'value' in box <param /> tag :(")
				return BioNerError

		self.box = aBox

	def readForces(self, el):

		# For each force in the file
		for c in el:

			# Get a force
			anObj = BioNetInputForce()

			# Get name first
			try:
				anObj.name = c.attrib["name"]
			except(KeyError):
				printError("Expected a 'name' in <protein /> tag :(")
				return BioNetError

			# Remaining attributes
			anObj.attributes = c.attrib
			del anObj.attributes["name"]

			# Append to the list
			self.force.append(anObj)
			self.numForces += 1

	def writeParameters(self, f):
		f.write("\t\t<params>\n")
		self.params.write(f)
		f.write("\t\t</params>\n")

	def writeNodes(self, f):
		f.write("\t\t<nodes>\n")
		for n in self.node:
			n.write(f)
		f.write("\t\t</nodes>\n")

	def writeConnections(self, f):
		f.write("\t\t<connections>\n")
		for c in self.connection:
			if c != None:
				c.write(f)
		f.write("\t\t</connections>\n")

	def writeSimulationBox(self, f):
		f.write("\t\t<box>\n")
		self.box.write(f)
		f.write("\t\t</box>\n")

	def writeSystem(self, f):
		f.write("\t<system>\n")
		self.writeParameters(f)
		self.writeNodes(f)
		self.writeConnections(f)
		self.writeSimulationBox(f)
		self.writeForces(f)
		f.write("\t</system>\n")

	def writeBonds(self, f):
		f.write("\t\t<bonds>\n")
		for b in self.bond:
			b.write(f)
		f.write("\t\t</bonds>\n")

	def writeProteins(self, f):
		f.write("\t\t<proteins>\n")
		for p in self.protein:
			p.write(f)
		f.write("\t\t</proteins>\n")

	def writeForces(self, f):
		f.write("\t\t<forces>\n")
		for force in self.force:
			force.write(f)
		f.write("\t\t</forces>\n")

	def writeComponents(self, f):
		f.write("\t<components>\n")
		self.writeProteins(f)
		self.writeBonds(f)
		f.write("\t</components>\n")
		
	def writeToFile(self, fname):
		fout = open(fname, "w")
		fout.write("<BioNet>\n")
		self.writeComponents(fout)
		self.writeSystem(fout)
		fout.write("</BioNet>\n")
		fout.close()


