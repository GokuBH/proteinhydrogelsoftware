import sys
import numpy as np
from UserInfo import *
from SystemParameters import *

class BioNetTopologyFrame:

	def __init__(self):

		self.time = 0.0
		self.numConnections = 0
		self.cConnections = None
		self.nConnections = None
		self.nCoord = None
		self.offset = None
		self.bIndex = None

		self.loaded = False

	def initialise(self, numNodes, numConnections, terminals):

		self.cConnections = np.array([[[-1, -1] for i in range(terminals)] for i in range(numConnections)])
		self.offset = np.array([[0.0,0.0,0.0] for i in range(numConnections)])
		self.nCoord = np.array([0 for i in range(numNodes)])
		self.bIndex = np.array([0 for i in range(numConnections)])

	def load(self, fin, numNodes):

		# First, get all required details
		start = fin.tell()

		# Skip time
		line = fin.readline()
		if "#####" in line or line.strip() == "":
			self.__init__()
			return BioNetSuccess

		# Get first line
		line = fin.readline()

		terminals = 0

		if "#" not in line:
			self.numConnections += 1

			sline = line.split()
			terminals = len(sline) - 2
	
			while "#" not in fin.readline():
				self.numConnections += 1

		fin.seek(start)

		# Sort memory out
		self.initialise(numNodes, self.numConnections, terminals)

		# Get time and check if we can even read
		self.time = fin.readline()
		if "#####" in self.time or self.time.strip() == "":
			self.__init__()
			return BioNetSuccess
		else:
			self.time = float(self.time)

		for i in range(self.numConnections):
			try:
				# Split by object
				sline = fin.readline().split()

				for j in range(len(sline) - 2):

					# Split by component
					ssline = sline[j].split(",")

					if(len(ssline) == 1):					# Parent only
						self.nCoord[int(ssline[0])] += 1
						self.cConnections[i][j][1] = int(ssline[0])

					elif(len(ssline) == 2):				# Sites exist
						self.nCoord[int(ssline[-1])] += 1
						for k in range(2):
							self.cConnections[i][j][k] = int(ssline[k])

				ssline = sline[-2].split(",")		
				for k in range(3):	# Offset bit
					self.offset[i][k] = float(ssline[k])

				self.bIndex[i] = int(sline[-1])

			except:
				print("Unable to read frame. Possible incorrect number of connections :(")

				self.__init__()
				return BioNetError

				
		# Check final line
		if fin.readline().strip() != "###":
			print("Unable to read frame. Possible incorrect number of connections :(")

			self.__init__()
			return BioNetError

		self.loaded = True

	def buildNodeConnections(self, numNodes):

		self.nConnections = [[] for i in range(numNodes)]

		for con in self.cConnections:

			# Get indices
			nIndex = [con[0][1], con[1][1]]

			# Rotate
			if(nIndex[0] > nIndex[1]):

				temp = nIndex[0]
				nIndex[0] = nIndex[1]
				nIndex[1] = temp

			self.nConnections[nIndex[0]].append(nIndex[1])
			self.nConnections[nIndex[1]].append(nIndex[0])

class BioNetTopology:

	def __init__(self):

		# Data parameters
		self.numNodes = 0
		self.numConnections = []
		self.initType = InitType.NoInit
		self.simType = SimType.NoSim

		self.numFrames = 0
		self.frame = []

	def load(self, fname, numFrames=np.inf, rate=1):

		# Test the file
		try:
			fin = open(fname, "r")
			
			# Check header
			line = fin.readline().strip()
			if line != "BioNet Topology":
				printError("Found '%s', expected '%s'.\n\tThis doesn't seem to be the correct file type :(" % (line, "BioNet Topology"))
				return BioNetError

			# Read pointless lines
			while "#" not in line:
				line = fin.readline()

			# Get the header details
			self.readHeader(fin)
		except:
			printError("Unable to read file header :(")
			fin.close()
			return BioNetError
		
		# Close file
		fin.close()

		# Read the trajectory
		self.readTrajectory(fname, numFrames, rate)

	def loadFinalFrame(self, fname):

		# Test the file
		try:
			fin = open(fname, "r")
			
			# Check header
			line = fin.readline().strip()
			if line != "BioNet Topology":
				printError("Found '%s', expected '%s'.\n\tThis doesn't seem to be the correct file type :(" % (line, "BioNet Topology"))
				return BioNetError

			# Read pointless lines
			while "#" not in line:
				line = fin.readline()

			# Get the header details
			self.readHeader(fin)
		except:
			printError("Unable to read file header :(")
			fin.close()
			return BioNetError
		
		# Close file
		fin.close()

		# Read the trajectory
		self.readFinalFrame(fname)

	def loadFrame(self, fname, fIndex, inScript=None):

		# Test the file
		try:
			fin = open(fname, "r")
			
			# Check header
			line = fin.readline().strip()
			if line != "BioNet Topology":
				printError("Found '%s', expected '%s'.\n\tThis doesn't seem to be the correct file type :(" % (line, "BioNet Trajectory"))
				return BioNetError

			# Read pointless lines
			while "#" not in line:
				line = fin.readline()

			# Get the header details
			self.readHeader(fin)
		except:
			printError("Unable to read file header :(")
			fin.close()
			return BioNetError
		
		# Close file
		fin.close()

		# Read the trajectory
		self.readFrame(fname, fIndex, inScript)

	def readHeader(self, fin):

		# Get traj types
		line = fin.readline()
		typeString = line.split("-")[1].strip()[:-1]

		if "Simulation" in line:
			self.simType = getSimType(typeString)
		else:
			self.initType = getInitType(typeString)

		# Get numNodes
		line = fin.readline().split(",")
		self.numNodes = int(line[0].split(":")[1])
		self.numConnections.append(int(line[1].split(":")[1]))

		# Get final lines
		while "#" not in fin.readline():
			pass

	def readTrajectory(self, fname, numFrames=np.inf, rate=1):

		# Sort input
		rate = int(rate)

		# File object
		fin = open(fname, "r")

		# Get to the right line
		count = 0
		while count != 2:
			if "#####" in fin.readline():
				count += 1

		# Load frames
		batchSize = 100
		totalFrames = 0

		# Write
		sys.stdout.write("Reading topology...\n")

		while(self.numFrames < numFrames):

			# Load based on rate
			if (totalFrames) % rate != 0:
				totalFrames += 1
				if self.skipFrame(fin) == BioNetError:
					break

			else:

				# Write
				sys.stdout.write("\r\tReading frame %d (out of %d)..." % (self.numFrames, totalFrames))

				# Extend frames in batches of 100 for speed
				if(self.numFrames % batchSize == 0):
					self.frame.extend([None for i in range(batchSize)])

				# Get a frame
				frame = BioNetTopologyFrame()
				if(frame.load(fin, self.numNodes) == BioNetError):
					print("Unable to load topology further than frame %d. Will continue from here" % (self.numFrames))

				if(frame.loaded):
					self.numConnections.append(frame.numConnections)
					self.frame[self.numFrames] = frame
					self.numFrames += 1
					totalFrames += 1
				else:
					break

		# Write
		sys.stdout.write("\nTopology completed. Read %d frames out of %d.\n" % (self.numFrames, totalFrames))

		# Reduce trajectory to correct size
		self.frame = self.frame[:self.numFrames]

		# Close
		fin.close()

	def readFinalFrame(self, fname):

		# File object
		fin = open(fname, "r")

		# Get to the right line
		fPos = [None, None]
		breakCount = 0
		while True:
			line = fin.readline()
			if "#####" in line:
				breakCount += 1
				if breakCount == 3:
					break

			elif "###" in line:
				fPos[1] = fPos[0]
				fPos[0] = fin.tell()
			elif line.strip() == "":
				break

		try:
			fin.seek(fPos[1])
		except:
			print("No frames in file")
			return BioNetError
	
		# Get a frame
		frame = BioNetTopologyFrame()

		if(frame.load(fin, self.numNodes) == BioNetError):
			print("Unable to load trajectory further than frame %d. Will continue from here" % (self.numFrames))

		if(frame.loaded):
			self.frame = [frame]
			self.numFrames = 1
			print("Trajectory completed. Read %d frames." % (self.numFrames))

	def readFrame(self, fname, fIndex, inScript=None):

		# File object
		fin = open(fname, "r")

		# Get to the right line
		for i in range(2):
			while "#####" not in fin.readline():
				continue
	
		# Write
		self.numFrames = 0
		totalFrames = 0
		while(True):

			# Load only one frame (if it exists)
			if totalFrames != fIndex:
				if self.skipFrame(fin) == BioNetError:
					print("Unable to find more than %d frames. Cannot load frame %d" % (totalFrames, fIndex))
					return

				totalFrames += 1

			else:

				# Write
				sys.stdout.write("\r\tReading frame %d..." % (totalFrames))

				# Extend frames
				self.frame.extend([None])

				# Get a frame
				frame = BioNetTopologyFrame()

				if(frame.load(fin, self.numNodes) == BioNetError):
					print("Unable to load frame %d" % (totalFrames))
					return BioNetError

				if(frame.loaded):
					self.frame[self.numFrames] = frame
					self.numFrames += 1
					break
				else:
					return BioNetError

		sys.stdout.write("\nTrajectory completed. Read %d frame.\n" % (self.numFrames))
	
		# Close
		fin.close()

		return BioNetSuccess

	def skipFrame(self, fin):

		# Get current location and next line
		line = fin.readline()

		while "#" not in line and line.strip() != "":
			line = fin.readline()

		if "#" in line:
			return BioNetSuccess
		else:
			return BioNetError

