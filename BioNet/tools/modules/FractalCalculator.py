import sys, os
import numpy as np
from scipy.optimize import curve_fit
from bitarray import bitarray

import BioNetTrajectory
from UserInfo import *
from FunctionDefinitions import *

class FractalCalculator:

	def __init__(self):
	
		pass

	# From https://github.com/rougier/numpy-100 (#87)
	def boxCount(self, pos, Nx, k0, radii = [None], periodic=False):
		

		def calculateVoxIndex(indices, numVoxelsXYZ):
			return indices[2] + numVoxelsXYZ[2] * (indices[1] + numVoxelsXYZ[1] * indices[0])

		def calculateVoxIndices(index, numVoxelsXYZ):
			indices = np.array([0,0,0])
		
			# Generalised
			for i in reversed(range(3)):
				indices[i] = index % numVoxelsXYZ[i]
				index = (index - indices[i]) / numVoxelsXYZ[i]

			return indices

		# Stackexchange
		def sphereIntersects(sPos, sRad, vPos, vDims):

			# vDims halved
			vDimsHalf = vDims / 2.0

			# Get corners
			C1 = vPos - vDimsHalf
			C2 = vPos + vDimsHalf

			# Initial distance
			d2 = sRad * sRad

			for i in range(3):
				if sPos[i] < C1[i]:
					d2 -= np.square(sPos[i] - C1[i])
				elif sPos[i] > C2[i]:
					d2 -= np.square(sPos[i] - C2[i])
					
			return d2 > 0					
					
					
		# Get some parameters
		boxDimensions = np.array([k0,k0,k0]).astype(np.float64)
		numBoxesXYZ = np.array([Nx,Nx,Nx]).astype(int)
		numBoxes = numBoxesXYZ[0]*numBoxesXYZ[1]*numBoxesXYZ[2]
		voxelDimensions = boxDimensions / numBoxesXYZ

		# Empty the grid
		S = bitarray(numBoxes)
		S.setall(False)
		
		
		# For each particle, get 3 indices
		pIndex = 0
		for p, r in zip(pos, radii):

			if r == 0.0:
				continue

			twoR = 2 * r

			# Minimum lengthscale (this is the loop step size to get indices)
			numSteps = np.array([int(np.ceil(twoR / v)) + 1 for v in voxelDimensions])
			
			
			# Loop in 3D
			for x in np.linspace(p[0]-r, p[0] + r, numSteps[0]):
				for y in np.linspace(p[1]-r, p[1] + r, numSteps[1]):
					for z in np.linspace(p[2]-r, p[2] + r, numSteps[2]):

						# Get test voxel indices
						newP = np.array([x,y,z])
						indices = (newP / voxelDimensions).astype(int)

						# Get the new position
						testPos = (indices + 0.5) * voxelDimensions

						# Are we in the voxel?
						if(sphereIntersects(p, r, testPos, voxelDimensions)):

							# Periodic stuff
							if periodic:
								indices = np.mod(indices + numBoxesXYZ, numBoxesXYZ)
							else:
								if np.any(indices < 0) or np.any(indices >= numBoxesXYZ):
									continue

							# Fill
							S[calculateVoxIndex(indices, numBoxesXYZ)] = True
			
	
		return S.count(True)
					
	##
	# Modified version of "viveksck/fractal-dimension.py" from GitHub
	##
	def calculateFractalDimension(self, trajFrame, boxDims, lowThresh, fitType=["Box", "2Piece"], inp=None, periodic=False, dense=True):
	
		# We need to calculate the fractal characteristics of our simulation box. It will have two lengthscale limits,
		# given by upThresh = min(boxDims) and lowThresh. It may also be periodic, which adds a bit of calculation time.
		# Dense adds more points to the trace to get a better idea of the curve.
		# if an input file is present, we will use the specific structure of the subunits to get a better idea of the fractalness 
		# There are many possible fits. We may do 2/3 piecewise fits to N=N(N1D), 2/3 piecewise fits to N=N(r) or continuous fits to N=N(r)
		# All will be switched between, and all relevant functions and data will be returned.
		
		# Parse parameters (and check)
		upThresh = min(boxDims)
		if lowThresh > upThresh:
			printError("Upper threshold %f less than lower threshold %f. Cannot continue calculation :(" % (upThresh, lowThresh))
			raise Exception
				
		try:
			if fitType[0].lower() == "box" or fitType[0].lower() == "b":
				fitNumBoxes=True
			elif fitType[0].lower() == "length" or fitType[0].lower() == "l":
				fitNumBoxes=False
			else:
				printError('Unknown fitType. Expected "box" or "length". Cannot continue calculation :(' % (fitType[0]))
				raise Exception

			if fitType[1].lower() == "2piece" or fitType[1].lower() == "2p":
				fitFunc = pwLinearTwo
			elif fitType[1].lower() == "3piece" or fitType[1].lower() == "2p":
				fitFunc = pwLinearThree
			elif fitType[1].lower() == "powerlaw" or fitType[1].lower() == "power" or fitType[1].lower() == "pl":
				fitFunc = fractalPowerLawTwoAlpha
			else:
				printError('Unknown fitType. Expected "2piece", "3piece" or "powerlaw". Cannot continue calculation :(' % (fitType[1]))
				raise Exception
		except:
			printError("Badly formed fitType. Expected an array of length 2")
			raise IndexError

		if fitNumBoxes and (fitFunc == fractalPowerLawTwoNoAlpha or fitFunc == fractalPowerLawTwoBaseAlpha or fitFunc == fractalPowerLawTwoAlpha):
			printError("Unsupported fitType pair %s,%s :(" % (fitType[0], fitType[1]))
			raise Exception
		if inp != None:
		
			if inp.numNodes != len(trajFrame.pos):
				printError("Input file nodes (%d) not compatible with trajectory nodes (%d)" % (inp.numNodes, traj.numNodes))
				raise Exception

			radii = np.array([n.protein.radius for n in inp.node])			
			
		# Get list of box sizes
		NxMax = int(np.ceil(upThresh / lowThresh))
		twoPowMax = int(np.ceil(np.log2(NxMax)))
		NxMax = 2**twoPowMax
		if not dense:
		
			# Every factor of 2
			Nxs = np.array([2**i for i in range(twoPowMax)]).astype(int)
		else:
		
			# factor times more common (arbitrary factor)
			factor = 4.0
			Nxs = np.unique(np.array([2**(i / factor) for i in range(int(factor * twoPowMax))]).astype(int))

		# Get lengths as well
		rs = upThresh / Nxs
		
		# Box count at every size
		counts = np.array([0.0 for Nx in Nxs])

		print("\tCalculating fractal dimension via box counting...")
		nIndex = 0
		for Nx in Nxs:

			sys.stdout.write("\r\t\tLength %5.2f (of %5.2f)" % (rs[nIndex], upThresh))
			aCount = self.boxCount(trajFrame.pos, Nx, upThresh, radii=radii, periodic=periodic)
			counts[nIndex] = aCount
			nIndex += 1
			
		sys.stdout.write("done!\n")


		# Now we have the counts, we have to decide on the fits and the plots. Different for every option
		eps = 0.01
		if fitNumBoxes:
		
			X = np.log2(Nxs)
			Y = np.log2(counts)

			if fitFunc == pwLinearTwo:
				guess = [0.0, 3.0, np.log2(NxMax) - 1, 2.5]  # starts at (0.0,0.0) gradient 3, moves to fractal bit
				bounds = ([-eps,3.0-eps,0.0,0.0],[eps,3.0+eps,np.inf, 3.0])
				
				# Fit
				popt, pcov = curve_fit(fitFunc, X, Y, guess, bounds=bounds)

				fd = popt[3]
				fderr = np.sqrt(pcov[3][3])

			elif fitFunc == pwLinearThree:
				guess = [0.0, 3.0, np.log2(NxMax) - 1, 2.5,np.log2(NxMax),3.0]  # starts at (0.0,0.0) gradient 3, moves to fractal bit
				bounds = ([-eps,3.0-eps,0.0,0.0,0.0,3.0-eps],[eps,3.0+eps,np.inf,3.0, np.inf, 3.0+eps])
				
				# Fit
				popt, pcov = curve_fit(fitFunc, X, Y, guess, bounds=bounds)


				fd = popt[3]
				fderr = np.sqrt(pcov[3][3])

			else:
				# Error catching means should not get here (for now)
				pass
				
		else:
			X = np.log2(rs)
			Y = np.log2(counts)

			if fitFunc == fractalPowerLawTwoNoAlpha:
				guess = [upThresh, 3.0 - eps,upThresh]  # starts at (0.0,0.0) gradient 3, moves to fractal bit
				bounds = ([lowThresh-eps,0.0,upThresh-eps],[upThresh+eps,3.0,upThresh+eps])

				# Fit
				popt, pcov = curve_fit(fitFunc, X, Y, guess, bounds=bounds)
				fd = popt[1]
				fderr = np.sqrt(pcov[1][1])
				
			elif fitFunc == fractalPowerLawTwoBaseAlpha or fitFunc == fractalPowerLawTwoAlpha:
				guess = [upThresh, 3.0 - eps,upThresh, 1.0]  # starts at (0.0,0.0) gradient 3, moves to fractal bit
				bounds = ([lowThresh - eps,0.0,upThresh-eps, 0.0],[upThresh + eps,3.0,upThresh+eps, np.inf])

				# Fit
				popt, pcov = curve_fit(fitFunc, X, Y, guess, bounds=bounds)
				fd = popt[1]
				fderr = np.sqrt(pcov[1][1])
				
		# Return many properties
		return (fd, fderr), (X,Y,fitFunc, popt, pcov)

	"""

	##
	# Modified version of "viveksck/fractal-dimension.py" from GitHub
	##
	def calculateFractalDimension_BoxCount(self, boxDims, threshold, fitFuncIndex, radius = [None], periodic=False, dense=True):

		def calculateVoxIndex(indices, numVoxelsXYZ):
			return indices[2] + numVoxelsXYZ[2] * (indices[1] + numVoxelsXYZ[1] * indices[0])

		def calculateVoxIndices(index, numVoxelsXYZ):
			indices = np.array([0,0,0])
		
			# Generalised
			for i in reversed(range(3)):
				indices[i] = index % numVoxelsXYZ[i]
				index = (index - indices[i]) / numVoxelsXYZ[i]

			return indices

		# Stackexchange
		def sphereIntersects(sPos, sRad, vPos, vDims):

			# vDims halved
			vDimsHalf = vDims / 2.0

			# Get corners
			C1 = vPos - vDimsHalf
			C2 = vPos + vDimsHalf

			# Initial distance
			d2 = sRad * sRad

			for i in range(3):
				if sPos[i] < C1[i]:
					d2 -= np.square(sPos[i] - C1[i])
				elif sPos[i] > C2[i]:
					d2 -= np.square(sPos[i] - C2[i])

			return d2 > 0

		# From https://github.com/rougier/numpy-100 (#87)
		def boxcount(pos, Nx, k0, radii = [None], periodic=False):
			

			# Get some parameters
			boxDimensions = np.array([k0,k0,k0]).astype(np.float64)
			numBoxesXYZ = np.array([Nx,Nx,Nx]).astype(int)
			numBoxes = numBoxesXYZ[0]*numBoxesXYZ[1]*numBoxesXYZ[2]
			voxelDimensions = boxDimensions / numBoxesXYZ

			# Empty the grid
			S = bitarray(numBoxes)
			S.setall(False)
			
			
			# For each particle, get 3 indices
			pIndex = 0
			for p, r in zip(pos, radii):

				if r == 0.0:
					continue

				twoR = 2 * r

				# Minimum lengthscale (this is the loop step size to get indices)
				numSteps = np.array([int(np.ceil(twoR / v)) + 1 for v in voxelDimensions])
				
				
				# Loop in 3D
				for x in np.linspace(p[0]-r, p[0] + r, numSteps[0]):
					for y in np.linspace(p[1]-r, p[1] + r, numSteps[1]):
						for z in np.linspace(p[2]-r, p[2] + r, numSteps[2]):

							# Get test voxel indices
							newP = np.array([x,y,z])
							indices = (newP / voxelDimensions).astype(int)

							# Get the new position
							testPos = (indices + 0.5) * voxelDimensions

							# Are we in the voxel?
							if(sphereIntersects(p, r, testPos, voxelDimensions)):

								# Periodic stuff
								if periodic:
									indices = np.mod(indices + numBoxesXYZ, numBoxesXYZ)
								else:
									if np.any(indices < 0) or np.any(indices >= numBoxesXYZ):
										continue

								# Fill
								S[calculateVoxIndex(indices, numBoxesXYZ)] = True
				
		
			return S.count(True), voxelDimensions[0]
			

		# Check inputs
		if(fitFuncIndex == 1):
			fitFunc = linear
		elif (fitFuncIndex == 2):
			fitFunc = pwLinearTwo
		elif (fitFuncIndex == 3):
			fitFunc = pwLinearThree
		else:
			raise IndexError


		# Minimum dimension
		maxS = min(boxDims)

		# Assume minimum lengthscale. Get list of box counts
		Nx = []
		c = 1
		s = maxS
		maxP = -1
		while s > threshold:
			Nx.append(c)
			maxP += 1
			s /= 2.0
			c *= 2
		
		# Add some extras
		if(dense):
			logrange = np.arange(0,maxP + 0.25,0.25)
			Nx.extend([int(np.power(2,i)) for i in logrange])

		Nx = sorted(list(set(Nx)))
		Nx = np.array(Nx).astype(int)

		# Actual box counting with decreasing size
		sizes = []
		counts = []
		sIndex = 0

		print("\tCalculating fractal dimension...")
		for x in Nx:

			sys.stdout.write("\r\t\tLength %5.2f (of %5.2f)" % (maxS / x, maxS))
			aCount, aSize = boxcount(self.pos, x, maxS, radii=radius, periodic=periodic)
			counts.append(aCount)
			sizes.append(aSize)
			sIndex += 1
		sys.stdout.write("done!\n")

		# Data
		X = np.log2(Nx)
		Y = np.log2(counts)

		# Fit parameters (always starts at zero and has gradient 3)
		eps = 0.01
		if fitFunc == linear:
			guess = [0, 2.5]
			bounds = ([-eps, 0], [eps,3])			
		elif fitFunc == pwLinearTwo:
			guess = [0, 3,2.5,2.5]
			bounds = ([-eps, 3 * (1 - eps),0,0], [eps,3 * (1 + eps),np.inf,3])
		elif fitFunc == pwLinearThree:
			guess = [0, 3,2.5,2.5,6.5,3]
			bounds = ([-eps, 3 * (1 - eps),0,0,0,3 * (1 - eps)], [eps,3 * (1 + eps),np.inf,3,np.inf,3 * (1 + eps)])

		# Fit piecewise with given fitFunc
#		vf = self.pos.shape[0] * (4.0/3.0) * np.pi * np.power(radius[0], 3.0) / np.power(maxS, 3)
#		
#		guess = [6,2.8,np.log2(vf)]
##		bounds = ([-np.inf,0,np.log2(vf)*1.05],[np.inf,3,np.log2(vf)*0.95])
#		bounds = ([-np.inf,0,-np.inf],[np.inf,3,np.inf])
#		pwPopt, pwPcov = curve_fit(func, X, Y, guess, bounds=bounds)

		# Fit
		popt, pcov = curve_fit(fitFunc, X, Y, guess, bounds=bounds)

		if fitFunc == linear:
			fd = popt[1]
			fderr = np.sqrt(pcov[1][1])

		else:
			fd = popt[3]
			fderr = np.sqrt(pcov[3][3])

		# Return many properties
		return (fd, fderr), (X,Y,fitFunc, popt, pcov)
#		return (popt[1], np.sqrt(pcov[1][1])), (popt[0], np.sqrt(pcov[0][0])), (X,Y,popt)

	"""

