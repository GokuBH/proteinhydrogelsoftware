import sys
import numpy as np
from scipy.optimize import curve_fit
from scipy.special import gamma, gammainc
from matplotlib import pyplot as plt
from UserInfo import *
from SystemParameters import *
from VoxelGrid import *

from FunctionDefinitions import *

def ZXZToEuler(mat):

	s = np.sin
	c = np.cos
	t = np.tan
	arcs = np.arcsin
	arcc = np.arccos
	arct = np.arctan2

	euler = np.array([0.0,0.0,0.0])
	if(mat[2][2] == 1.0):
	
		# No x rotation at all
		euler[1] = 0
		euler[2] = 0
		euler[0] = arct(mat[1][0], mat[0][0])

	else:

		# First, assume standard angle
		euler[1] = arcc(mat[2][2])
		euler[0] = arct(mat[2][0] / s(euler[1]), mat[2][1] / s(euler[1]))
		euler[2] = arct(mat[0][2] / s(euler[1]), -1 * mat[1][2] / s(euler[1]))


	return euler

def EulerToZXZ(a,b,g):

	s = np.sin
	c = np.cos

	mat = np.array([[0.0 for i in range(3)] for j in range(3)])
	mat[0][0] = c(a)*c(g) - s(a)*c(b)*s(g)
	mat[1][0] = c(a)*s(g) + s(a)*c(b)*c(g)
	mat[2][0] = s(a)*s(b)
	mat[0][1] = -s(a)*c(g) - c(a)*c(b)*s(g)
	mat[1][1] = -s(a)*s(g) + c(a)*c(b)*c(g)
	mat[2][1] = c(a)*s(b)
	mat[0][2] = s(b)*s(g)
	mat[1][2] = -s(b)*c(g)
	mat[2][2] = c(b)

	return mat

class BioNetTrajectoryFrame:
	
	def __init__(self):

		self.time = 0.0
		self.pos = None
		self.sPos = None
		self.vel = None
		self.force = None
		self.wrap = None

		self.euler = None
		self.rMat = None
		self.loaded = False

	def initialise(self, numNodes, numSites):

		self.pos = np.array([[0.0,0.0,0.0] for i in range(numNodes)])
		self.sPos = [np.array([[0.0,0.0,0.0] for j in range(numSites[i])]) for i in range(numNodes)]
		self.vel = np.array([[0.0,0.0,0.0] for i in range(numNodes)])
		self.force = np.array([[0.0,0.0,0.0] for i in range(numNodes)])
		self.wrap = np.array([[0,0,0] for i in range(numNodes)])

		self.euler = np.array([[0.0,0.0,0.0] for i in range(numNodes)])
		self.rMat = np.array([[[1.0,0.0,0.0],[0.0,1.0,0.0],[0.0,0.0,1.0]] for i in range(numNodes)])

	def load(self, fin, numNodes, numSites, inScript = None):

		# Sort memory out
		self.initialise(numNodes, numSites)

		# Get time and check if we can even read
		self.time = fin.readline()
		if "#####" in self.time or self.time.strip() == "":
			self.__init__()
			return BioNetSuccess
		else:
			self.time = float(self.time)

		for i in range(numNodes):

			try:
				sline = fin.readline().split(",")
				for j in range(3):
					self.pos[i][j] = float(sline[j])
					self.vel[i][j] = float(sline[j + 3])
					self.force[i][j] = float(sline[j + 6])
					self.wrap[i][j] = int(sline[j - 3])		# At the back

				# Apply to initial state from input script
				if(inScript != None and len(sline) > 12):

#					self.euler[i] = np.array([0.0,0.0,0.0])
#					self.rMat[i] = np.array([[0.0 for w in range(3)] for p in range(3)])

					for w in range(3):
						for p in range(3):
							self.rMat[i][p][w] = float(sline[9 + 3 * w + p])
#							self.rMat[i][w][p] = float(sline[9 + 3 * w + p])
					# Get euler angles
					self.euler[i] = ZXZToEuler(self.rMat[i])

					# And from here, site positions
					for k in range(numSites[i]):
						direction = np.dot(self.rMat[i],inScript.node[i].protein.site[k].position)
						self.sPos[i][k] = self.pos[i] + inScript.node[i].protein.radius * direction

			except:
				print("Unable to read frame. Possible incorrect number of nodes :(")
				self.__init__()
				return BioNetError

		# Check final line
		if fin.readline().strip() != "###":
			print("Unable to read frame. Possible incorrect number of nodes :(")

			self.__init__()
			return BioNetError

		self.loaded = True
		return BioNetSuccess

	def removeWrapping(self, inp):

		# Remove all pos and sPos wraps
		for i in range(self.pos.shape[0]):
			self.pos[i] += inp.box.dimension * (self.wrap[i] - inp.node[i].wrap)

			if self.sPos != []:
				for j in range(self.sPos[i].shape[0]):
					self.sPos[i][j] += inp.box.dimension * (self.wrap[i] - inp.node[i].wrap)

			# Remove wrapping explicitly		
#			self.wrap[i] = np.array([0,0,0])
			self.wrap[i] = inp.node[i].wrap

	def addWrapping(self, inp):

		# Remove all pos and sPos wraps
		for i in range(self.pos.shape[0]):

			oldPos = self.pos[i]
			self.pos[i] = np.fmod(self.pos[i] + inp.box.dimension, inp.box.dimension)
			trans = self.pos[i] - oldPos

			if self.sPos != None and self.sPos != []:
				for j in range(self.sPos[i].shape[0]):
					self.sPos[i][j] += trans

			# Remove wrapping explicitly
			self.wrap[i] = np.array([int(w) for w in trans])
			for j in range(3):
				if(self.wrap[i][j] != 0):
					self.wrap[i][j] /= np.fabs(self.wrap[i][j])

	def calculateSpan(self):
		return [np.amin(self.pos, axis=0), np.amax(self.pos, axis=0)]

	def calculateDimensions(self):

		span = self.calculateSpan()
			
		return span[1] - span[0]

	def calculateCentroid(self):

		centroid = np.array([0.0,0.0,0.0])
		for p in self.pos:
			centroid += p
		return centroid / self.pos.shape[0]

	# Built from my interpretation of Arand and Hesser, Computers & Geosciences, 2017
	def calculatePoreDistribution(self, inp, voxDims=1.0, calculateThroats = False, fname = None):

		# Get stuff from input file
		radius = np.array([n.protein.radius for n in inp.node])
		numNodes = inp.numNodes

		# Try to get more stuff from input files
		bc = inp.box.BC.lower()
		periodic = False
		if bc == "":
			periodic = False
			boxDims = self.calculateDimensions()
		else:
			if bc == "PBC":
				periodic = True

			boxDims = inp.box.dimension
		
		numVoxelsXYZ = (boxDims / voxDims).astype(int)

		# Get a voxel grid
		sys.stdout.write("\t-> Building voxels and assigning memory...")
		sys.stdout.flush()
		voxGrid = VoxelGrid()
		voxGrid.build(boxDims, numVoxelsXYZ, periodic = periodic)
		sys.stdout.write("done!\n")
		sys.stdout.flush()

		# Calculate void space
		sys.stdout.write("\t-> Calculating void space (to 1 voxel accuracy)...")
		sys.stdout.flush()

		# Fill the voxel grid
		voxGrid.fill(self.pos, radius)

		# Get list of void and filled
		numVoid = voxGrid.numVoxels - voxGrid.numFilledVoxels
		voidVox = []
		filledVox = []
		for v in voxGrid.voxel:
			if v.filled:
				filledVox.append(v)
			else:
				voidVox.append(v)
		sys.stdout.write("done!\n")
		sys.stdout.flush()

		sys.stdout.write("\t-> Calculating maximum ball radius per voxel...")
		sys.stdout.flush()
		filledPos = np.array([v.pos for v in filledVox])
		for v in voidVox:

			if periodic:
				v.calculateMaxBallRadius(self.pos, radii = radius, dimensions = boxDims)
			else:
				v.calculateMaxBallRadius(self.pos, radii = radius)

		sys.stdout.write("done!\n")
		sys.stdout.flush()

		sys.stdout.write("\t-> Calculating the local area within each voxel...")
		sys.stdout.flush()
		
		# All non-void have lengthscale zero, so local connectivity will skip them
		voxGrid.connectLocal()

		sys.stdout.write("done!\n")
		sys.stdout.flush()

		# Calculate max ball radii
		sys.stdout.write("\t-> Sorting based on maximum ball radii...")
		sys.stdout.flush()

		voidVox = sorted(voidVox, key=lambda v: v.maxBallRadius)[::-1]
		sys.stdout.write("done!\n")
		sys.stdout.flush()

		# Building pore / throat hierarchy
		sys.stdout.write("\t-> Calculating pore/throat hierarchy...")
		sys.stdout.flush()

		# All non-void have lengthscale zero, so local connectivity will skip them
		voxGrid.buildLocalHierarchy(calculateThroats = calculateThroats)

		if fname != None:
			voxGrid.writePoresToFile(fname)

		sys.stdout.write("done!\n")
		sys.stdout.flush()

		return voxGrid.pores		

	def calculateNumberDensityDistribution(self, inp, numBins, dr):

		density = np.array([[[0.0 for i in range(numBins[0])] for j in range(numBins[0])] for k in range(numBins[2])])

		# Populate (roughly, not bothering about particles being in two boxes)
		pIndex = 0
		for p in self.pos:

			# Get indices
			indices = [int(a/b) for a,b in zip(p,dr)]
			for i in range(3):
				if indices[i] == numBins[i]:
					indices[i] -= 1

			# Calculate mass
			density[indices[0]][indices[1]][indices[2]] += 1

			pIndex += 1

		# Divide entire thing by the bin size
		density /= dr[0] * dr[1] * dr[2]

		return density

	def calculateScatteringFunction(self, inp, wavelength, qMin=None, qMax=None, numParticles=None):

		def calculateSphereFormFactor(R, q, contrast=1):

			V = (4.0/3.0) * np.pi * np.power(R,3)

			# Limiting case
			if(q == 0):
				return V

			qr = q*R
			
			return 3 * V * (np.sin(qr) - qr * np.cos(qr)) / np.power(qr,3)

		def calculateQ(theta, ld):
			return 4 * np.pi * np.sin(theta) / ld

		def calculateTheta(q, ld):
			return np.arcsin(q * ld / (4 * np.pi)) + (np.pi / 2.0)

		# For now we are assuming spherical particles. The calculation will get a bit dodgier with non-spheres
		# and will also require a bit of refactoring to get the appropriate form factors (this method would take
		# in a pSys, for example, no an inp

		# Get a theta / q range
		if qMin == None:
			thetaMin = 0
			qMin = 0
		else:
			thetaMin = calculateTheta(qMin, wavelength)

		if qMax == None:
			thetaMax = np.pi / 2.0
			qMax = calculateQ(thetaMax, wavelength)
		else:
			thetaMax = calculateTheta(qMax, wavelength)

		if numParticles == None:
			numParticles = self.pos.shape

		# Get a q range (qMax inclusive)
		numQs = 20
		qs = np.logspace(np.log10(qMin), np.log10(qMax), numQs)

		# Intensity range
		intensity = np.array([0.0 for q in qs])

		# Build up intensity from all particle pairs (for all qs)
		for qIndex in range(numQs):
			q = qs[qIndex]

			print(qIndex, q)

			# Double loop
			count = 0
			for i in range(numParticles):

				# Form factor
				fi = calculateSphereFormFactor(inp.node[i].radius, q, contrast=1)

				# Particles with themselves			
				intensity[qIndex] += fi * fi
				count += 1
				for j in range(i + 1, numParticles):

					# Form factor
					fj = calculateSphereFormFactor(inp.node[j].radius, q, contrast=1)

					# Separation
					r = np.linalg.norm(self.pos[i] - self.pos[j])
					qr = q * r

					# Particles not with themselves (multiplied by 2 for reverse indices)
					if(q == 0.0):
						intensity[qIndex] += 2 * fi * fj
						count += 2
					else:
						intensity[qIndex] += 2 * fi * fj * np.sin(qr) / qr

		return qs, intensity

	def calculateRadialDistributionFunction(self, inp=None, maxRadius=np.inf, numParticles=np.inf, dr = None):

		def calculateVolumeOverlap(d, r1, r2):

			r1pr2 = r1 + r2
			r1mr2 = r1 - r2

			return (np.pi / (12.0 * d)) * np.power(r1pr2 - d, 2) * (np.power(d,2) + 2 * d * r1pr2 - 3 * np.power(r1mr2,2))

		#
		# Calculate the relevent structural information
		#

		# System span
		if inp != None:
			if inp.box != None:
				dims = inp.box.dimension
				span = [np.array([0.0,0.0,0.0]), dims]
			minRadius = np.min([n.protein.radius for n in inp.node])
		else:
			dims = self.calculateDimensions()
			span = self.calculateSpan()
			minRadius = 1.0

		# Validate input variables
		if maxRadius > np.min(dims) / 2.0:
			 maxRadius = np.min(dims) / 2.0

		if numParticles > self.pos.shape[0]:
			numParticles = self.pos.shape[0]

		#
		# Calculation information
		#

		# Bin size
		if(dr == None):
			dr = minRadius / 2.0

		# Radial value list and bins
		rVals = np.arange(0, maxRadius + dr, dr)
		bins = [[rVals[i] - (dr/2.0), rVals[i + 1] - (dr/2.0)] for i in range(rVals.size - 1)]
#		rVals = np.array([np.mean(b) for b in bins])
		rVals = rVals[:-1]

		# Compile list of particles that can be used for the calculation
		okList = []
		for i in range(self.pos.shape[0]):

			# Position
			pc = self.pos[i]
			R = inp.node[i].radius

			# Get closest distance to walls
			rLimit = np.min(np.append(span[1] - (pc + R), (pc - R) - span[0]))

			# Test whether it's out of range of the box limits
			if rLimit > maxRadius:
				okList.append(i)
			
		# Resize number of particles list
		if(len(okList) < numParticles):
			numParticles = len(okList)

		# Get a rougly random spread of independent particles
		particleIndices = [None for i in range(numParticles)]
		index = 0
		while None in particleIndices:
			lIndex = np.random.randint(low=0, high=len(okList))
			if(okList[lIndex] not in particleIndices):
				particleIndices[index] = okList[lIndex]
				index += 1

		#
		# RDF calculation
		#
		sys.stdout.write("Calculating RDF...\n")
		pcIndex = 0
		runs = 0

		# Loop over every particle and get a radial frequency distribution
		freqs = []
		for pcIndex in particleIndices:

			# Output for user
			if(runs % 50 == 0):
				sys.stdout.write("\r\t Particle %d..." % (runs))

			# Get the position
			pc = self.pos[pcIndex]

			# Initialise
			freq = np.array([0.0 for b in bins])

			# This particle?
			

			# Loop over all other particles
			pIndex = 0

			for p in self.pos:

				# Same particle has it's own stuff
				if(pIndex == pcIndex):
					pIndex += 1
					continue

				# Get the position
				p = self.pos[pIndex]

				# Get radial distance
				rCent = np.linalg.norm(p - pc)

				# If distance is greater than our max radius, ignore
				if rCent > maxRadius:
					pIndex += 1
					continue

				#
				# Populate all bins
				#
				bIndex = int(rCent / dr)
				freq[bIndex] += 1.0

				pIndex += 1

			# Append to set
			freqs.append(freq)
			runs += 1

		sys.stdout.write("\r\t Particle %d...done!\n" % (runs))
		sys.stdout.write("Finalising...")

		# Averaging
		freq = np.mean(freqs, axis=0)
		freqErr = np.std(freqs, axis=0) / np.sqrt(len(freqs))

		# Divide
		rdf = np.array([f / (4 * np.pi * np.power(r,2) * dr) for f,r in zip(freq, rVals)])
		rdfErr = np.array([f / (4 * np.pi * np.power(r,2) * dr) for f,r in zip(freqErr, rVals)])

		# Calculate effective number density
		dims2 = dims - 2 * maxRadius
		nd = len(okList) / (dims2[0] * dims2[1] * dims2[2])

		# And final divide
		rdf /= nd
		rdfErr /= nd

		return rVals, rdf, rdfErr
		
class BioNetTrajectory:

	def __init__(self):

		# Data parameters
		self.numNodes = 0
		self.numConnections = 0
		self.numSites = []
		self.initType = InitType.NoInit
		self.simType = SimType.NoSim

		self.numFrames = 0
		self.frame = []

	def load(self, fname, inScript=None, numFrames=np.inf, rate=1):

		# Test the file
		try:
			fin = open(fname, "r")
			
			# Check header
			line = fin.readline().strip()
			if line != "BioNet Trajectory" and line != "BioNet Trajectory":
				printError("Found '%s', expected '%s'.\n\tThis doesn't seem to be the correct file type :(" % (line, "BioNet Trajectory"))
				return BioNetError

			# Read pointless lines
			while "#" not in line:
				line = fin.readline()

			# Get the header details
			self.readHeader(fin)
		except:
			printError("Unable to read file header :(")
			fin.close()
			return BioNetError
		
		# Close file
		fin.close()

		# Read the trajectory
		self.readTrajectory(fname, inScript, numFrames, rate)

	def loadFinalFrame(self, fname, inScript=None):

		# Test the file
		try:
			fin = open(fname, "r")
			
			# Check header
			line = fin.readline().strip()
			if line != "BioNet Trajectory" and line != "BioNet Trajectory":
				printError("Found '%s', expected '%s'.\n\tThis doesn't seem to be the correct file type :(" % (line, "BioNet Trajectory"))
				return BioNetError

			# Read pointless lines
			while "#" not in line:
				line = fin.readline()

			# Get the header details
			self.readHeader(fin)
		except:
			printError("Unable to read file header :(")
			fin.close()
			return BioNetError
		
		# Close file
		fin.close()

		# Read the trajectory
		self.readFinalFrame(fname, inScript)

	def loadFirstFrame(self, fname, inScript=None):

		# Test the file
		try:
			fin = open(fname, "r")
			
			# Check header
			line = fin.readline().strip()
			if line != "BioNet Trajectory" and line != "BioNet Trajectory":
				printError("Found '%s', expected '%s'.\n\tThis doesn't seem to be the correct file type :(" % (line, "BioNet Trajectory"))
				return BioNetError

			# Read pointless lines
			while "#" not in line:
				line = fin.readline()

			# Get the header details
			self.readHeader(fin)
		except:
			printError("Unable to read file header :(")
			fin.close()
			return BioNetError
		
		# Close file
		fin.close()

		# Read the trajectory
		self.readFirstFrame(fname, inScript)

	def loadFrame(self, fname, fIndex, inScript=None):

		# Test the file
		try:
			fin = open(fname, "r")
			
			# Check header
			line = fin.readline().strip()
			if line != "BioNet Trajectory" and line != "BioNet Trajectory":
				printError("Found '%s', expected '%s'.\n\tThis doesn't seem to be the correct file type :(" % (line, "BioNet Trajectory"))
				return BioNetError

			# Read pointless lines
			while "#" not in line:
				line = fin.readline()

			# Get the header details
			self.readHeader(fin)
		except:
			printError("Unable to read file header :(")
			fin.close()
			return BioNetError
		
		# Close file
		fin.close()

		# Read the trajectory
		self.readFrame(fname, fIndex, inScript)

	def readHeader(self, fin):

		# Get traj types
		line = fin.readline()
		typeString = line.split("-")[1].strip()[:-1]
		if "Simulation" in line:
			self.simType = getSimType(typeString)
		else:
			self.initType = getInitType(typeString)

		# Get numNodes
		line = fin.readline().split(",")
		self.numNodes = int(line[0].split(":")[1])
		self.numConnections = int(line[1].split(":")[1])
		self.numSites = [int(s) for s in line[2].split(":")[1].split()]


		# Get final lines
		while "#" not in fin.readline():
			pass

	def readTrajectory(self, fname, inScript=None, numFrames=np.inf, rate=1):

		# Sort input
		rate = int(rate)

		# File object
		fin = open(fname, "r")

		# Get to the right line
		count = 0
		while count != 2:
			if "#####" in fin.readline():
				count += 1

		# Load frames
		batchSize = 100
		totalFrames = 0

		# Write
		sys.stdout.write("Reading trajectory...\n")

		while(self.numFrames < numFrames):

			# Load based on rate
			if (totalFrames) % rate != 0:
				totalFrames += 1
				if self.skipFrame(fin) == BioNetError:
					break

			else:

				# Write
				sys.stdout.write("\r\tReading frame %d (out of %d)..." % (self.numFrames, totalFrames))

				# Extend frames in batches of 100 for speed
				if(self.numFrames % batchSize == 0):
					self.frame.extend([None for i in range(batchSize)])

				# Get a frame
				frame = BioNetTrajectoryFrame()

				if(frame.load(fin, self.numNodes, self.numSites, inScript) == BioNetError):
					print("Unable to load trajectory further than frame %d. Will continue from here" % (self.numFrames))
					break

				if(frame.loaded):
					self.frame[self.numFrames] = frame
					self.numFrames += 1
					totalFrames += 1
				else:
					break

		sys.stdout.write("\nTrajectory completed. Read %d frames out of %d.\n" % (self.numFrames, totalFrames))
	
		# Reduce trajectory to correct size
		self.frame = self.frame[:self.numFrames]

		# Close
		fin.close()

	def readFinalFrame(self, fname, inScript=None):

		# File object
		fin = open(fname, "r")

		# Get to the right line
		fPos = [None, None]
		breakCount = 0
		while True:
			line = fin.readline()
			if "#####" in line:
				breakCount += 1
				if breakCount == 3:
					break

			elif "###" in line:
				fPos[1] = fPos[0]
				fPos[0] = fin.tell()
			elif line.strip() == "":
				break

		try:
			fin.seek(fPos[1])
		except:
			print("No frames in file")
			return BioNetError
	
		# Get a frame
		frame = BioNetTrajectoryFrame()

		if(frame.load(fin, self.numNodes, self.numSites, inScript) == BioNetError):
			print("Unable to load trajectory further than frame %d. Will continue from here" % (self.numFrames))

		if(frame.loaded):
			self.frame = [frame]
			self.numFrames = 1
			print("Trajectory completed. Read %d frames." % (self.numFrames))

	def readFrame(self, fname, fIndex, inScript=None):

		# File object
		fin = open(fname, "r")

		# Get to the right line
		for i in range(2):
			while "#####" not in fin.readline():
				continue
	
		# Write
		self.numFrames = 0
		totalFrames = 0
		while(True):

			# Load only one frame (if it exists)
			if totalFrames != fIndex:
				if self.skipFrame(fin) == BioNetError:
					print("Unable to find more than %d frames. Cannot load frame %d" % (totalFrames, fIndex))
					return

				totalFrames += 1

			else:

				# Write
				sys.stdout.write("\r\tReading frame %d..." % (totalFrames))

				# Extend frames
				self.frame.extend([None])

				# Get a frame
				frame = BioNetTrajectoryFrame()

				if(frame.load(fin, self.numNodes, self.numSites, inScript) == BioNetError):
					print("Unable to load frame %d" % (totalFrames))
					return BioNetError

				if(frame.loaded):
					self.frame[self.numFrames] = frame
					self.numFrames += 1
					break
				else:
					return BioNetError

		sys.stdout.write("\nTrajectory completed. Read %d frame.\n" % (self.numFrames))
	
		# Close
		fin.close()

		return BioNetSuccess

	def readFirstFrame(self, fname, inScript=None):

		# File object
		fin = open(fname, "r")

		# Get to the right line
		for i in range(2):
			while "#####" not in fin.readline():
				continue
	
		# Get a frame
		frame = BioNetTrajectoryFrame()

		if(frame.load(fin, self.numNodes, self.numSites, inScript) == BioNetError):
			print("Unable to load trajectory further than frame %d. Will continue from here" % (self.numFrames))

		if(frame.loaded):
			self.frame = [frame]
			self.numFrames = 1
			print("Trajectory completed. Read %d frames." % (self.numFrames))

	def countFrames(self, fname):

		# Test the file
		try:
			fin = open(fname, "r")
			
			# Check header
			line = fin.readline().strip()
			if line != "BioNet Trajectory" and line != "BioNet Trajectory":
				printError("Found '%s', expected '%s'.\n\tThis doesn't seem to be the correct file type :(" % (line, "BioNet Trajectory"))
				return BioNetError

			# Read pointless lines
			while "#" not in line:
				line = fin.readline()

			# Get the header details
			self.readHeader(fin)
		except:
			printError("Unable to read file header :(")
			fin.close()
			return BioNetError
		
		# Close file
		fin.close()

		# Reopen object
		fin = open(fname, "r")

		# Get to the right line
		for i in range(2):
			while "#####" not in fin.readline():
				continue

		# 'Read' the trajectory
		numFrames = 0
		while(True):

			if self.skipFrame(fin) == BioNetError:
				break

			numFrames += 1

		return numFrames

	def skipFrame(self, fin):

		# Get current location and next line
		line = fin.readline()
		while "#" not in line and line.strip() != "":
			line = fin.readline()

		if "#####" in line:
			return BioNetError
		elif "#" in line:
			return BioNetSuccess
		else:
			return BioNetError

	def thin(self, percentage):

		# Get frame rate
		frameRate = int(np.ceil(100.0 / percentage))

		# Can't have zero
		if(frameRate == 0):
			frameRate = 1

		# Get the frames
		self.frame = [self.frame[i] for i in range(0, self.numFrames, frameRate)]
		oldNumFrames = self.numFrames
		self.numFrames = len(self.frame)

		print("Retained %d frames" % (self.numFrames))
		print("Lost %d frames" % (oldNumFrames - self.numFrames))

	def removeWrapping(self, inScript):

		try:
			dims = inScript.box.dimension
			if(inScript.box.BC.lower() != "pbc"):
				print("Provided script has boundary conditions '%s' specified, not PBC. May give unexpected results..." % (inScript.box.BC))
		except:
			print("Unable to extract box dimensions. Please provide suitable script.")
			return BioNetError

		for i in range(self.numFrames):

			self.frame[i].removeWrapping(inScript)

		return BioNetSuccess

	def addWrapping(self, inScript):

		try:
			dims = inScript.box.dimension
			if(inScript.box.BC.lower() != "pbc"):
				print("Provided script has boundary conditions '%s' specified, not PBC. May give unexpected results..." % (inScript.box.BC))
		except:
			print("Unable to extract box dimensions. Please provide suitable script.")
			return BioNetError

		for i in range(self.numFrames):

			self.frame[i].addWrapping(inScript)

		return BioNetSuccess

	def writeToFile(self, fname, fmt="bionet"):

		if fmt == "bionet":
			with open(fname, "w") as fout:

				# Write header bit
				fout.write("BioNet Trajectory\n")
				fout.write("#####\n")
				if (self.simType != SimType.NoSim):
					fout.write("Simulation (Type - " + str(self.simType) + ")\n")
				else:
					fout.write("Simulation (Type - " + str(self.initType) + ")\n")
	#			fout.write("Nodes: %d, Connections: %d\n" % (self.numNodes, self.numConnections))
				fout.write("Nodes: %d, Connections: %d, Sites:" % (self.numNodes, self.numConnections))
				for s in self.numSites:
					fout.write(" %d" % (s))
				fout.write("\n")
				fout.write("#####\n")

				for i in range(self.numFrames):
					f = self.frame[i]

					fout.write("%f\n" % (f.time))

					for j in range(self.numNodes):
						fout.write("%.3f,%.3f,%.3f" % (f.pos[j][0], f.pos[j][1], f.pos[j][2]))
						fout.write(",%.2f,%.2f,%.2f" % (f.vel[j][0], f.vel[j][1], f.vel[j][2]))
						fout.write(",%.2f,%.2f,%.2f" % (f.force[j][0], f.force[j][1], f.force[j][2]))

						# This may not exist
#						if f.rMat != None:
#						if np.all(f.rMat == []):
						for k in range(3):
							for l in range(3):
								fout.write(",%.2f" % (f.rMat[j][l][k]))

						fout.write(",%d,%d,%d" % (f.wrap[j][0], f.wrap[j][1], f.wrap[j][2]))
						fout.write("\n")

					fout.write("###\n")
				fout.write("#####\n")

		elif fmt == "pdb":

			self.writeToPDB(fname)

	def writeToPDB(self, fname):

		# Nodes only, sites ignored

		# Every frame is a model
		with open(fname, "w") as fout:
			
			for i in range(self.numFrames):
				f = self.frame[i]

				# Model
				fout.write("MODEL     %4d\n" % (i + 1))

				for j in range(self.numNodes):
					fout.write("ATOM  ")
					fout.write("%5d " % (j + 1))
					fout.write("C   ")
					fout.write(" ")
					fout.write("Arg ")
					fout.write("A")
					fout.write("%4d" % (0))
					fout.write("    ")
					fout.write("%8.3f%8.3f%8.3f" % (f.pos[j][0], f.pos[j][1], f.pos[j][2]))
					fout.write("%6d" % (1))
					fout.write("%6d" % (1))
					fout.write("        ")
					fout.write(" C")
					fout.write(" 0")
					fout.write("\n")
				fout.write("TER\n")

				# End
				fout.write("ENDMDL\n")

	def calculateStress(self, fStart = 0, fEnd = None, fRate = 1, dims = []):

		# We need to average force * distance over a few frames
		stress = np.array([[0.0 for i in range(3)]for j in range(3)])

		# Check input parameters
		if fEnd == None or fEnd > self.numFrames:
			fEnd = self.numFrames

		if fStart >= fEnd:
			fStart = fEnd - 1

		framesUsed = int(fEnd - fStart)
		if fRate > framesUsed:
			fRate = framesUsed
		
		framesUsed /= fRate

		# Do we need a volume and dimensions?
		if dims == []:
			volume = -np.inf
			for f in self.frame[fStart:fEnd:fRate]:
				dims = f.calculateDimensions()
				vol = dims[0] * dims[1] * dims[2]
				if vol > volume:
					volume = vol


		# Centroid and volume
		volume = dims[0] * dims[1] * dims[2]
		centroid = dims / 2.0
		
		# Each component (these loops are slow. Refactor for slicing. Also, are the forces correct? Do they include noise?)
		for i in range(3):
			for j in range(3):

				# Each particle
				totalExpPR = 0.0
				for n in range(self.numNodes):

					expFR = 0.0

					# Average over trajectory frames
					for f in self.frame[fStart:fEnd:fRate]:
						#expFR += f.force[n][i] * (f.pos[n][j] - centroid[j])
						expFR += f.force[n][i] * f.pos[n][j]
					expFR /= framesUsed

					totalExpPR += expFR

				stress[i][j] = -1.0 * totalExpPR / volume

		return stress
