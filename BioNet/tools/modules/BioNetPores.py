import sys
import numpy as np

from UserInfo import *
from SystemParameters import *

class BioNetPoreFrame:
	
	def __init__(self):

		self.numPores = 0
		self.numThroats = 0

		self.porePos = None
		self.poreRadius = None
		self.throatPos = None
		self.throatRadius = None

		self.loaded = False

	def initialise(self, numPores, numThroats):

		self.numPores = numPores
		self.numThroats = numThroats

		self.porePos = np.array([[0.0,0.0,0.0] for i in range(numPores)])
		self.poreRadius = np.array([0.0 for i in range(numPores)])
		self.throatPos = np.array([[0.0,0.0,0.0] for i in range(numThroats)])
		self.throatRadius = np.array([0.0 for i in range(numThroats)])

	def load(self, fin):

		# Read first line and then initialise (if possible)
		line = fin.readline()
		if "#####" in line or line.strip() == "":
			self.__init__()
			return BioNetSuccess

		sline = line.split()
		numPores = int(sline[0])
		numThroats = int(sline[1])
		self.initialise(numPores, numThroats)

		# Read
		for i in range(numPores):

			try:
				sline = fin.readline().split()
				for j in range(3):
					self.porePos[i][j] = float(sline[j])
				self.poreRadius[i] = float(sline[3])

			except:
				print("Unable to read frame. Possible incorrect number of pores specified :(")
				self.__init__()
				return BioNetError

		for i in range(numThroats):

			try:
				sline = fin.readline().split()
				for j in range(3):
					self.throatPos[i][j] = float(sline[j])
				self.throatRadius[i] = float(sline[3])

			except:
				print("Unable to read frame. Possible incorrect number of throats specified :(")
				self.__init__()
				return BioNetError
		
		# Check final line
		if fin.readline().strip() != "#":
			print("Unable to read frame. Possible incorrect number of pores /throats :(")

			self.__init__()
			return BioNetError

		self.loaded = True
		return BioNetSuccess

class BioNetPores:

	def __init__(self):

		# Data parameters
		self.numFrames = 0
		self.frame = []

	def load(self, fname, numFrames=np.inf, rate=1):

		# Test the file
		try:
			fin = open(fname, "r")
			
			# Check header
			line = fin.readline().strip()
			if line != "BioNet Pores":
				printError("Found '%s', expected '%s'.\n\tThis doesn't seem to be the correct file type :(" % (line, "BioNet Trajectory"))
				return BioNetError

			# Read pointless lines
			while "#" not in line:
				line = fin.readline()

		except:
			printError("Unable to read file header :(")
			fin.close()
			return BioNetError
		
		# Close file
		fin.close()

		# Read the trajectory
		self.readTrajectory(fname, numFrames, rate)

	def readTrajectory(self, fname, numFrames=np.inf, rate=1):

		# Sort input
		rate = int(rate)

		# File object
		fin = open(fname, "r")

		# Get to the right line
		count = 0
		while count != 1:
			if "#####" in fin.readline():
				count += 1

		# Load frames
		batchSize = 100
		totalFrames = 0

		# Write
		sys.stdout.write("Reading pore trajectory...\n")

		while(self.numFrames < numFrames):

			# Load based on rate
			if (totalFrames) % rate != 0:
				totalFrames += 1
				if self.skipFrame(fin) == BioNetError:
					break

			else:

				# Write
				sys.stdout.write("\r\tReading frame %d (out of %d)..." % (self.numFrames, totalFrames))

				# Extend frames in batches of 100 for speed
				if(self.numFrames % batchSize == 0):
					self.frame.extend([None for i in range(batchSize)])

				# Get a frame
				frame = BioNetPoreFrame()

				if(frame.load(fin) == BioNetError):
					print("Unable to load pore trajectory further than frame %d. Will continue from here" % (self.numFrames))
					break

				if(frame.loaded):
					self.frame[self.numFrames] = frame
					self.numFrames += 1
					totalFrames += 1
				else:
					break

		sys.stdout.write("\nTrajectory completed. Read %d frames out of %d.\n" % (self.numFrames, totalFrames))
	
		# Reduce trajectory to correct size
		self.frame = self.frame[:self.numFrames]

		# Close
		fin.close()

	def skipFrame(self, fin):

		# Get current location and next line
		line = fin.readline()
		while "#" not in line and line.strip() != "":
			line = fin.readline()

		if "#####" in line:
			return BioNetError
		elif "#" in line:
			return BioNetSuccess
		else:
			return BioNetError

