from enum import Enum
class SimType(Enum):

	NoSim = 0
	GNM = 1
	ANM = 2
	ENM = 3
	VGNM = 4
	VANM = 5
	VENM = 6
	Langevin = 7
	Brownian = 8

	def __str__(self):
		
		simTypeString = ["No Simulation", "GNM", "ANM", "ENM", "VGNM", "VANM", "VENM", "Langevin", "Brownian"];
		return simTypeString[self.value]

class StaticSimType(Enum):

	NoSim = 0
	GNM = 1
	ANM = 2
	ENM = 3
	VGNM = 4
	VANM = 5
	VENM = 6

	def __str__(self):
		simTypeString = ["No Simulation", "GNM", "ANM", "ENM", "VGNM", "VANM", "VENM"];
		return simTypeString[self.value]

class DynamicSimType(Enum):

	NoSim = 0
	Langevin = 1
	Brownian = 2

	def __str__(self):
		
		simTypeString = ["No Simulation", "Langevin", "Brownian"];
		return simTypeString[self.value]

class InitType(Enum):
	NoInit = 0
	Relax = 1
	Random = 2 

	def __str__(self):
		
		initTypeString = ["No Initialisation", "Relax", "Random"];
		return initTypeString[self.value]

def getSimType(s):
	if(s == "GNM"):
		return SimType.GNM
	elif(s == "ANM"):
		return SimType.ANM
	elif(s == "ENM"):
		return SimType.ENM
	elif(s == "VGNM"):
		return SimType.VGNM
	elif(s == "VANM"):
		return SimType.VANM
	elif(s == "VENM"):
		return SimType.VENM
	elif(s == "Langevin"):
		return SimType.Langevin
	elif(s == "Brownian"):
		return SimType.Brownian
	else:
		return SimType.NoSim

def getStaticSimType(s):
	if(s == "GNM"):
		return StaticSimType.GNM
	elif(s == "ANM"):
		return StaticSimType.ANM
	elif(s == "ENM"):
		return StaticSimType.ENM
	elif(s == "VGNM"):
		return StaticSimType.VGNM
	elif(s == "VANM"):
		return StaticSimType.VANM
	elif(s == "VENM"):
		return StaticSimType.VENM
	else:
		return StaticSimType.NoSim

def getDynamicSimType(s):
	if(s == "Langevin"):
		return DynamicSimType.Langevin
	elif(s == "Brownian"):
		return DynamicSimType.Brownian
	else:
		return DynamicSimType.NoSim

def getInitType(s):
	if(s == "Random"):
		return InitType.Random
	elif(s == "Relax"):
		return InitType.Relax
	else:
		return InitType.NoInit

def requiresInit(initType):
	if(initType != InitType.NoInit):
		return True

	return False

