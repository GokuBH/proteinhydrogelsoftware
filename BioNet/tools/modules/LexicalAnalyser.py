from numpy import pi as M_PI

from UserInfo import *

global numMathOperators
global numMathConstants
global mathOperators
global mathConstants
global mathConstantStrings

numMathOperators = 4
numMathConstants = 4
mathOperators = ["*", "/", "+","-"]
mathConstants = [M_PI, M_PI, 2*M_PI, 2*M_PI]
mathConstantStrings = ["pi", "PI", "tau","TAU"]

# Methods
def ProcessMaths(expression):

	# Replace the bit
	for i in range(numMathConstants):
		expression = expression.replace(mathConstantStrings[i], str(mathConstants[i]))

	# Try all operators
	for i in range(numMathOperators):
		if(mathOperators[i] in expression):
			splitVector = expression.split(mathOperators[i])
			if(i == 0):
				value = float(splitVector[0]) * float(splitVector[1])
			elif(i == 1):
				value = float(splitVector[0]) / float(splitVector[1])
			elif(i == 2):
				value = float(splitVector[0]) + float(splitVector[1])
			else:
				# Ignore negative numbers
				if(splitVector[0] != ""):
					value = float(splitVector[0]) - float(splitVector[1])
				else:
					value = -1 * float(splitVector[1])
			return value
		

	# If it got here, just try to return the value. Or give up
	try:
		return float(expression)
	except:
		printErrorAndExit(":(")

	return 0
