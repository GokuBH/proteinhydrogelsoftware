from sys import stdout, exit
#from colorama import Fore, Style

global BioNetError
global BioNetSuccess
global BioNetWarning
global BioNetValid
global BioNetInvalid

BioNetError = -1
BioNetSuccess = 0
BioNetWarning = 1

BioNetValid = True
BioNetInvalid = False

class DisplayErrorMessage:

	def __init__(self, p=None, v=None, m=""):

		self.param = p
		self.value = v
		self.message = m

def printIntro():

	printHeader()
	printProgramDetails()
	printDevDetails()


def printHeader():
	stdout.write("\n\n")
	stdout.write("******************************************\n")
	stdout.write("***********                    ***********\n")
	stdout.write("*** Protein Hydrogel Network Visualiser ***\n")
	stdout.write("***********                    ***********\n")
	stdout.write("******************************************\n")
	stdout.write("\n")


def printProgramDetails():

	stdout.write("An open source software package designed to visualise both static and dynamic")
	stdout.write(" simulations of protein hydrogel structures.\n")
	stdout.write("\n")


def printDevDetails():

	stdout.write("Theory -      Benjamin Hanson (b.s.hanson@leeds.ac.uk)\n")
	stdout.write("Coding -      Benjamin Hanson (b.s.hanson@leeds.ac.uk)\n")
	stdout.write("\n")


def printOutro():

	stdout.write("\n")
	stdout.write("Visualisation completed. Thanks for playing!\n\n")


def printFailedOutro():
	stdout.write("\n")
	stdout.write("Visualisation failed. Please try again or contact support :)\n\n")


def printMethodTask(message, level=0):

	stdout.write(level * "\t" + "--> " + message)


#def printMethodSuccess():
#
#	stdout.write("...done!\n")


def printMethodSuccess(level=0):

	# Shouldn't need to clear line

	# Tab in
	stdout.write(level * "\t" + "...done!\n")


def printErrorTag():
	#stdout.write("\033[91mERROR:\t\t\033[00m")
	#stdout.write(Fore.RED + "ERROR:\t\t" + Style.RESET_ALL)
	stdout.write("ERROR:\t")

def printWarningTag():
	#stdout.write("\033[93mWARNING:\t\033[00m")
	#stdout.write(Fore.YELLOW + "WARNING:\t\t" + Style.RESET_ALL)
	stdout.write("Warning:\t")

def printParamTag():
	#stdout.write("\033[96mParameter: \033[00m")
	#stdout.write(Fore.BLUE + "Parameter: " + Style.RESET_ALL)
	stdout.write("Parameter: ")

def printValueTag():
	#stdout.write("\033[92mMessage: \033[00m")
	#stdout.write(Fore.GREEN + "Message: " + Style.RESET_ALL)
	stdout.write("Value: ")

def printMessageTag():
	#stdout.write("\033[92mMessage: \033[00m")
	#stdout.write(Fore.GREEN + "Message: " + Style.RESET_ALL)
	stdout.write("Message: ")

def printErrorAndExit(errMessage):
	printError(errMessage)
	printFailedOutro()
	exit(BioNetError)


def printError(errMessage, param=None, value=None):
	stdout.write("\n")
	printErrorTag()
	if(param != None and value != None):
		printParamTag()
		stdout.write(str(param) + "\n\t")
		printValueTag()
		stdout.write(str(value) + "\n\t")
		printMessageTag()

	stdout.write(errMessage + "\n")

def printWarning(warnMessage, param=None, value=None):
	stdout.write("\n")
	printWarningTag()
	if(param != None and value != None):
		printParamTag()
		stdout.write(str(param) + "\n\t\t")
		printValueTag()
		stdout.write(str(value) + "\n\t\t")
		printMessageTag()

	stdout.write(warnMessage + "\n")

