import sys, os
import numpy as np
from scipy.spatial.distance import cdist, pdist

class Voxel:

	def __init__(self):

		# Location
		self.index = 0
		self.indices = np.array([0,0,0])
		self.pos = np.array([0.0,0.0,0.0])

		# Structure
		self.neighbour = set()
		self.uniqueNeighbour = set()

		self.lengthscale = 0.0
		self.local = set()
		self.uniqueLocal = set()

		# Contains
		self.numObjects = 0
		self.obj = set()
		self.populated = False
		self.filled = False

		# Hierarchy
		self.maxBallRadius = 0.0
		self.parent = set()
		self.child = set()
		
	def setFilled(self, f=True):
		self.filled = f

	def setLengthscale(self, l):
		self.lengthscale = l

	def add(self, obj):
		
		self.obj.add(obj)
		self.numObjects = len(self.obj)
		self.populated = bool(self.numObjects)
		self.filled = self.populated

	def remove(self, obj):
		
		self.obj.remove(obj)
		self.numObjects = len(self.obj)
		self.populated = bool(self.numObjects)
		self.filled = self.populated

	def getAllChildren(self, kids):
		for c in self.child:
			kids = c.getAllChildren(kids)
			kids.add(c)

		return kids

	# Stackexchange
	def sphereIntersects(self, sPos, sRad, vDims):

		# vDims halved
		vDimsHalf = vDims / 2.0

		# Get corners
		C1 = self.pos - vDimsHalf
		C2 = self.pos + vDimsHalf

		# Initial distance
		d2 = sRad * sRad

		for i in range(3):
			if sPos[i] < C1[i]:
				d2 -= np.square(sPos[i] - C1[i])
			elif sPos[i] > C2[i]:
				d2 -= np.square(sPos[i] - C2[i])

		return d2 > 0

	def calculateMaxBallRadius(self, pos, radii = [], dimensions = []):

		# Alwasy do normally first
		distlist = cdist([self.pos], pos)
		self.maxBallRadius = np.min(distlist)
		if radii != []:
			self.maxBallRadius -= radii[np.argmin(distlist)]

		# If dimensions is set, assume periodic and maybe alter
		if dimensions != []:

			# Which periodicities do we need to check?
			indices = [[0],[0],[0]]
			for i in range(3):
				if self.pos[i] + self.maxBallRadius > dimensions[i]:
					indices[i].append(1)
				elif self.pos[i] - self.maxBallRadius < 0:
					indices[i].append(-1)

			for i in indices[0]:
				for j in indices[1]:
					for k in indices[2]:
 
						# Move
						direction = np.array([i,j,k]).astype(int)
						if np.all(direction == 0):
							continue

						newPos = self.pos + direction * dimensions

						# Max Ball
						distlist = cdist([newPos], pos)
						aMin = np.min(distlist)

						if aMin < self.maxBallRadius:
							self.maxBallRadius = aMin


		# Also set lengthscale for local connectivity
		self.lengthscale = self.maxBallRadius

class VoxelGrid:

	def __init__(self):

		# Size
		self.numVoxels = 0
		self.numVoxelsXYZ = np.array([0,0,0])
		self.dimensions = np.array([0.0,0.0,0.0])
		self.voxelDimension = np.array([0.0,0.0,0.0])

		# Structure
		self.voxel = []
		self.periodic = False
		self.numPopulatedVoxels = 0
		self.numFilledVoxels = 0

		# Hierarchy
		self.pores = set()
		self.throats = set()

	def setPeriodic(self, p=True):
		self.periodic = p

	def calculateVoxelIndex(self, indices):
		return indices[2] + self.numVoxelsXYZ[2] * (indices[1] + self.numVoxelsXYZ[1] * indices[0])

	def calculateVoxelIndices(self, index):
		indices = np.array([0,0,0])
	
		# Generalised
		for i in reversed(range(3)):
			indices[i] = index % self.numVoxelsXYZ[i]
			index = (index - indices[i]) / self.numVoxelsXYZ[i]

		return indices

	def getVoxelIndices(self, index):
		return self.voxels[index].indices

	def connectLocal(self):
		
		# Get a test voxel
		testVoxel = Voxel()

		# For every voxel
		vIndex = 0
		sys.stdout.write("\n")
		for cV in self.voxel:
			if vIndex % 1000 == 0:
				sys.stdout.write("\r\t\t-> Connecting Voxel %d of %d" % (vIndex + 1, self.numVoxels))
			vIndex += 1

			# Loop over distance specified by the lengthscale (spherically)
			p = cV.pos
			r = cV.lengthscale
			twoR = 2 * r

			# Minimum lengthscale (this is the loop step size to get indices)
			numSteps = np.array([int(np.ceil(twoR / v)) + 1 for v in self.voxelDimension])

			# Can't be neighbours with ourselves
			if(np.all(numSteps == 1)):
				continue

			# Loop in 3D
			for x in np.linspace(p[0]-r, p[0] + r, numSteps[0]):
				for y in np.linspace(p[1]-r, p[1] + r, numSteps[1]):
					for z in np.linspace(p[2]-r, p[2] + r, numSteps[2]):

						# Get test voxel indices
						newP = np.array([x,y,z])
						indices = (newP / self.voxelDimension).astype(int)

						# Get the new position
						testVoxel.pos = (indices + 0.5) * self.voxelDimension

						# Are we in the voxel?
						if(testVoxel.sphereIntersects(p, r, self.voxelDimension)):

							# Periodic stuff
							if self.periodic:
								indices = np.mod(indices + self.numVoxelsXYZ, self.numVoxelsXYZ)
							else:
								if np.any(indices < 0) or np.any(indices >= self.numVoxelsXYZ):
									continue

							# Get the voxel
							nV = self.voxel[self.calculateVoxelIndex(indices)]
							if cV == nV:
								continue

							# Add to lists
							cV.local.add(nV)
							cV.uniqueLocal.add(nV)


		# Make the unique list
		for cV in self.voxel:
			for nV in cV.uniqueLocal:

				# Only if they are in each other do we need to remove one
				if cV in nV.uniqueLocal:
					nV.uniqueLocal.remove(cV)

		sys.stdout.write("\n")

	def connect(self):

		# For every voxel
		for cV in self.voxel:
			
			# Get all neighbours
			for x in [-1,0,1]:
				for y in [-1,0,1]:
					for z in [-1,0,1]:

						# Check it's not self
						trans = np.array([x,y,z], dtype=int)
						if(np.all(trans == 0)):
							continue

						# New indices
						indices = cV.indices + trans

						# Periodic stuff
						if self.periodic:
							indices = np.mod(indices + self.numVoxelsXYZ, self.numVoxelsXYZ)
						else:
							if np.any(indices < 0) or np.any(indices >= self.numVoxelsXYZ):
								continue

						# Get the voxel
						nV = self.voxel[self.calculateVoxelIndex(indices)]

						# Add to lists
						cV.neighbour.add(nV)
						if cV.index < nV.index:
							cV.uniqueNeighbour.add(nV)

		
	def build(self, dimensions, numVoxelsXYZ, periodic = False):

		# Set all paramters
		self.numVoxelsXYZ = np.array([int(b) for b in numVoxelsXYZ], dtype=int)
		self.numVoxels = int(self.numVoxelsXYZ[0] * self.numVoxelsXYZ[1] * self.numVoxelsXYZ[2])
		self.dimensions = dimensions
		self.voxelDimension = self.dimensions / self.numVoxelsXYZ
		self.periodic = periodic

		# Build the voxel set
		self.voxel = [Voxel() for i in range(self.numVoxels)]

		# Individual voxels
		index = 0
		for i in range(self.numVoxelsXYZ[0]):
			for j in range(self.numVoxelsXYZ[1]):
				for k in range(self.numVoxelsXYZ[2]):

					self.voxel[index].index = index
					self.voxel[index].indices[0] = i
					self.voxel[index].indices[1] = j
					self.voxel[index].indices[2] = k

					self.voxel[index].pos = (self.voxel[index].indices + 0.5) * self.voxelDimension
					index += 1

		# Connectivity
		self.connect()

	# Add these objects to the voxels. The obj should have a pos variable
	def populate(self, obj):
		
		failedToAdd = set()
		for o in obj:

			# Get central indices
			indices = np.floor(o.pos / self.voxelDimension).astype(int)

			# Periodic stuff
			if self.periodic:
				indices = np.mod(indices + self.numVoxelsXYZ, self.numVoxelsXYZ)
			else:
				if np.any(indices < 0) or np.any(indices >= self.numVoxelsXYZ):
					failedToAdd.add(o)
					continue

			# Populate
			index = self.calculateVoxelIndex(indices)
			if not self.voxel[index].populated:
				self.numPopulatedVoxels += 1

			if not self.voxel[index].filled:
				self.numFilledVoxels += 1

			self.voxel[index].add(o)


		return failedToAdd

	# Just set either populated or not
	def fill(self, pos, radius):

		# Get a voxel
		testVoxel = Voxel()

		for p, r in zip(pos, radius):

			twoR = 2 * r

			# Minimum lengthscale (this is the loop step size to get indices)
			numSteps = np.array([int(np.ceil(twoR / v)) + 1 for v in self.voxelDimension])

			# Loop in 3D
			for x in np.linspace(p[0]-r, p[0] + r, numSteps[0]):
				for y in np.linspace(p[1]-r, p[1] + r, numSteps[1]):
					for z in np.linspace(p[2]-r, p[2] + r, numSteps[2]):

						# Get test voxel indices
						newP = np.array([x,y,z])
						indices = (newP / self.voxelDimension).astype(int)

						# Get the new position
						testVoxel.pos = (indices + 0.5) * self.voxelDimension

						# Are we in the voxel?
						if(testVoxel.sphereIntersects(p, r, self.voxelDimension)):

							# Periodic stuff
							if self.periodic:
								indices = np.mod(indices + self.numVoxelsXYZ, self.numVoxelsXYZ)
							else:
								if np.any(indices < 0) or np.any(indices >= self.numVoxelsXYZ):
									continue

							# Fill
							index = self.calculateVoxelIndex(indices)
							if not self.voxel[index].filled:
								self.numFilledVoxels += 1
							self.voxel[index].setFilled()
		

	# Add these objects to the voxels. The obj should have a pos variable
	def populateAndFill(self, obj, radius):
		
		# Get a voxel
		testVoxel = Voxel()

		failedToAdd = set()
		for o, r in zip(obj, radius):

			# Get central indices
			indices = np.floor(o.pos / self.voxelDimension).astype(int)

			# Periodic stuff
			if self.periodic:
				indices = np.mod(indices + self.numVoxelsXYZ, self.numVoxelsXYZ)
			else:
				if np.any(indices < 0) or np.any(indices >= self.numVoxelsXYZ):
					failedToAdd.add(o)
					continue

			# Populate
			index = self.calculateVoxelIndex(indices)
			if not self.voxel[index].populated:
				self.numPopulatedVoxels += 1

			if not self.voxel[index].filled:
				self.numFilledVoxels += 1

			self.voxel[index].add(o)


			# Start filling
			p = o.pos
			twoR = 2 * r

			# Minimum lengthscale (this is the loop step size to get indices)
			numSteps = np.array([int(np.ceil(twoR / v)) + 1 for v in self.voxelDimension])

			# Loop in 3D
			for x in np.linspace(p[0]-r, p[0] + r, numSteps[0]):
				for y in np.linspace(p[1]-r, p[1] + r, numSteps[1]):
					for z in np.linspace(p[2]-r, p[2] + r, numSteps[2]):

						# Get test voxel indices
						newP = np.array([x,y,z])
						indices = (newP / self.voxelDimension).astype(int)

						# Get the new position
						testVoxel.pos = (indices + 0.5) * self.voxelDimension

						# Are we in the voxel?
						if(testVoxel.sphereIntersects(p, r, self.voxelDimension)):

							# Periodic stuff
							if self.periodic:
								indices = np.mod(indices + self.numVoxelsXYZ, self.numVoxelsXYZ)
							else:
								if np.any(indices < 0) or np.any(indices >= self.numVoxelsXYZ):
									continue

							# Fill
							index = self.calculateVoxelIndex(indices)
							if not self.voxel[index].filled:
								self.numFilledVoxels += 1
							self.voxel[index].setFilled()
		

		return failedToAdd

	def buildLocalHierarchy(self, calculateThroats):

		# Make parents and children
		for cV in self.voxel:
			for nV in cV.uniqueLocal:
				d = np.linalg.norm(cV.pos - nV.pos)

				# If centre of nV within cV and it's smaller
				if nV.maxBallRadius < cV.maxBallRadius:

					if d < cV.maxBallRadius:
					
						# nV is child of cV
						nV.parent.add(cV)
						cV.child.add(nV)

				# If centre of cV within nV and it's smaller
				elif nV.maxBallRadius > cV.maxBallRadius:
				
					if d < nV.maxBallRadius:
					
						# cV is child of nV
						cV.parent.add(nV)
						nV.child.add(cV)

				# If equal, just pick one of them
				else:
					if d < nV.maxBallRadius:
					
						# nV is child of cV
						nV.parent.add(cV)
						cV.child.add(nV)

		# Make pore cores
		for cV in self.voxel:
			
			if len(cV.parent) == 0 and cV.maxBallRadius != 0.0:
				self.pores.add(cV)

		# Only do throats if necessary (incredibly labour intensive)
		if calculateThroats:

			# Recursively add kids
			pIndex = 0
			for pore in self.pores:
				sys.stdout.write("%d,%f,%d\n" % (pIndex, pore.maxBallRadius, len(self.pores)))
				sys.stdout.flush()
				pIndex += 1

				# Get all kids
				kids = set()
				kids = pore.getAllChildren(kids)

				# Remap parents
				for kid in kids:
					parentsToRemove = set()
					for p in kid.parent:
						if p in kids:
							parentsToRemove.add(p)

					# Remove parents
					for p in parentsToRemove:
						kid.parent.remove(p)

					# Add pore core
					kid.parent.add(pore)

			# Throats
			for cV in self.voxel:
				if len(cV.parent) > 1:
					self.throats.add(cV)

	def writePoresToFile(self, oFname):

		fout = open(oFname, "w")		
		fout.write("BioNet Pores\n#####\n")
		fout.write("%d %d\n" % (len(self.pores), len(self.throats)))
		for p in self.pores:
			fout.write("%f %f %f %f\n" % (p.pos[0], p.pos[1], p.pos[2], p.maxBallRadius))
		for p in self.throats:
			fout.write("%f %f %f %f\n" % (p.pos[0], p.pos[1], p.pos[2], p.maxBallRadius))
		fout.write("#\n#####\n")
		fout.close()
