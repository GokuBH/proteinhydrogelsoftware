import sys, os
import numpy as np

def linear(x, x0, m1):
	return m1 * (x - x0)

def pwLinearTwo(x, x0, m1, x01, m2):
	return np.piecewise(x, [x < x01], [lambda x:m1*(x - x0), lambda x:m2*(x-x01) + m1 * (x01 - x0)])

def pwLinearThree(x, x0, m1, x01, m2, x12, m3):
	return np.piecewise(x, [x < x01, x >= x12], [lambda x:m1*(x - x0), lambda x:m3*(x-x12) + m2 * (x12 - x01) + m1 * (x01 - x0), lambda x: m2 * (x - x01) + m1 * (x01 - x0)])
	

def fractalPowerLawTwoNoAlpha(log2r, rp, df, L):

	r = 2**log2r
	Lonrp = L / rp
	ronrp = r / rp

	return np.log2(Lonrp**3 + Lonrp**df) - np.log2(ronrp**3 + ronrp**df)

def fractalPowerLawTwoBaseAlpha(log2r, rp, df, L, alpha):

	r = 2**log2r
	Lonrp = L / rp
	ronrp = r / rp
	return np.log2(Lonrp**3 + Lonrp**df) - alpha * np.log2(ronrp**3 + ronrp**df)
	
def fractalPowerLawTwoAlpha(log2r, rp, df, L, alpha):

	r = 2**log2r
	Lonrp = L / rp
	ronrp = r / rp
	return np.log2(Lonrp**3 + Lonrp**df) - alpha * np.log2(ronrp**(3 / alpha) + ronrp**(df / alpha))
