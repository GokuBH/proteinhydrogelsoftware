import numpy as np
from UserInfo import *
from SystemParameters import *

class BioNetMeasurement:

	def __init__(self):

		# Data parameters
		self.numNodes = 0
		self.numConnections = 0
		self.numSites = 0
		self.initType = InitType.NoInit
		self.simType = SimType.NoSim

		# Measurement lists
		self.headers = None
		self.time = None
		self.centroid = None
		self.kineticEnergy = None
		self.elasticEnergy = None
		self.stericEnergy = None
		self.vdwEnergy = None
		self.wallEnergy = None
		self.coordination = None

	def load(self, fname, numFrames=np.inf, rate=1):

		# Test the file
		try:
			fin = open(fname, "r")
			
			# Check header
			line = fin.readline().strip()
			if line != "BioNet Measurement" and line != "BioNet Measurement":
				printError("Found '%s', expected '%s'.\n\tThis doesn't seem to be the correct file type :(" % (line, "BioNet Measurement"))
				return BioNetError

			# Read pointless lines
			while "#" not in line:
				line = fin.readline()

			# Get the header details
			self.readHeader(fin)
			fin.close()
		except:
			printError("Unable to read file :(")
			return BioNetError

		# Read the trajectory
		self.readTrajectory(fname, numFrames, rate)

	def readHeader(self, fin):

		# Get traj types
		line = fin.readline()
		typeString = line.split("-")[1].strip()[:-1]
		if "Simulation" in line:
			self.simType = getSimType(typeString)
		else:
			self.initType = getInitType(typeString)

		# Get numNodes
		line = fin.readline().split(",")
		self.numNodes = int(line[0].split(":")[1])
		self.numConnections = int(line[1].split(":")[1])
		self.numSites = [int(s) for s in line[2].split(":")[1].split()]

		# Get data headers
		sline = fin.readline().split(",")
		self.headers = []
		for s in sline:

			s = s.lower()
			self.headers.append(s)

		# Get final lines
		while "#" not in fin.readline():
			pass

	def readTrajectory(self, fname, numFrames=np.inf, rate=1):

		# Use numpy to get data
		data = np.genfromtxt(fname, delimiter=",", skip_header=6)

		# Resize
		data = data[0::rate]
		if data.shape[0] > numFrames:
			data = data[:numFrames]

		data = data.transpose()
		skip = 0
		for i in range(len(self.headers)):

			j = i + skip
			s = self.headers[i]
			if "time" in s:
				self.time = data[j]
			if "centroid" in s:
				self.centroid = np.array([data[j], data[j+1], data[j+2]])
				skip += 2
			if "kinetic" in s:
				self.kineticEnergy = data[j]
			if "elastic" in s:
				self.elasticEnergy = data[j]
			if "steric" in s:
				self.stericEnergy = data[j]
			if "vdw" in s:
				self.vdwEnergy = data[j]
			if "wall" in s:
				self.wallEnergy = data[j]
			if "coordination" in s:
				self.coordination = data[j]
			self.headers[i] = data[i]
