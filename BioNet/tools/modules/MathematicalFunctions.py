import numpy as np

def cartesian(theta):
	
	if(type(theta) == float):
		cart=np.array([0.0,0.0])
		cart[0] = np.cos(theta)
		cart[1] = np.sin(theta)
	
	elif(type(theta) == list or type(theta) == np.ndarray):
		theta = np.array(theta)
		cart=np.array([0.0,0.0,0.0])
		if(theta.size == 2):
			cart[0] = np.sin(theta[0]) * np.cos(theta[1])
			cart[1] = np.sin(theta[0]) * np.sin(theta[1])
			cart[2] = np.cos(theta[0])
		else:
			cart[0] = theta[0] * np.sin(theta[1]) * np.cos(theta[2])
			cart[1] = theta[0] * np.sin(theta[1]) * np.sin(theta[2])
			cart[2] = theta[0] * np.cos(theta[1])
	return cart
"""
Vector2 MathFunctions::spherical2(Vector3 cart) {

	Vector2 sphere
	scalar radius

	radius = cart.norm()

	// Try to acos
	sphere[0] = acos(cart[2]/radius)

	// Make sure cos is ok
	if(isnan(sphere[0])) {

		// Radius must've been zero. Return all angles as zero
		for(int i = 0 i < 2 ++i) {
			sphere[i] = 0
		}
		return sphere
	}

	// Try to atan (atan2 has quadrants)
	sphere[1] = atan2(cart[1], cart[0])
	
	// Make sure tan is ok
	if(isnan(sphere[1])) {

		// x must be 0. Do something about this
		if(cart[1] > 0) {
			sphere[1] = M_PI / 2.0
		} else if (cart[1] < 0) {
			sphere[1] = -M_PI / 2.0
		} else {
			sphere[1] = 0.0
		}
	}
	return sphere
}

Vector3 MathFunctions::spherical3(Vector3 cart) {

	Vector3 sphere

	sphere[0] = cart.norm()

	// Try to acos
	sphere[1] = acos(cart[2]/sphere[0])

	// Make sure cos is ok
	if(isnan(sphere[1])) {

		// Radius must've been zero. Return all angles as zero
		for(int i = 0 i < 3 ++i) {
			sphere[i] = 0
		}
		return sphere
	}

	// Try to atan
	sphere[2] = atan2(cart[1], cart[0])
	
	// Make sure tan is ok
	if(isnan(sphere[2])) {

		// x must be 0. Do something about this
		if(cart[1] > 0) {
			sphere[2] = M_PI / 2.0
		} else if (cart[1] < 0) {
			sphere[2] = -M_PI / 2.0
		} else {
			sphere[2] = 0.0
		}
	}
	return sphere
}
"""
