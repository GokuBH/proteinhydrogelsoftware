import sys, os
import numpy as np
import warnings


#
# Plugin data
#

path = os.path.dirname(__file__)
sys.path.append(path)

# Import this pluin stuff
from pymol import cmd
import ControlWindow

def __init__(self):

	""" 
	Initialise PyMOL, and add the Network Loader to the GUI
	""" 

	self.menuBar.addmenuitem('Plugin', 'command', 'BioNet Loader (1.0)', label = 'BioNet Loader (1.0)...', command = lambda s=self: ControlWindow.ControlWindow(s))
