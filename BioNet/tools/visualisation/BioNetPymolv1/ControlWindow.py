# General stuff
import sys
import numpy as np

# PyMOL stuff
from pymol import cmd
from pymol.cgo import *
import subprocess, traceback, Pmw

if sys.version_info[0] < 3:
	from Tkinter import *
	import tkFileDialog
	import tkMessageBox
	import tkColorChooser
else:
	from tkinter import *
	from tkinter import filedialog as tkFileDialog
	from tkinter import messagebox as tkMessageBox
	from tkinter import colorchooser as tkColorChooser

# BioNet stuff
from UserInfo import *
import PymolFrame
import BioNetInput
import BioNetTrajectory, BioNetTopology, BioNetMeasurement, BioNetPores

# Global stuff
def getVersionString():
	return 1.0

class ControlWindow():

	def __init__(self, app):

		# Set hierarchy for frames and widgets
		self.parent = app.root
		self.root = Tk()
		Pmw.initialise(self.root)

		# Set some general data
		self.root.geometry("400x400")
		self.root.title("BioNet Loader")

		# Build a frame
		self.topFrame = Frame(self.root)
		self.topFrame.pack()

		# Initialise all system variables
		self.initVars()

		# Build a UI
		self.buildUI(self.topFrame)

	def exit(self):
		self.root.destroy()

	def getRandomSystemName(self):
		return self.possibleSystemNames[np.random.randint(0, high=len(self.possibleSystemNames))]

	def setRandomSystemName(self):

		self.newSystemName.set(self.getRandomSystemName())

	def initVars(self):

		# Additioanl frames
		self.newSysFrame = None
		self.newTrajFrame = None
		self.newTopFrame = None
		self.newPoreFrame = None

		# Paths
		self.inputDir = os.getcwd()
		self.outputDir = os.getcwd()

		# System names
		fnames = ["system_names_greekletters.txt", "system_names_dbzcharacters.txt", "system_names_pokemon.txt"]
		self.possibleSystemNames = []
		for f in fnames:
			self.possibleSystemNames.extend(list(np.genfromtxt(os.path.dirname(os.path.realpath(__file__)) + "/dataFiles/" + f, dtype='str')))

		self.systemNames = []
		self.selectedSystemName = StringVar(value="")
		self.newSystemName = StringVar(value=self.getRandomSystemName())

		# System variables
		self.drawNodes = BooleanVar(value=True)
		self.drawConnections = BooleanVar(value=False)
		self.drawSites = BooleanVar(value=False)
		self.drawBox = BooleanVar(value=True)
		self.colouriseInt = IntVar(value = 0)

		self.nodeColour = [0.0,0.0,1.0]
		self.conColour = [105.0/250.0, 105.0/250.0, 105.0/250.0]
		self.siteColour = [100.0/255.0, 100.0/255.0, 100.0/255.0]
		self.boxColour = [0.0, 1.0, 0.0]
#		self.poreColour = [1.0,0.0,0.0]
		self.poreColour = [81.0 / 255.0, 235.0 / 255.0, 251.0 / 255.0]
		self.throatColour = [1.0,0.0,0.0]

		# Traj vars
		self.framesToLoad = IntVar(value=100000)
		self.frameRate = IntVar(value=1)

		# Error messages
		self.displayErrorMessages = {}

		# Structural variables
		self.systems = {}
		self.numSystems = 0

		self.trajectories = {}
		self.numTrajectories = 0

		self.topologies = {}
		self.numTopologies = 0

		self.pores = {}
		self.numPores = 0

		# Drawable objects
		self.frame = []
		self.numFrames = 0
		self.numTopologyFrames = 0

	def buildSystemFromInput(self, inFile):

		# Easy, just load a single frame straight from the network
		self.frame = [PymolFrame.PymolFrame()]
		self.numFrames = 1

		self.frame[0].buildFromInput(inFile, dn=self.drawNodes.get(), dc=self.drawConnections.get(), ds=self.drawSites.get(), cn=self.nodeColour, cc=self.conColour, cs=self.siteColour, cb=self.boxColour)

		# Finalise
		self.selectedSystemName.set(self.newSystemName.get())
		self.systemNames.append(self.selectedSystemName.get())
		return BioNetSuccess

	def rebuildFrame(self, fIndex, aSys, mode):

		if mode == 0 or mode > 1:
			print("Currently unsupported :(")
			return BioNetSuccess

		return self.frame[fIndex].rebuild(mode, aSys, cn=self.nodeColour, cc=self.conColour, cs=self.siteColour, cb=self.boxColour)

	def buildSystemFromTrajectory(self, tFname, aSys, inScript=None):

		# Read traj 1 frame at a time for memory
		batchSize = 100

		# Init
		self.frame = []
		self.numFrames = 0

		# Get a trajectory and an associated system
		traj = BioNetTrajectory.BioNetTrajectory()

		# Open
		fin = open(tFname, "r")

		# Check header
		line = fin.readline().strip()
		if line != "BioNet Trajectory" and line != "BioNet Trajectory":
			printError("Found '%s', expected '%s'.\n\tThis doesn't seem to be the correct file type :(" % (line, "BioNet Trajectory"))
			return BioNetError

		# Read pointless lines
		while "#" not in line:
			line = fin.readline()

		# Read the header and test
		traj.readHeader(fin)

		if self.drawNodes.get() and traj.numNodes != aSys.numNodes:
			printError("Incorrect number of nodes. Trajectory does not correspond to selected system")
			return BioNetError

		for i in range(aSys.numNodes):

			# Check if sites were used
			if traj.numSites[i] == 0:
				aSys.node[i].protein.numSites = 0

			elif self.drawSites.get() and aSys.node[i].protein.numSites != traj.numSites[i]:
				printError("Incorrect number of sites. Trajectory does not correspond to selected system")
				return BioNetError

		#
		# Read the trajectory
		#
		rate = self.frameRate.get()

		# Get to the right line
#		count = 0
#		while count != 2:
#			if "#####" in fin.readline():
#				count += 1

		# Load frames
		totalFrames = 0

		numFrames = self.framesToLoad.get()
	
		while(self.numFrames < numFrames):

			# Load based on rate
			if (totalFrames) % rate != 0:

				# Update
				totalFrames += 1
				if traj.skipFrame(fin) == BioNetError:
					break

			else:

				# Write				
				sys.stdout.write("\r\tRead %d frames..." % (self.numFrames))
				sys.stdout.flush()

				# Extend frames in batches of 100 for speed
				if(self.numFrames % batchSize == 0):
					self.frame.extend([None for i in range(batchSize)])

				# Get a frame
				frame = BioNetTrajectory.BioNetTrajectoryFrame()
				if(frame.load(fin, traj.numNodes, traj.numSites, inScript=aSys) == BioNetError):
					print("Unable to load trajectory further than frame %d. Will continue from here" % (self.numFrames))
				if(frame.loaded):
					self.frame[self.numFrames] = PymolFrame.PymolFrame()
					self.frame[self.numFrames].buildFromTrajectoryFrame(frame, aSys, frameIndex=self.numFrames + 1, dn=self.drawNodes.get(), dc=self.drawConnections.get(), ds=self.drawSites.get(), cn=self.nodeColour, cc=self.conColour, cs=self.siteColour, cb=self.boxColour)
					self.numFrames += 1
					totalFrames += 1
				else:
					break
		sys.stdout.write("\nTrajectory completed. Read %d frames out of %d." % (self.numFrames, totalFrames))
		sys.stdout.flush()

		# Finalise
		fin.close()
		self.frame = self.frame[:self.numFrames]
		return BioNetSuccess

	def buildSystemFromTopology(self, tpFname, aSys, inScript=None):

		# Init
		self.numTopologyFrames = 0

		# Get a topology and an associated system
		top = BioNetTopology.BioNetTopology()

		# Open
		fin = open(tpFname, "r")

		# Check header
		line = fin.readline().strip()
		if line != "BioNet Topology":
			printError("Found '%s', expected '%s'.\n\tThis doesn't seem to be the correct file type :(" % (line, "BioNet Topology"))
			return BioNetError

		# Read pointless lines
		while "#" not in line:
			line = fin.readline()

		# Read the header and test
		top.readHeader(fin)

		if self.drawNodes.get() and top.numNodes != aSys.numNodes:
			printError("Incorrect number of nodes. Topology does not correspond to selected system")
			return BioNetError

		#
		# Read the topology until we reach the num frames on the associated trajectory
		#
		rate = self.frameRate.get()

		# Load frames
		totalFrames = 0

		numFrames = self.numFrames

		while(self.numTopologyFrames < numFrames):

			# Load based on rate
			if (totalFrames) % rate != 0:

				# Update
				totalFrames += 1
				if top.skipFrame(fin) == BioNetError:
					break

			else:

				# Write				
				sys.stdout.write("\r\tRead %d frames..." % (self.numTopologyFrames))
				sys.stdout.flush()

				# Get a frame
				frame = BioNetTopology.BioNetTopologyFrame()
				if(frame.load(fin, top.numNodes) == BioNetError):
					print("Unable to load trajectory further than frame %d. Will continue from here" % (self.numTopologyFrames))

				# Move into pymol language
				if(frame.loaded):
					self.frame[self.numTopologyFrames].buildFromTopologyFrame(frame, aSys, frameIndex=self.numTopologyFrames + 1, dn=self.drawNodes.get(), dc=True, ds=self.drawSites.get(), cn=self.nodeColour, cc=self.conColour, cs=self.siteColour, cb=self.boxColour)

					self.numTopologyFrames += 1
					totalFrames += 1
				else:
					break
		sys.stdout.write("\nTopology completed. Read %d frames out of %d." % (self.numTopologyFrames, totalFrames))
		sys.stdout.flush()

		# Finalise
		fin.close()
		return BioNetSuccess

	def buildSystemFromPores(self, pFname, aSys, inScript=None):

		# Read traj 1 frame at a time for memory
		batchSize = 1

		# Init
		self.numPoreFrames = 1

		# Get a trajectory and an associated system
		pores = BioNetPores.BioNetPores()

		#
		# Read the pores (currently very simple, 1 frame)
		#
		pores.load(pFname)
		frame = pores.frame[0]
		if(frame.loaded):
			self.frame[0].addPores(pores.frame[0], frameIndex=1, cp=self.poreColour, ct=self.throatColour)

		return BioNetSuccess

	def draw(self, source, frame):

		# Box
		if(len(self.frame) != 0 and self.drawBox.get()):
			cmd.load_cgo(frame[0].bDrawable, self.selectedSystemName.get() + "BOX", 1)
			cmd.set("cgo_line_width", 5, self.selectedSystemName.get() + "BOX")

		# Network (PyMOL starts counting frames from 1 for some reason...)
	
		# Always draw (unless flagged)
		for f in frame:
			if(self.drawNodes.get() and f.nDrawable != []):
				cmd.load_cgo(f.nDrawable, self.selectedSystemName.get() + "NODE", f.index)
			if(self.drawSites.get() and f.sDrawable != []):
				cmd.load_cgo(f.sDrawable, self.selectedSystemName.get() + "SITE", f.index)
			if(f.index == 1 and f.pDrawable != []):
				cmd.load_cgo(f.pDrawable, self.selectedSystemName.get() + "PORES", f.index)
			if(f.index == 1 and f.tDrawable != []):
				cmd.load_cgo(f.tDrawable, self.selectedSystemName.get() + "THROATS", f.index)
#			fIndex += 1

		# Sometimes draw
		if(source == "topology" or (self.drawConnections.get() and (source == "script" or source == "trajectory"))):
#			fIndex = 1
			something = False
			for f in frame:
				if(f.cDrawable != []):
					something = True
					cmd.load_cgo(f.cDrawable, self.selectedSystemName.get() + "CONNECTION", f.index)
#					fIndex += 1
			if something:
				cmd.set("cgo_line_width", 10, self.selectedSystemName.get() + "CONNECTION")

	def loadRecolourise(self):

		# The system is already load, so we just need the mode, the frame and the colours
		mode = self.colouriseInt.get()
		fIndex = cmd.get_state()
		print("findex: ", fIndex) 

		# Load the system from this
		if(self.rebuildFrame(fIndex - 1, self.systems[self.selectedSystemName.get()], mode) == BioNetError):
			printError("Unable to rebuild frame %d to recolour :(" % (fIndex))
			return BioNetError

		if(self.draw("trajectory", [self.frame[fIndex - 1]]) == BioNetError):
			printError("Unable to draw system")
			return BioNetError

		# If everything is ok, close the window
		self.newColFrame.destroy()
	
	def loadSystem(self):

		# Get a filename (.xml)
		ext = ".xml"
		options = {}
		options['defaultextension'] = ext
		options['filetypes'] = [('network files', ext), ('all files', '.*')]
		options['initialdir'] = self.inputDir
		options['title'] = 'Load Network Data File'

		# Ask user to select a file. If nothing, just return
		iFname = tkFileDialog.askopenfilename(**options)
		if iFname == "" or len(iFname) == 0:
#			self.addErrorMessage("oFname", "None", "Output filename not selected. Please try again")
			return -1

		# Set the new input directory for next time
		self.inputDir = os.path.dirname(iFname)
		
		# Load the input script
		inFile = BioNetInput.BioNetInput()
		if(inFile.load(iFname) == BioNetError):
			printError("Unable to load input file")
			return BioNetError

		# Load the system from this
		if(self.buildSystemFromInput(inFile) == BioNetError):
			printError("Unable to build system from input file")
			return BioNetError

		if(self.draw("script", self.frame) == BioNetError):
			printError("Unable to draw system")
			return BioNetError


		# Assign system
		self.systems[self.newSystemName.get()] = inFile
		self.numSystems += 1

		# Flag trajectorie and topology as not loaded
		self.trajectories[self.newSystemName.get()] = False
		self.topologies[self.newSystemName.get()] = False

		# If everything is ok, close thenew system window
		self.newSysFrame.destroy()

	def loadTrajectory(self):

		# Get a filename (.bntrj)
		ext = ".bntrj"
		options = {}
		options['defaultextension'] = ext
		options['filetypes'] = [('network files', ext), ('all files', '.*')]
		options['initialdir'] = self.outputDir
		options['title'] = 'Load Network Data File'

		# Ask user to select a file. If nothing, just return
		tFname = tkFileDialog.askopenfilename(**options)
		if tFname == "" or len(tFname) == 0:
#			self.addErrorMessage("oFname", "None", "Output filename not selected. Please try again")
			return -1

		# Set the new input directory for next time
		self.outputDir = os.path.dirname(tFname)

		# Load the system from this
		if(self.buildSystemFromTrajectory(tFname, self.systems[self.selectedSystemName.get()]) == BioNetError):
			printError("Unable to build system from trajectory file")
			return BioNetError

		if(self.draw("trajectory", self.frame) == BioNetError):
			printError("Unable to draw system")
			return BioNetError

		# Assign trajectory object as loaded
		self.trajectories[self.selectedSystemName.get()] = True
		self.numTrajectories += 1

		# If everything is ok, close thenew system window
		self.newTrajFrame.destroy()

	def loadTopology(self):

		# Get a filename (.bntop)
		ext = ".bntop"
		options = {}
		options['defaultextension'] = ext
		options['filetypes'] = [('network files', ext), ('all files', '.*')]
		options['initialdir'] = self.outputDir
		options['title'] = 'Load Network Data File'

		# Ask user to select a file. If nothing, just return
		tpFname = tkFileDialog.askopenfilename(**options)
		if tpFname == "" or len(tpFname) == 0:
#			self.addErrorMessage("oFname", "None", "Output filename not selected. Please try again")
			return -1

		# Set the new input directory for next time
		self.outputDir = os.path.dirname(tpFname)

		
		# Load the system from this
		if(self.buildSystemFromTopology(tpFname, self.systems[self.selectedSystemName.get()]) == BioNetError):
			printError("Unable to build system from topology file")
			return BioNetError

		
		if(self.draw("topology", self.frame) == BioNetError):
			printError("Unable to draw system")
			return BioNetError
		

		# If everything is ok, close thenew system window
		self.newTopFrame.destroy()
		pass

	def loadPores(self):

		# Get a filename (.bntrj)
		ext = ".bnpores"
		options = {}
		options['defaultextension'] = ext
		options['filetypes'] = [('network files', ext), ('all files', '.*')]
		options['initialdir'] = self.outputDir
		options['title'] = 'Load Network Data File'

		# Ask user to select a file. If nothing, just return
		pFname = tkFileDialog.askopenfilename(**options)
		if pFname == "" or len(pFname) == 0:
#			self.addErrorMessage("oFname", "None", "Output filename not selected. Please try again")
			return -1

		# Set the new input directory for next time
		self.outputDir = os.path.dirname(pFname)

		# Load the system from this
		if(self.buildSystemFromPores(pFname, self.systems[self.selectedSystemName.get()]) == BioNetError):
			printError("Unable to load pores into the system")
			return BioNetError

		if(self.draw("pores", self.frame) == BioNetError):
			printError("Unable to draw pores")
			return BioNetError

		# Assign pores object as loaded
		self.pores[self.selectedSystemName.get()] = True
		self.numPores += 1

		# If everything is ok, close thenew system window
		self.newPoreFrame.destroy()


	def loadEigensystem(self):
		print("Load eigen")
		pass

	def showNewSystemWindow(self):

		# This method loads the input file only!
		
		# Open a new window
		self.newSysFrame = Toplevel()
		self.newSysFrame.geometry("400x300")
		self.newSysFrame.title("New System")

		# System name (randomise)
		nameLabel = Label(self.newSysFrame, text="System Name")
		nameLabel.pack(side=LEFT)

		self.setRandomSystemName()
		nameEntry = Entry(self.newSysFrame, bd = 5, textvariable=self.newSystemName, validate="focus")#, validatecommand=(self.validCommand, '%W', '%P'))
		nameEntry.pack(side=LEFT)

		nameRandomButton = Button(self.newSysFrame, text="Randomise", command=self.setRandomSystemName)
		nameRandomButton.pack(side=RIGHT)

		loadButton = Button(self.newSysFrame, text="Load", command=self.loadSystem)
		loadButton.pack(side=BOTTOM)

	def showNewColouriseMenu(self):

		# This method loads the trajectory, if possible only!
		if(len(self.systemNames) == 0):
			printError("No systems loaded")
			return BioNetError

		# Can only draw forces if trajectory has been loaded already. Check for trajectories
		sysNames = []
		for n in self.systemNames:
#			if self.trajectories[n] == True:
			sysNames.append(n)

		if(len(sysNames) == 0):
			printError("No trajectories loaded")
			return BioNetError

		# Open a new window
		self.newColFrame = Toplevel()
		self.newColFrame.geometry("700x300")
		self.newColFrame.title("Recolour")

		# System selector
		nameLabel = Label(self.newColFrame, text="System Name")
		nameLabel.pack(side=LEFT, anchor=N)

		sysNames = tuple(sysNames)
		nameOptionMenu = OptionMenu(self.newColFrame, self.newSystemName, *sysNames)
		nameOptionMenu.pack()

		# Colourise method
		nameLabel = Label(self.newColFrame, text="Colourise by:")
		nameLabel.pack(side=LEFT)

		colouriseModes = [("Forces", 1),("Stresses", 2),("Volume", 3),("Connectivity", 4)]

		for text, mode in colouriseModes:
			colRB = Radiobutton(self.newColFrame, text=text,variable=self.colouriseInt, value=mode)
			colRB.pack(side=LEFT)

		# Load button
		colButton = Button(self.newColFrame, text="Recolourise!", command=self.loadRecolourise)
		colButton.pack(side=RIGHT, anchor=S)


	def showNewTrajectoryWindow(self):

		# This method loads the trajectory, if possible only!
		if(len(self.systemNames) == 0):
			printError("No systems loaded")
			return BioNetError

		# Open a new window
		self.newTrajFrame = Toplevel()
		self.newTrajFrame.geometry("400x300")
		self.newTrajFrame.title("New Trajectory")

		# System selector
		nameLabel = Label(self.newTrajFrame, text="System Name")
		nameLabel.pack(side=LEFT, anchor=N)

		sysNames = (n for n in self.systemNames)
		nameOptionMenu = OptionMenu(self.newTrajFrame, self.newSystemName, *sysNames)
		nameOptionMenu.pack()


		loadButton = Button(self.newTrajFrame, text="Load", command=self.loadTrajectory)
		loadButton.pack(side=BOTTOM)

	def showNewTopologyWindow(self):

		# This method loads the topology, if possible only!
		if(len(self.systemNames) == 0):
			printError("No systems loaded")
			return BioNetError

		# Can only load if trajectory has been loaded already. Check for trajectories
		sysNames = []
		for n in self.systemNames:
			if self.trajectories[n] == True:
				sysNames.append(n)

		if(len(sysNames) == 0):
			printError("No trajectories loaded")
			return BioNetError

		# Open a new window
		self.newTopFrame = Toplevel()
		self.newTopFrame.geometry("400x300")
		self.newTopFrame.title("New Topology")

		# System selector
		nameLabel = Label(self.newTopFrame, text="System Name")
		nameLabel.pack(side=LEFT, anchor=N)

		sysNames = tuple(sysNames)
		nameOptionMenu = OptionMenu(self.newTopFrame, self.newSystemName, *sysNames)
		nameOptionMenu.pack()


		loadButton = Button(self.newTopFrame, text="Load", command=self.loadTopology)
		loadButton.pack(side=BOTTOM)

	def showNewPoresWindow(self):

		# This method loads the pores, if possible only!
		if(len(self.systemNames) == 0):
			printError("No systems loaded")
			return BioNetError

		# Open a new window
		self.newPoreFrame = Toplevel()
		self.newPoreFrame.geometry("400x300")
		self.newPoreFrame.title("New Pores")

		# System selector
		nameLabel = Label(self.newPoreFrame, text="System Name")
		nameLabel.pack(side=LEFT)

		sysNames = (n for n in self.systemNames)
		nameOptionMenu = OptionMenu(self.newPoreFrame, self.newSystemName, *sysNames)
		nameOptionMenu.pack()


		loadButton = Button(self.newPoreFrame, text="Load", command=self.loadPores)
		loadButton.pack(side=BOTTOM)

	def showAboutWindow(self):

		aboutString = "******************************************\n**************                      **************\n****** Biological Network Visualiser ******\n**************                      **************\n******************************************\n\nVisualisation software associated with BioNet, a Biological Network simulation package.\n\nVersion -	%s\n\nTheory  -	Benjamin Hanson (b.s.hanson@leeds.ac.uk)\nCoding  -	Benjamin Hanson (b.s.hanson@leeds.ac.uk)\n\n" % (getVersionString())
		tkMessageBox.showinfo("About", aboutString)

	def buildUI(self, parentFrame):

		#
		# Build a file layout
		#

		# Main menu
		mainMenu = Menu(self.root)
		self.root.config(menu=mainMenu)

		# System Menu
		sysMenu = Menu(mainMenu)

		# New
		sysMenu.add_command(label="New", command=self.showNewSystemWindow)
		
		# Load
		loadMenu = Menu(sysMenu)
		loadMenu.add_command(label="Load Trajectory", command=self.showNewTrajectoryWindow)
		loadMenu.add_command(label="Load Topology", command=self.showNewTopologyWindow)
		loadMenu.add_command(label="Load Pores", command=self.showNewPoresWindow)
		loadMenu.add_command(label="Load Eigensystem", command=self.loadEigensystem)

		# Add to parent
		sysMenu.add_cascade(label="Load...", menu=loadMenu)

		# Exit
		sysMenu.add_command(label="Exit", command=self.exit)

		# Add to parent
		mainMenu.add_cascade(label="File", menu=sysMenu)

		# Edit menu
		editMenu = Menu(mainMenu)

		# Colour menu
		colourMenu = Menu(editMenu)

		# Colour options


		# Add to parent
		editMenu.add_command(label="Recolour", command=self.showNewColouriseMenu)

		# Add to parent
		mainMenu.add_cascade(label="Edit", menu=editMenu)

		# Help Menu
		helpMenu = Menu(mainMenu)

		# About
		helpMenu.add_command(label="About", command=self.showAboutWindow)

		# Add to parent
		mainMenu.add_cascade(label="Help", menu=helpMenu)

		#
		# Make some frames
		#
		sysFrame = Frame(self.root)
		sysFrame.pack(anchor=NW)
		frameSep1 = Frame(self.root, height=2, bd=1, relief=SUNKEN)
		frameSep1.pack(fill=X, padx=5, pady=5)
		trajFrame = Frame(self.root)
		trajFrame.pack()

		#
		# System design
		#

		sysLabel = Label(sysFrame, text="System")
#		sysLabel.pack(anchor=NW)
		sysLabel.grid(row=0)

		# Draw nodes
		drawNodeCheck = Checkbutton(sysFrame, text="Draw Nodes", variable = self.drawNodes, onvalue = True, offvalue = False, height=2, command=self.boolHackNode)

		if(self.drawNodes.get()):
			drawNodeCheck.select()

#		drawNodeCheck.pack(anchor=NW)
		drawNodeCheck.grid(row=1)

		nodeColourButton = Button(sysFrame, text='Select Node Color', command=self.setNodeColor)
#		nodeColourButton.pack(anchor=NE)
		nodeColourButton.grid(row=1, column=1)

		# Draw connections
		drawConCheck = Checkbutton(sysFrame, text="Draw Connections", variable = self.drawConnections, onvalue = True, offvalue = False, height=2, command=self.boolHackCon)
		if(self.drawConnections.get()):
			drawConCheck.select()

#		drawConCheck.pack(anchor=NW)
		drawConCheck.grid(row=2)

		conColourButton = Button(sysFrame, text='Select Connection Color', command=self.setConColor)
#		conColourButton.pack(anchor=NE)
		conColourButton.grid(row=2, column=1)

		# Draw sites
		drawSitesCheck = Checkbutton(sysFrame, text="Draw Sites", variable = self.drawSites, onvalue = True, offvalue = False, height=2, command=self.boolHackSite)

		if(self.drawSites.get()):
			drawSitesCheck.select()

#		drawSitesCheck.pack(anchor=NW)
		drawSitesCheck.grid(row=3)

		siteColourButton = Button(sysFrame, text='Select Site Color', command=self.setSiteColor)
#		siteColourButton.pack(anchor=NE)
		siteColourButton.grid(row=3, column=1)

		# Draw box
		drawBoxCheck = Checkbutton(sysFrame, text="Draw Box", variable = self.drawBox, onvalue = True, offvalue = False, height=2, command=self.boolHackBox)

		if(self.drawBox.get()):
			drawBoxCheck.select()

#		drawBoxCheck.pack(anchor=NW)
		drawBoxCheck.grid(row=4)

		boxColourButton = Button(sysFrame, text='Select Box Color', command=self.setBoxColor)
#		boxColourButton.pack(anchor=NE)
		boxColourButton.grid(row=4, column=1)

		#
		# Trajectory stuff
		#
		framesLabel = Label(trajFrame, text="Frames to Load").grid(row=0)
		self.framesEntry = Entry(trajFrame, validate="focus", validatecommand=self.framesEntryHack)
		self.framesEntry.grid(row=0, column=1)
		self.framesEntry.insert(END, self.framesToLoad.get())

		rateLabel = Label(trajFrame, text="Frame Load Rate").grid(row=1)
		self.rateEntry = Entry(trajFrame, validate="focus", validatecommand=self.rateEntryHack)
		self.rateEntry.grid(row=1, column=1)
		self.rateEntry.insert(END, self.frameRate.get())

	def boolHackNode(self):
		self.drawNodes.set(not self.drawNodes.get())

	def setNodeColor(self):
		newCol = tkColorChooser.askcolor()[0]
		if(newCol == None):
			return
		else:
			self.nodeColour = np.array(newCol) / 255.0
			for i in range(3):
				if self.nodeColour[i] > 1.0:
					self.nodeColour[i] = 1.0
				elif self.nodeColour[i] < 0.0:
					self.nodeColour[i] = 0.0

	def boolHackCon(self):
		self.drawConnections.set(not self.drawConnections.get())

	def setConColor(self):
		newCol = tkColorChooser.askcolor()[0]
		if(newCol == None):
			return
		else:
			self.conColour = np.array(newCol) / 255.0
			for i in range(3):
				if self.conColour[i] > 1.0:
					self.conColour[i] = 1.0
				elif self.conColour[i] < 0.0:
					self.conColour[i] = 0.0

	def boolHackSite(self):
		self.drawSites.set(not self.drawSites.get())

	def setSiteColor(self):
		newCol = tkColorChooser.askcolor()[0]
		if(newCol == None):
			return
		else:

			self.siteColour = np.array(newCol) / 255.0
			for i in range(3):
				if self.siteColour[i] > 1.0:
					self.siteColour[i] = 1.0
				elif self.siteColour[i] < 0.0:
					self.siteColour[i] = 0.0

	def boolHackBox(self):
		self.drawBox.set(not self.drawBox.get())

	def setBoxColor(self):
		newCol = tkColorChooser.askcolor()[0]
		if(newCol == None):
			return
		else:

			self.boxColour = np.array(newCol) / 255.0
			for i in range(3):
				if self.boxColour[i] > 1.0:
					self.boxColour[i] = 1.0
				elif self.boxColour[i] < 0.0:
					self.boxColour[i] = 0.0

	def framesEntryHack(self):

		try:
			self.framesToLoad.set(self.framesEntry.get())
			if(self.framesToLoad.get() < 0):
				self.framesToLoad.set(self.framesToLoad.get() * -1)

		except:
			self.framesToLoad.set(10000)

		return True

	def rateEntryHack(self):

		try:
			self.frameRate.set(self.rateEntry.get())
			if(self.frameRate.get() < 0):
				self.frameRate.set(self.frameRate.get() * -1)

		except:
			self.frameRate.set(1)
	
		return True
