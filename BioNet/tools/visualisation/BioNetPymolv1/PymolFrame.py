import numpy as np
import copy
import BioNetInput, BioNetTrajectory, BioNetMeasurement, BioNetTopology

from pymol.cgo import * 

from UserInfo import *

from BioNetTrajectory import ZXZToEuler, EulerToZXZ

nodeDefaultColour = [0.0,0.0,1.0]
conDefaultColour = [105.0/250.0, 105.0/250.0, 105.0/250.0]
siteDefaultColour = [100.0/255.0, 100.0/255.0, 100.0/255.0]

boxDefaultColour = [0.0,1.0,0.0]
boxDefaultWidth = 3

#poreDefaultColour = [1.0,0.0,0.0]
poreDefaultColour = [81.0 / 255.0, 235.0 / 255.0, 251.0 / 255.0]
throatDefaultColour = [1.0,0.0,0.0]

class PymolFrame:

	def __init__(self):

		self.index = 1
		self.pos = []
		self.sPos = []
		self.cPos = []

		self.pPos = []
		self.pRad = []
		self.tPos = []
		self.pRad = []

		self.vel = None
		self.acc = None
		self.force = None

		self.radius = None
		self.connectionWidth = 0.12 # amino acid width

		self.nodeAttach = True

		self.nDrawable = []
		self.sDrawable = []
		self.cDrawable = []
		self.bDrawable = []

		self.pDrawable = []
		self.tDrawable = []

	def build(self, inFile, cn=nodeDefaultColour, cc=conDefaultColour, cs=siteDefaultColour, cb=boxDefaultColour):


		#
		# Build the actual drawable objects
		#

		#
		# Nodes
		#

		if(len(self.pos) != 0):
			self.nDrawable = [COLOR]
			self.nDrawable.extend(cn)

			offset = len(self.nDrawable)
			self.nDrawable.extend([None for i in range(5 * inFile.numNodes)])
				
			for i in range(inFile.numNodes):

				self.nDrawable[offset + 5 * i] = SPHERE
				self.nDrawable[offset + 5 * i + 1:offset + 5 * i + 4] = list(self.pos[i])
				self.nDrawable[offset + 5 * i + 4] = self.radius[i]
#				self.nDrawable[offset + 5 * i + 4] = 0.2
		else:
			self.nDrawable = []

		#
		# Sites
		#

		if(len(self.sPos) != 0):
			self.sDrawable = [COLOR]
			self.sDrawable.extend(cs)
			
			offset = len(self.sDrawable)
			self.sDrawable.extend([None for i in range(5 * int(np.sum([n.protein.numSites for n in inFile.node])))])
			for i in range(inFile.numNodes):
				for j in range(inFile.node[i].protein.numSites):
					index = 5 * (int(np.sum([inFile.node[n].protein.numSites for n in range(i)])) + j)
	#				index = 5 * (inFile.node[i].protein.numSites * i + j)
					self.sDrawable[offset + index] = SPHERE
					self.sDrawable[offset + index + 1:offset + index + 4] = list(self.sPos[i][j])
					self.sDrawable[offset + index + 4] = 0.18	# Radius, amino-acid sized
					
		else:
			self.sDrawable = []

		#
		# Connections
		#
		# Init size in bulk where possible

		if(len(self.cPos) != 0):
			"""
			self.cDrawable = [BEGIN, LINES]
			self.cDrawable.extend([COLOR, 105.0/250.0, 105.0/250.0, 105.0/250.0]) # Grey

			offset = len(self.cDrawable)
			self.cDrawable.extend([None for i in range(8 * inFile.numConnections + 1)])

			for i in range(inFile.numConnections):
				for j in range(2):
					self.cDrawable[offset + 8 * i + 4 * j] = VERTEX
					self.cDrawable[offset + 8 * i + 4 * j + 1:offset + 8 * i + 4 * j + 4] = list(self.cPos[i][j])

			self.cDrawable[-1] = END

			"""
			self.cDrawable = []
			offset = 0
			self.cDrawable.extend([None for i in range(14 * len(self.cPos))])
			for i in range(len(self.cPos)):

				# Check against box
				if inFile.box.BC == "pbc":

#					L = np.linalg.norm(self.cPos[i][0] - self.cPos[i][1])
#					if(np.any(2 * L > inFile.box.dimension)):
#						offset -= 14
#						continue

					# Get the images
					transFlag = np.array([0, 0, 0])
					trans = np.array([0.0,0.0,0.0])
					for j in range(3):
						L = self.cPos[i][1][j] - self.cPos[i][0][j]
						if L > inFile.box.dimension[j] / 2.0:
							transFlag[j] = 1
							trans[j] = inFile.box.dimension[j]
						elif L < -inFile.box.dimension[j] / 2.0:
							transFlag[j] = -1
							trans[j] = -inFile.box.dimension[j]


					if np.any(transFlag != 0):

						# We'll need two connections to cross the boundary
						
						self.cDrawable.extend([None for i in range(14)])	# Extend array

						p = [self.cPos[i][0], self.cPos[i][1]]
						pIm = [self.cPos[i][0] + trans, self.cPos[i][1] - trans]
						for j in range(2):
							r = pIm[(j + 1) % 2] - p[j]
							rMag = np.linalg.norm(r)
							rHat = r / rMag
							
							scale = 1.0
							for k in range(3):
								newScale = 1.0
								if pIm[(j + 1) % 2][k] < 0.0:
									newScale = np.fabs(p[j][k] / r[k])
								elif pIm[(j + 1) % 2][k] > inFile.box.dimension[k]:
									newScale = np.fabs((p[j][k] - inFile.box.dimension[k]) / r[k])

								if newScale < scale:
									scale = copy.deepcopy(newScale)
								
							target = p[j] + r * scale
#							for k in range(3):
#								if target[k] < 0.0 or target[k] > inFile.box.dimension[k]:
#									print("Shit %d: %f" % (j,target[k]))
									
							self.cDrawable[offset + 14 * i] = CYLINDER
							self.cDrawable[offset + 14 * i + 1:offset + 14 * i + 4] = list(p[j])
							self.cDrawable[offset + 14 * i + 4:offset + 14 * i + 7] = list(target)
							self.cDrawable[offset + 14 * i + 7] = self.connectionWidth
							self.cDrawable[offset + 14 * i + 8:offset + 14 * i + 11] = cc
							self.cDrawable[offset + 14 * i + 11:offset + 14 * i + 14] = cc

							if j == 0:
								offset += 14
						
					else:
					
						self.cDrawable[offset + 14 * i] = CYLINDER
						self.cDrawable[offset + 14 * i + 1:offset + 14 * i + 4] = list(self.cPos[i][0])
						self.cDrawable[offset + 14 * i + 4:offset + 14 * i + 7] = list(self.cPos[i][1])
						self.cDrawable[offset + 14 * i + 7] = self.connectionWidth
						self.cDrawable[offset + 14 * i + 8:offset + 14 * i + 11] = cc
						self.cDrawable[offset + 14 * i + 11:offset + 14 * i + 14] = cc
							
				else:
				
					self.cDrawable[offset + 14 * i] = CYLINDER
					self.cDrawable[offset + 14 * i + 1:offset + 14 * i + 4] = list(self.cPos[i][0])
					self.cDrawable[offset + 14 * i + 4:offset + 14 * i + 7] = list(self.cPos[i][1])
					self.cDrawable[offset + 14 * i + 7] = self.connectionWidth
					self.cDrawable[offset + 14 * i + 8:offset + 14 * i + 11] = cc
					self.cDrawable[offset + 14 * i + 11:offset + 14 * i + 14] = cc

			# Resize
			self.cDrawable = self.cDrawable[:14 * len(self.cPos) + offset]
		else:
			self.cDrawable = []

		#
		# Box
		#
		self.bDrawable = [BEGIN, LINES]
		self.bDrawable.extend([COLOR])
		self.bDrawable.extend(cb)

		self.bDrawable.extend([LINEWIDTH])
		self.bDrawable.extend([boxDefaultWidth])
		
		offset = len(self.bDrawable)
		self.bDrawable.extend([None for i in range(8 * 12 + 1)])

		# Refactor!
		self.bDrawable[offset + 0] = VERTEX
		self.bDrawable[offset + 1:offset + 4] = [0.0,0.0,0.0]
		self.bDrawable[offset + 4] = VERTEX
		self.bDrawable[offset + 5:offset + 8] = [inFile.box.dimension[0],0.0,0.0]

		self.bDrawable[offset + 8] = VERTEX
		self.bDrawable[offset + 9:offset + 12] = [0.0,inFile.box.dimension[1],0.0]
		self.bDrawable[offset + 12] = VERTEX
		self.bDrawable[offset + 13:offset + 16] = [inFile.box.dimension[0],inFile.box.dimension[1],0.0]

		self.bDrawable[offset + 16] = VERTEX
		self.bDrawable[offset + 17:offset + 20] = [0.0,0.0,inFile.box.dimension[2]]
		self.bDrawable[offset + 20] = VERTEX
		self.bDrawable[offset + 21:offset + 24] = [inFile.box.dimension[0],0.0,inFile.box.dimension[2]]

		self.bDrawable[offset + 24] = VERTEX
		self.bDrawable[offset + 25:offset + 28] = [0.0,inFile.box.dimension[1],inFile.box.dimension[2]]
		self.bDrawable[offset + 28] = VERTEX
		self.bDrawable[offset + 29:offset + 32] = [inFile.box.dimension[0],inFile.box.dimension[1],inFile.box.dimension[2]]

		self.bDrawable[offset + 32] = VERTEX
		self.bDrawable[offset + 33:offset + 36] = [0.0,0.0,0.0]
		self.bDrawable[offset + 36] = VERTEX
		self.bDrawable[offset + 37:offset + 40] = [0.0,inFile.box.dimension[1],0.0]

		self.bDrawable[offset + 40] = VERTEX
		self.bDrawable[offset + 41:offset + 44] = [inFile.box.dimension[0],0.0,0.0]
		self.bDrawable[offset + 44] = VERTEX
		self.bDrawable[offset + 45:offset + 48] = [inFile.box.dimension[0],inFile.box.dimension[1],0.0]

		self.bDrawable[offset + 48] = VERTEX
		self.bDrawable[offset + 49:offset + 52] = [0.0,0.0,inFile.box.dimension[2]]
		self.bDrawable[offset + 52] = VERTEX
		self.bDrawable[offset + 53:offset + 56] = [0.0,inFile.box.dimension[1],inFile.box.dimension[2]]

		self.bDrawable[offset + 56] = VERTEX
		self.bDrawable[offset + 57:offset + 60] = [inFile.box.dimension[0],0.0,inFile.box.dimension[2]]
		self.bDrawable[offset + 60] = VERTEX
		self.bDrawable[offset + 61:offset + 64] = [inFile.box.dimension[0],inFile.box.dimension[1],inFile.box.dimension[2]]

		self.bDrawable[offset + 64] = VERTEX
		self.bDrawable[offset + 65:offset + 68] = [0.0,0.0,0.0]
		self.bDrawable[offset + 68] = VERTEX
		self.bDrawable[offset + 69:offset + 72] = [0.0,0.0,inFile.box.dimension[2]]

		self.bDrawable[offset + 72] = VERTEX
		self.bDrawable[offset + 73:offset + 76] = [inFile.box.dimension[0],0.0,0.0]
		self.bDrawable[offset + 76] = VERTEX
		self.bDrawable[offset + 77:offset + 80] = [inFile.box.dimension[0],0.0,inFile.box.dimension[2]]

		self.bDrawable[offset + 80] = VERTEX
		self.bDrawable[offset + 81:offset + 84] = [0.0,inFile.box.dimension[1],0.0]
		self.bDrawable[offset + 84] = VERTEX
		self.bDrawable[offset + 85:offset + 88] = [0.0,inFile.box.dimension[1],inFile.box.dimension[2]]

		self.bDrawable[offset + 88] = VERTEX
		self.bDrawable[offset + 89:offset + 92] = [inFile.box.dimension[0],inFile.box.dimension[1],0.0]
		self.bDrawable[offset + 92] = VERTEX
		self.bDrawable[offset + 93:offset + 96] = [inFile.box.dimension[0],inFile.box.dimension[1],inFile.box.dimension[2]]

		self.bDrawable[-1] = END

		#
		# Corners
		#

		# Init size in bulk where possible
		self.bDrawable.extend([COLOR])
		self.bDrawable.extend(cb)

		offset = len(self.bDrawable)
		self.bDrawable.extend([None for i in range(40)])
		width = 0.01 * inFile.box.dimension.min()
		for i in range(2):
			for j in range(2):
				for k in range(2):
					start = offset + 20 * i + 10 * j + 5 * k
					self.bDrawable[start] = SPHERE
					self.bDrawable[start + 1:start + 4] = [i * inFile.box.dimension[0], j * inFile.box.dimension[1], k * inFile.box.dimension[2]]
					self.bDrawable[start + 4] = width

	def rebuild(self, mode, inFile, cn=nodeDefaultColour, cc=conDefaultColour, cs=siteDefaultColour, cb=boxDefaultColour):


		#
		# Build the actual drawable objects, but only depending on what the mode is
		#

		# Draw by forces
		if mode == 1:

			# Check for consistency
			if self.pos == []:
				print("Cannot draw forces as nodes do not exist :(")
				return BioNetError

			# Set max and min forces and colours
			forceMag = np.array([np.linalg.norm(f) for f in self.force])

#			maxForce = np.max(forceMag)
			maxForce = 150
			minForce = 0.0
			forceRange = maxForce - minForce

			for i in range(forceMag.size):
				if forceMag[i] > maxForce:
					forceMag[i] = maxForce

			maxCol = np.array([1.0,0.0,0.0])
			minCol = np.array([0.0,0.0,1.0])
			colRange = maxCol - minCol

			# Rebuild drawable object (force and node should be the same length)
			numNodes = forceMag.size

			self.nDrawable = [None for i in range(9 * numNodes)]

			for i in range(numNodes):
				forceFrac = (forceMag[i] - minForce) / forceRange
				col = minCol + forceFrac * colRange

				self.nDrawable[9 * i] = COLOR
				self.nDrawable[9 * i + 1:9 * i + 4] = col
				self.nDrawable[9 * i + 4] = SPHERE
				self.nDrawable[9 * i + 5:9 * i + 8] = list(self.pos[i])
				self.nDrawable[9 * i + 8] = self.radius[i]


		else:

			pass

		return BioNetSuccess

		"""
		#
		# Nodes
		#

		if(self.pos != []):
			self.nDrawable = [COLOR]
			self.nDrawable.extend(cn)

			offset = len(self.nDrawable)
			self.nDrawable.extend([None for i in range(5 * inFile.numNodes)])
				
			for i in range(inFile.numNodes):

				self.nDrawable[offset + 5 * i] = SPHERE
				self.nDrawable[offset + 5 * i + 1:offset + 5 * i + 4] = list(self.pos[i])
				self.nDrawable[offset + 5 * i + 4] = self.radius[i]

		else:
			self.nDrawable = []
		"""

	def buildFromTrajectoryFrame(self, tFrame, inFile, frameIndex=1, dn=True, dc=False, ds=False, cn=nodeDefaultColour, cc=conDefaultColour, cs=siteDefaultColour, cb=boxDefaultColour):

		# Get arrays
		self.index = frameIndex

		# Store forces just in case
		self.force = tFrame.force

		if(dn):
			self.pos = tFrame.pos
		else:
			self.pos = []

		if(ds):
			self.sPos = tFrame.sPos
		else:
			self.sPos = []

		if(dc):
			if(ds):
				self.cPos = np.array([[None for i in range(2)] for c in inFile.connection])
				i = 0
				for c in inFile.connection:
					for j in range(2):
						try:
							self.cPos[i][j] = self.sPos[c.nIndex[j]][c.sIndex[j]]
						except:
							self.cPos[i][j] = self.pos[c.nIndex[j]]
					i += 1

#				self.cPos = np.array([[self.sPos[c.nIndex[i]][c.sIndex[i]] for i in range(2)] for c in inFile.connection])
			else:
				self.cPos = np.array([[self.pos[c.nIndex[i]] for i in range(2)] for c in inFile.connection])
		else:
			self.cPos = []


		self.radius = np.array([n.protein.radius for n in inFile.node])

		# Check types for special radius definitions
		nIndex = 0
		for n in inFile.node:
			if n.protein.type.lower() == "point":
				self.radius[nIndex] = 0.18	# Amino acid sized
				self.connectionWidth = 0.12

			nIndex += 1

		self.build(inFile, cn, cc, cs, cb)

	def buildFromTopologyFrame(self, tpFrame, inFile, frameIndex=0, dn=True, dc=True, ds=False, cn=nodeDefaultColour, cc=conDefaultColour, cs=siteDefaultColour, cb=boxDefaultColour):

		# Get arrays
		self.index = frameIndex

		try:
			if(dc):
				if(ds):
					self.cPos = np.array([[None for i in range(2)] for c in tpFrame.cConnections])
					i = 0
					
					for c in tpFrame.cConnections:
						for j in range(2):
							try:
								self.cPos[i][j] = self.sPos[c[j][1]][c[j][0]]
							except:
								self.cPos[i][j] = self.pos[c[j][1]]
						i += 1

#					self.cPos = np.array([[self.sPos[c[i][1]][c[i][0]] for i in range(2)] for c in tpFrame.cConnections])
				else:
					self.cPos = np.array([[self.pos[c[i][1]] for i in range(2)] for c in tpFrame.cConnections])
			else:
				self.cPos = []
		except:
			print("Unknown error. Unable to load topology. Will build everything else instead...")
			self.cPos = []

		self.build(inFile, cn, cc, cs, cb)

	def buildFromInput(self, inFile, dn=True, dc=False, ds=False, cn=nodeDefaultColour, cc=conDefaultColour, cs=siteDefaultColour, cb=boxDefaultColour):

		# Initialise
		self.index = 1

		# Hold a force object just in case
		self.force = np.array([[1.0,0.0,0.0] for n in inFile.node])

		# Get arrays that we need
		if(dn):
			self.pos = np.array([n.pos for n in inFile.node])
		else:
			self.pos = []

		if(ds):

			# Get max num sites
			maxNumSites = -5
			for n in inFile.node:
				if len(n.protein.site) > maxNumSites:
					maxNumSites = len(n.protein.site)

			self.sPos = np.full((len(inFile.node),maxNumSites),None)
			nIndex = 0
			for n in inFile.node:
				sIndex = 0
				for s in n.protein.site:

					if len(n.orientation) == 3:
						self.sPos[nIndex][sIndex] = n.pos + np.dot(EulerToZXZ(n.orientation[0], n.orientation[1], n.orientation[2]), s.position) * n.protein.radius
					else:
						mat = n.orientation.reshape(3,3).transpose()
						self.sPos[nIndex][sIndex] = n.pos + np.dot(mat, s.position) * n.protein.radius
					sIndex += 1

				nIndex += 1

#			self.sPos = np.array([[n.pos + np.dot(EulerToZXZ(n.orientation[0], n.orientation[1], n.orientation[2]), s.position) * n.protein.radius for s in n.protein.site] for n in inFile.node])

				
		else:
			self.sPos = []

		if(dc):
			if(ds):
				self.cPos = np.array([[None for i in range(2)] for c in inFile.connection])
				i = 0
				for c in inFile.connection:
					for j in range(2):
						try:
							self.cPos[i][j] = self.sPos[c.nIndex[j]][c.sIndex[j]]
						except:
							self.cPos[i][j] = self.pos[c.nIndex[j]]
					i += 1

#				self.cPos = np.array([[self.sPos[c.nIndex[i]][c.sIndex[i]] for i in range(2)] for c in inFile.connection])
			else:
#				for c in inFile.connection:
#					print(c.nIndex, self.pos[c.nIndex[0]], self.pos[c.nIndex[1]])
				self.cPos = np.array([[self.pos[c.nIndex[i]] for i in range(2)] for c in inFile.connection])
		else:
			self.cPos = []

		self.radius = np.array([n.protein.radius for n in inFile.node])
		
		# Check types for special radius definitions
		nIndex = 0
		for n in inFile.node:
			if n.protein.type.lower() == "point":
				self.radius[nIndex] = 0.18	# Amino acid sized
				self.connectionWidth = 0.12

			nIndex += 1

		self.build(inFile, cn, cc, cs, cb)

	def addPores(self, pFrame, frameIndex=0, cp=poreDefaultColour, ct=throatDefaultColour):

		self.index = frameIndex

		# Add stuff to frame
		self.pPos = pFrame.porePos
		self.pRad = pFrame.poreRadius
		self.tPos = pFrame.throatPos
		self.tRad = pFrame.throatRadius

		#
		# Build the actual drawable objects
		#


		# Pores
		self.pDrawable = [COLOR]
		self.pDrawable.extend(cp)

		offset = len(self.pDrawable)
		self.pDrawable.extend([None for i in range(5 * pFrame.numPores)])
			
		for i in range(pFrame.numPores):
			self.pDrawable[offset + 5 * i] = SPHERE
			self.pDrawable[offset + 5 * i + 1:offset + 5 * i + 4] = list(self.pPos[i])
			self.pDrawable[offset + 5 * i + 4] = self.pRad[i]

		# Throats
		self.tDrawable = [COLOR]
		self.tDrawable.extend(ct)

		offset = len(self.tDrawable)
		self.tDrawable.extend([None for i in range(5 * pFrame.numThroats)])
			
		for i in range(pFrame.numThroats):

			self.tDrawable[offset + 5 * i] = SPHERE
			self.tDrawable[offset + 5 * i + 1:offset + 5 * i + 4] = list(self.tPos[i])
			self.tDrawable[offset + 5 * i + 4] = self.tRad[i]
		
