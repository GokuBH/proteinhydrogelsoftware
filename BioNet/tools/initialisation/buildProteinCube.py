import sys, os

# Get box dimensions (in num proteins)
if(len(sys.argv) != 5):
	sys.exit("Usage: " + sys.argv[0] + " [x dim] [y dim] [z dim] [oFname]")

dims = [int(sys.argv[i + 1]) for i in range(3)]
oFname = sys.argv[4]

# Open the dumbell template and get lines
fin = open("../../test/dumbbell.xml", "r")
lines = fin.readlines()
fin.close()

# Open output file
fout = open(oFname, "w")

# Write stuff that is staying
for line in lines:
	fout.write(line)
	if "<nodes>" in line:
		break

# Write all nodes
startPos = [1,1,1]
for x in range(dims[0]):
	for y in range(dims[1]):
		for z in range(dims[2]):
			line = "\t\t\t<node protein=\"Protein\" pos="
			line += "\"" + str(startPos[0] + x * 2) + "," + str(startPos[1] + y * 2) + "," + str(startPos[2] + z * 2) + "\""
			line +=  " vel=\"-1,0,0\"/>\n"
			fout.write(line)

# Write closing bit
fout.write("\t\t</nodes>\n")

# Start springs
fout.write("\t\t<springs>\n")
for i in range(dims[0] * dims[1] * dims[2] - 1):
	line = "\t\t\t<spring bond=\"CrossLink\" nodes="
	line += "\"" + str(i) + ", " + str(i + 1) + "\""
	line += " iSites=\"0,0\"/>\n"
	fout.write(line)

# Write remainder
write = False
for line in lines:
	if "</springs>" in line:
		write = True
	if write:
		fout.write(line)
fout.close()
