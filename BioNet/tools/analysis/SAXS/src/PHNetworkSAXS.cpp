#include <iostream>
#include <exception>
#include <string>

#include "UserInfo.hpp"
#include "SystemParameters.hpp"
#include "NetworkExceptions.hpp"
#include "CommandLineArgs.hpp"
#include "NetworkFileReader.hpp"
#include "NetworkFileWriter.hpp"

#include "PhysicalSystem.hpp"

#include "NetworkAnalysis.hpp"

using namespace std;

class MainObject {

	public:

		//
		// Methods
		//
		MainObject() {
			cmArgs = NULL;
			fileIn = NULL;
			fileOut = NULL;
			pSystem = NULL;
		}

		~MainObject() {
			if(cmArgs != NULL) {
				delete cmArgs;
			}
			if(fileIn != NULL) {
				delete fileIn;
			}
			if(fileOut != NULL) {
				delete fileOut;
			}
			if(pSystem != NULL) {
				delete pSystem;
			}

			cmArgs = NULL;
			fileIn = NULL;
			fileOut = NULL;
			pSystem = NULL;
		}

		//
		// Variables
		//
		CommandLineArgs *cmArgs;
		NetworkFileReader *fileIn;
		NetworkFileWriter *fileOut;
		PhysicalSystem *pSystem;


};

int main(int argc, char **argv) {

	// Give user a welcome
	printIntro();

	// Get a string for writing errors
	string errString;

	// Get the object container for this method
	MainObject *mainObject = new MainObject();

	// We're making a network model! Lets read in the structure from a file
	mainObject->cmArgs = new CommandLineArgs();
	if(mainObject->cmArgs->read(argc, argv) == PHNetworkError) {
		errString = "Could not read command line arguments :( ";
		delete mainObject;
		printError(errString);
		return PHNetworkError;
	}

	if(mainObject->cmArgs->validate() == PHNetworkInvalid) {
		errString = "Command line arguments inconsistent or invalid :( ";
		delete mainObject;
		printError(errString);
		return PHNetworkError;
	}

	// We have some filenames now. Read in the data
	mainObject->fileIn = new NetworkFileReader();

	if(mainObject->fileIn->readInputFile(mainObject->cmArgs->getInputFname()) == PHNetworkError) {
		errString = "Unable to load data from '" + mainObject->cmArgs->getInputFname() + "' :( ";
		delete mainObject;
		printError(errString);
		return PHNetworkError;
	}

	// Now we have all of th input data, we can build a physical system
	mainObject->pSystem = new PhysicalSystem();

	// Build
	if(mainObject->pSystem->build(mainObject->cmArgs, mainObject->fileIn) == PHNetworkError) {
		errString = "The physical system could not be built :( ";
		delete mainObject;
		printError(errString);
		return PHNetworkError;
	}

	//
	// Run SAXS
	//
	SAXSAnalysis(mainObject->pSystem);

	// Complete programme :)	
	delete mainObject;

	printOutro();
	return PHNetworkSuccess;
}
