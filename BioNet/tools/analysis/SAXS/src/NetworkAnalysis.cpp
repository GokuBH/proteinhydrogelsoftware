#include "NetworkAnalysis.hpp"

int SAXSAnalysis(PhysicalSystem *pSystem) {

	Network *net;
	SimulationBox *box;
	Voxel *cV, *nV;
	vObject *o1, *o2;
	vObject **node;
	int numNodes, numVoxels, numNeighbours, numObjects1, numObjects2, nIndex;
	vector<bool> checked;
	vector<scalar> distances;
	vector<scalar> radii;
	Vector3 sep;
	int i, j, k, l;

	// Get all objects that we'll need
	net = pSystem->getNetwork();
	box = pSystem->getSimulationBox();
	node = net->getNodes();
	numNodes = net->getNumNodes();
	numVoxels = box->getTotalNumVoxels();
	
	// Initialise
	checked.resize(numVoxels);
	for(i = 0; i < numVoxels; ++i) {
		checked.at(i) = false;
	}

	// Loop over the simulation box to get the node distances
	for(i = 0; i < numVoxels; ++i) {
		cV = box->getVoxel(i);
		numObjects1 = cV->getNumObjects();

		// Get all distances from within voxels
		for(k = 0; k < numObjects1; ++k) {
			o1 = cV->getObject(k);
			for(l = k + 1; l < numObjects1; ++l) {
				o2 = cV->getObject(l);
				sep = o1->getPosition() - o2->getPosition();
				distances.push_back(sep.norm());
			}
		}

		// Now loop over neighbours
		numNeighbours = cV->getNumNeighbours();
		for(j = 0; j < numNeighbours; j++) {
	
			nV = cV->getNeighbour(j);
			
			// Check if checked
			nIndex = nV->getIndex();

			if(checked.at(nIndex)) {
				continue;
			}

			numObjects2 = nV->getNumObjects();

			// Get all distances from between voxels
			for(k = 0; k < numObjects1; ++k) {
				o1 = cV->getObject(k);
				for(l = 0; l < numObjects2; ++l) {
					o2 = nV->getObject(l);
					sep = o1->getPosition() - o2->getPosition();
					distances.push_back(sep.norm());
				}
			
			}
		}

		checked.at(i) = true;
	}

	// Now use distances to make a SAXS curve (this currently only works for homogeneous particles)
	vector<scalar> qs(101);
	vector<scalar> Is(101);
	scalar theta, q, qr, intensity, ff, scatterContrast = 1, radius = 1;
	scalar sphereVolume = (4.0/3.0) * M_PI * pow(radius, 3.0);

	// Units from code are nanometers 
	scalar lambda = 1;

	// For zero degree scattering, do it explicitly
	qs.at(0) = 0;
	ff = scatterContrast * sphereVolume;
	Is.at(0) = ff * ff * distances.size();
	for(i = 1; i < 101; ++i) {
	
		// Get scattering angle
		theta = ((M_PI / 2.0) / 100.0) * i;

		// Associated scattering vector
		q = 4 * M_PI * sin(theta) / lambda;
		qs.at(i) = q;

		// Build up intensity
		intensity = 0;
		for(j = 0; j < distances.size(); ++j) {
			qr = distances.at(j) * q;
			intensity += sin(qr) / qr;
		}
		
		// Form factor
		qr = q * radius;	
		ff = sphereVolume * (3 * (sin(qr) - qr * cos(qr))) / (pow(qr, 3));
		intensity *= ff * ff;
		Is.at(i) = intensity;
	}

	// Write data to file
	ofstream oFile;
	oFile.open("./results/data.txt");
	for(i = 0; i < qs.size(); ++i) {
		oFile << qs.at(i) << " " << Is.at(i) << endl;
	}
	oFile.close();
	return PHNetworkSuccess;
}
