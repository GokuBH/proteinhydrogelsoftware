#ifndef NETANAL_H_INCLUDED
#define NETANAL_H_INCLUDED

#include <iostream>
#include <fstream>

#include <string>
#include <sstream>
#include <cmath>
#include <Eigen/Core>

#include "UserInfo.hpp"
#include "Utilities.hpp"
#include "SystemParameters.hpp"
#include "MathematicalFunctions.hpp"

#include "PhysicalSystem.hpp"

#include "NetworkParameters.hpp"

#include "Network.hpp"
#include "ElasticNetwork.hpp"
#include "ViscoElasticNetwork.hpp"

#include "SimulationBox.hpp"

using namespace std;

int SAXSAnalysis(PhysicalSystem *pSystem);

#endif
