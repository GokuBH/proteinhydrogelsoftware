import sys, os
import numpy as np
from matplotlib import pyplot as plt

# Open file
data = np.genfromtxt("data.txt").transpose()

plt.plot(data[0], data[1])
plt.xlabel(r"q (nm$^{-1}$)")
plt.ylabel("I(q)")
plt.title("SAXS Data (Test)")
plt.show()
