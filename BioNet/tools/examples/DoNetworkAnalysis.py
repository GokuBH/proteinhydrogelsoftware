import numpy as np
import BioNetInput, BioNetTrajectory
import FractalCalculator

from matplotlib import pyplot as plt

#
# Methods
#
def loadMeasurement(fname):

	# Get file contents (and skip header)
	fin = open(fname, "r")
	lines = fin.readlines()[6:-1]
	fin.close()

	# Get number of frames
	numFrames = len(lines)
	
	# Get some data structures
	time = np.zeros(numFrames, dtype=float)
	stericEnergy = np.zeros(numFrames, dtype=float)
	vdwEnergy = np.zeros(numFrames, dtype=float)

	# Read data (could read centroid and coordination as well, but no point here)
	n = 0
	for line in lines:
	
		# Split line
		sline = line.split(",")
		numElements = len(sline)
		
		# Assign data to arrays
		time[n] = float(sline[0])
		
		if numElements == 7:
			stericEnergy[n] = float(sline[5])
		elif numElements == 8:
			stericEnergy[n] = float(sline[5])
			vdwEnergy[n] = float(sline[6])
			
			
		# Increment
		n += 1
	
	return time, stericEnergy, vdwEnergy

def calculateDiffusion(frames):

	# Average r2 over all nodes for each time step (this assumes ergodic hypothesis)

	# Data containers
	time = np.array([frame.time - frames[0].time for frame in frames], dtype=float)
	r2 = np.zeros(time.size, dtype=float)
	
	fIndex = 0
	for frame in frames:
	
		# Average distance squared from initial step
		r2[fIndex] = np.mean([np.dot(frame.pos[i] - frames[0].pos[i],frame.pos[i] - frames[0].pos[i]) for i in range(len(frames))])
		
		fIndex += 1
		
	return time, r2

def calculateFractalDimension(frames, inp):
	
	# Calculate fd evolution over time

	# Data containers
	time = np.array([frame.time - frames[0].time for frame in frames], dtype=float)
	fd = np.zeros(time.size, dtype=float)
	fdErr = np.zeros(time.size, dtype=float)
	
	# Get a calculator object
	fCalc = FractalCalculator.FractalCalculator()

	fIndex = 0
	for frame in frames:
		fdContainer, _ = fCalc.calculateFractalDimension(frame, inp.box.dimension, 1.5, inp=inp, periodic=True, dense=True)
		fd[fIndex] = fdContainer[0]
		fdErr[fIndex] = fdContainer[1]
		
		fIndex += 1
		
	return time, fd, fdErr
#
# Main analysis script
#

# Plotting things
SMALL = 8
MEDIUM = 10
LARGE = 12

# First, load the files
iFname = "network.xml"
inp = BioNetInput.BioNetInput()
inp.load(iFname)

tFname = "output/network.bntrj"
traj = BioNetTrajectory.BioNetTrajectory()
traj.load(tFname, inScript=inp, rate=10)

#
# Measurement analysis
#

# Load the measurement data as well (BioNetMeasurement needs fixing, sorry!)
mFname = "output/network.bnms"
time, stericEnergy, vdwEnergy = loadMeasurement(mFname)

# Plot some measurement energies over time
energyFig = plt.figure(figsize=(3.2,3.2)) 

plt.plot(time, stericEnergy, "bx", label = "Steric Energy")
plt.plot(time, vdwEnergy, "gx", label = "VdW Energy")

plt.xlabel("Time (ns)", fontsize=MEDIUM)
plt.ylabel("Energy (pN.nm)", fontsize=MEDIUM)
plt.legend(loc=0, fontsize=SMALL)

plt.tight_layout()
oFname = "results/EnergyOverTime.png"
plt.savefig(oFname, dpi=100)

#
# Trajectory analysis
#

# Plot average diffusion behaviour over time against the theoretical value (for the production run)

# Simulation
time, rSquared = calculateDiffusion(traj.frame[6:])

# Theory
diffConst = 1.38e-2*inp.params.temperature / (6*np.pi*inp.params.viscosity*inp.protein[0].radius)
rSquaredTheory = 6 * diffConst * time

# Plot
diffusionFig = plt.figure(figsize=(3.2,3.2)) 

plt.plot(time, rSquared, "bx", label = "Simulation")
plt.plot(time, rSquaredTheory, "--k", label = "Theory")

plt.xlabel("Time (ns)", fontsize=MEDIUM)
plt.ylabel(r"$\langle (r-r_0)^2 \rangle$ (nm$^2$)", fontsize=MEDIUM)
plt.legend(loc=0, fontsize=SMALL)

plt.tight_layout()
oFname = "results/DiffusionOverTime.png"
plt.savefig(oFname, dpi=100)

# Fractal analysis (for the production run)

# Simulation
time, fDimension, fDimensionError = calculateFractalDimension(traj.frame[6:], inp)

# Plot
diffusionFig = plt.figure(figsize=(3.2,3.2)) 

plt.errorbar(time, fDimension, yerr=fDimensionError, fmt="bx", label = "Simulation")

plt.xlabel("Time (ns)", fontsize=MEDIUM)
plt.ylabel("Fractal Dimension", fontsize=MEDIUM)
plt.legend(loc=0, fontsize=SMALL)

plt.tight_layout()
oFname = "results/FractalDimensionOverTime.png"
plt.savefig(oFname, dpi=100)
