import sys, os
import glob
import numpy as np
import BioNetInput, BioNetTrajectory

# Get filenames
iFnames = glob.glob("./boundaryConditions/input/*Point*.xml")

# Error threshold
eps = 0.01
for iFname in iFnames:
	
	bFname = os.path.basename(iFname)
	bFname = os.path.splitext(bFname)[0]
	oFname = "./boundaryConditions/output/" + bFname + ".bntrj"

	inp = BioNetInput.BioNetInput()
	inp.load(iFname)
	
	traj = BioNetTrajectory.BioNetTrajectory()
	traj.load(oFname)
	print(iFname)
	# Directions and negatives
	if "x.xml" in iFname:
		direction = 0
		if "nx.xml" in iFname:
			negative = True
		else:
			negative = False			
	elif "y.xml" in iFname:
		direction = 1
		if "ny.xml" in iFname:
			negative = True
		else:
			negative = False
	else:
		direction = 2
		if "nz.xml" in iFname:
			negative = True
		else:
			negative = False
	
	# Test finish positions
	if "HBC" in iFname or "PBC" in iFname:
		expFin = inp.node[0].pos[direction]
	else:
		if negative:
			expFin = inp.node[0].protein.radius
		else:
			expFin = inp.box.dimension[direction] - inp.node[0].protein.radius
	measFin = traj.frame[-1].pos[0][direction]
	err = np.fabs(measFin - expFin) / expFin		
	if err > eps:
		errCode = 1
		sys.exit(errCode)

	# Test correct trajectory
	if "HBC" in iFname or "CBC" in iFname:
		if negative:
			for frame in traj.frame:
				if frame.pos[0][direction] > traj.frame[0].pos[0][direction]:
					errCode = 1
					sys.exit(errCode)
		else:
			for frame in traj.frame:
				if frame.pos[0][direction] < traj.frame[0].pos[0][direction]:
					errCode = 1
					sys.exit(errCode)
	else:
		check = 0
		wrap = 0
		if negative:
			for i in range(traj.numFrames - 1):
				frame = traj.frame[i + 1]
				lastFrame = traj.frame[i]
				
				# Have we flipped?
				if frame.pos[0][direction] > lastFrame.pos[0][direction]:
					if wrap == 0:
						wrap = 1
						continue
					else:
						errCode = 1
						sys.exit(errCode)
		else:
			for i in range(traj.numFrames - 1):
				frame = traj.frame[i + 1]
				lastFrame = traj.frame[i]
				
				# Have we flipped?
				if frame.pos[0][direction] < lastFrame.pos[0][direction]:
					if wrap == 0:
						wrap = 1
						continue
					else:
						errCode = 1
						sys.exit(errCode)
errCode = 0
sys.exit(errCode)
