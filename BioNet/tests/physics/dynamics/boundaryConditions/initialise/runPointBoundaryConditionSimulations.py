import sys, os
import glob

executable = sys.argv[1]
iFnames = glob.glob("./boundaryConditions/input/*Point*.xml")

for iFname in iFnames:
	
	bFname = os.path.basename(iFname)
	bFname = os.path.splitext(bFname)[0]
	oFname = "./boundaryConditions/output/" + bFname + ".out"
	
	os.system(executable + " -i %s -o %s -t Langevin --no-thermal --no-steric --no-vdw --no-kinetics --verbose" % (iFname, oFname))
