import sys, os
import numpy as np

import BioNetInput, BioNetTrajectory, BioNetMeasurement

# Objects
objects = ["Point", "Sphere"]
sites = ["Site", ""]
numPairs = [8,8,8]	# Num sets of objects for each pairing

# Data
eps = 0.05

# For all possible pair, check separation and energy
index = 0
for n in range(len(objects)):
	ob1 = objects[n]
	for m in range(n, len(objects)):
		ob2 = objects[m]
			
		for siteString in sites:

			# Load objects
			iFname = "./interactionBonds/RelaxBonds/input/%s%s%sBonds.xml" % (ob1,ob2,siteString)
			tFname = "./interactionBonds/RelaxBonds/output/%s%s%sBonds.bntrj" % (ob1,ob2,siteString)
			mFname = "./interactionBonds/RelaxBonds/output/%s%s%sBonds.bnms" % (ob1,ob2,siteString)

			# Hardcoded skip
			if ob1 == "Point" and ob2 == "Point" and siteString == "Site":
				continue
				
			inp = BioNetInput.BioNetInput()
			inp.load(iFname)

			traj = BioNetTrajectory.BioNetTrajectory()
			traj.load(tFname, inScript=inp)

			meas = BioNetMeasurement.BioNetMeasurement()
			meas.load(mFname)
			 
			# There are pairs of objects. The final (center-center) distance between all should be 4. The y and z should not change
			for i in range(numPairs[index]):
				obs = []

#				R1pR2preq = inp.node[2*i].protein.radius + inp.node[2*i + 1].protein.radius + 2.0
				R1pR2preq = 4.0
				rx = np.fabs(traj.frame[-1].pos[2*i+1][0] - traj.frame[-1].pos[2*i][0])
				
				if i >= numPairs[index] / 2.0:
					rx = inp.box.dimension[0] - rx
					
				obs.append(rx)
				obs.append(np.fabs(traj.frame[-1].pos[2*i][1] - traj.frame[0].pos[2*i][1]))
				obs.append(np.fabs(traj.frame[-1].pos[2*i+1][1] - traj.frame[0].pos[2*i+1][1]))
				obs.append(np.fabs(traj.frame[-1].pos[2*i][2] - traj.frame[0].pos[2*i][2]))
				obs.append(np.fabs(traj.frame[-1].pos[2*i+1][2] - traj.frame[0].pos[2*i+1][2]))
				for j in range(5):
					if j == 0:

						if R1pR2preq == 0:
							if obs[j] > eps:
								errCode = 1
								sys.exit(errCode)
						
						else:
							if np.fabs(obs[j] - R1pR2preq) / R1pR2preq > eps:
								errCode = 1
								sys.exit(errCode)

					else:
						if obs[j] > eps:
							errCode = 1
							sys.exit(errCode)
							
			# Now energy
			expEnergy = 0.0
			if np.fabs(meas.elasticEnergy[-1]) > eps:
				errCode = 1
				sys.exit(errCode)
		index += 1
errCode = 0
sys.exit(errCode)
