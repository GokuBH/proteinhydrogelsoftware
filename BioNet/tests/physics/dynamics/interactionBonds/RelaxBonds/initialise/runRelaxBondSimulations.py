import sys, os
import glob

executable = sys.argv[1]

iFnames = glob.glob("./interactionBonds/RelaxBonds/input/*.xml")
for iFname in iFnames:
		
	bFname = os.path.basename(iFname)
	bFname = os.path.splitext(bFname)[0]
	oFname = "./interactionBonds/RelaxBonds/output/" + bFname + ".out"
	
	if "Site" in iFname:
		os.system(executable + " -i %s -o %s -t Brownian -l --no-steric --no-vdw --no-kinetics --no-thermal --verbose" % (iFname, oFname))
	else:
		os.system(executable + " -i %s -o %s -t Brownian --no-steric --no-vdw --no-kinetics --no-thermal --verbose" % (iFname, oFname))
