import sys, os
import glob

executable = sys.argv[1]

iFnames = glob.glob("./interactionBonds/FENEBonds/input/*.xml")
for iFname in iFnames:
		
	bFname = os.path.basename(iFname)
	bFname = os.path.splitext(bFname)[0]
	oFname = "./interactionBonds/FENEBonds/output/" + bFname + ".out"
	
	os.system(executable + " -i %s -o %s -t Brownian -l --no-steric --no-vdw --no-kinetics --no-thermal --constant --verbose" % (iFname, oFname))
