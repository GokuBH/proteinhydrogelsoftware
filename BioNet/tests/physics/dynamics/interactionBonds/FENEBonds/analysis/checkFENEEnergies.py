import sys, os
import numpy as np
import BioNetTrajectory, BioNetInput, BioNetMeasurement

def calculateEffectiveElasticConstant(r, l, lm, k):

	# FENE df/dx
	
	extension = r - l
	
	powerBit = np.power(extension / lm, 2)

	return (k / (1 - powerBit)) * (1 + 2 * powerBit / (1 - powerBit))

def calculateFENEStuff(l, lm, k, klim):

	# Do a quick in half way interpolation to find the length limit

	# Initialise
	eps = 0.001
	minR = 0.0
	maxR = lm
	testR = maxR* (1 - eps)
	keff = calculateEffectiveElasticConstant(testR, l, lm, k)
		
	# Algorithm to get the length limit
	while(np.fabs(keff - klim) / klim > eps):
		testR = 0.5 * (minR + maxR)
		keff = calculateEffectiveElasticConstant(testR, l, lm, k)
		
		if (keff > klim):
			maxR = testR
		else:
			minR = testR
	
	# Set the new k value
	klim = keff
	rlim = testR
	
	feneForce =  k * ((testR - l) / (1 - pow((testR - l) / lm, 2)))

	# Set the new equilibrium length
	llim = testR - (feneForce / keff)
	
	return klim, llim, rlim
	
def FENEForce(r, k, l, lm, klim, llim):

	extension = r - l
	if r < rlim:
		return k * extension / (1 - (extension / lm)**2)
	else:
		return klim * (r - llim)
		
def FENEEnergy(r, k, l, lm, klim, llim, rlim):

	extension = r - l
	if r < rlim:
		return -0.5 * k * lm**2 * np.log(1 - (extension / lm)**2)
	else:
		print("hi!")
		return 0.5 * klim * (r - llim)**2
		
# Data
forces = [1,10,100,1000,10000]

# Accuracy
eps = 0.05

for force in forces:

	# Files and objects
	iFname = "./interactionBonds/FENEBonds/input/SphereSphereSiteBonds_%dpN.xml" % (force)
	tFname = "./interactionBonds/FENEBonds/output/SphereSphereSiteBonds_%dpN.bntrj" % (force)
	mFname = "./interactionBonds/FENEBonds/output/SphereSphereSiteBonds_%dpN.bnms" % (force)
	
	inp = BioNetInput.BioNetInput()
	inp.load(iFname)
	
	traj = BioNetTrajectory.BioNetTrajectory()
	traj.loadFinalFrame(tFname)
	
	meas = BioNetMeasurement.BioNetMeasurement()
	meas.load(mFname)
	
	# Data we need
	k = inp.connection[0].bond.k
	l = inp.connection[0].bond.l
	lm = inp.connection[0].bond.lm
	klim = inp.connection[0].bond.klim
		
	r = (traj.frame[-1].pos[1][0] - inp.node[0].protein.radius) - (traj.frame[-1].pos[0][0] + inp.node[0].protein.radius)
	obs = meas.elasticEnergy[-1]
	
	klim, llim, rlim = calculateFENEStuff(l, lm, k, klim)

	# Get the expected energy
	exp = FENEEnergy(r, k, l, lm, klim, llim, rlim)
	if np.fabs(obs - exp) / exp > eps:
		errCode = 1
		sys.exit(errCode)
		
errCode = 0
sys.exit(errCode)
