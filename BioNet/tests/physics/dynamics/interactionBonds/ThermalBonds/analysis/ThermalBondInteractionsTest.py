import sys, os
import numpy as np

import BioNetInput, BioNetTrajectory, BioNetMeasurement

from matplotlib import pyplot as plt

# Objects
sites = ["Site", ""]
numPairs = 3	# Num sets of objects for each pairing

# Data
eps = 0.1

# For all possible pair, check separation and energy
for siteString in sites:

	# Load objects
	
	iFname = "./interactionBonds/ThermalBonds/input/SingleBond%s.xml" % (siteString)
	mFname = "./interactionBonds/ThermalBonds/output/SingleBond%s.bnms" % (siteString)
		
	inp = BioNetInput.BioNetInput()
	inp.load(iFname)

	meas = BioNetMeasurement.BioNetMeasurement()
	meas.load(mFname)
	
#	plt.plot(meas.time, meas.elasticEnergy)
#	plt.plot(meas.time, [0.5 * inp.params.kbt * numPairs for t in meas.time])
	
	plt.show()
	
	# Energy is all we need
	expEnergy = 0.5 * inp.params.kbt * numPairs
	avgEnergy = np.mean(meas.elasticEnergy)
	print(expEnergy, avgEnergy, np.fabs(avgEnergy - expEnergy) / expEnergy, eps)
	if np.fabs(avgEnergy - expEnergy) / expEnergy > eps:
		errCode = 1
		sys.exit(errCode)
		
errCode = 0
sys.exit(errCode)
