import sys, os
import numpy as np
import copy

import BioNetInput, BioNetTrajectory
from matplotlib import pyplot as plt

# Diffusion should be r=vt at short times, but r=2Dt at long times. Must check!

# Load objects
iFname = "./diffusion/input/SphereRotationalDiffusion.xml"
tFname = "./diffusion/output/SphereRotationalNoMassDiffusion.bntrj"

inp = BioNetInput.BioNetInput()
inp.load(iFname)

traj = BioNetTrajectory.BioNetTrajectory()
traj.load(tFname, inScript=inp)#, numFrames=10)

# Data
kT = 4.11
mass = inp.protein[0].density * (4.0 / 3.0) * np.pi * inp.protein[0].radius ** 3
moI = (2.0 / 5.0) * mass * inp.protein[0].radius**2

dt = inp.params.timePerFrame
drag = 8*np.pi*inp.protein[0].radius**3
D = kT / drag
eps = 0.1

# Plain angles
thetas = np.zeros(traj.numFrames, dtype=float)
rVecReference = np.array([0.0,1.0,0.0])
lastTheta = 0.0
for i in range(1, traj.numFrames):

	# Rotating vector defined by sites
	rVec = traj.frame[i].sPos[0][3] - traj.frame[i].pos[0]
	
	# Remove x coordinate to get 1 direction
	rVec[0] = 0.0
	
	# Normalise
	rVec /= np.linalg.norm(rVec)
	
	# Angle(s) are anticlockwise from y axis 
	theta = np.arccos(np.dot(rVec, rVecReference))
	if rVec[2] < 0.0:
		theta = -theta
		
	# Difference
	dTheta = theta - lastTheta
	
	# Wraps
	if dTheta > np.pi:
		dTheta = 2 * np.pi - dTheta
	elif dTheta < -np.pi:
		dTheta = -(2 * np.pi + dTheta)

	# Move
	thetas[i] = thetas[i - 1] + dTheta
	lastTheta = theta
	
# Calculate expected diffusion for single dt on 1 axis (should be r2=v2t2 = (kT / I)t2)
theta2s = np.array([(thetas[i + 1] - thetas[i])**2 for i in range(traj.numFrames - 1)])
thetaAvg = np.mean(theta2s)

thetaExp = 2 * D * dt
smallErr = np.fabs(thetaAvg - thetaExp) / thetaExp

# Calculate expected diffusion for long dt (every 400th frame, should be r2=2Dt)
frameRate = 500
theta2s = np.array([(thetas[i + frameRate] - thetas[i])**2 for i in range(traj.numFrames - frameRate)])
thetaAvg = np.mean(theta2s)

thetaExp = 2 * D * dt * frameRate
largeErr = np.fabs(thetaAvg - thetaExp) / thetaExp

errCode = 0
if smallErr > eps or largeErr > eps:
	errCode = 1

sys.exit(errCode)
