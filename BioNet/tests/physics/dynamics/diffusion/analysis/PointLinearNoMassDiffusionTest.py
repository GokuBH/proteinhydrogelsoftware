import sys, os
import numpy as np

import BioNetInput, BioNetTrajectory

# Diffusion should r2=6Dt at all times. Must check!
# More statistics will give a smaller error. Being further below (small times) or above (long times) tau in your frame rate will also give
# better results

# Load objects
iFname = "./diffusion/input/PointLinearDiffusion.xml"
tFname = "./diffusion/output/PointLinearNoMassDiffusion.bntrj"

inp = BioNetInput.BioNetInput()
inp.load(iFname)

traj = BioNetTrajectory.BioNetTrajectory()
traj.load(tFname, inScript=inp)

# Data
kT = 4.11
mass = inp.protein[0].mass
dt = inp.params.timePerFrame
drag = inp.protein[0].drag
D = kT / drag
eps = 0.1

# Calculate expected diffusion for single dt (should be r2=v2t2 = (3kT / m)t2)
rVecs = np.array([traj.frame[i + 1].pos[0] - traj.frame[i].pos[0] for i in range(traj.numFrames - 1)])
r2s = np.array([np.linalg.norm(r)**2 for r in rVecs])
rAvg = np.mean(r2s)

rExp = 6 * D * dt
smallErr = np.fabs(rAvg - rExp) / rExp
print("Small time: r2Avg = ", rAvg, " r2Exp = ", rExp, " Err = ", smallErr)

# Calculate expected diffusion for long dt (every 400th frame, should be r2=6Dt)
frameRate = 200
rVecs = np.array([traj.frame[i + frameRate].pos[0] - traj.frame[i].pos[0] for i in range(0, traj.numFrames - frameRate, frameRate)])
r2s = np.array([np.linalg.norm(r)**2 for r in rVecs])
rAvg = np.mean(r2s)

rExp = 6 * D * dt * frameRate
largeErr = np.fabs(rAvg - rExp) / rExp

print("Large time: r2Avg = ", rAvg, " r2Exp = ", rExp, " Err = ", largeErr)
errCode = 0
if smallErr > eps or largeErr > eps:
	errCode = 1

sys.exit(errCode)
