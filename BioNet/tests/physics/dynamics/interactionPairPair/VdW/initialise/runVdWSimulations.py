import sys, os
import glob

executable = sys.argv[1]
iFnames = glob.glob("./interactionPairPair/VdW/input/*.xml")

for iFname in iFnames:
	
	bFname = os.path.basename(iFname)
	bFname = os.path.splitext(bFname)[0]
	oFname = "./interactionPairPair/VdW/output/" + bFname + ".out"
	
	os.system(executable + " -i %s -o %s -t Brownian --no-thermal --no-steric --vdw --no-kinetics --verbose" % (iFname, oFname))
