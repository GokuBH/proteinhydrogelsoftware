import sys, os
import glob

executable = sys.argv[1]
iFnames = glob.glob("./interactionPairPair/Steric/input/*.xml")

for iFname in iFnames:
	
	bFname = os.path.basename(iFname)
	bFname = os.path.splitext(bFname)[0]
	oFname = "./interactionPairPair/Steric/output/" + bFname + ".out"
	
	os.system(executable + " -i %s -o %s -t Brownian --no-thermal --steric --no-vdw --no-kinetics --verbose" % (iFname, oFname))
