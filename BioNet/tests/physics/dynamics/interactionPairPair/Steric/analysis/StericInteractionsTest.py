import sys, os
import numpy as np

import BioNetInput, BioNetTrajectory

# Objects
objects = ["Point", "Sphere"]
numPairs = [1,4,6]	# Num sets of objects for each pairing

# Data
eps = 0.05

# For all possible pair, check steric
index = 0
for n in range(len(objects)):
	ob1 = objects[n]
	for m in range(n, len(objects)):
		ob2 = objects[m]
		print(ob1,ob2)		
		# Load objects
		iFname = "./interactionPairPair/Steric/input/%s%sSteric.xml" % (ob1,ob2)
		tFname = "./interactionPairPair/Steric/output/%s%sSteric.bntrj" % (ob1,ob2)

		inp = BioNetInput.BioNetInput()
		inp.load(iFname)

		traj = BioNetTrajectory.BioNetTrajectory()
		traj.load(tFname, inScript=inp)

		# There are three pairs of objects. The final distance between all should be R1 + R2. The y and z should not change
		for i in range(numPairs[index]):
			obs = []
			R1pR2 = inp.node[2*i].protein.radius + inp.node[2*i + 1].protein.radius
			print(ob1, ob2, inp.node[2*i].protein.radius, inp.node[2*i+1].protein.radius, R1pR2)

			rx = np.fabs(traj.frame[-1].pos[2*i+1][0] - traj.frame[-1].pos[2*i][0])
			if i >= numPairs[index] / 2.0:
				rx = inp.box.dimension[0] - rx

			obs.append(rx)
			obs.append(np.fabs(traj.frame[-1].pos[2*i][1] - traj.frame[0].pos[2*i][1]))
			obs.append(np.fabs(traj.frame[-1].pos[2*i+1][1] - traj.frame[0].pos[2*i+1][1]))
			obs.append(np.fabs(traj.frame[-1].pos[2*i][2] - traj.frame[0].pos[2*i][2]))
			obs.append(np.fabs(traj.frame[-1].pos[2*i+1][2] - traj.frame[0].pos[2*i+1][2]))
			for j in range(5):
				if j == 0:
					if R1pR2 == 0:
						if obs[j] > eps:
							errCode = 1
							sys.exit(errCode)
					
					else:
						if np.fabs(obs[j] - R1pR2) / R1pR2 > eps:
							errCode = 1
							sys.exit(errCode)
				else:
					if obs[j] > eps:
						errCode = 1
						sys.exit(errCode)
		index += 1
errCode = 0
sys.exit(errCode)
