import sys, os
import glob

executable = sys.argv[1]
iFnames = glob.glob("./interactionBox/VdW/input/*.xml")

for iFname in iFnames:
	
	bFname = os.path.basename(iFname)
	bFname = os.path.splitext(bFname)[0]
	oFname = "./interactionBox/VdW/output/" + bFname + ".out"
	
	os.system(executable + " -i %s -o %s -t Brownian --no-thermal --no-steric --no-vdw --no-kinetics --wall --verbose" % (iFname, oFname))
