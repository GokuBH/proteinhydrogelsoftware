import sys, os
import numpy as np

import BioNetInput, BioNetTrajectory, BioNetMeasurement

# Objects
walls = ["One", "Two"]
numParticlesPerWall = 3
totalNumParticles = 2 * numParticlesPerWall

# Data
eps = 0.05

# All objects need to be rEq away from the respective wall (if wall is active) and at eps energy
for wall in walls:

	iFname = "./interactionBox/VdW/input/VdWWall%s.xml" % (wall)
	tFname = "./interactionBox/VdW/output/VdWWall%s.bntrj" % (wall)
	mFname = "./interactionBox/VdW/output/VdWWall%s.bnms" % (wall)
	
	inp = BioNetInput.BioNetInput()
	inp.load(iFname)

	traj = BioNetTrajectory.BioNetTrajectory()
	traj.load(tFname, inScript=inp)

	meas = BioNetMeasurement.BioNetMeasurement()
	meas.load(mFname)
	
	# There should be no change in the x or y directions of any of the particles
	for i in range(totalNumParticles):
	
		xDisp = traj.frame[-1].pos[i][0] - traj.frame[0].pos[i][0]
		yDisp = traj.frame[-1].pos[i][2] - traj.frame[0].pos[i][2]
		
		if np.fabs(xDisp) > eps or np.fabs(yDisp) > eps:
		
			print("X or Y displacement occured")
			errCode = 1
			sys.exit(errCode)
			
	# For the first three particles, the distance from the surface to the wall should be rEq
	for i in range(numParticlesPerWall):
	
		obs = traj.frame[-1].pos[i][1] - inp.node[i].protein.radius
		exp = float(inp.force[0].attributes["req"])
		if np.fabs(obs - exp) / exp > eps:
			print("Bottom wall interaction failed")
			errCode = 1
			sys.exit(errCode)
			
	# For the second three particles, the same should only be true if 1 wall is activated
	if wall == "One":
		for i in range(numParticlesPerWall):
			zDisp = traj.frame[-1].pos[i + 3][1] - traj.frame[0].pos[i + 3][1]
		
			if np.fabs(zDisp) > eps:
		
				print("Z displacement occured")
				errCode = 1
				sys.exit(errCode)
	else:
		for i in range(numParticlesPerWall):
		
			obs = inp.box.dimension[1] - traj.frame[-1].pos[i + 3][1] - inp.node[i].protein.radius
			exp = float(inp.force[0].attributes["req"])
			
			if np.fabs(obs - exp) / exp > eps:
				print("Top wall interaction failed")
				errCode = 1
				sys.exit(errCode)
				
	# Now energies
	if wall == "One":
		exp = -1 * numParticlesPerWall * float(inp.force[0].attributes["eps"])
	else:
		exp = -1 * totalNumParticles * float(inp.force[0].attributes["eps"])
		
	obs = meas.wallEnergy[-1]
	if np.fabs(obs - exp) / exp > eps:
		print("Energy incorrect")
		errCode = 1
		sys.exit(errCode)
errCode = 0
sys.exit(errCode)
