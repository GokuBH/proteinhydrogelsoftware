import sys, os
import BioNetInput, BioNetTrajectory, BioNetTopology, BioNetMeasurement

# Models
models = ["PointPoint", "PointSphere", "SphereSphere", "PointSphereSite", "SphereSphereSite"]

rate=100
for model in models:

	# Filenames
	iFname = "./bondFormation/input/" + model + "Bonds.xml"
	tFname = "./bondFormation/output/" + model + "Bonds.bntrj"
	mFname = "./bondFormation/output/" + model + "Bonds.bnms"
	tpFname = "./bondFormation/output/" + model + "Bonds.bntop"
	
	# Objects
	inp = BioNetInput.BioNetInput()
	inp.load(iFname)
	
	traj = BioNetTrajectory.BioNetTrajectory()
	traj.load(tFname, inScript=inp, rate=100)
	
	meas = BioNetMeasurement.BioNetMeasurement()
	meas.load(mFname, rate=100)
	
	top = BioNetTopology.BioNetTopology()
	top.load(tpFname, rate=100)
	
	# Non-primitives
	top.frame[-1].buildNodeConnections(inp.numNodes)
#	print(dir(top.frame[-1]))
#	print(top.frame[-1].nCoord)
	
	
	# Analyse by section
	
	if "PointSphere" in model:
		loops = 4	
	else:
		loops = 2
		
	for i in range(loops):

		try:
			# Group 1 (Cannot bond beyond maxNumconnections)
			assert top.frame[-1].nCoord[41 * i + 2] == 2	# maxNumConnections
			assert top.frame[-1].nCoord[41 * i + 8] == 2	# maxNumConnections
			assert top.frame[-1].nCoord[41 * i + 8] == 2	# maxNumConnections
			
		except AssertionError:
		
			print(model + ": Group 1 failed test :(")
			errCode = 1
			sys.exit(errCode)
		
		try:
			# Group 2 (Cannot have 2 kinetic bonds between the same pair)
			if "Site" in model:
				assert top.frame[-1].nCoord[41 * i + 13] == 1
				assert top.frame[-1].nCoord[41 * i + 14] == 1
				assert top.frame[-1].nCoord[41 * i + 15] == 1
				assert top.frame[-1].nCoord[41 * i + 16] == 2
				assert top.frame[-1].nCoord[41 * i + 17] == 1
			
			else:
				assert top.frame[-1].nCoord[41 * i + 13] == 1
				assert top.frame[-1].nCoord[41 * i + 14] == 1
				assert top.frame[-1].nCoord[41 * i + 15] == 2
				assert top.frame[-1].nCoord[41 * i + 16] == 3
				assert top.frame[-1].nCoord[41 * i + 17] == 1
			
		except AssertionError:
		
			print(model + ": Group 2 failed test :(")
			errCode = 1
			sys.exit(errCode)

		try:
			# Group 3 (bond in order of lowest to highest energy)
			if "Site" in model:
				if "Point" in model:
					if i < 2:	# Point in centre
						assert top.frame[-1].nCoord[41 * i + 18] == 3
						assert top.frame[-1].nCoord[41 * i + 19] == 1
						assert top.frame[-1].nCoord[41 * i + 20] == 1
						assert top.frame[-1].nCoord[41 * i + 21] == 1
						assert top.frame[-1].nCoord[41 * i + 22] == 0
						
						assert top.frame[-1].nCoord[41 * i + 23] == 3
						assert top.frame[-1].nCoord[41 * i + 24] == 1
						assert top.frame[-1].nCoord[41 * i + 25] == 1
						assert top.frame[-1].nCoord[41 * i + 26] == 0
						assert top.frame[-1].nCoord[41 * i + 27] == 1
						
					else:		# Sphere (with sites) in centre
						assert top.frame[-1].nCoord[41 * i + 18] == 3
						assert top.frame[-1].nCoord[41 * i + 19] == 1
						assert top.frame[-1].nCoord[41 * i + 20] == 0
						assert top.frame[-1].nCoord[41 * i + 21] == 1
						assert top.frame[-1].nCoord[41 * i + 22] == 1
						
						assert top.frame[-1].nCoord[41 * i + 23] == 3
						assert top.frame[-1].nCoord[41 * i + 24] == 1
						assert top.frame[-1].nCoord[41 * i + 25] == 0
						assert top.frame[-1].nCoord[41 * i + 26] == 1
						assert top.frame[-1].nCoord[41 * i + 27] == 1
						
				else:	# Sphere (with sites) everywhere
					assert top.frame[-1].nCoord[41 * i + 18] == 3
					assert top.frame[-1].nCoord[41 * i + 19] == 1
					assert top.frame[-1].nCoord[41 * i + 20] == 0
					assert top.frame[-1].nCoord[41 * i + 21] == 1
					assert top.frame[-1].nCoord[41 * i + 22] == 1
					
					assert top.frame[-1].nCoord[41 * i + 23] == 3
					assert top.frame[-1].nCoord[41 * i + 24] == 1
					assert top.frame[-1].nCoord[41 * i + 25] == 0
					assert top.frame[-1].nCoord[41 * i + 26] == 1
					assert top.frame[-1].nCoord[41 * i + 27] == 1


			else:
				assert top.frame[-1].nCoord[41 * i + 18] == 3
				assert top.frame[-1].nCoord[41 * i + 19] == 1
				assert top.frame[-1].nCoord[41 * i + 20] == 1
				assert top.frame[-1].nCoord[41 * i + 21] == 1
				assert top.frame[-1].nCoord[41 * i + 22] == 0
				
				assert top.frame[-1].nCoord[41 * i + 23] == 3
				assert top.frame[-1].nCoord[41 * i + 24] == 1
				assert top.frame[-1].nCoord[41 * i + 25] == 1
				assert top.frame[-1].nCoord[41 * i + 26] == 0
				assert top.frame[-1].nCoord[41 * i + 27] == 1

		except AssertionError:
		
			print(model + ": Group 3 failed test :(")
			print(i)
			errCode = 1
			sys.exit(errCode)

		try:
			# Group 4 (cannot bond beyond the cutoff distance)
			assert top.frame[-1].nCoord[41 * i + 28] == 0
			assert top.frame[-1].nCoord[41 * i + 29] == 0
			assert top.frame[-1].nCoord[41 * i + 30] == 2
			assert top.frame[-1].nCoord[41 * i + 31] == 1
			assert top.frame[-1].nCoord[41 * i + 32] == 1
			
			assert top.frame[-1].nCoord[41 * i + 33] == 0
			assert top.frame[-1].nCoord[41 * i + 34] == 1
			assert top.frame[-1].nCoord[41 * i + 35] == 1
			assert top.frame[-1].nCoord[41 * i + 36] == 3
			assert top.frame[-1].nCoord[41 * i + 37] == 3
			assert top.frame[-1].nCoord[41 * i + 38] == 0
			assert top.frame[-1].nCoord[41 * i + 39] == 1
			assert top.frame[-1].nCoord[41 * i + 40] == 1

		except AssertionError:
		
			print(model + ": Group 4 failed test :(")
			errCode = 1
			sys.exit(errCode)

errCode = 0
sys.exit(errCode)
