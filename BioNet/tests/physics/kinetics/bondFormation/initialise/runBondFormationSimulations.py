import sys, os
import glob

executable = sys.argv[1]

iFnames = glob.glob("./bondFormation/input/*.xml")
for iFname in iFnames:
		
	bFname = os.path.basename(iFname)
	bFname = os.path.splitext(bFname)[0]
	oFname = "./bondFormation/output/" + bFname + ".out"
	
	if "Site" in iFname:
		os.system(executable + " -i %s -o %s -t Brownian -l --no-steric --no-vdw --bond-kinetics --no-thermal --verbose" % (iFname, oFname))
	else:
		os.system(executable + " -i %s -o %s -t Brownian --no-steric --no-vdw --bond-kinetics --no-thermal --verbose" % (iFname, oFname))
