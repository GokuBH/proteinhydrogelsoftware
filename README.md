# README #

Welcome to BioNet, a simulation package designed to simulate arbitrary biophysical objects, from proteins to colloids. Fundamental structures are parameterised based
on their shape, with binding sites explicitly defined and leading to the emergence of properties from polymeric stiffness to fractal network structures.

## How do I get set up? ##

### Installation ###
BioNet can be configured and installed using cmake, but doesn't yet have any of the requirements bundled. As such, the following dependencies are currently required:

- Eigen (3.3.7 ideally)
- Spectra (Eigen Wrapper) (0.8.1)
- Boost (1.75.0)
- TinyXML2 (https://github.com/leethomason/tinyxml2)
- Python (Ver 3.x)

BioNet cmake scripts will tell you what environment variables are required for linking to these required libraries. From a directory containing the BioNet source code, the following commands should be used to configure BioNet:

  `mkdir buildBioNet`\
  `cd buildBioNet`\
  `cmake ../BioNet/BioNet -DCMAKE_INSTALL_PREFIX=/path/to/install -DUSE_DEBUG=NO -DUSE_OPENMP=NO`

The flag `-DUSE_DEBUG` should be set to `YES` if one wishes to use a debugger tool like gdb or memory scanner like valgrind. It de-optimises the compilation process
The flag `-DUSE_OPENMP` should be set to `YES` if you want some shared memory paralellisation. It's accelerates a little bit by sharing the load of pair-pair interactions, but it's not perfect, and won't go beyond 1 blade on a supercomputer (no mpi)

To compile (and install) BioNet, run the commands:

  `make -j 4`\
  `make install`

To have access to BioNet executables, add the following to your `PATH` environment variable:

  `export PATH=/path/to/install/BioNet/bin:$PATH`

If you don't have a Python install, you will not be able to use the visualiser. If you do, you can add the required modules to your PYTHONPATH:

  `export PYTHONPATH=/path/to/install/BioNet/lib/pythonX.Y/site-packages/modules/:$PYTHONPATH`

where `X.Y` is your Python version.

### Testing ###

To test BioNet, run the following command from the build directory:

  `make test`

This will run cmake's internal testing processes, which run a few unit tests but mostly physical consistency tests. To run individual tests, you'll need to use ctest. Documentation is online :)

### Using BioNet ###
In the directory `/path/to/install/BioNet/tools/examples`, you will find two example scripts.

- `polymers.xml`
- `network.xml`

Feel free to open both of these files and observe their contents (in plain text and the PyMOL visualiser). Here is a description of each section:

- `<BioNet></BioNet>` - The global wrapper for the script
	- `<components></components>` - The wrapper for structural templates
		- `<proteins></proteins>` - The wrapper for network positional objects i.e. proteins, colloids
			- `<protein></protein>` - The tag for a single positional object. May be a wrapper for `<site/>s`, but may not.
				- `name` - Protein name, required for `<node/>`s to lookup the template
				- `type` - Structural type, usually "sphere"
				- `radius` - Physical size of the object (in nm)

				- `<site/>` - The tag for binding sites on objects
					- `name` - Site name, required for `<protein/>`s to lookup the template
					- `type` - Structural type, usually "point"
					- `position` - Unit vector in the direction of the site from the center of the <protein/> e.g. `position="0.000,0.000,1.000"`
			    	
			- `<bonds></bonds>` - The wrapper for network connection objects i.e. chemical, amino-acid chains
				- `<bond/>` - The tag for a single connection object. 
					- `name` - Bond name, required for `<connection/>`s to lookup the template
					- `type` - Structural type, usually "spring"
					- `k` - Linear spring constant (in pN/nm)
					- `l` - Equilibrium length (in nm)
					- `cutoff` - Distance limit for new bond formation (in nm)

	- `<system></system>` - The wrapper for physical objects
		- `<params></params>` - The wrapper for system parameters
			- `<param/>` - The tag for defining a system parameter
				- `name` - Parameter name
					- `dt` - Numerical simulation timestep (in ns). Can be approximated as the drag on the smallest particle in the system divided by the largest elastic constant, and then dividing by an additional factor of 10
					- `simTime` - Total amount of simulation time required (in ns)
					- `timePerFrame` - The time between writing to the output files. Effectively the simulation frame rate
				- `value` - Parameter value
		- `<nodes></nodes>` - The wrapper for physical nodes
			- `<node/> - The wrapper for single physical nodes
				- `protein` - The `name` of the template `<protein/>`
				- `pos` - Position vector of the node (in nm) e.g. `pos = "50.000,30.000,10.500"`
				- `vel` - Velocity vector of the node (in nm/ns) e.g. `vel = "10.000,20.000,0.000"`
				- `orientation` - Orientation of the node expressed as 3 Euler angles (in radians) e.g. `orientation="0,3.14,1.23"` or a rotation matrix of 9 components
		- `<connections></connections>` - The wrapper for physical bonds / linkers
			- `<connection/>` - The wrapper for single physical bonds / linkers
				- `bond` - The `name` of the template `<bond/>`
				- `nodes` - The indices of the nodes this connection is attached to, written as a comma-separated pair e.g. `nodes=1,50`
				- `sites` - The (local) indices of the sites this connection is attached to, written as a comma-separated pair e.g. `sites=2,0`. They are in the same order as the `nodes` attribute
		- `<box></box>` - The wrapper for simulation box
			- `<param/>` - The tag for defining a box parameter
				- `BC` - Box boundary conditions. Can be HBC (hard) or PBC (periodic) e.g. `BC="PBC"`
				- `dimension` - Physical dimensions of the simulation box (in nm) e.g. `dimension="150.000,150.000,150.000"`
				- `numVoxels` - The number of voxels the box is split into in each dimension e.g. `numVoxels=5,5,5`. Each voxel should be larger than the size of a largest particle in your system, but not too large. The optimum split is a bit of an art form, but it only affects simulation speed.
		- `<forces></forces>` - The wrapper for additional forces
			-`<force/>` - The wrapper for a single defined force
				- `name` - The name of the force e.g. `name="vdw"`
				- `req` - The equilibrium distance of the force (in nm) e.g. `req="1.5"`
				- `eps` - The energy minimum of the force (in pN.nm) e.g. `eps="4.11"`

### Running BioNet simulations ###
Running a BioNet simulation is relatively simple. Using `polymers.xml` as an example:

`BioNet-Sim -i polymers.xml -o polymers_output.dat -t Brownian -l --steric --thermal --no-vdw --no-kinetics --verbose`

The above command runs BioNet on the initial structure contained in `polymers.xml`, and creates a series of files called `polymers_output.*` The simulation type is `Brownian`, and local sites are included on the proteins )`-l`). `steric` interactions are included, as are `thermal` forces. However, `vdw` interactions are not included, and `kinetic` bond formation is not used. Finally, the simulation will produce `verbose` output.

The above is the general structure of the simulation code. You can run:

`BioNet-Sim --help`

To get more information on what flags you can use, but the above is generally ok. The only additional one is that adding `-r` to the command will restart the simulation from where it previously ended. Below, I will proceed through a standard simulation series:

#### Thermal equilibration
When we have an initial, randomly distributed series of particles, we need to randomise them further as per the laws of physics. To do this, we can run a simulation with only thermal forces active, as:

`BioNet-Sim -i polymers.xml -o polymers.dat -t Brownian -l --no-steric --thermal --no-vdw --no-kinetics --verbose`

We should set `simTime` to allow the particles to diffuse at least as far as their own mean-free path to properly equilibrate them. If there are no connections, `dt` can be anything at this stage. If there are connections, `dt` must account for the elastic force. The result is (roughly speaking) the microcanonical ensemble. More accurately, it is the canonical ensemble for an ideal gas (of non-interacting particles).

#### Steric equilibration
In our current simulation state, the particles are not interacting, and hence may be overlapping. However, real particles do not overlap<sup>1</sup> so we need to push them apart. If we were to add both thermal and steric forces at this stage, the forces would be too big and the simulation would go numerically unstable. Hence, we will just do steric interactions:

`BioNet-Sim -i polymers.xml -o polymers.dat -r -t Brownian -l --steric --no-thermal --no-vdw --no-kinetics --verbose`

We should increase `simTime` to allow the particles to diffuse at least as far as their own radius. `dt` must account for the steric force, and I have found `dt = 0.01` works quite well. The result here is not physically realistic. Some particle positions are locally correlated and the total energy is almost zero. It's a necessary step though!

<sup>1</sup>BioNet was actually invented to investigate how physical properties emerge from steric interactions, so...no particle overlaps please!

#### Thermal-steric equilibration
In our current simulation state, the particles are (steric) interacting but are not properly distributed. However, now they are not overlapping they are energetically stable again so we can distribute them again. We will do thermal and steric interactions:

`BioNet-Sim -i polymers.xml -o polymers.dat -r -t Brownian -l --steric --thermal --no-vdw --no-kinetics --verbose`

We should increase `simTime` to allow the particles to diffuse at least as far as their own diameter. `dt` must still account for the steric force, and I have found `dt = 0.01` works quite well. The result is now physically realistic! We have a canonical ensemble for particles with no pair-pair interactions.

#### Additional equilibration
For every additional force, we need to equilibrate with respect to that force. Say we include Van der Waals interactions. Then we will need:

`BioNet-Sim -i polymers.xml -o polymers.dat -r -t Brownian -l --steric --thermal --vdw --no-kinetics --verbose`

We should increase `simTime` to allow the particles to diffuse at least as far as their own diameter. `dt` must still account for the steric force and all additional forces, and will be limited by whatever the strongest force is. Again,I have found `dt = 0.01` works quite well. The result is now physically realistic! We have a canonical ensemble for particles with additional pair-pair interactions.

#### Production simulations
Now we have equilibrated, we can perform simulations suitable for analysis<sup>2</sup>. These simulations are generally much longer. For example, if I care about the long term evolution of a VdW network:

`BioNet-Sim -i polymers.xml -o polymers.dat -r -t Brownian -l --steric --thermal --vdw --no-kinetics --verbose`

If I want to observe chemical network formation, add `kinetics`:

`BioNet-Sim -i polymers.xml -o polymers.dat -r -t Brownian -l --steric --thermal --vdw --kinetics --verbose`

We should increase `simTime` for an arbitrarily long time<sup>3</sup>. I tend to use batches of 5000ns-10000ns, and restart the simulation for longer if it hasn't finished. Keep `dt = 0.01`, and `timePerFrame` to give enough simulation frames to be statistically analysed after the simulation. Maybe `timePerFrame=10`, or `timePerFrame=25`? Whatever you like. But not too many! It'll slow your simulation down and fill your computer up with junk if you use, say,`timePerFrame=0.01`.

<sup>2</sup>At this stage, I often build a new input (.xml) script from the current output files. To separate the equilibration and production parts.

<sup>3</sup>How long a simulation will take is unpredictable. If we could predict it, we wouldn't need the simulation in the first place!

#### Final thoughts
Sometimes you want to run a simulation and then leave the computer alone, or log out entirely. This means you should run your BioNet command within some other bits, as follows:

`nohup BioNet-Sim -i polymers.xml -o polymers.dat -r -t Brownian -l --steric --thermal --vdw --no-kinetics --verbose &> store.log &`

### Visualising BioNet Simulations ###
BioNet can be visualised in PyMOL. On open-source version of PyMOL is available online, for example, within anaconda. Details at https://anaconda.org/conda-forge/pymol-open-source

Once you have PyMOL installed, open it and click "Plugin->Plugin Manager". Then click "Install New Plugin->Choose file...". The visualiser is contained within the source code directory, `BioNet/BioNet/tools/visualisation/BioNetPymolv1.tar.gz`. If you editted your PYTHONPATH variable as above, the visualiser will install.

Within the PyMOL "Plugin" menu, you can open the visualiser window. From there, "File->New" will allow you to open the BioNet .xml structure files. Then, "File->Load..." will allow you to load the .bntrj trajectory files.

### Analysing BioNet Simulations ###
BioNet has a series of modules available for analysis using Python. These modules load in the simulation trajectory and measurement files as numpy objects so you can immediately use Python to do maths and plot graphs. The script `DoNetworkAnalyis.py` in the `examples` folder shows how to use these files to do standard and non-standard analysis.

`DoNetworkAnalysis.py` uses the `network.xml` input file together with `network.bntrj` and `network.bnms`. You can build these files using the above protocol with `network.xml`; I used `40ns` of equilibration and `400ns` of (production) simulation time just as a test, with Van der Waals interactions of strength `2k_bT` and equilibrium distance `r_0 = 0.75nm`. From this, I was able to see that energy was minimised and remained minimised throughout the production simulation, as expected of equilibrium simulations. I saw that my particles diffused less than the expected (theoretical) diffusion of particles freely diffusing over time, and this was due to the action of Van der Waals interactions. The particles were in a sub-linear diffusive regime due to the locally limiting effect of Van der Waals interactions. I also saw that the fractal dimension did not significantly change over time and remained at around 2, indicating that there was no persistent network structure.

To run `DoNetworkAnalysis.py`, create 2 new folders in the `examples` folder: `output` and `results`. Store your output files (`network.bnms` and `network.bntrj` in this folder). Below, I will describe each of the Python module files that will be useful in your own analysis.

#### BioNetInput.py

`BioNetInput.py` contains the class which holds the data from the input `.xml` files. It can be imported with:

`import BioNetInput`

and from there:

`inp = BioNetInput.BioNetInput()`

`inp.load("network.xml")`

From there, you can access various things on the object. For example:

`print(inp.numNodes)`

`print(inp.numConnections)`

will tell you the number of physical nodes and connections defined in this file.

`print(inp.numProteins)`

`print(inp.numBonds)`

will tell you the number of protein and bond templates defined in this file. If you want access to specific node information:

`node4InitialPosition = inp.node[4].pos`

will store the position of node 4 as a variable. You can get velocities with the `vel` parameter. You can also access the protein template the node has assigned and from there, its information:

`print(inp.node[4].protein.name)`

`print(inp.node[4].protein.numSites)`

Incidentally, if you ever have a Python object and you don't know what variables or methods it has, simply use:

`print(dir(inp.node[4].protein))`

for example, for the `protein` object, and you will get a list of things you can access from that object.

`inp.node` is a list of `BioNetInputNode` objects, which has parameters aligned with what the .xml file contains. So if you wanted to add a new node:

`newNode = BioNetInput.BioNetInputNode()`

`newNode.pos = np.array([1.0,2.0,3.0])`

`newNode.protein = inp.protein[0]`

`inp.node.append(newNode)`

`inp.numNodes += 1`

and this also similar for a new `BioNetInputConnection`:

`newConnection = BioNetInput.BioNetInputConnection()`

`newConnection.nIndex = [3,50]`

`newConnection.sIndex = [1,0]`

`inp.connection.append(newConnection)`

`inp.numConnections += 1`

The other object that you may want to access is the `BioNetInputSimulationBox`:

`inp.box`

When you have built your structure, you can simply say:

`inp.writeToFile("new_network.xml")`

and it will create you a new .xml file. Use the Python object to programmatically create input scripts for your simulation.

#### BioNetTrajectory.py
`BioNetTrajectory.py` contains the class which holds the data from the output `.bntrj` files. It can be imported with:

`import BioNetTrajectory`

and from there:

`traj = BioNetTrajectory.BioNetTrajectory()`

`traj.load("network.bntrj", rate=10)`

Be clever with your rate variable. Otherwise, it will take ages to load a simulation. If you need to explicitly do analysis of every frame, then set `rate = 1`. If you just want to get a general trend over time, then `rate = 10` may work, maybe even larger. If you just want the final frame of the simulation, then instead of the above `load` method, use:

`traj.loadFinalFrame("network.bntrj")`

That's much quicker and will just give you the final state of the simulation.

The hierarchy in the trajectory object is `BioNetTrajectory->BioNetTrajectoryFrame`, and within a frame are all the physical parameters you need. Each frame effectively represents a statistical microstate of the system, with positions and velocities of all particles. In principle, if you know the positions and velocities of all particles, and the force fields acting between them, then you can reconstruct everything about the simulation. 

For example, say you want the position of the 4th node of the 10th frame (10th saved time point from the simulations):

`print(traj.frame[10].pos[4])`

Unlike with the input file, the trajectory has no `Node` object. Rather, it stores all the positions as an `N x 3` numpy array for fast maths, where `N` is the number of nodes. Hence, you could quickly calculate the average position for the 10th frame:

`centroid = np.mean(traj.frame[10].pos)`

which should align with what is in the `.bnms` file for the same frame. What simulation time is associated with that frame? Well:

`print(traj.frame[10].time)`

which should correspond to `10 x frameRate`, where `frameRate` is what you specified in the `.xml` file.

### Who do I talk to? ###

For any questions regarding use or compilation, please email Ben Hanson at b.s.hanson@leeds.ac.uk
